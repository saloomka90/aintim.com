<?
$_SERVER['DOCUMENT_ROOT'] = '/home/gotovimsam/aintim.com/docs';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $APPLICATION;

$APPLICATION->RestartBuffer();

//Импорт прайс-листа. Все-таки перешёл на импорт прайса из XML
//CVS файл получается не полным.
\Bitrix\Main\Loader::IncludeModule("iblock");
\Bitrix\Main\Loader::IncludeModule("catalog");

//Выбираем значения свойств размер и цвет.
$arPropColor = array();

$rsResult = \CIBlockProperty::GetPropertyEnum(109,array(),array());

while($arRow = $rsResult->Fetch())
{

    $arPropColor[$arRow['ID']] = $arRow['VALUE'];

}

$arPropSize = array();

$rsResult = \CIBlockProperty::GetPropertyEnum(108,array(),array());

while($arRow = $rsResult->Fetch())
{

    $arPropSize[$arRow['ID']] = $arRow['VALUE'];

}

$arPropBrand = array();

$rsResult = \CIBlockProperty::GetPropertyEnum(103,array(),array());

while($arRow = $rsResult->Fetch())
{

    $arPropBrand[$arRow['ID']] = $arRow['VALUE'];

}

//Выбираем разделы
$arSection = array();

$rsResult = \CIBlockSection::GetList(
    array(),
    array(
        'IBLOCK_ID' => 5
    ),
    false,
    array(
        'ID',
        'XML_ID'
    )
);

while($arRow = $rsResult->Fetch())
{

    if(strlen($arRow['XML_ID'])>0) {

        $arSection[$arRow['XML_ID']] = $arRow['ID'];

    }

}

//Устанавливаем Кещ загрузки
$sHash = randString(14);

//Разбираем XML
$oXML = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/_import/www.erosklad.com.xml');

$iElementFound = 0;
$iElementNotFound = 0;
$iSectionNotFound = 0;
$iElementSearchByName = 0;
$iOffersFound = 0;
$iOffersCount = 0;

//Обрабатываем элементы
foreach($oXML->shop->offers->offer as $oOffer)
{

    $iOffersCount++;

    $arOffer = json_decode(json_encode((array)$oOffer), TRUE);

    if(isset($arSection[$arOffer['categoryId']]))
    {

        $arElement = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => 5,
                'PROPERTY_ARTICLE' => trim($arOffer['article'])
            ),
            false,
            array(
                'nTopCount' => 1
            ),
            array(
                'ID',
                'IBLOCK_ID'
            )
        )->Fetch();

        /**
         *
         * ЭЛЕМЕНТ НАЙДЕН ОБНОЛЯЕМ ИНФОРМАЦИЯ О ЭЛЕМЕНТЕ
         *
         * */
        if($arElement)
        {

            /*Обновляем анонсное и детальное описание товаров*/
            $arFields = array(
                'PREVIEW_TEXT' => $arOffer['description'],
                'DETAIL_TEXT' => $arOffer['description']
            );

            $oElement = new \CIBlockElement;
            $oElement->Update($arElement['ID'],$arFields);


            /*Устанавливаем кеш загрузки*/
            \CIBlockElement::SetPropertyValuesEx(
                $arElement['ID'],
                $arElement['IBLOCK_ID'],
                array(
                    'HASH_LOADER' => $sHash
                )
            );

            /*Перебираем торговые предложения*/

            $iOffersCount = $iOffersCount+count($arOffer['sizes']['size']);

            foreach ($arOffer['sizes']['size'] as $arSize) {


                if(isset($arSize['@attributes']))
                {

                    $arSize = $arSize['@attributes'];

                }

                if(preg_match('/\d+/',$arSize['quantity'],$arMatch))
                {

                    $iQuantity = $arMatch[0];

                }
                else
                {

                    $iQuantity = IntVal($arSize['quantity']);

                }

                $rsResult = \CIBlockElement::GetList(
                    array(),
                    array(
                        'IBLOCK_ID' => 6,
                        'PROPERTY_CML2_LINK' => $arElement['ID'],
                        'PROPERTY_ARTICLE' => $arSize['article']
                ),
                    false,
                    false,
                    array(
                        'ID',
                        'IBLOCK_ID'
                    )
                );

                //Обновляем торговое предложение
                if($rsResult->SelectedRowsCount()==1)
                {

                    //Пока закомментировал, чтобы разобраться со свойствами.
                    $arElementOffer = $rsResult->Fetch();

                    //Устанавливаем кеш загрузки
                    \CIBlockElement::SetPropertyValuesEx(
                        $arElementOffer['ID'],
                        $arElementOffer['IBLOCK_ID'],
                        array(
                            'HASH_LOADER' => $sHash
                        )
                    );



                    SetPriceQuantity($arElementOffer['IBLOCK_ID'],$arElementOffer['ID'],$arOffer['price_old'],$iQuantity);

                }
                //Добавляем торговые предложения есл таковые отсутствуют
                else
                {

                    $iOfferSize = 0;
                    $iOfferColor = 0;

                    $sOfferSize = trim($arSize['size']);
                    $sOfferColor = trim($arSize['color']);

                    if(!empty($sOfferSize))
                    {

                        if(($iOfferSize = array_search($sOfferSize,$arPropSize))==false)
                        {

                            $oProperty = new \CIBlockPropertyEnum;

                            $PropID = $oProperty->Add(
                                Array(
                                    'PROPERTY_ID'=>108,
                                    'VALUE'=> $sOfferSize
                                )
                            );

                            $arPropSize[$PropID] = $sOfferSize;
                            $iOfferSize = $PropID;

                        }

                    }

                    if(!empty($sOfferColor))
                    {

                        if(($iOfferColor = array_search($sOfferColor,$arPropColor))==false)
                        {

                            $oProperty = new \CIBlockPropertyEnum;

                            $PropID = $oProperty->Add(
                                Array(
                                    'PROPERTY_ID'=>109,
                                    'VALUE'=> $sOfferColor
                                )
                            );

                            $arPropColor[$PropID] = $sOfferColor;

                            $iOfferColor = $PropID;


                        }

                    }

                    $arProperties = array(
                        'ARTICLE' => $arSize['article'],
                        'HASH_LOADER' => $sHash,
                        'CML2_LINK' => $arElement['ID'],
                        'BARCODE' => $arSize['barcode'],
                        'PRICE_NEW' => $arOffer['price_old']
                    );

                    if(IntVal($iOfferColor)>0)
                    {

                        $arProperties['COLOR_NEW'] = $iOfferColor;

                    }

                    if(IntVal($iOfferSize)>0)
                    {

                        $arProperties['SIZE_NEW'] = $iOfferSize;

                    }

                    $arFields = array(
                        'NAME' => $arOffer['name'],
                        'IBLOCK_ID' => 6,
                        'PROPERTY_VALUES' => $arProperties,
                        //'CATALOG_QUANTITY' => $arSize['@attributes']['quantity']
                    );

                    $oElement = new \CIBlockElement;
                    $iID = $oElement->Add($arFields);

                    if(!$iID)
                    {

                        echo $oElement->LAST_ERROR;
                        echo "\n";

                    }
                    else {

                        SetPriceQuantity(6, $iID, $arOffer['price_old'], $iQuantity);

                    }

                }

            }

            $iElementFound++;

        }
        /**
         *
         * ЭЛЕМЕНТ НЕ НАЙДЕН, ДОБАВЛЯЕМ ЭЛЕМЕНТ, СОЗДАЁМ ТОРГОВЫЕ ПРЕДЛОЖЕНИЯ
         *
         * */
        else
        {

            $iOfferVendor = 0;
            $sOfferVendor = $arOffer['vendor'];


            if(!empty($sOfferVendor))
            {

                if(($iOfferVendor = array_search($sOfferVendor,$arPropBrand))==false)
                {

                    $oProperty = new \CIBlockPropertyEnum;

                    $PropID = $oProperty->Add(
                        Array(
                            'PROPERTY_ID'=>109,
                            'VALUE'=> $sOfferVendor
                        )
                    );

                    $arPropBrand[$PropID] = $sOfferVendor;

                    $iOfferVendor = $PropID;


                }

            }

            $arProperties = array(
                'HASH_LOADER' => $sHash,
                'article' => $arOffer['article'],
                'SITE_LINK' => $arOffer['url'],
                'pict1' => $arOffer['picture'],
                'pict2' => $arOffer['picture2'],
                'MANUFACTURER_NEW' => $iOfferVendor
            );

            $arParams = array("replace_space" => "-", "replace_other" => "-");
            $sCode = \Cutil::translit($arOffer['name'], "ru", $arParams);

            $arFields = array(
                'IBLOCK_ID' => 5,
                'NAME' => $arOffer['name'],
                'PREVIEW_TEXT' => $arOffer['description'],
                'DETAIL_TEXT' => $arOffer['description'],
                'IBLOCK_SECTION_ID' => $arSection[$arOffer['categoryId']],
                'CODE' => $sCode,
                'PROPERTY_VALUES' => $arProperties
            );

            $oElement = new CIBlockElement;
            $iElementNew = $oElement->Add($arFields);

            if($iElementNew) {

                $iOffersCount = $iOffersCount+count($arOffer['sizes']['size']);

                foreach ($arOffer['sizes']['size'] as $arSize) {


                    /**/
                    if(isset($arSize['@attributes']))
                    {

                        $arSize = $arSize['@attributes'];

                    }

                    if(preg_match('/\d+/',$arSize['quantity'],$arMatch))
                    {

                        $iQuantity = $arMatch[0];

                    }
                    else
                    {

                        $iQuantity = IntVal($arSize['quantity']);

                    }

                    /**/
                    $iOfferSize = 0;
                    $iOfferColor = 0;

                    $sOfferSize = trim($arSize['size']);
                    $sOfferColor = trim($arSize['color']);

                    if(!empty($sOfferSize))
                    {

                        if(($iOfferSize = array_search($sOfferSize,$arPropSize))==false)
                        {

                            /**/
                            $oProperty = new \CIBlockPropertyEnum;

                            $PropID = $oProperty->Add(
                                Array(
                                    'PROPERTY_ID'=>108,
                                    'VALUE'=> $sOfferSize
                                )
                            );

                            $arPropSize[$PropID] = $sOfferSize;
                            $iOfferSize = $PropID;
                            /**/

                        }

                    }

                    if(!empty($sOfferColor))
                    {

                        if(($iOfferColor = array_search($sOfferColor,$arPropColor))==false)
                        {

                            $oProperty = new \CIBlockPropertyEnum;

                            $PropID = $oProperty->Add(
                                Array(
                                    'PROPERTY_ID'=>109,
                                    'VALUE'=> $sOfferColor
                                )
                            );

                            $arPropColor[$PropID] = $sOfferColor;

                            $iOfferColor = $PropID;


                        }

                    }

                    $arProperties = array(
                        'ARTICLE' => $arSize['article'],
                        'HASH_LOADER' => $sHash,
                        'CML2_LINK' => $iElementNew,
                        'BARCODE' => $arSize['barcode'],
                        'PRICE_NEW' => $arOffer['price_old']
                    );

                    if(IntVal($iOfferColor)>0)
                    {

                        $arProperties['COLOR_NEW'] = $iOfferColor;

                    }

                    if(IntVal($iOfferSize)>0)
                    {

                        $arProperties['SIZE_NEW'] = $iOfferSize;

                    }

                    $arFields = array(
                        'NAME' => $arOffer['name'],
                        'IBLOCK_ID' => 6,
                        'PROPERTY_VALUES' => $arProperties,
                        //'CATALOG_QUANTITY' => $iQuantity
                    );

                    $oElement = new \CIBlockElement;
                    $iID =  $oElement->Add($arFields);

                    SetPriceQuantity(6,$iID,$arOffer['price_old'],$iQuantity);
                    /**/

                }

                $iElementNotFound++;

            }


        }

//        if($arOffer['article']=='TS1091003')
//        {
//
//            break;
//
//        }

   }
   else
   {

       $iSectionNotFound++;

   }

}

//Очищаем память
unset($oXML);

//Дективируем все элементы которых нет в выгрузке
//$rsResult = \CIBlockElement::GetList(
//    array(),
//    array(
//        'IBLOCK_ID' => 5,
//        '!PROPERTY_HASH_LOADER' => $sHash
//    ),
//    false,
//    false,
//    array(
//        'ID'
//    )
//);
//
//while($arRow = $rsResult->Fetch())
//{
//
//    $oElement = new \CIBlockElement;
//
//    $arFields = array(
//        'ACTIVE' => 'Y'
//    );
//
//    $oElement->Update($arRow['ID'],$arFields);
//
//}


//Устанавливаем нулевые остатки для торговых предложений которые отсутствуют в выгрузке
$rsResult = \CIBlockElement::GetList(
    array(),
    array(
        'IBLOCK_ID' => 6,
        '!PROPERTY_HASH_LOADER' => $sHash
    ),
    false,
    false,
    array(
        'ID'
    )
);

while($arRow = $rsResult->Fetch())
{

    $arFields = array(
        'QUANTITY' => 0
    );

    \CCatalogProduct::Update($arRow['ID'],$arFields);

}


//Выводим статистику
echo "Всего найденно элементов - ".$iOffersCount;
echo "\n";
echo "Найденно торговых предложений - ".$iOffersFound;
echo "\n";
echo "Не найденно корневых разделов - ".$iSectionNotFound;
echo "\n";
echo "Количество найденных элементов - ".$iElementFound;
echo "\n";
echo "Количество не найденных элементов - ".$iElementNotFound;
echo "\n";
echo "Количество элементов найденных по названию - ".$iElementSearchByName;
echo "\n";


/*
 * Общие функции.
 * */

function xml2array($xml)
{
    $arr = array();

    foreach ($xml as $element)
    {
        $tag = $element->getName();
        $e = get_object_vars($element);
        if (!empty($e))
        {
            $arr[$tag] = $element instanceof SimpleXMLElement ? xml2array($element) : $e;
        }
        else
        {
            $arr[$tag] = trim($element);
        }
    }

    return $arr;
}

/*function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

    return $out;
}*/


function SetPriceQuantity($IBLOCK_ID, $ID,$sPrice,$iQuantity)
{
    //if (\Bitrix\Main\Loader::includeModule('catalog'))
    //{
        //if ($arProperty = \CIBlockElement::GetProperty($IBLOCK_ID, $ID, array(), array('CODE' => 'PRICE_NEW', 'EMPTY' => 'N'))->Fetch())
        //{

            $resProduct = \CCatalogProduct::GetList(array(), array('ID' => $ID), false, array('nTopCount' => 1), array('ID'));

            if ($resProduct->SelectedRowsCount() <= 0)
                \CCatalogProduct::Add(array('ID' => $ID));

            $resPrice = \CPrice::GetListEx(array(), array('PRODUCT_ID' => $ID, 'CATALOG_GROUP_ID' => 1), false, array('nTopCount' => 1), array('ID', 'PRICE'));

            if ($arPrice = $resPrice->Fetch()) {

                \CPrice::Update($arPrice['ID'], array('PRICE' => floatval($sPrice)));

            }
            else
            {
                $newprice = \CPrice::Add(array(
                    'PRODUCT_ID' => $ID,
                    'CATALOG_GROUP_ID' => 1,
                    'PRICE' => floatval($sPrice),
                    'QUANTITY' => $iQuantity,
                    'CURRENCY' => 'RUB',
                ));

            }

            $arFields = array(
                'QUANTITY' => IntVal($iQuantity)
            );

            CCatalogProduct::Update($ID,$arFields);
        //}
    //}
}

return;

//


echo "<pre>";
var_dump($arSection);
echo "</pre><hr />";

$sFilePath = $_SERVER['DOCUMENT_ROOT']."/_import/new_import.csv";

$oFileHandler = fopen($sFilePath,"r+");

$i = 0;

while(!feof($oFileHandler))
{

    $sLine = fgets($oFileHandler);
    $arLine = explode(';',$sLine);

    echo "<pre>";
    var_dump($arLine[15]);
    echo "</pre><hr />";

//    if($i>0)
//    {
//
//        if(!isset($arSection[$arLine[15]]))
//        {
//
//            echo "Раздел не найден -".$arLine[15];
//            echo "<hr />";
//
//        }
//
//    }
//
//    $i++;

}


fclose($oFileHandler);

return false;

\Bitrix\Main\Loader::IncludeModule("iblock");

$sFilePath = $_SERVER['DOCUMENT_ROOT']."/_import/erosklad_.csv";

$oFileHandler = fopen($sFilePath,"r+");

$i=1;
$j = 0;
$iFound = 1;
$arArticles = array();

while(!feof($oFileHandler))
{

    $sLine = fgets($oFileHandler);
    $arLine = explode(';',$sLine);

    if($i>1)
    {

        $arElement = \CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => 6,
                'PROPERTY_ARTICLE' => trim($arLine[1])
            ),
            false,
            array(
                'nTopCount' => 1
            ),
            array(
                'ID',
                'IBLOCK_ID'
            )
        )->Fetch();

        if($arElement)
        {

            $iFound++;

            $arFields = array(
                'XML_ID' => $arLine[0]
            );

            $oElement = new CIBlockElement;
            $oElement->Update($arElement['ID'],$arFields);


            \CIBlockElement::SetPropertyValuesEx(
                $arElement['ID'],
                $arElement['IBLOCK_ID'],
                array(
                    'PRICE_RETAIL_NEW' => $arLine[2]
                )
            );

        }
        else
        {

            //echo $arLine[2]."-".$arLine[1]."<hr />";

            $j++;

            $arArticles[] = trim($arLine[1]);

        }

    }

    $i++;

}

echo "Не найдены: -".$j."<hr />";
echo implode("<br />",$arArticles);
echo "<hr />";
echo "Найденны - ".$iFound;
echo "<hr />";

fclose($oFileHandler);

return;

\Bitrix\Main\Loader::IncludeModule("iblock");

$rsResult =\CIBlockElement::GetList(
    array(
        "ID" => "DESC"
    ),
    array(
        //"ID" => 217479,
        "IBLOCK_ID" => 5,
        "ACTIVE" => "Y"
    ),
    false,
    array(
        'nTopCount' => 1000
    ),
    array(
        "ID",
        "PROPERTY_pict1",
        "PROPERTY_pict2",
        "PROPERTY_pict3",
        "PROPERTY_pict4",
        "PROPERTY_pict5",
    )
);

$iElementCompare = 0;

while($arRow = $rsResult->Fetch())
{

    if(!empty($arRow['PROPERTY_PICT1_VALUE']) && !empty($arRow['PROPERTY_PICT2_VALUE'])) {


        $md5image1 = md5(file_get_contents($arRow['PROPERTY_PICT1_VALUE']));
        $md5image2 = md5(file_get_contents($arRow['PROPERTY_PICT2_VALUE']));


        if ($md5image1 == $md5image2)
        {

            $iElementCompare++;

//            echo "Сходяться ".$arRow['ID'];
//            echo "<br />";

        }


    }

}

echo $iElementCompare;

exit();
return;

//
//while($arRow = $rsResult->Fetch()) {
//
//    \CIBlockElement::SetPropertyValuesEx(
//        $arRow['ID'],
//        $arRow['IBLOCK_ID'],
//        array(
//            'MORE_PHOTO_CHECK' => false
//        )
//    );
//
//}
//
//exit();


while($arRow = $rsResult->Fetch())
{


    $arImageFile = array();

    $oCurl = curl_init();
    curl_setopt($oCurl, CURLOPT_HEADER, 0);
    curl_setopt($oCurl, CURLOPT_VERBOSE, 0);
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($oCurl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
    curl_setopt($oCurl, CURLOPT_URL, $arRow['PROPERTY_SITE_LINK_VALUE']);
    curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($oCurl, CURLOPT_FOLLOWLOCATION, true);
    $sResponce = curl_exec($oCurl);

    if(!$sResponce)
    {

        exit();

    }

    curl_close($oCurl);

    $oDom = new DOMDocument;
    $oDom->loadHTML($sResponce);

    foreach ($oDom->getElementsByTagName('a') as $link) {

        $arClasses = explode(' ', $link->getAttribute('class'));

        if (in_array('google-base-img', $arClasses) || in_array('images',$arClasses)) {

            $arImageFile[] = "https://www.erosklad.com".$link->getAttribute('href');

        }

    }

//    echo "<pre>";
//    var_dump($arRow['ID'],$arImageFile);
//    echo "</pre><hr />";

    if(count($arImageFile)>1)
    {


        for($i=2;$i<=10;$i++)
        {

            if(!empty($arImageFile[$i-2]))
            {

                \CIBlockElement::SetPropertyValuesEx(
                    $arRow['ID'],
                    $arRow['IBLOCK_ID'],
                    array(
                        "pict".$i => $arImageFile[$i-2]
                    )
                );

            }

        }


    }

    \CIBlockElement::SetPropertyValuesEx(
        $arRow['ID'],
        $arRow['IBLOCK_ID'],
        array(
            'MORE_PHOTO_CHECK' => 941
        )
    );

}


return;

//error_reporting(E_ALL);
ignore_user_abort(true);
set_time_limit(0);

\Bitrix\Main\Loader::includeModule("iblock");

$rsResult = \CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_ID" => 5,
        "ACTIVE" => "Y"
    ),
    false,
    array(),
    array(
        "IBLOCK_ID",
        "ID",
        "DETAIL_PICTURE"
    )
);

while($arRow = $rsResult->Fetch())
{

    if(IntVal($arRow['DETAIL_PICTURE'])>0)
    {


        $sPath = CFile::GetPath($arRow['DETAIL_PICTURE']);
        $sPath = "http://aintim.com".$sPath;

        \CIBlockElement::SetPropertyValuesEx(
                $arRow['ID'],
                $arRow['IBLOCK_ID'],
                array(
                    'MORE_PHOTO_CHECK' => 941
                )
        );

    }

    //SetPrice($arRow['IBLOCK_ID'], $arRow['ID']);

}

function SetPrice($IBLOCK_ID, $ID)
{
    if (\Bitrix\Main\Loader::includeModule('catalog'))
    {
        if ($arProperty = \CIBlockElement::GetProperty($IBLOCK_ID, $ID, array(), array('CODE' => 'PRICE_NEW', 'EMPTY' => 'N'))->Fetch())
        {
            $resProduct = \CCatalogProduct::GetList(array(), array('ID' => $ID), false, array('nTopCount' => 1), array('ID'));
            if ($resProduct->SelectedRowsCount() <= 0)
                \CCatalogProduct::Add(array('ID' => $ID));

            $resPrice = \CPrice::GetListEx(array(), array('PRODUCT_ID' => $ID, 'CATALOG_GROUP_ID' => 1), false, array('nTopCount' => 1), array('ID', 'PRICE'));
            if ($arPrice = $resPrice->Fetch())
                \CPrice::Update($arPrice['ID'], array('PRICE' => floatval($arProperty['VALUE'])));
            else
            {
                $newprice = \CPrice::Add(array(
                    'PRODUCT_ID' => $ID,
                    'CATALOG_GROUP_ID' => 1,
                    'PRICE' => floatval($arProperty['VALUE']),
                    'CURRENCY' => 'RUB',
                ));

//                if (!$newprice)
//                {
//                    global $APPLICATION;
//                    $ex = $APPLICATION->GetException();
//                    addmessage2log($ex->toString());
//                }else
//                    addmessage2log($newprice);
            }
        }
    }
}



return;

//Выгрузка свойств в инфоблок торговых предложений

//$rsResult = \CIBlockElement::GetList(
//    array(
//        "ID" => "DESC"
//    ),
//    array(
//        "IBLOCK_ID" => 11,
//        "ACTIVE" => "Y",
//        "PROPERTY_UPLOAD_CATALOG" => false
//    ),
//    false,
//    array(
//        'nTopCount' => 100,
//    ),
//    array(
//        "ID",
//        "NAME",
//        "XML_ID",
//        "PROPERTY_SIZE",
//        "PROPERTY_SECTION",
//        "PROPERTY_ID_ELEMENT",
//        "PROPERTY_NAME",
//        "PROPERTY_VENDOR",
//        "PROPERTY_ARTICLE",
//        "PROPERTY_LINK_SITE",
//        "PROPERTY_SIZE_ARTICLE"
//    )
//);
//
//while($arRow = $rsResult->Fetch())
//{
//
//    $arElements[] = $arRow;
//
//
//}
//
////ЗАПОЛНЯЕМ ЦВЕТА НЕОБХОДИМОЙ ИНФОМАЦИЕЙ
//$arElementProps = array();
//
//foreach($arElements as $arElement)
//{
//
//
//    if(
//        !empty($arElement['PROPERTY_SIZE_VALUE']) &&
//        array_search($arElement['PROPERTY_SIZE_VALUE'],$arElementProps)==false
//    )
//    {
//
//        $arElementProps[] = $arElement['PROPERTY_SIZE_VALUE'];
//
//    }
//
//}
//
//$arSize = array();
//$arColor = array();
//
//foreach($arElementProps as $sValue)
//{
//
//    $arValue = parseProps($sValue);
//
//    foreach($arValue as $sValue1)
//    {
//
//        if(isSize($sValue1))
//        {
//
//            if(array_search($sValue1,$arSize)===false)
//            {
//
//                $arSize[] = $sValue1;
//
//            }
//
//        }
//        else
//        {
//
//            if(array_search($sValue1,$arColor)===false)
//            {
//
//                $arColor[] = $sValue1;
//
//            }
//
//        }
//
//    }
//
//}
//
//foreach($arColor as $sColor)
//{
//
//
//    $oProperty = new \CIBlockPropertyEnum;
//
//    $PropID = $oProperty->Add(
//        Array(
//            'PROPERTY_ID'=>109,
//            'VALUE'=> $sColor
//        )
//    );
//
//    echo $PropID." => '".$sColor."',<br />";
//
//}




//Загрузка изображений
//$rsResult = CIBlockElement::GetList(
//    array(
//        "ID" => "DESC"
//    ),
//    array(
//        "IBLOCK_ID" => 11,
//        "PROPERTY_IMAGE_CHECK" => false
//    ),
//    false,
//    array(
//        "nTopCount" => 200
//    ),
//    array(
//        "ID",
//        "PROPERTY_IMAGE_LINK"
//    )
//);
//
//while($arRow = $rsResult->Fetch())
//{
//
//    /**/
//    if(!empty($arRow['PROPERTY_IMAGE_LINK_VALUE']))
//    {
//
//        $arFilePath = explode("/",$arRow['PROPERTY_IMAGE_LINK_VALUE']);
//        $sFileName = $arFilePath[count($arFilePath)-1];
//        $sFilePath = $_SERVER['DOCUMENT_ROOT']."/_import/files/".$sFileName;
//
//        if(!file_exists($sFilePath)) {
//
//            if (remote_file_exists($arRow['PROPERTY_IMAGE_LINK_VALUE'])) {
//
//
//                remote_file_download($arRow['PROPERTY_IMAGE_LINK_VALUE'],$sFilePath);
//
//                if(file_exists($sFilePath))
//                {
//
//                    $arFields = array(
//                        'DETAIL_PICTURE' => CFile::MakeFileArray($sFilePath)
//
//                    );
//
//                    $oElement = new CIBlockElement;
//                    $oElement->Update($arRow['ID'],$arFields);
//
//                    /**/
//                    \CIBlockElement::SetPropertyValuesEx(
//                        $arRow['ID'],
//                        false,
//                        array(
//                            'IMAGE_CHECK' => 817
//                        )
//                    );
//                    /**/
//
//                    //unlink($sFilePath);
//
//
//                }
//
//            }
//
//        }
//        else{
//
//            $arFields = array(
//                'DETAIL_PICTURE' => CFile::MakeFileArray($sFilePath)
//
//            );
//
//            $oElement = new CIBlockElement;
//            $oElement->Update($arRow['ID'],$arFields);
//
//            /**/
//            \CIBlockElement::SetPropertyValuesEx(
//                $arRow['ID'],
//                false,
//                array(
//                    'IMAGE_CHECK' => 817
//                )
//            );
//            /**/
//
//        }
//
//
//    }
//    /**/
//
//}
//
//function remote_file_exists($url)
//{
//    $curl = curl_init($url);
//
//    //don't fetch the actual page, you only want to check the connection is ok
//    curl_setopt($curl, CURLOPT_NOBODY, true);
//    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//
//    //do request
//    $result = curl_exec($curl);
//
//    $ret = false;
//
//    //if request did not fail
//    if($result !== false)
//    {
//        //if request was ok, check response code
//        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//
//        if ($statusCode == 200)
//        {
//            $ret = true;
//        }
//    }
//
//    curl_close($curl);
//
//    return $ret;
//}
//
//function remote_file_download($url,$saveto){
//
//    $ch = curl_init ($url);
//    curl_setopt($ch, CURLOPT_HEADER, 0);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
//    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//    $raw=curl_exec($ch);
//    curl_close ($ch);
//
//    if(file_exists($saveto)){
//        unlink($saveto);
//    }
//
//    $fp = fopen($saveto,'w+');
//    fwrite($fp, $raw);
//    fclose($fp);
//
//}


//return;


//Удаление
//$rsResult = CIBlockElement::GetList(
//    array(),
//    array(
//        "IBLOCK_ID" => 5,
//        "INCLUDE_SUBSECTIONS" => "Y"
//    ),
//    false,
//    false,
//    array(
//        "ID"
//    )
//);
//
//while($arRow = $rsResult->Fetch())
//{
//
//    \CIBlockElement::Delete($arRow['ID']);
//
//}

//Загрузка изображений


$arSectionOld = array(
    357	=> 1093,
    358	=> 1094,
    359	=> 1103,
    360	=> 1109,
    361	=> 1124,
    362	=> 1113,
    363	=> 1122,
    364	=> 1119,
    365	=> 1121,
    366	=> 1356,
    367	=> 1117,
    368	=> 1238,
    369	=> 1120,
    370	=> 1123,
    371	=> 1148,
    372	=> 1114,
    373	=> 1257,
    375	=> 1099,
    376	=> 1102,
    377	=> 1101,
    378	=> 1100,
    379	=> 1098,
    380	=> 1097,
    381	=> 1096,
    382	=> 1095,
    383	=> 1167,
    384	=> 1290,
    385	=> 1366,
    386	=> 1417,
    387	=> 1418,
    388	=> 1422,
    389	=> 1423,
    390	=> 1438,
    391	=> 1440,
    392	=> 1453,
    393	=> 1456,
    394	=> 1466,
    395	=> 1469,
    398	=> 1546,
    //Новые разделы
    399 => 1108,
    400 => 1106,
    401 => 1105,
    402 => 1107,
    403 => 1104,
    404 => 1222,
    405 => 1450,
    406 => 1110,
    407 => 1111,
    408 => 1112,
    409 => 1261,
    410 => 1293,
    411 => 1300,
    412 => 1427,
    413 => 1531,
    374 => 1115,
    414 => 1116,
    415 => 1151,
    416 => 1268,
    417 => 1336,
    418 => 1419,
    419 => 1472,
    420 => 1473,
    421 => 1125,
    422 => 1126,
    423 => 1127,
    424 => 1128,
    425 => 1233,
    426 => 1234,
    427 => 1429,
    428 => 1437,
    429 => 1548,
    433 => 1129,
    434 => 1131,
    435 => 1162,
    436 => 1196,
    437 => 1228,
    438 => 1229,
    439 => 1230,
    440 => 1132,
    441 => 1130,
    442 => 1133,
    443 => 1150,
    444 => 1197,
    445 => 1199,
    256 => 1156,
    446 => 1143,
    447 => 1231,
    448 => 1235,
    449 => 1140,
    450 => 1139,
    451 => 1141,
    452 => 1138,
    453 => 1137,
    454 => 1136,
    455 => 1146,
    456 => 1147,
    457 => 1224,
    458 => 1362,
    459 => 1142,
    460 => 1134,
    461 => 1164,
    462 => 1165,
    463 => 1166,
    464 => 1361,
    465 => 1487,
    466 => 1528,
    467 => 1529,
    468 => 1541,
    469 => 1170,
    470 => 1171,
    471 => 1172,
    472 => 1262,
    473 => 1263,
    474 => 1468,
    475 => 1175,
    476 => 1176,
    477 => 1177,
    478 => 1178,
    479 => 1179,
    480 => 1180,
    481 => 1181,
    482 => 1182,
    483 => 1203,
    484 => 1205,
    485 => 1237,
    486 => 1521,
    487 => 1188,
    488 => 1187,
    489 => 1192,
    490 => 1193,
    491 => 1223,
    492 => 1258,
    493 => 1259,
    494 => 1264,
    495 => 1265,
    496 => 1266,
    497 => 1267,
    498 => 1426,
    499 => 1470,
    500 => 1471,
    501 => 1543,
    502 => 1544,
    503 => 1433,
    504 => 1434,
    505 => 1435,
    506 => 1436,
    "407_11" => 1111,
    "407_10" => 1441,
    "407_9" => 1442,
    "407_8" => 1443,
    "407_7" => 1444,
    "407_6" => 1445,
    "407_5" => 1446,
    "407_4" => 1447,
    "407_3" => 1448,
    "407_2" => 1449,
    "407_1" => 1451,
    507 => 1482,
    508 => 1483,
    509 => 1484,
    510 => 1485,
    511 => 1486,
    "444_2" => 1488,
    "444_1" => 1489,
    "421_10" => 1500,
    "421_9" => 1501,
    "421_8" => 1502,
    "421_7" => 1503,
    "421_6" => 1504,
    "421_5" => 1505,
    "421_4" => 1506,
    "421_3" => 1507,
    "421_2" => 1508,
    "421_1" => 1125,
    "426_5" => 1509,
    "426_4" => 1510,
    "426_3" => 1511,
    "426_2" => 1512,
    "426_1" => 1515,
    "417_3" => 1532,
    "417_2" => 1535,
    "417_1" => 1545,
    512 => 1499
);

$arPropSize = array(
    818 => 'S',
    819 => 'L',
    820 => 'M',
    821 => 'XXL',
    822 => 'XL',
    823 => 'XXXL',
    824 => 'XXXXL',
    825 => 'XS',
    826 => 'One Size'
);

$arPropColor = array(
    827 => 'Белый',
    828 => 'Розово-пурпурный',
    829 => 'Фиолетовый',
    830 => 'Синий',
    831 => 'Прозрачный',
    832 => 'Телесный',
    833 => 'Бежевый',
    834 => 'Персик',
    835 => 'Голубой',
    836 => 'Сиреневый',
    837 => 'Розовый',
    838 => 'Черный',
    839 => 'Серый',
    840 => 'Красный',
    841 => 'Бирюзовый',
    842 => 'Черно-белый',
    843 => 'Леопард',
    844 => 'Серебряный',
    845 => 'Золотой',
    846 => 'Зеленый',
    847 => 'Зелёный чай',
    848 => 'Черника',
    849 => 'Яблоко',
    850 => 'Кокос',
    851 => 'Желтый',
    852 => 'Манго',
    853 => 'Ассорти',
    854 => 'Фуксия',
    855 => 'Салатовый',
    856 => 'Коричневый',
    857 => 'Черно-красный',
    858 => 'Клубничное вино',
    859 => 'Банан',
    860 => 'Нежный',
    861 => 'Бордовый',
    862 => 'Зебра',
    863 => 'Тигровый',
    864 => 'Белый с черным',
    865 => 'Молочный шоколад',
    866 => 'Лиловый',
    867 => 'Оранжевый',
    868 => 'Светло-фиолетовый',
    869 => 'Синий с желтым',
    870 => 'Платиновый',
    871 => 'Красно-белый',
    872 => 'Изумрудный',
    873 => 'Рубиновый',
    874 => 'Бриллиант',
    875 => 'Вишня',
    876 => 'Арбуз',
    877 => 'Черно-розовый',
    878 => 'Белый с красным',
    879 => 'Серебристый с розовым',
    880 => 'Клубника',
    881 => 'Маракуйя',
    882 => 'Восточный аромат',
    883 => 'Черный с черным бантом',
    884 => 'Нежно-розовый',
    885 => 'Фиолетово-белый',
    886 => 'Телесно-черный',
    887 => 'Красно-черный',
    888 => 'Тропические фрукты',
    889 => 'Кокос и сливки',
    890 => 'Грейпфрут',
    891 => 'Малина',
    892 => 'Дикие ягоды',
    893 => 'Иланг-иланг',
    894 => 'Эвкалипт и лимон',
    895 => 'Огурец и дыня',
    896 => 'Розово-черный',
    897 => 'Мулат',
    898 => 'Черешня',
    899 => 'Киви и клубника',
    900 => 'Черный с белым золотом',
    901 => 'Белый с золотом',
    902 => 'Белый с медным',
    903 => 'Черный с медным',
    904 => 'Коричневый с золотом',
    905 => 'Коричневый с белым золотом',
    906 => 'Белый с белым золотом',
    907 => 'Черный с золотом',
    908 => 'Имбирь',
    909 => 'Серебристый с черным',
    910 => 'Шоколад',
    911 => 'Жасмин',
    912 => 'Ваниль',
    913 => 'Корица',
    914 => 'Апельсин',
    915 => 'Экзотические фрукты',
    916 => 'Мята',
    917 => 'Ментоловый',
    918 => 'Кремовый',
    919 => 'Какао',
    920 => 'Манго-дыня',
    921 => 'Яблоко и ягоды',
    922 => 'Виноград',
    923 => 'Зелёное яблоко',
    924 => 'Мандарин',
    925 => 'Жемчужный',
    926 => 'Вишневый',
    927 => 'Лаванда',
    928 => 'Цветы',
    929 => 'Роза',
    930 => 'Огурец',
    931 => 'Бурбон',
    932 => 'Лотос',
    933 => 'Хаки',
    934 => 'Яблоко и корица',
    935 => 'Мужской',
    936 => 'Слоновая кость',
    937 => 'Поменять',
    938 => 'Черный с розовой шнуровкой',
    939 => 'Черный с красной шнуровкой'
);

$arVendor = array(
    612 => 'REAL DOLL',
    613 => 'Inverma',
    614 => 'Toy Joy',
    615 => 'Nexus',
    616 => 'Seven Creations',
    617 => 'DreamDoll',
    618 => 'California Exotic Novelties',
    619 => 'Topco Sales',
    620 => 'Shunga',
    621 => 'Leg Avenue',
    622 => 'Doc Johnson',
    623 => 'HOT',
    624 => 'Ellie Shoes',
    625 => 'Roxana',
    626 => 'Joy Division',
    627 => 'Sharon Sloane Latex',
    628 => 'Milan',
    629 => 'Spencer & Fleetwood',
    630 => 'Scala',
    631 => 'Cobeco',
    632 => 'LELO',
    633 => 'Shiblue Couture',
    634 => 'Fetish Fantasy',
    635 => 'Фанты',
    636 => 'SHARON SLOANE / JOYCE JON',
    637 => 'Tenga',
    638 => 'Costumania',
    639 => 'Mystim',
    640 => 'Clean Point',
    641 => 'BASIX',
    642 => 'Shiatsu',
    643 => 'Подиум',
    644 => 'Cloneboy',
    645 => 'Pipedream',
    646 => 'Bathmate',
    647 => 'Swan',
    648 => 'Rocks-Off',
    649 => 'Fun Factory',
    650 => 'Sexy Life',
    651 => 'Feelztoys',
    652 => 'FleshLight',
    653 => 'XLsucker',
    654 => 'PowerBullet',
    655 => 'Viamax',
    656 => 'Male Edge',
    657 => 'Stud 100',
    658 => 'Energizer',
    659 => 'Plaisirs Secrets',
    660 => 'Sqweel',
    661 => 'Be Wicked',
    662 => 'Луншань',
    663 => 'Pjur',
    664 => 'Jimmyjane',
    665 => 'LOVERSPREMIUM',
    666 => 'НПК Бионика',
    667 => 'Bewicked',
    668 => 'Sex & Mischief',
    669 => 'OVO',
    670 => 'KISS ME',
    671 => 'Taboom',
    672 => 'Hitachi',
    673 => 'Beastly',
    674 => 'SPORTSHEETS',
    675 => 'Luxe',
    676 => 'Sagami Rubber',
    677 => 'Chilirose',
    678 => 'Чжин Юань Тхан',
    679 => 'Allure',
    680 => 'NS Novelties',
    681 => 'Svakom',
    682 => 'Pheromax',
    683 => 'Eroflame',
    684 => 'BIG TEAZE TOYS',
    685 => 'Bijoux Indiscrets',
    686 => 'COLT GEAR',
    687 => 'MEDIACRAFT',
    688 => 'Тчэнгдун Жеинхуа Фарма',
    689 => 'РОСПАРФЮМ',
    690 => 'Sinthetics',
    691 => 'MAISON CLOSE',
    692 => 'We-Vibe',
    693 => 'Stimul8',
    694 => 'LE FRIVOLE',
    695 => 'ENTICE',
    696 => 'AUTOBLOW',
    697 => 'Fun Toys',
    698 => 'NOMI TANG',
    699 => 'ПИКАНТНЫЕ ШТУЧКИ',
    700 => 'JE JOUE',
    701 => 'Topco Sales (CSM)',
    702 => 'SCALA SELECTION',
    703 => 'LOVENSE',
    704 => 'Screaming O',
    705 => 'Turkuaz Medikal',
    706 => 'К-Артель',
    707 => 'CRAVE',
    708 => 'Bodywand',
    709 => 'PERFECTFIT',
    710 => 'Linx',
    711 => 'System JO',
    712 => 'Rock Rings',
    713 => 'Nanma',
    714 => 'Kinx',
    715 => 'Александр Полеев',
    716 => 'Fifty Shades of Grey',
    717 => 'Womanizer',
    718 => 'ИнформМед',
    719 => 'Demoniq',
    720 => 'Avanza',
    721 => 'Le Frivole costumes',
    722 => 'Casmir',
    723 => 'Avanua',
    724 => 'Temptlife',
    725 => 'Livia Corsetti',
    726 => 'MONKEY SPANKER',
    727 => 'Dupu',
    728 => 'Accessories',
    729 => 'LUCISTER SERVICES',
    730 => 'DAME',
    731 => 'IMTOY',
    732 => 'Таниер Биотехнология (Шанчу)',
    733 => 'РеалКапс',
    734 => 'Fever',
    735 => 'ME SEDUCE',
    736 => 'Obsessive',
    737 => 'SATISFYER',
    738 => 'Howells',
    739 => 'Mae B',
    740 => 'Биоклон',
    741 => 'Erotic soap',
    742 => 'X-PLAY',
    743 => 'FlirtON',
    744 => 'СК-Визит',
    745 => 'БДСМ арсенал',
    746 => 'HOT Production',
    747 => 'Wild Lust',
    748 => 'Baile',
    749 => 'Contemporary Novelties',
    750 => 'Shotsmedia',
    751 => 'Toyz4lovers',
    752 => 'Passion',
    753 => 'Vibe Therapy',
    754 => 'Erokay',
    755 => 'ERASEXA',
    756 => 'Steel Power Tools',
    757 => 'Unilatex',
    758 => 'Парфюм Престиж',
    759 => 'Luxe презервативы',
    760 => 'Luxurious Tail',
    761 => 'Forplay',
    762 => 'LOLA TOYS',
    763 => 'ART-STYLE',
    764 => 'WET, Trigg Laboratories Inc',
    765 => 'NMC, Канада',
    766 => 'Chisa',
    767 => 'Bswish',
    768 => 'VITALIS',
    769 => 'SPLASHGLIDE',
    770 => 'OKAMOTO',
    771 => 'Ситабелла',
    772 => 'R&S Consumer goods GmbH',
    773 => 'GANZO',
    774 => 'Lola Lingerie',
    775 => 'MEN`S SECRET',
    776 => 'JUJU',
    777 => 'Dolce Piccante Lingerie',
    778 => 'WHITE LABEL',
    779 => 'А-Полимер',
    780 => 'Lolitta',
    781 => 'Лаоху',
    782 => 'MisterX',
    783 => 'Mens dreams',
    784 => 'Idoll',
    785 => 'Wet',
    786 => 'Rebelts',
    787 => 'HOT PLANET',
    788 => 'PREMIUM PARFUM',
    789 => 'Казанова',
    790 => 'Iroha',
    791 => 'Mister B',
    792 => 'Lola Toys Bondage Collections',
    793 => 'KiberVR',
    794 => 'Lola Toys Back Door Collection',
    795 => 'Lola Toys First Time',
    796 => 'Lola Toys Emotions',
    797 => 'KIIROO',
    798 => 'СП Стилмарк',
    799 => 'Shiri Zinn',
    800 => 'Тибетская Компания Иканв',
    801 => 'SHUANGBAO',
    802 => 'HAO TOYS',
    803 => 'RESTART',
    804 => 'Fantasy Lingerie',
    805 => 'Caution Wear Corp',
    806 => 'MY.SIZE',
    807 => 'LIBERATOR',
    808 => 'ADULTBODYART',
    809 => 'Kama Sutra',
    810 => 'TICKLER VIBES',
    811 => 'REALOV',
    812 => 'PETITS JOUJOUX',
    813 => 'Rianne S',
    814 => 'Minx',
    815 => 'SenseMax Technology Limited',
    816 => 'Playhouse'
);

$rsResult = \CIBlockElement::GetList(
    array(
        "ID" => "DESC"
    ),
    array(
        "IBLOCK_ID" => 11,
        "ACTIVE" => "Y",
        "PROPERTY_UPLOAD_CATALOG" => false
    ),
    false,
    array(
        'nTopCount' => 300,
    ),
    array(
        "ID",
        "NAME",
        "XML_ID",
        "DETAIL_PICTURE",
        "PREVIEW_TEXT",
        "PROPERTY_SIZE",
        "PROPERTY_SECTION",
        "PROPERTY_ID_ELEMENT",
        "PROPERTY_NAME",
        "PROPERTY_VENDOR",
        "PROPERTY_ARTICLE",
        "PROPERTY_LINK_SITE",
        "PROPERTY_SIZE_ARTICLE",
        "PROPERTY_PRICE"
    )
);

while($arRow = $rsResult->Fetch())
{

    $arElements[] = $arRow;


}

foreach($arElements as $arElement)
{

    if(($iSection=array_search($arElement['PROPERTY_SECTION_VALUE'],$arSectionOld))!==false) {

        //Проверяем есть ли основной товаро
        $arElementTMP = \CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => 5,
                "PROPERTY_article" => $arElement['PROPERTY_ARTICLE_VALUE']
            ),
            false,
            array(
                'nTopCount' => 1
            ),
            array(
                "ID"
            )
        )->Fetch();

        $iID = 0;

        if ($arElementTMP) {

            $iID = $arElementTMP['ID'];

        }
        else
        {

            //Добавляем основной элемент
            $arPropertyElement = array();

            if(!empty($arElement['PROPERTY_VENDOR_VALUE']) && ($iVendorID = array_search($arElement['PROPERTY_VENDOR_VALUE'],$arVendor))!==false)
            {

                $arPropertyElement['MANUFACTURER_NEW'] = array( "VALUE" => $iVendorID);

            }

//            if(!empty($arElement['PROPERTY_PRICE_VALUE']))
//            {
//
//                $arPropertyElement['PRICE_NEW'] = $arElement['PROPERTY_PRICE_VALUE'];
//
//            }

            if(!empty($arElement['PROPERTY_ARTICLE_VALUE']))
            {

                $arPropertyElement[49] = $arElement['PROPERTY_ARTICLE_VALUE'];

            }

            if(!empty($arElement['PROPERTY_LINK_SITE_VALUE']))
            {

                $arPropertyElement['SITE_LINK'] = $arElement['PROPERTY_LINK_SITE_VALUE'];

            }

            $arParams = array("replace_space" => "-", "replace_other" => "-");
            $sCode = \Cutil::translit($arElement['PROPERTY_NAME_VALUE'], "ru", $arParams);

            if(strpos($iSection,"_")!==false)
            {

                $arSectionPath = explode("_",$iSection);
                $iSection = $arSectionPath[0];

            }

            $arFields = array(
                "IBLOCK_ID" => 5,
                "NAME" => $arElement['PROPERTY_NAME_VALUE'],
                "CODE" => $sCode,
                "DETAIL_TEXT" =>  $arElement['PREVIEW_TEXT'],
                "PREVIEW_TEXT" =>  $arElement['PREVIEW_TEXT'],
                "IBLOCK_SECTION_ID" => $iSection,
                "XML_ID" => $arElement['PROPERTY_ID_ELEMENT_VALUE'],
                "PROPERTY_VALUES" => $arPropertyElement
            );

            if(IntVal($arElement['DETAIL_PICTURE'])>0)
            {

                $arFields['DETAIL_PICTURE'] = \CFile::MakeFileArray($arElement['DETAIL_PICTURE']);

            }

            $oElement = new CIBlockElement;

            if(($iElementID = $oElement->Add($arFields)))
            {

                $iID = $iElementID;

            }

        }

        if(IntVal($iID)>0)
        {

            //Добавляем торговые предложения
            $arPropertyOffers = array();

            $arPropertyOffers['CML2_LINK'] = $iID;

            if(!empty($arElement['PROPERTY_PRICE_VALUE']))
            {

                $arPropertyOffers['PRICE_NEW'] = $arElement['PROPERTY_PRICE_VALUE'];

            }

            if(!empty($arElement['PROPERTY_SIZE_ARTICLE_VALUE']))
            {

                $arPropertyOffers['ARTICLE'] = $arElement['PROPERTY_SIZE_ARTICLE_VALUE'];

            }

            if(!empty($arElement['PROPERTY_SIZE_VALUE']))
            {

                $arPropertyValues = parseProps($arElement['PROPERTY_SIZE_VALUE']);

                $arSize = array();
                $arColor = array();

                foreach($arPropertyValues as $sValue)
                {

                    if(isSize($sValue))
                    {

                        if(($iSize=array_search($sValue,$arPropSize))!==false)
                        {

                            $arSize[] = $iSize;

                        }

                    }
                    else
                    {

                        if(($iColor=array_search($sValue,$arPropColor))!==false)
                        {

                            $arColor[] = $iColor;

                        }


                    }

                }

                if(count($arSize)>0)
                {

                    $arPropertyOffers['SIZE_NEW'] = $arSize;

                }

                if(count($arColor)>0)
                {

                    $arPropertyOffers['COLOR_NEW'] = $arColor;

                }

            }

            /**/
            $arFields = array(
                "IBLOCK_ID" => 6,
                "NAME" => $arElement['PROPERTY_NAME_VALUE'],
                "PROPERTY_VALUES" => $arPropertyOffers
            );

            $oElement = new CIBlockElement;
            $oElement->Add($arFields);

            \CIBlockElement::SetPropertyValuesEx(
                $arElement['ID'],
                false,
                array(
                    'UPLOAD_CATALOG' => 940
                )
            );



        }


    }
    else
    {

        \CIBlockElement::SetPropertyValuesEx(
            $arElement['ID'],
            false,
            array(
                'UPLOAD_CATALOG' => 940
            )
        );

    }

}



return;


/*
 *
 * ДАЛЬШЕ НЕ ОБРАБАТЫВАЕМ.
 *
 * */


$rsResult = \CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_ID" => 5,
        "INCLUDE_SUBSECTION" => "Y"
    ),
    false,
    false,
    array(
        "ID",
        "XML_ID"
    )
);


return;



if(false) {

    $sHTML = '<li class="active">
                                    <a href="/catalog/1094/1499/">Satisfyer</a>
                                </li>';

    $xml = new DOMDocument();

    $xml->loadHTML(mb_convert_encoding($sHTML, 'HTML-ENTITIES', 'UTF-8'));

    $result = array();
    $resultLink = array();

    foreach ($xml->getElementsByTagName('li') as $li) {
        foreach ($li->getElementsByTagName('a') as $links) {
            $resultLink[] = array(
                'HREF' => $links->getAttribute('href'),
                'NAME' => $links->nodeValue
            );
        }
    }

//error_reporting(E_ALL);

    foreach ($resultLink as $arLink) {


        if (preg_match('/^\/catalog\/([\d]{1,})\/([\d]{1,})\/$/ism', $arLink['HREF'], $arMatch)) {

            if (array_search($arMatch[1], $arSectionOld) != false && array_search($arMatch[2],
                    $arSectionOld) == false
            ) {

                $arParams = array("replace_space" => "-", "replace_other" => "-");
                $sCode = \Cutil::translit($arLink['NAME'], "ru", $arParams);

                $sSection = array_search($arMatch[1], $arSectionOld);

                $bFound = true;

                $arSection = \CIBlockSection::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => 5,
                        "NAME" => $arLink['NAME'],
                        "SECTION_ID" => $sSection
                    ),
                    false,
                    array(
                        "ID",
                        "NAME"
                    )
                )->Fetch();


                if (!$arSection) {

                    //echo "Раздел не найден: ".$arLink['NAME'];
                    //echo "<hr>";

                } else {

                    //echo "Раздел найден: ".$arSection['ID']." => ".$arMatch[2].",";
                    //echo "<hr>";

                }

                if ($bFound) {

                    $arFieldsNew = array(
                        "ACTIVE" => "Y",
                        "IBLOCK_SECTION_ID" => $sSection,
                        "IBLOCK_ID" => 5,
                        "NAME" => $arLink['NAME'],
                        "CODE" => $sCode,
                        "UF_NEW_SECTION" => 1
                    );

                    $oSection = new CIBlockSection;

                    if (($iID = $oSection->Add($arFieldsNew))) {

                        echo $iID . " => " . $arMatch[2] . ",";
                        echo "<br />";

                    } else {

                        echo $oSection->LAST_ERROR;
                        echo "<br />";

                    }

                }

            } else {

                echo "Раздел найден1";
                echo "<hr />";

            }

        }

        if (preg_match('/^\/catalog\/([\d]{1,})\/([\d]{1,})\/([\d]{1,})\/$/ism', $arLink['HREF'], $arMatch)) {

            if (array_search($arMatch[2], $arSectionOld) != false && array_search($arMatch[3],
                    $arSectionOld) == false
            ) {

                $arParams = array("replace_space" => "-", "replace_other" => "-");
                $sCode = \Cutil::translit($arLink['NAME'], "ru", $arParams);

                $sSection = array_search($arMatch[2], $arSectionOld);

                $bFound = true;

                $arSection = \CIBlockSection::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => 5,
                        "NAME" => $arLink['NAME'],
                        "SECTION_ID" => $sSection
                    ),
                    false,
                    array(
                        "ID",
                        "NAME"
                    )
                )->Fetch();


                if (!$arSection) {

                    //echo "Раздел не найден: ".$arLink['NAME'];
                    //echo "<hr>";

                } else {

                    //echo "Раздел найден: ".$arSection['ID']." => ".$arMatch[2].",";
                    //echo "<hr>";

                }

                if ($bFound) {

                    $arFieldsNew = array(
                        "ACTIVE" => "Y",
                        "IBLOCK_SECTION_ID" => $sSection,
                        "IBLOCK_ID" => 5,
                        "NAME" => $arLink['NAME'],
                        "CODE" => $sCode,
                        "UF_NEW_SECTION" => 1
                    );

                    $oSection = new CIBlockSection;

                    if (($iID = $oSection->Add($arFieldsNew))) {

                        echo $iID . " => " . $arMatch[3] . ",";
                        echo "<br />";

                    } else {

                        echo $oSection->LAST_ERROR;
                        echo "<br />";

                    }

                }

            } else {

                echo "Раздел найден1";
                echo "<hr />";

            }

        }

    }

    return;

}

$arSectionNew = array();
$arSectionLink = array();
$arSectionElementCount = array();

$rsResult = \CIBlockElement::GetList(
    array(
        "ID" => "DESC"
    ),
    array(
        "IBLOCK_ID" => 11,
        "ACTIVE" => "Y"
    ),
    false,
    false,
    array(
        "ID",
        "NAME",
        "XML_ID",
        "PROPERTY_SIZE",
        "PROPERTY_SECTION",
        "PROPERTY_ID_ELEMENT",
        "PROPERTY_NAME",
        "PROPERTY_VENDOR",
        "PROPERTY_ARTICLE",
        "PROPERTY_LINK_SITE",
        "PROPERTY_SIZE_ARTICLE"
    )
);

$arElementProps = array();

while($arRow = $rsResult->Fetch())
{

    $arElements[] = $arRow;


}

//ЗАПОЛНЯЕМ КАТАЛОГ ТОВАРОВ
$i = 1;

$arElementCodes = array();

foreach($arElements as $arElement)
{

    if(($iSection=array_search($arElement['PROPERTY_SECTION_VALUE'],$arSectionOld))!==false)
    {


        $arPropertyElement = array();

        if(!empty($arElement['PROPERTY_VENDOR_VALUE']) && ($iVendorID = array_search($arElement['PROPERTY_VENDOR_VALUE'],$arVendor))!==false)
        {

            $arPropertyElement['MANUFACTURER_NEW'] = array( "VALUE" => $iVendorID);

        }

        if(!empty($arElement['PROPERTY_SIZE_VALUE']))
        {

            $arPropertyValues = parseProps($arElement['PROPERTY_SIZE_VALUE']);

            $arSize = array();
            $arColor = array();

            foreach($arPropertyValues as $sValue)
            {

                if(isSize($sValue))
                {

                    if(($iSize=array_search($sValue,$arPropSize))!==false)
                    {

                        $arSize[] = $iSize;

                    }

                }
                else
                {

                    if(($iColor=array_search($sValue,$arPropColor))!==false)
                    {

                        $arColor[] = $iColor;

                    }


                }

            }

            if(count($arSize)>0)
            {

                $arPropertyElement['SIZE_NEW'] = $arSize;

            }

            if(count($arColor)>0)
            {

                $arPropertyElement['COLOR_NEW'] = $arColor;

            }

        }

        if(!empty($arElement['PROPERTY_ARTICLE_VALUE']))
        {

            $arPropertyElement[49] = $arElement['PROPERTY_ARTICLE_VALUE'];

        }

        if(!empty($arElement['PROPERTY_ARTICLE_VALUE']))
        {

            $arPropertyElement[49] = $arElement['PROPERTY_ARTICLE_VALUE'];

        }

        if(!empty($arElement['PROPERTY_SIZE_ARTICLE_VALUE']))
        {

            $arPropertyElement['SIZE_ARTICLE'] = $arElement['PROPERTY_SIZE_ARTICLE_VALUE'];

        }

        if(!empty($arElement['PROPERTY_LINK_SITE_VALUE']))
        {

            $arPropertyElement['SITE_LINK'] = $arElement['PROPERTY_LINK_SITE_VALUE'];

        }

        $arParams = array("replace_space" => "-", "replace_other" => "-");
        $sCode = \Cutil::translit($arElement['PROPERTY_NAME_VALUE'], "ru", $arParams);

        $bRand = "";

        if(array_search($sCode,$arElementCodes)===false)
        {

            $arElementCodes[] = $sCode;

        }
        else
        {

            $bRand = "_".randString(5);

        }

        if(strpos($iSection,"_")!==false)
        {

            $arSectionPath = explode("_",$iSection);
            $iSection = $arSectionPath[0];

        }


        $arFields = array(
            "IBLOCK_ID" => 5,
            "NAME" => $arElement['PROPERTY_NAME_VALUE'],
            "CODE" => $sCode.$bRand,
            "IBLOCK_SECTION_ID" => $iSection,
            "XML_ID" => $arElement['PROPERTY_ID_ELEMENT_VALUE'],
            "PROPERTY_VALUES" => $arPropertyElement
        );

        $oElement = new CIBlockElement;
        $oElement->Add($arFields);


//        echo "<pre>";
//        var_dump($arFields);
//        echo "</pre><hr />";
//
//        if($i>10)
//        {
//
//            break;
//
//        }


        $i++;

    }


}

//foreach($arVendor as  $sValue)
//{
//
//    $oProperty = new \CIBlockPropertyEnum;
//
//    $PropID = $oProperty->Add(
//        Array(
//            'PROPERTY_ID'=>103,
//            'VALUE'=> $sValue
//        )
//    );
//
//    echo $PropID." => '".$sValue."',<br />";
//
//}

//Цвета, размеры, деление

//ЗАПОЛНЯЕМ ЦВЕТА НЕОБХОДИМОЙ ИНФОМАЦИЕЙ
//foreach($arElements as $arElement)
//{
//
//
//    if(
//        !empty($arElement['PROPERTY_SIZE_VALUE']) &&
//        array_search($arElement['PROPERTY_SECTION_VALUE'],$arSectionOld)!=false &&
//        array_search($arElement['PROPERTY_SIZE_VALUE'],$arElementProps)==false
//    )
//    {
//
//        $arElementProps[] = $arElement['PROPERTY_SIZE_VALUE'];
//
//    }
//
//}

//$arSize = array();
//$arColor = array();
//
//foreach($arElementProps as $sValue)
//{
//
//    $arValue = parseProps($sValue);
//
//    foreach($arValue as $sValue1)
//    {
//
//        if(isSize($sValue1))
//        {
//
//            if(array_search($sValue1,$arSize)===false)
//            {
//
//                $arSize[] = $sValue1;
//
//            }
//
//        }
//        else
//        {
//
//            if(array_search($sValue1,$arColor)===false)
//            {
//
//                $arColor[] = $sValue1;
//
//            }
//
//        }
//
//    }
//
//}

//ПЕРЕБИРАЕМ ЭЛЕМЕНТЫ И СМОТРИМ ТОВАРЫ

//$arElementArray = array();
//
//foreach($arElements as $arElement)
//{
//
//    $arElementArray[$arElement['XML_ID']][] = $arElement;
//
//}
//
//
//
//foreach($arElementArray as $arElements1)
//{
//
//    echo count($arElements1);
//    echo "<br />";
//
//}

//echo "<b>Размеры</b><hr />";
//
//foreach($arSize as $sSize)
//{
//
//    $oProperty = new \CIBlockPropertyEnum;
//
//    $PropID = $oProperty->Add(
//        Array(
//            'PROPERTY_ID'=>101,
//            'VALUE'=> $sSize
//        )
//    );
//
//    echo $PropID." => '".$sSize."'<br />";
//
//
//
//}
//
//echo "<b>Цвета</b><hr />";
//
//foreach($arColor as $sColor)
//{
//
//    $oProperty = new \CIBlockPropertyEnum;
//
//    $PropID = $oProperty->Add(
//        Array(
//            'PROPERTY_ID'=>102,
//            'VALUE'=> $sColor
//        )
//    );
//
//    echo $PropID." => '".$sColor."'<br />";
//
//}


function parseProps($sStr)
{

    if(strpos($sStr,",")!==false)
    {

        $arValue = array();

        $arTmpValue = explode(",",$sStr);

        foreach($arTmpValue as $sValue)
        {

            if(strpos($sValue,"/")!==false)
            {

                $arTmpValue1 = explode("/",$sValue);

                foreach($arTmpValue1 as $sValue1)
                {

                    if($sValue1=='XL-XXL')
                    {

                        $arValue[] = 'XL';
                        $arValue[] = 'XXL';

                    }
                    else {

                        $arValue[] = $sValue1;

                    }

                }

            }
            else
            {

                if($sValue=='XL-XXL')
                {

                    $arValue[] = 'XL';
                    $arValue[] = 'XXL';

                }
                else {

                    $arValue[] = $sValue;

                }

            }

        }

    }
    else
    {

        if(trim($sStr)=="XL-XXL")
        {

            $arValue[] = "XL";
            $arValue[] = "XXL";

        }elseif(trim($sStr)=="XL-XXL")
        {

            $arValue[] = "XL";
            $arValue[] = "XXL";

        }
        elseif(strpos($sStr,"/")!==false)
        {

            $arTmpValue1 = explode("/",$sStr);

            foreach($arTmpValue1 as $sValue1)
            {

                $arValue[] = $sValue1;

            }

        }
        else
        {

            $arValue[] = $sStr;

        }

    }

//    foreach($arValue as $sValue)
//    {
//
//
//        echo $sValue."<br />";
//
//    }

    return $arValue;

}

function isSize($sStr)
{

    if(strpos($sStr,'X')!==false || strpos($sStr,'M')!==false || strpos($sStr,'L')!==false || strpos($sStr,'S')!==false)
    {

        return true;

    }
    else
    {

        return false;

    }

}


//foreach($arElements as $arRow)
//{
//
//    $arSectionLink[$arRow['PROPERTY_SECTION_VALUE']] = $arRow['PROPERTY_LINK_SITE_VALUE'];
//    $arSectionNew[] = $arRow['PROPERTY_SECTION_VALUE'];
//
//    if(!isset($arSectionElementCount[$arRow['PROPERTY_SECTION_VALUE']]))
//    {
//
//        $arSectionElementCount[$arRow['PROPERTY_SECTION_VALUE']] = 1;
//
//    }
//    else {
//
//        $arSectionElementCount[$arRow['PROPERTY_SECTION_VALUE']] += 1;
//
//    }
//
//}
//
//$iCountFind = 0;
//$iCountNotFind = 0;
//
//foreach($arSectionNew as $sValue)
//{
//
//    if(array_search(IntVal($sValue),$arSectionOld)==false)
//    {
//
//        $arElement = \CIBlockElement::GetList(
//            array(),
//            array(
//                "IBLOCK_ID" => 11,
//                "ACTIVE" => "Y",
//                "PROPERTY_SECTION" => $sValue
//            ),
//            false,
//            false,
//            array(
//                "ID",
//                "PROPERTY_LINK_SITE"
//            )
//        )->Fetch();
//
//        echo "Раздел не найден: ".$sValue." - ".$arElement['PROPERTY_LINK_SITE_VALUE']." - ".$arSectionElementCount[$sValue];
//        echo "<hr />";
//
//        $iCountNotFind++;
//
//    }
//    else
//    {
//
//        echo "Раздел найден: ".$sValue;
//        echo "<hr />";
//
//        $iCountFind++;
//
//    }
//
//}
//
//echo "Найдено: ".$iCountFind;
//echo "<hr />";
//echo "Не найдено: ".$iCountNotFind;


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>