<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>                <div class="information-blocks">
                    <div class="container-404">
                        <div class="title">404</div>
                        <div class="description">Страница не найдена</div>
						<div class="text">Запрошенная вами страница не найдена на сайте. Попробуйте начать с <a href="/">главной страницы</a> нашего сайта.</div>
                    </div>
                </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>