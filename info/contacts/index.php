<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><div class="information-blocks">
	 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2249.1345764018643!2d37.56895131617452!3d55.686648004559366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54c95e75b2157%3A0x4d7b9c9f6686abd4!2sProfsoyuznaya+Ulitsa%2C+6%2C+Moskva%2C+117292!5e0!3m2!1sen!2sru!4v1521708751982" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> <br>
 <br>
	<div class="row">
		<div class="col-md-4 information-entry">
			<h3 class="block-title main-heading">24 часа на связи</h3>
			<div class="article-container style-1">
				<p>
					 Искренне рады видеть вас в интим магазине "АИНТИМ"– одном из крупнейших секс шопов на просторах Рунета.
				</p>
				<h5>Наш адрес</h5>
				<p>
					 Москва, ул. Профсоюзная, д. 6А, стр. 2
				</p>
				<h5>Контактная информация</h5>
				<p>
					 Email: <a href="mailto:info@aintim.com">info@aintim.com</a><br>
					 Toll-free: +7(499) 375-65-60.
				</p>
			</div>
			<div class="share-box detail-info-entry">
				<div class="title">
					 Мы в социальных сетях
				</div>
				<div class="socials-box">
 <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-youtube"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-instagram"></i></a>
				</div>
				<div class="clear">
				</div>
			</div>
		</div>
		<div class="col-md-4 information-entry">
			<h3 class="block-title main-heading">Наши приемущества</h3>
			<div class="article-container style-1">
				<h5><i class="fa fa-calculator"></i> БЕСПЛАТНАЯ ДОСТАВКА</h5>
				<p>
					 Бесплатная доставка по России при сумме заказа от 3000 р.
				</p>
			</div>
			<div class="article-container style-1">
				<h5><i class="fa fa-scissors"></i> ГАРАНТИЯ НИЗКОЙ ЦЕНЫ</h5>
				<p>
					 Постоянные распродажи со скидками до 70% + необыкновенные акции
				</p>
			</div>
			<div class="article-container style-1">
				<h5><i class="fa fa-desktop"></i> МЕГАМАРКЕТ 10.000 ТОВАРОВ</h5>
				<p>
					 Представленные в нашем каталоге товары действительно есть в наличии и готовы к продаже!
				</p>
			</div>
		</div>
		<div class="col-md-4 information-entry">
			<h3 class="block-title main-heading">Честные отзывы</h3>
			<div class="article-container style-1">
				<h5><i class="fa fa-star"></i> 5 звезд от Яндекс.Маркета</h5>
				<p>
					 Более 450 честных отзывов на Яндекс. Маркете
				</p>
			</div>
			<div class="accordeon">
				<div class="accordeon-title">
					 Вежливые и общительные сотрудники. Быстрая доставка...
				</div>
				<div class="accordeon-entry">
					<div class="article-container style-1">
						<p>
							 Первая моя покупка в подобном магазине увенчалась успехом. Магазин выбирал по отзывам. Остановился на StripMag.ru. И не пожалел. Были трудности с выбором. Девушка оператор все доходчиво обьяснила. Все по полочкам расставила. Заказ 129337 ехал неделю до Курганской обл. Скидки, баллы, все это только прибавляет плюсы к работе этого замечательного магазина. Товары еще не проверяли т.к в командировке. Но визуально супруга все оценила на 5+. Вообще все понравилось, будем заказывать только здесь!
						</p>
					</div>
				</div>
				<div class="accordeon-title">
					 Ассортимент, обслуживание,скорость доставки и цены.
				</div>
				<div class="accordeon-entry">
					<div class="article-container style-1">
						<p>
							 Решили с женой немного пошалить! Долго искали сайт и нам попался ваш сайт! Заглянули и обомлели! Огромный выбор, низкие цены, а самое главное бонусы при покупке и невероятные скидки! Оформили заказ сразу на 7товаров!В течение часа позвонила девушка очень вежливая с приятным голосом ;)и сказала что товар будет доставлен в течение дня! Так и получилось! Товар доставил курьер, все запечатано и очень скрыто, словно привезли книги :) Игрушки испробовали в туже ночь! Все просто супер! Теперь прописались с женой на этом сайте! Спасибо сотрудникам магазина за их работу!!!
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>