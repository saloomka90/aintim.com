<?
$MESS["TARGETSMS_SMS_OPT_TITLE"] = "TargetSMS: СМС-рассылки и уведомления";
$MESS["TARGETSMS_SMS_OPT_TAB1"] = "Настройки модуля";
$MESS["TARGETSMS_SMS_OPT_TAB1_T"] = "Настройки модуля";
$MESS["TARGETSMS_SMS_OPT_TAB2"] = "Права доступа";
$MESS["TARGETSMS_SMS_OPT_TAB2_T"] = "Права доступа";
$MESS["TARGETSMS_SMS_OPT_SHLUZ_MAIN"] = "Настройки подключения к шлюзу";
$MESS["TARGETSMS_SMS_OPT_SHLUZ_PARAMS"] = "Дополнительные настройки";
$MESS["TARGETSMS_SMS_OPT_LIMITSMS"] = "Лимит отправки смс<br/> за 1 запрос к сервису";
$MESS["TARGETSMS_SMS_OPT_LOGIN"] = "Логин";
$MESS["TARGETSMS_SMS_OPT_API"] = "Имя хоста для отправки запросов (Сервер API)";
$MESS["TARGETSMS_SMS_OPT_PASSW"] = "Пароль";
$MESS["TARGETSMS_SMS_OPT_SHLUZ_OTP"] = "Отправитель смс";
$MESS["TARGETSMS_SMS_OPT_SENDER"] = "Отправитель смс<br/> по умолчанию";
$MESS["TARGETSMS_SMS_OPT_DLY"] = "для сайта";
$MESS["TARGETSMS_SMS_OPT_SEND"] = "Сохранить параметры";
$MESS["TARGETSMS_SMS_OPT_ERROR1"] = "Не удалось подключиться к сервису рассылок.";
$MESS["TARGETSMS_SMS_OPT_OK1"] = "Успешное подключение к сервису";
$MESS["TARGETSMS_SMS_OPT_BALANCE"] = "Остаток";
$MESS["TARGETSMS_SMS_OPT_CURENCY"] = "руб.";
$MESS["TARGETSMS_SMS_OPT_SENDER_GETTITLE"] = "Доступные имена отправителей";
$MESS["TARGETSMS_SMS_OPT_SENDERSTATE_ORDER"] = "оформляется";
$MESS["TARGETSMS_SMS_OPT_SENDERSTATE_COMPLETED"] = "готов к использованию";
$MESS["TARGETSMS_SMS_OPT_SENDERSTATE_REJECTED"] = "отклонен";
$MESS["TARGETSMS_SMS_OPT_SHLUZ_ADDSENDER"] = "Добавить новое имя отправителя";
$MESS["TARGETSMS_SMS_REGISTER_MESS"] = '<p>Для начала работы с установленным модулем "TargetSMS: СМС-рассылки и уведомления" необходимо 
пройти <a href="https://sms.targetsms.ru/ru/reg.html" target="_blank">регистрацию</a> на сервисе TargetSMS.</p>
<p>
Для использования в смс уникального имени отправителя необходимо согласование с вашим менеджером сервиса TargetSMS.</p>';

?>