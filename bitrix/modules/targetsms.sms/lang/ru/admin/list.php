<?
$MESS["TARGETSMS_SMS_LIST_TITLE"] = "История сообщений";
$MESS["TARGETSMS_SMS_LIST_ERR_1"] = "Ошибка в параметрах";
$MESS["TARGETSMS_SMS_LIST_ERR_2"] = "Неверный логин или пароль";
$MESS["TARGETSMS_SMS_LIST_ERR_3"] = "Недостаточно средств на счете Клиента";
$MESS["TARGETSMS_SMS_LIST_ERR_4"] = "IP-адрес временно заблокирован из-за частых ошибок в запросах, либо другая серверная ошибка на сервисе";
$MESS["TARGETSMS_SMS_LIST_ERR_5"] = "Неверный формат даты";
$MESS["TARGETSMS_SMS_LIST_ERR_6"] = "Сообщение запрещено (по тексту или по имени отправителя)";
$MESS["TARGETSMS_SMS_LIST_ERR_7"] = "Неверный формат номера телефона";
$MESS["TARGETSMS_SMS_LIST_ERR_8"] = "Сообщение на указанный номер не может быть доставлено";
$MESS["TARGETSMS_SMS_LIST_ERR_9"] = "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты";
$MESS["TARGETSMS_SMS_LIST_ERR_9999"] = "Неизвестная ошибка";
$MESS["TARGETSMS_SMS_LIST_ERR_9998"] = "Сервис недоступен";
$MESS["TARGETSMS_SMS_LIST_PHONE"] = "Номер получателя";
$MESS["TARGETSMS_SMS_LIST_SENDER"] = "Отправитель";
$MESS["TARGETSMS_SMS_LIST_SMSID"] = "ID смс";
$MESS["TARGETSMS_SMS_LIST_TIME"] = "Дата отправки";
$MESS["TARGETSMS_SMS_LIST_TIME_ST"] = "Статус получен";
$MESS["TARGETSMS_SMS_LIST_TIME_ST_L"] = "Нет данных";
$MESS["TARGETSMS_SMS_LIST_MESS"] = "Сообщение";
$MESS["TARGETSMS_SMS_LIST_PRIM"] = "Примечание";
$MESS["TARGETSMS_SMS_LIST_STATUS"] = "Cтатус сообщения";
$MESS["TARGETSMS_SMS_LIST_PROVIDER"] = "Шлюз";
$MESS["TARGETSMS_SMS_LIST_SQLDELERR"] = "Ошибка удаления записи";
$MESS["TARGETSMS_SMS_LIST_STATUS_1"] = "ожидает отправки с сайта";
$MESS["TARGETSMS_SMS_LIST_STATUS_2"] = "передано на шлюз";
$MESS["TARGETSMS_SMS_LIST_STATUS_3"] = "передано оператору";
$MESS["TARGETSMS_SMS_LIST_STATUS_4"] = "доставлено";
$MESS["TARGETSMS_SMS_LIST_STATUS_5"] = "просрочено";
$MESS["TARGETSMS_SMS_LIST_STATUS_6"] = "ожидает отправки на шлюзе";
$MESS["TARGETSMS_SMS_LIST_STATUS_7"] = "невозможно доставить";
$MESS["TARGETSMS_SMS_LIST_STATUS_8"] = "неверный номер";
$MESS["TARGETSMS_SMS_LIST_STATUS_9"] = "запрещено на сервисе";
$MESS["TARGETSMS_SMS_LIST_STATUS_10"] = "недостаточно средств";
$MESS["TARGETSMS_SMS_LIST_STATUS_11"] = "недоступный номер";
$MESS["TARGETSMS_SMS_LIST_STATUS_12"] = "неизвестный статус";
$MESS["TARGETSMS_SMS_LIST_FILTER_ERROR_ID"] = "ID должно быть числом болше 0!";
$MESS["TARGETSMS_SMS_LIST_FILTER_ERROR_PHONE"] = "Неверный формат номера телеффона!";
$MESS["TARGETSMS_SMS_LIST_FILTER_ERROR_TIME1"] = "Неверный формат даты старта!";
$MESS["TARGETSMS_SMS_LIST_FILTER_ERROR_TIME2"] = "Неверный формат даты окончания!";
$MESS["TARGETSMS_SMS_LIST_ENTITY_ID_FIELD"] = "ID";
$MESS["TARGETSMS_SMS_LIST_ENTITY_PROVIDER_FIELD"] = "Шлюз";
$MESS["TARGETSMS_SMS_LIST_ENTITY_SMSID_FIELD"] = "Ид на шлюзе";
$MESS["TARGETSMS_SMS_LIST_ENTITY_SENDER_FIELD"] = "Отправитель";
$MESS["TARGETSMS_SMS_LIST_ENTITY_PHONE_FIELD"] = "Телефон";
$MESS["TARGETSMS_SMS_LIST_ENTITY_TIME_SEND_FIELD"] = "Время отправки";
$MESS["TARGETSMS_SMS_LIST_ENTITY_TIME_STATE_FIELD"] = "Время статуса";
$MESS["TARGETSMS_SMS_LIST_ENTITY_MESS_FIELD"] = "Текст сообщения";
$MESS["TARGETSMS_SMS_LIST_ENTITY_PRIM_FIELD"] = "Примечание";
$MESS["TARGETSMS_SMS_LIST_ENTITY_EVENT_FIELD"] = "Код события";
$MESS["TARGETSMS_SMS_LIST_ENTITY_EVENT_NAME_FIELD"] = "Описание события";
$MESS["TARGETSMS_SMS_LIST_ENTITY_SORT_FIELD"] = "Приоритет отправки";
$MESS["TARGETSMS_SMS_LIST_ENTITY_STATUS_FIELD"] = "Статус";
$MESS["TARGETSMS_SMS_LIST_LIST_NAV_TEXT"] = "Сообщения";
$MESS["TARGETSMS_SMS_LIST_LIST_GROUP_DELETE"] = "Удалить";
$MESS["TARGETSMS_SMS_LIST_LIST_GROUP_EDIT"] = "Редактировать";
$MESS["TARGETSMS_SMS_LIST_LIST_BUTTON_CONTECST_BTN_NEW"] = "Отправить смс";
$MESS["TARGETSMS_SMS_LIST_LIST_TITLE"] = "Список сообщений";
?>