<?
$MESS["TARGETSMS_SMS_SENDFORM_TITLE"] = "Отправка смс";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_REQ"] = "Не заполнены обязательные поля";
$MESS["TARGETSMS_SMS_SENDFORM_NOTICE_SEND"] = "Сообщение отправлено";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_PHONE"] = "Неверный формат номера телефона";
$MESS["TARGETSMS_SMS_SENDFORM_PHONE"] = "Номер телефона";
$MESS["TARGETSMS_SMS_SENDFORM_MESS"] = "Сообщение";
$MESS["TARGETSMS_SMS_SENDFORM_DATE"] = "Дата и время отправки";
$MESS["TARGETSMS_SMS_SENDFORM_SEND"] = "Отправить смс";
$MESS["TARGETSMS_SMS_SENDFORM_TAB"] = "Форма отправки смс";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_1"] = "Ошибка в параметрах";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_2"] = "Неверный логин или пароль";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_3"] = "Недостаточно средств на счете Клиента";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_4"] = "IP-адрес временно заблокирован из-за частых ошибок в запросах, либо другая серверная ошибка на сервисе";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_5"] = "Неверный формат даты";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_6"] = "Сообщение запрещено (по тексту или по имени отправителя)";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_7"] = "Неверный формат номера телефона";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_8"] = "Сообщение на указанный номер не может быть доставлено";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_9"] = "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_9999"] = "Неизвестная ошибка";
$MESS["TARGETSMS_SMS_SENDFORM_ERR_9998"] = "Сервис недоступен";
$MESS["TARGETSMS_SMS_SENDFORM_PRIM"] = "Отправлено из формы";
$MESS["TARGETSMS_SMS_SENDFORM_TRANSLIT"] = "Отправить транслитом?";
$MESS["TARGETSMS_SMS_SENDFORM_SENDER"] = "Отправитель";
$MESS["TARGETSMS_SMS_SENDFORM_ORDERPHONE"] = "Найдено несколько телефонов в заказе";
$MESS["TARGETSMS_SMS_SENDFORM_ORDER"] = "Данное смс будет привязано к заказу с идентификатором: ";
?>