<?
$MESS["TARGETSMS_SMS_EVENTLIST_TITLE"] = "Шаблоны смс";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_ID_FIELD"] = "ID";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_SITE_ID_FIELD"] = "ID сайта";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_NAME_FIELD"] = "Название шаблона";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_SENDER_FIELD"] = "Отправитель";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_EVENT_FIELD"] = "Код события";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_TEMPLATE_FIELD"] = "Шаблон";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_PARAMS_FIELD"] = "Параметры";
$MESS["TARGETSMS_SMS_EVENTLIST_ENTITY_ACTIVE_FIELD"] = "Активность";
$MESS["TARGETSMS_SMS_EVENTLIST_LIST_NAV_TEXT"] = "Шаблоны";
$MESS["TARGETSMS_SMS_EVENTLIST_LIST_GROUP_DELETE"] = "Удалить";
$MESS["TARGETSMS_SMS_EVENTLIST_LIST_GROUP_EDIT"] = "Редактировать";
$MESS["TARGETSMS_SMS_EVENTLIST_LIST_BUTTON_CONTECST_BTN_NEW"] = "Добавить шаблон";
$MESS["TARGETSMS_SMS_EVENTLIST_LIST_TITLE"] = "Список шаблонов";
?>