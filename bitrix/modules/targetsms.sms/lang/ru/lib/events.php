<?
$MESS['TARGETSMS_SMS_EVENTCODE_ISMS_NEWORDER'] = 'Новый заказ';
$MESS['TARGETSMS_SMS_EVENTCODE_ISMS_STATUSUPDATE'] = 'Обновление статуса';
$MESS['TARGETSMS_SMS_EVENTCODE_ISMS_PAYED'] = 'Оплата заказа';
$MESS['TARGETSMS_SMS_EVENTCODE_ISMS_BXEVENT'] = 'Отправка письма';
$MESS['TARGETSMS_SMS_EVENTCODE_TABCONTROL_NAME'] = 'Отправленные смс';
$MESS['TARGETSMS_SMS_EVENTCODE_SENDSMS'] = 'Отправить смс к данному заказу';

$MESS["TARGETSMS_SMS_LIST_STATUS_1"] = "ожидает отправки с сайта";
$MESS["TARGETSMS_SMS_LIST_STATUS_2"] = "передано на шлюз";
$MESS["TARGETSMS_SMS_LIST_STATUS_3"] = "передано оператору";
$MESS["TARGETSMS_SMS_LIST_STATUS_4"] = "доставлено";
$MESS["TARGETSMS_SMS_LIST_STATUS_5"] = "просрочено";
$MESS["TARGETSMS_SMS_LIST_STATUS_6"] = "ожидает отправки на шлюзе";
$MESS["TARGETSMS_SMS_LIST_STATUS_7"] = "невозможно доставить";
$MESS["TARGETSMS_SMS_LIST_STATUS_8"] = "неверный номер";
$MESS["TARGETSMS_SMS_LIST_STATUS_9"] = "запрещено на сервисе";
$MESS["TARGETSMS_SMS_LIST_STATUS_10"] = "недостаточно средств";
$MESS["TARGETSMS_SMS_LIST_STATUS_11"] = "недоступный номер";
$MESS["TARGETSMS_SMS_LIST_STATUS_12"] = "неизвестный статус";

?>