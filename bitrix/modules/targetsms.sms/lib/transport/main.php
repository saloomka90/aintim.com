<?php
namespace Targetsms\Sms\Transport;

class Main{
	
	private $config;
	
	//конструктор, получаем данные доступа к шлюзу
	function __construct($params) {
		$this->config = $this->getConfig($params);
	}
	
	public function _getAllSender() {
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/originator.php';
		$response = $this->openHttp($url, $xml);
		
		$data = new \stdClass();
		
		if(!$response){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<originator state="([A-z]+)">(.*)<\/originator>/Ui',$response, $matches);

		if($count_resp>0 && is_array($matches[2])) {
			foreach($matches[2] as $k=>$sender) {
				$ob = new \stdClass();
				$ob->sender = $sender;
				$ob->state = $matches[1][$k];
				$arr[] = $ob;
			}
			$data = $arr;
		}
		else{
			$data->error = 'Error';
			$data->error_code = '9999';
			return $data;
		}
		
		return $data;
	}
	
	public function _sendSms ($phones, $mess, $time=0, $sender=false) {
	
		$data = new \stdClass();

		$phones = preg_replace("/[^0-9A-Za-z]/", "", $phones);
		if(toLower(SITE_CHARSET)=='windows-1251') {
			$mess = $GLOBALS['APPLICATION']->ConvertCharset($mess, SITE_CHARSET, 'UTF-8');
		}
		if(!$sender) {
			$sender = $this->config->sender;
		}
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		<message type="sms">
			<sender>'.$sender.'</sender>
			<text>'.$mess.'</text>
			<abonent phone="'.$phones.'"/>
		</message>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/';
		
		$response = $this->openHttp($url, $xml);
		
		if(!$response){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp_err = preg_match_all('/<information number_sms="">(.*)<\/information>/Ui',$response, $matches_err);
		if($count_resp_err>0) {
			$err = $GLOBALS['APPLICATION']->ConvertCharset($matches_err[1][0], 'UTF-8', SITE_CHARSET);
			$data->error = $err;
			$data->error_code = $this->chechErrorCode($err);
			return $data;
		}
		
		$count_resp = preg_match_all('/<information.*id_sms="(.*)".*parts="(.*)">(.*)<\/information>/Ui',$response, $matches);
		
		if($count_resp>0){
			$data->id = $matches[1][0];
			$data->cnt = $matches[2][0];
			$data->cost = '';
			$data->balance = '';
			return $data;
		}
		else{
			$data->error = 'Parse response error';
			$data->error_code = '9999';
			return $data;
		}
	
	}
	
	public function _getBalance () {
	
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		</request>';

		$url = 'https://'.$this->config->server.'/xml/balance.php';
		
		$response = $this->openHttp($url, $xml);
		
		$data = new \stdClass();
		
		if(!$response){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<money.*>(.*)<\/money>/Ui',$response, $matches);
		
		if($count_resp>0) {
			$data->balance = $matches[1][0];
		}else{
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		return $data;
		
	}
	
	//multiple sms
	public function _getStatusSms($arAll=array()){
		
		$arLinkSmsId = array();
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		<get_state>';
		foreach($arAll as $data){
			$arLinkSmsId[$data['SMSID']] = $data['ID']; //для оптимизации все обновления по ид на стороне клиента
			$xml .=	'<id_sms>'.$data['SMSID'].'</id_sms>'."\n";
		}
		$xml .= '</get_state>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/state.php';
		
		$response = $this->openHttp($url, $xml);
		
		$allResponce = array();
		
		if(!$response){
			foreach($arLinkSmsId as $serviceId=>$baseId){
				$data = new \stdClass();
				$data->error = 'Service is not available';
				$data->error_code = '9998';
				$data->baseId = $baseId;
				$allResponce[] = $data;
			}
			return $allResponce;
		}
		
		$error = $this->checkError($response);
		
		if($error) {
			foreach($arLinkSmsId as $serviceId=>$baseId){
			$data = new \stdClass();
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			$data->baseId = $baseId;
			$allResponce[] = $data;
			}
			return $allResponce;
		}
		
		$count_resp = preg_match_all('/<state id_sms=(.*) time="(.*)".*>(.*)<\/state>/Ui',$response, $matches);
		
		if($count_resp>0){
		
			foreach($matches[2] as $key=>$m){
				if($this->_checkStatus($matches[3][$key])){
					$data = new \stdClass();
					$data->last_timestamp = strtotime($matches[2][$key]);
					$data->status = $this->_checkStatus($matches[3][$key]);
					$data->baseId = $arLinkSmsId[preg_replace("/([^0-9])/is","",$matches[1][$key])];
					$allResponce[] = $data;
				}
			}
			
			return $allResponce;
		}
		
		foreach($arLinkSmsId as $serviceId=>$baseId){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			$data->baseId = $baseId;
			$allResponce[] = $data;
		}
		return $allResponce;
		
	}
	
	//old method for get status
	public function _getStatusSms_old($smsid,$phone=false) {
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		<get_state>
			<id_sms>'.$smsid.'</id_sms>
		</get_state>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/state.php';
		
		$response = $this->openHttp($url, $xml);

		$data = new \stdClass();
		
		if(!$response){
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<state.*time="(.*)".*>(.*)<\/state>/Ui',$response, $matches);
		
		if($count_resp>0){
			if($this->_checkStatus($matches[2][0])){
			$data->last_timestamp = strtotime($matches[1][0]);
			$data->status = $this->_checkStatus($matches[2][0]);
			return $data;
			}
		}
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		
	}
	
	private function getConfig($params) {
		
		$c = new \stdClass();
		if(strpos($params['login'],"||")!==false){
			$arPrm = explode("||",$params['login']);
		}else{
			$arPrm = array('sms.targetsms.ru',$params['login']);
		}
		$c->login = $arPrm[1];
		$c->server = $arPrm[0];
		$c->passw = $params['passw'];
		$c->sender = $params['sender'];
		
		return $c;
		
	}
	
	private function openHttp($url, $xml) {
		
		//default bitrix HttpClient
		$curl = \Bitrix\Main\Config\Option::get("targetsms.sms","curl","");
		
		if($curl != 'Y'){
		
			$httpClient = new \Bitrix\Main\Web\HttpClient();
			$httpClient->setHeader('Content-Type', 'text/xml; charset=utf-8', true);
			$result = $httpClient->post($url, $xml);
			
			return $result;
			
		}
		
		if (!function_exists('curl_init')) {
		    return false;
		}

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-type: text/xml; charset=utf-8' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_CRLF, true );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );

		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
		
	}
	
	private function checkError($resp) {
		$count = preg_match_all('/<error>(.*)<\/error>/Ui',$resp, $matches);
		if($count>0) {
			$error = $GLOBALS['APPLICATION']->ConvertCharset($matches[1][0], 'UTF-8', SITE_CHARSET);
			return $error;
		}
		return false;
	}
	
	private function chechErrorCode($code) {
		if(toLower(SITE_CHARSET)=='windows-1251') $code = $GLOBALS['APPLICATION']->ConvertCharset($code, SITE_CHARSET, 'UTF-8');
		$code = trim($code);
		if(strpos($code,'логин')!==false) return 2;
		if(strpos($code,'XML')!==false) return 1;
		if(strpos($code,'POST')!==false) return 1;
		
		if(strpos($code,'логин или')!==false) return 2; //'Неправильный логин или пароль'
		if(strpos($code,'формат XML')!==false) return 1; //'Неправильный формат XML документа'
		if(strpos($code,'аккаунт заблокирован')!==false) return 2; //'Ваш аккаунт заблокирован'
		if(strpos($code,'POST данные')!==false) return 1; //'POST данные отсутствуют'
		if(strpos($code,'закончились SMS')!==false) return 3; //'У нас закончились SMS. Для разрешения проблемы свяжитесь с менеджером.'
		if(strpos($code,'Закончились SMS')!==false) return 3; //'Закончились SMS.'
		if(strpos($code,'Аккаунт заблокирован')!==false) return 2; //'Аккаунт заблокирован.'
		if(strpos($code,'Укажите номер')!==false) return 1; //'Укажите номер телефона.'
		if(strpos($code,'стоп-листе')!==false) return 8; //'Номер телефона присутствует в стоп-листе.'
		if(strpos($code,'направление закрыто')!==false) return 6; //'Данное направление закрыто для вас.'
		if(strpos($code,'направление закрыто')!==false) return 6; //'Данное направление закрыто.'
		if(strpos($code,'SMS отклонен')!==false) return 6; //'Текст SMS отклонен модератором.'
		if(strpos($code,'Нет отправителя')!==false) return 6; //'Нет отправителя.'
		if(strpos($code,'символов для цифровых')!==false) return 6; //'Отправитель не должен превышать 15 символов для цифровых номеров и 11 символов для буквенно-числовых.'
		if(strpos($code,'телефона должен')!==false) return 7; //'Номер телефона должен быть меньше 15 символов.'
		if(strpos($code,'текста сообщения')!==false) return 1; //'Нет текста сообщения.'
		if(strpos($code,'Нет ссылки')!==false) return 1; //'Нет ссылки.'
		if(strpos($code,'название контакта')!==false) return 1; //'Укажите название контакта и хотя бы один параметр для визитной карточки.'
		if(strpos($code,'отправителя нет')!==false) return 6; //'Такого отправителя нет.'
		if(strpos($code,'не прошел модерацию')!==false) return 6; //'Отправитель не прошел модерацию.'
		
		return 9999;
	
	}
	
	private function _checkStatus($code) {
	
		if($code=='send') return 3;
		if($code=='not_deliver') return 7;
		if($code=='expired') return 5;
		if($code=='deliver') return 4;
		if($code=='partly_deliver') return false;
		return false;
		
	}
	
}
?>