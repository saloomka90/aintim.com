<?php
namespace Targetsms\Sms;

class Agent {
	
	function turnSms() {
		$ob = new \Targetsms\Sms\Sender();
		$ob->getTurnSms();
		return '\\Targetsms\\Sms\\Agent::turnSms();';
	}

	function statusSms() {
		$ob = new \Targetsms\Sms\Sender();
		$ob->getStatusSms();
		return '\\Targetsms\\Sms\\Agent::statusSms();';
	}
	
}