<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$module_id = "targetsms.sms";
$showAddOptions = false;
$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
$zr = "";
if (! ($MODULE_RIGHT >= "R"))
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$APPLICATION->SetTitle(Loc::getMessage("TARGETSMS_SMS_OPT_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

\Bitrix\Main\Loader::includeModule($module_id);

$obCache = \Bitrix\Main\Data\Cache::createInstance();
$obCache->cleanDir("/targetsms/sms/admin/");

if ($_SERVER["REQUEST_METHOD"] == "POST" && $MODULE_RIGHT == "W" && strlen($_REQUEST["Update"]) > 0 && check_bitrix_sessid()){
	
	\Bitrix\Main\Config\Option::set($module_id, "api", $_REQUEST["api"]);
	\Bitrix\Main\Config\Option::set($module_id, "login", $_REQUEST["login"]);
	\Bitrix\Main\Config\Option::set($module_id, "passw", $_REQUEST["passw"]);
	\Bitrix\Main\Config\Option::set($module_id, "limitsms", intval($_REQUEST["limitsms"]));
	\Bitrix\Main\Config\Option::set($module_id, "sender", $_REQUEST["sender"]);
	
}

$smsOb = new \Targetsms\Sms\Sender();
$senderList = $smsOb->getAllSender();
//print_r($senderList);
$balance = $smsOb->getBalance();

if(!$_REQUEST["sender"] && !$senderList->error && !\Bitrix\Main\Config\Option::get($module_id, "sender", "","")) {
	foreach($senderList as $sender){
		if($sender->state == 'completed') \Bitrix\Main\Config\Option::set($module_id, "sender", $sender->sender);
	}
}

$aTabs = array();
$aTabs[] = array("DIV" => "edit3", "TAB" => Loc::getMessage("TARGETSMS_SMS_OPT_TAB1"), "ICON" => "vote_settings", "TITLE" => Loc::getMessage("TARGETSMS_SMS_OPT_TAB1_T"));
$aTabs[] = array("DIV" => "edit4", "TAB" => Loc::getMessage("TARGETSMS_SMS_OPT_TAB2"), "ICON" => "vote_settings2", "TITLE" => Loc::getMessage("TARGETSMS_SMS_OPT_TAB2_T"));

$tabControl = new \CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($module_id)?>&lang=<?=LANGUAGE_ID?>&mid_menu=1" id="FORMACTION">
<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td colspan="2">
			<?=Loc::getMessage("TARGETSMS_SMS_REGISTER_MESS")?>
		</td>
	</tr>
	<tr class="heading"><td colspan="2"><?=Loc::getMessage("TARGETSMS_SMS_OPT_SHLUZ_MAIN")?></td></tr>
	<tr>
		<td><?=Loc::getMessage("TARGETSMS_SMS_OPT_API")?>:</td>
		<td>
			<?$val = \Bitrix\Main\Config\Option::get($module_id, "api", "sms.targetsms.ru","");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="api"></td>
	</tr>
	<tr>
		<td><?=Loc::getMessage("TARGETSMS_SMS_OPT_LOGIN")?>:</td>
		<td>
			<?$val = \Bitrix\Main\Config\Option::get($module_id, "login", "","");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="login"></td>
	</tr>
	<tr>
		<td><?=Loc::getMessage("TARGETSMS_SMS_OPT_PASSW")?>:</td>
		<td>
			<?$val = \Bitrix\Main\Config\Option::get($module_id, "passw", "","");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="passw"></td>
	</tr>
	<?if(\Bitrix\Main\Config\Option::get($module_id, "passw", "","") && \Bitrix\Main\Config\Option::get($module_id, "login", "","")){?>
	<?if($balance['main']->error){?>
	<tr><td colspan="2" style="text-align:center;"><font style="color:red;"><?=Loc::getMessage("TARGETSMS_SMS_OPT_ERROR1")?> <?=$balance['main']->error?>.</font></td></tr>
	<?}else{?>
	<tr><td colspan="2" style="text-align:center;"><font style="color:green;"><?=Loc::getMessage("TARGETSMS_SMS_OPT_OK1")?>. <?=Loc::getMessage("TARGETSMS_SMS_OPT_BALANCE")?>: <?=$balance['main']->balance?> <?=Loc::getMessage("TARGETSMS_SMS_OPT_CURENCY")?></font></td></tr>
	<?}?>
	<?}?>
	<tr class="heading"><td colspan="2"><?=Loc::getMessage("TARGETSMS_SMS_OPT_SHLUZ_OTP")?></td></tr>
	<tr>
		<td><?=Loc::getMessage("TARGETSMS_SMS_OPT_SENDER")?>:</td>
		<td>
			<?$val = \Bitrix\Main\Config\Option::get($module_id, "sender", "","");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="sender">
		</td>
	</tr>
	<?if(!$senderList->error){?>
	<tr class="heading"><td colspan="2"><?=Loc::getMessage("TARGETSMS_SMS_OPT_SENDER_GETTITLE")?></td></tr>
	<?foreach($senderList as $sender){?>
		<tr>
			<td><b><?=$sender->sender?></b></td>
			<td>
			<?
			$style = '';
			if($sender->state == 'completed') $style = ' style="color:green;"';
			if($sender->state == 'rejected') $style = ' style="color:red;"';
			?>
			<font<?=$style?>><?=Loc::getMessage("TARGETSMS_SMS_OPT_SENDERSTATE_".toUpper($sender->state))?></font>
			</td>
		</tr>
	<?}?>
	<tr><td></td><td>

	<a target="_blank" href="https://<?=\Bitrix\Main\Config\Option::get($module_id, "api", "sms.targetsms.ru","");?>/ru/cabinet/originator.html"><?=Loc::getMessage("TARGETSMS_SMS_OPT_SHLUZ_ADDSENDER")?></a></td></tr>
	<?}?>
	<?if($showAddOptions){?>
	<tr class="heading"><td colspan="2"><?=Loc::getMessage("TARGETSMS_SMS_OPT_SHLUZ_PARAMS")?></td></tr>
	<tr>
		<td><?=Loc::getMessage("TARGETSMS_SMS_OPT_LIMITSMS")?>:</td>
		<td>
			<?$val = \Bitrix\Main\Config\Option::get($module_id, "limitsms", "10","");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="limitsms"></td>
	</tr>
	<?}?>
	
<?
$tabControl->BeginNextTab();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();
?>
	<input <?if ($MODULE_RIGHT<"W") echo "disabled" ?> type="submit" class="adm-btn-green" name="Update" value="<?=Loc::getMessage("TARGETSMS_SMS_OPT_SEND")?>" />
	<input type="hidden" name="Update" value="Y" />
<?$tabControl->End();
?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>