<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$module_id = "targetsms.sms";

\Bitrix\Main\Loader::includeModule($module_id);
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$APPLICATION->SetAdditionalCSS("/bitrix/css/".$module_id."/style.css");
	
class MlifeRowListAdmin extends \Targetsms\Sms\Main {
	
	public function __construct($params) {
		parent::__construct($params);
	}
	
	public function getMlifeRowListAdminCustomRow($row){

		$row->AddViewField("TIME_SEND", $row->arRes['TIME_SEND']->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "DD.MM.YYYY HH:MI"))));
		if($row->arRes['TIME_STATE']) {
			$row->AddViewField("TIME_STATE", $row->arRes['TIME_STATE']->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "DD.MM.YYYY HH:MI"))));
		} else {
			$row->AddViewField("TIME_STATE", Loc::getMessage("TARGETSMS_SMS_LIST_TIME_ST_L"));
		}
		$row->AddViewField("STATUS", '<font class="status_'.$row->arRes['STATUS'].'">'.Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_".$row->arRes['STATUS']).'</font>');
	}
	
}

$arParams = array(
	"PRIMARY" => "ID",
	"ENTITY" => "\\Targetsms\\Sms\\ListTable",
	"FILE_EDIT" => 'targetsms_sms_sendform.php',
	"BUTTON_CONTECST" => array(),
	"ADD_GROUP_ACTION" => array("delete"),
	"COLS" => true,
	"FIND" => array(
		"SENDER","PHONE","EVENT","MESS","SMSID",
		array("NAME"=>"STATUS", "GROUP"=>"STATUS", "FILTER_TYPE"=>"=", "TYPE"=>"LIST",
			"VALUES"=> array(
				"reference" => array(
					"-",
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_1"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_2"),
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_3"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_4"),
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_5"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_6"),
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_7"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_8"),
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_9"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_10"),
					Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_11"),Loc::getMessage("TARGETSMS_SMS_LIST_STATUS_12"),
				),
				"reference_id" => array("",1,2,3,4,5,6,7,8,9,10,11,12)
			)
		)
	),
	"LIST" => array("ACTIONS" => array("delete")),
	"CALLBACK_ACTIONS" => array()
);

$adminCustom = new MlifeRowListAdmin($arParams);
$adminCustom->defaultInterface();