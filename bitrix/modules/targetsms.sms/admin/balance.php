<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$module_id = "targetsms.sms";
$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if (! ($MODULE_RIGHT >= "R"))
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
	
$APPLICATION->SetTitle(Loc::getMessage("TARGETSMS_SMS_BALANCE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$APPLICATION->SetAdditionalCSS("/bitrix/css/".$module_id."/style.css");

\Bitrix\Main\Loader::includeModule($module_id);
$smsOb = new \Targetsms\Sms\Sender();
$balance = $smsOb->getBalance();
?>

<?if($balance['main']->error){?>
<div class="balance"><font style="color:red;"><?=Loc::getMessage("TARGETSMS_SMS_OPT_ERROR1")?> <?=$balance['main']->error?>.</font><br/><br/>
<a href="/bitrix/admin/settings.php?mid=targetsms.sms&lang=ru&mid_menu=1"><?=Loc::getMessage("TARGETSMS_SMS_SETTINGS")?></a>
</div>
<?}else{?>
<div class="balance"><font style="color:green;">
<?=Loc::getMessage("TARGETSMS_SMS_OPT_BALANCE")?>: <?=$balance['main']->balance?> <?=Loc::getMessage("TARGETSMS_SMS_OPT_CURENCY")?></font>
<br/>
<br/>
<form method="post" action="https://<?=\Bitrix\Main\Config\Option::get("targetsms.sms", "api", "sms.targetsms.ru","");?>/ru/cabinet/pay.html" target="_blank">
<input type="hidden" name="login" value="<?=\Bitrix\Main\Config\Option::get("targetsms.sms","login","")?>"/>
<input type="hidden" name="pass" value="<?=\Bitrix\Main\Config\Option::get("targetsms.sms","passw","")?>"/>
<input type="button" name="submit_button" onclick="this.form.submit()" value="<?=Loc::getMessage("TARGETSMS_SMS_OPT_BALANCE_ADD")?>">
</form>
</div>
<?}?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>