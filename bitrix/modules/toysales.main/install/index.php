<?
global $MESS;

class toysales_main extends CModule{
    var $MODULE_ID = 'toysales.main';
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;

    function toysales_main(){
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = 'Fire';
        $this->MODULE_DESCRIPTION = 'Fire';
        //$this->PARTNER_NAME = GetMessage("PARTNER");
        //$this->PARTNER_URI = GetMessage("PARTNER_URI");
    }

    function InstallDB()
    {
        return true;
    }


    function UnInstallDB()
    {
        return true;
    }

    function InstallEvents(){
        return true;
    }

    function UnInstallEvents(){
    }

    function InstallFiles(){
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", false, true);
		}
		
        return true;
    }

    function UnInstallFiles(){
		GLOBAL $MODULE_ID;
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			//admin files
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			//css
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");
			DeleteDirFilesEx("/bitrix/themes/.default/icons/".$this->MODULE_ID."/");//icons			
		}		
        return true;
    }

    function DoInstall(){

        global $DB, $DBType, $APPLICATION;
        $this->errors = false;

        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();

        RegisterModule($this->MODULE_ID);

        $settings = Array(
		'TOYSALES_POPUP_PICTURE' => '/img/popup-bg-image.jpg',
		'TOYSALES_POPUP_SHOW' => 'no',
		'TOYSALES_POPUP_HTML' => '<div class="newsletter-title">Секретный раздел</div><div class="newsletter-text">Оставьте E-mail и получите доступ в секретный раздел<br />сайта с самыми "жаркими" игрушками и<br />откровенным бельем!<br /> + <span class="inline-label red">лубрикант в подарок</span></div>',
        );

        foreach($settings as $key=>$value){
            COption::SetOptionString($this->MODULE_ID, $key, $value, SITE_ID);
        }

        //LocalRedirect('/bitrix/admin/settings.php?lang=ru&mid=toysales.main&mid_menu=1');

    }

    function DoUninstall(){

        global $DB, $DBType, $APPLICATION;
        $this->errors = false;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();

        UnRegisterModule($this->MODULE_ID);

    }
}
?>