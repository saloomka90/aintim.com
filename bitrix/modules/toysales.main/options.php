<?
$module_id = "toysales.main";
IncludeModuleLangFile(__FILE__);
include_once($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");
if(isset($_POST) && isset($_POST['TOYSALES_SAVE'])) {
	Toysales_Settings::SetSettings($_POST);
	if ($_FILES["TOYSALES_POPUP_FILE"]["name"] != "" and $_FILES["TOYSALES_POPUP_FILE"]["tmp_name"] != "")
	{
		$path_arr = pathinfo($_FILES["TOYSALES_POPUP_FILE"]["name"]);
		if (in_array($path_arr["extension"], array("jpg","jpeg","gif","png")))
		{
			if (move_uploaded_file($_FILES["TOYSALES_POPUP_FILE"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/img/".$path_arr["basename"]))
				COption::SetOptionString($module_id, "TOYSALES_POPUP_PICTURE", "/img/".$path_arr["basename"], SITE_ID);
		}
		else
			CAdminMessage::ShowMessage("Неверный формат файла");
	}
}

$settings = Toysales_Settings::GetSettings();
$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);
if($MOD_RIGHT>="W" && true):
	$aTabs = array(
		0 => array(
			'TAB'=>'Общие настройки', 
			'DIV'=>'edit1', 
			'TITLE' => 'Настройки модуля',
		)
	);
	$tab = new CAdminTabControl('Toysales_Settings_tab', $aTabs);
	$tab->Begin();
	
?>
	<form method="post" enctype="multipart/form-data">
<?	$tab->BeginNextTab();?>
        <tr class="heading">
            <td colspan="2">Попап с подпиской</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Задержка в секундах:</td>
			<td width="60%">
				<input type="text" name="TOYSALES_POPUP_DELAY"  value="<?=$settings['TOYSALES_POPUP_DELAY']?>" />
			</td>
        </tr>		
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Адрес картинки:</td>
			<td width="60%">
				<?=$settings['TOYSALES_POPUP_PICTURE']?><br><br><input type="file" name="TOYSALES_POPUP_FILE"> jpg, gif, png
			</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Показывать:</td>
			<td width="60%">
				<select name="TOYSALES_POPUP_SHOW">
					<option value="no" <?if ($settings["TOYSALES_POPUP_SHOW"] == "no"):?> selected<?endif?>>Не показывать</option>
					<option value="index_only" <?if ($settings["TOYSALES_POPUP_SHOW"] == "index_only"):?> selected<?endif?>>Показывать только на главной</option>
					<option value="yes" <?if ($settings["TOYSALES_POPUP_SHOW"] == "yes"):?> selected<?endif?>>На всех</option>
				</select>
			</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Текст:</td>
			<td width="60%">
				<textarea style="width:600px;height:70px" name="TOYSALES_POPUP_HTML"><?=$settings['TOYSALES_POPUP_HTML']?></textarea>
			</td>
        </tr>
		<tr class="heading">
            <td colspan="2">Импорт каталога</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Какую цену использовать:</td>
			<td width="60%">
				<select name="TOYSALES_IMPORT_PRICE_TYPE">
					<option value="1" <?if ($settings["TOYSALES_IMPORT_PRICE_TYPE"] == "1"):?> selected<?endif?>>Розничную</option>
					<option value="2" <?if ($settings["TOYSALES_IMPORT_PRICE_TYPE"] == "2"):?> selected<?endif?>>Оптовую</option>
				</select>
			</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Наценка в процентах:</td>
			<td width="60%">
				<input type="text" name="TOYSALES_IMPORT_NACENKA"  value="<?=$settings['TOYSALES_IMPORT_NACENKA']?>" />
			</td>
        </tr>			
		<tr class="heading">
            <td colspan="2">Обмен заказами с P5S</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Ключ для API:</td>
			<td width="60%">
				<input type="text" name="TOYSALES_P5S_API_KEY"  value="<?=$settings['TOYSALES_P5S_API_KEY']?>" />
			</td>
        </tr>					
		<tr class="heading">
            <td colspan="2">Мониторинг</td>
        </tr>
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">E-mail для уведомлений:</td>
			<td width="60%">
				<input type="text" name="TOYSALES_MONITORING_EMAIL"  value="<?=$settings['TOYSALES_MONITORING_EMAIL']?>" />
			</td>
        </tr>		
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Контролировать доступное дисковое пространство:</td>
			<td width="60%">
				<input type="checkbox" name="TOYSALES_MONITORING_DISK" value="Y"<?if ($settings["TOYSALES_MONITORING_DISK"] == "Y"):?> checked<?endif?>>
			</td>
        </tr>		
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Контролировать обновление товарной номенклатуры и остатков:</td>
			<td width="60%">
				<input type="checkbox" name="TOYSALES_MONITORING_CATALOG" value="Y"<?if ($settings["TOYSALES_MONITORING_CATALOG"] == "Y"):?> checked<?endif?>>
			</td>
        </tr>		
		<tr>
			<td width="40%" class="adm-detail-content-cell-l" style="white-space:nowrap">Контролировать обмен статусами заказов:</td>
			<td width="60%">
				<input type="checkbox" name="TOYSALES_MONITORING_ORDERS" value="Y"<?if ($settings["TOYSALES_MONITORING_ORDERS"] == "Y"):?> checked<?endif?>>
			</td>
        </tr>				
<?	$tab->EndTab();?>
<?	$tab->Buttons();?>
	<input type="submit" name="TOYSALES_SAVE" class="adm-btn-save"  value="Сохранить" title="Сохранить изменения" />
	<input type="button" onclick="window.document.location = '?lang=<?=LANGUAGE_ID ?>'" value="Отменить" title="Не сохранять изменения" />
</form>
<?$tab->End();?>
<?endif;?>