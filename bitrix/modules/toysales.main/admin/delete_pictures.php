<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
CModule::IncludeModule('iblock');

IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("subscribe");
if($POST_RIGHT=="D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


$APPLICATION->SetTitle("Удалениe картинок");

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
?>
<?
if (isset($_REQUEST["cnt"]))
{
	$cnt = intval($_REQUEST["cnt"]);
	if ($cnt > 0)
	{
		$cnt = (string) $cnt;
		if ($cnt[strlen($cnt)-1] == 1)
			$word = "товара";
		else
			$word = "товаров";
	?>
	<span style="color:red">Удалены картинки у <?=$cnt?> <?=$word?></span><br><br>
	<?
	}
}
else
if (isset($_REQUEST["delete"]) && !empty($_REQUEST["xml_id"]))
{
	$arFilter = array(
		"IBLOCK_ID" => 5,
		"XML_ID" => array(),
	);
	$xml_id_arr = explode(",", $_REQUEST["xml_id"]);
	foreach ($xml_id_arr as $xml_id)
	{
		if (intval($xml_id) > 0)
			$arFilter["XML_ID"][] = intval($xml_id);
	}
	if (!empty($arFilter["XML_ID"]))
	{
		$dir_path_arr = array(
			$_SERVER["DOCUMENT_ROOT"]."/images/"
		);
		$dir_path = $_SERVER["DOCUMENT_ROOT"]."/resized_images/";
		if ($handle = opendir($dir_path))
		{
			while (false !== ($entry = readdir($handle)))
			{
				if ($entry != "." && $entry != ".." && is_dir($dir_path.$entry))
				{
					$dir_path_arr[] = $dir_path.$entry."/";
				}
			}
			closedir($handle);
		}		
		$rsItems = CIBlockElement::GetList(array(), $arFilter);
		while ($obItem = $rsItems->GetNextElement())
		{
			$arItem = $obItem->GetFields();
			//echo $arItem["ID"]." ".$arItem["NAME"]."<br>";
			$arItem["PROPERTIES"] = $obItem->GetProperties();
			$i = 1;
			while ($i <= 10)
			{
				if ($arItem["PROPERTIES"]["pict".$i]["VALUE"] != "")
				{
					$filename = basename($arItem["PROPERTIES"]["pict".$i]["VALUE"]);
					foreach ($dir_path_arr as $dir_path)
					{
						if (file_exists($dir_path.$filename))
						{
							unlink($dir_path.$filename);
						}
					}
				}
				$i++;
			}
			$cnt++;
		}
		LocalRedirect($APPLICATION->GetCurPage()."?cnt=".$cnt);
	}
}
else
if (isset($_REQUEST["delete_all"]))
{
	$cnt = 0;
	$dir_path_arr = array(
		$_SERVER["DOCUMENT_ROOT"]."/images/"
	);
	$dir_path = $_SERVER["DOCUMENT_ROOT"]."/resized_images/";
	if ($handle = opendir($dir_path))
	{
		while (false !== ($entry = readdir($handle)))
		{
			if ($entry != "." && $entry != ".." && is_dir($dir_path.$entry))
			{
				$dir_path_arr[] = $dir_path.$entry."/";
			}
		}
		closedir($handle);
	}			
	foreach ($dir_path_arr as $dir_path)	
	{
		if ($handle = opendir($dir_path))
		{
			while (false !== ($entry = readdir($handle)))
			{
				if ($entry != "." && $entry != ".." && is_file($dir_path.$entry))
				{
					if (@unlink($dir_path.$entry))
						$cnt++;
				}
				//if ($cnt > 1000)
					//break;
			}
			closedir($handle);
		}
		//if ($cnt > 1000)
			//break;		
	}
	if ($cnt > 0)
	{
	?>
	<span style="color:red">Удалено файлов: <?=$cnt?></span><br><br>
	<?
	}
}
?>
<form method="POST" action="<?=$APPLICATION->GetCurPage()?>">
<b>prodID товаров(через запятую)</b><br><input type="text" name="xml_id" style="width:400px;margin:5px 0px 5px 0px;" value="<?=htmlspecialcharsbx($_REQUEST["xml_id"])?>"><br><input type="submit" name="delete" value="Удалить отмеченные">
</form>
<br>
<a href="<?=$APPLICATION->GetCurPage()?>?delete_all"><input type="button" value="Удалить все">

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');?>