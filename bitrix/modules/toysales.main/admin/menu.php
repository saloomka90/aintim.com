<?
IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("toysales.main")>"D")
{
	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "toysales.main",
		"sort" => 100,
		"text" => "Fire",
		"title" => "Fire",
		"icon" => "fire_menu_icon",
		"page_icon" => "form_page_icon",
		"module_id" => "toysales.main",
		"items" => array(
			array(
				"text" => "Удаление картинок товаров",
				"url" => "toysales_main_delete_pictures.php"
			)
		),
		"items_id" => "menu_toysales",
	);

	return $aMenu;
}
return false;
?>
