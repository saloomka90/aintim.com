<?
class Toysales_Settings {
	const MODULE_ID = 'toysales.main';
	
	public function GetSettings() {
		$arFields = array(
			'TOYSALES_POPUP_PICTURE' => 'TEXT',
			'TOYSALES_POPUP_SHOW' => 'TEXT',
			'TOYSALES_POPUP_HTML' => 'TEXT',
			'TOYSALES_POPUP_DELAY' => 'TEXT',			
			'TOYSALES_IMPORT_PRICE_TYPE' => 'TEXT',
			'TOYSALES_IMPORT_NACENKA' => 'TEXT',
			'TOYSALES_P5S_API_KEY' => 'TEXT',
			'TOYSALES_MONITORING_EMAIL' => 'TEXT',
			'TOYSALES_MONITORING_DISK' => 'TEXT',
			'TOYSALES_MONITORING_CATALOG' => 'TEXT',
			'TOYSALES_MONITORING_ORDERS' => 'TEXT',
		);
		
		$arSettings = array();
		foreach($arFields as $code=>$type){
			$value = COption::GetOptionString(self::MODULE_ID,$code,'',SITE_ID);
			if ($type == 'ARRAY') {
				$value = json_decode($value, true);
			}
			else
			if ($type == 'TEXT')
				$value = htmlspecialcharsbx($value);
			
			$arSettings[$code] = $value;

		}
		return $arSettings;
	}
	public function SetSettings($arFields) {
		$arSetFields = array(
			//'TOYSALES_POPUP_PICTURE' => 'TEXT',
			'TOYSALES_POPUP_SHOW' => 'TEXT',
			'TOYSALES_POPUP_HTML' => 'TEXT',
			'TOYSALES_POPUP_DELAY' => 'TEXT',
			'TOYSALES_IMPORT_PRICE_TYPE' => 'TEXT',
			'TOYSALES_IMPORT_NACENKA' => 'TEXT',
			'TOYSALES_P5S_API_KEY' => 'TEXT',
			'TOYSALES_MONITORING_EMAIL' => 'TEXT',
			'TOYSALES_MONITORING_DISK' => 'CHECKBOX',
			'TOYSALES_MONITORING_CATALOG' => 'CHECKBOX',
			'TOYSALES_MONITORING_ORDERS' => 'CHECKBOX',			
		);
		foreach($arSetFields as $code=>$value) {
			if ($value == 'CHECKBOX' && !isset($arFields[$code]))
				$val = '';
			elseif (isset($arFields[$code]))
			{
				if ($code == "TOYSALES_IMPORT_NACENKA")
				{
					$val = intval($arFields[$code]);
					if ($val < 0)
					{
						CAdminMessage::ShowMessage("Наценка не может быть отрицательной");
						continue;
					}
				}
				else
					$val = $arFields[$code];
			}
			if ($value == 'ARRAY') {
				$val = json_encode($arFields[$code]);
			}
			COption::SetOptionString(self::MODULE_ID, $code, $val, SITE_ID);
			
		}
	}
}
?>