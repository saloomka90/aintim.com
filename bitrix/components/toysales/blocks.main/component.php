<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 300;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
 	$arParams["IBLOCK_TYPE"] = "catalog";
if($arParams["IBLOCK_TYPE"]=="-")
	$arParams["IBLOCK_TYPE"] = "";

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
	$arParams["SORT_BY"] = "SORT";
if($arParams["SORT_ORDER"]!="ASC")
	 $arParams["SORT_ORDER"]="DESC";

$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";

if($this->StartResultCache(false, $USER->GetGroups()))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"ACTIVE"=>"Y",
		"ID"=>$arParams["IBLOCK_ID"],
		"SITE_ID" => SITE_ID,
		"CHECK_PERMISSIONS" => "Y",
	);

	$rsIBlock = CIBlock::GetList(array(), $arFilter);
	$arResult = $rsIBlock->GetNext();

	if(!$arResult)
	{
		$this->AbortResultCache();
		ShowError("������ �� ������");
		@define("ERROR_404", "Y");
		return;
	}

	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"PREVIEW_PICTURE",
	);

	$arFilter = array (
		"IBLOCK_ID" => $arResult["ID"],
		"ACTIVE" => "Y",
	);

	$arOrder = array(
		$arParams["SORT_BY"]=>$arParams["SORT_ORDER"],
	);
	if(!array_key_exists("ID", $arOrder))
		$arOrder["ID"] = "DESC";

	$arResult["ITEMS"] = array();
	$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while($obItem = $rsItems->GetNextElement())
	{
		$arItem = $obItem->GetFields();
		$arItem["PROPERTIES"] = $obItem->GetProperties();
		if ($arItem["PREVIEW_PICTURE"] != "")
		{
			$arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
		}
		$arResult["ITEMS"][]=$arItem;
	}
	$this->IncludeComponentTemplate();
}
if($USER->IsAuthorized())
{
	if($GLOBALS["APPLICATION"]->GetShowIncludeAreas() && CModule::IncludeModule("iblock"))
		$this->AddIncludeAreaIcons(CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, 0, 0, true));
	if($arParams["DISPLAY_PANEL"] && CModule::IncludeModule("iblock"))
		CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, 0, 0, false, $this->GetName());
}
?>