<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ITEMS"]))
{
?>
            <div class="demo-categories-wrapper">
                <div class="row">
<?
	foreach ($arResult["ITEMS"] as $arItem)
	{
	?>	
                    <div class="col-sm-4 information-entry-xs">
                        <div class="demo-categories-entry"<?if ($arItem["PREVIEW_PICTURE"]["SRC"] != "") echo ' style="background-image: url('.$arItem["PREVIEW_PICTURE"]["SRC"].');"';?>>
                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="<?=$arItem["PROPERTIES"]["url"]["VALUE"]?>" class="title"><?=$arItem["NAME"]?></a>
                                    <div class="list">
	<?
	if (is_array($arItem["PROPERTIES"]["sub_categories"]["VALUE"]))
	{
		foreach ($arItem["PROPERTIES"]["sub_categories"]["VALUE"] as $key => $value)
		{
			?><a href="<?=$arItem["PROPERTIES"]["sub_categories"]["DESCRIPTION"][$key]?>"><?=$value?></a><?
		}
	}
	?>
                                    </div>
                                </div>
                                <div class="col-lg-6 hidden-md hidden-sm hidden-xs">
                                    <div class="block-info">
                                        <div class="title">Раздел</div>
                                        <div class="description"><?=$arItem["PROPERTIES"]["bold_category"]["VALUE"]?></div>
                                        <a class="detail-link" href="<?=$arItem["PROPERTIES"]["url"]["VALUE"]?>">все товары</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>	
	<?	
	}
?>				
				</div>
			</div>
<?
}
?>