<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult["ITEMS"]))
{
?>	
        <div class="popup-container">
<?
	foreach	($arResult["ITEMS"] as $arItem)
	{
	?>
            <div class="cart-entry">
	<?
		if ($arItem["PICTURE"] != "")
		{
		?>
                <a class="image"><img src="<?=$arItem["PICTURE"]?>" alt="" /></a>
		<?
		}
	?>			
                <div class="content">
                    <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                    <div class="quantity">Количество: <?=$arItem["QUANTITY"]?></div>
                    <div class="price"><?=$arItem["PRICE_FORMATED"]?></div>
                </div>
                <div class="button-x"><i class="fa fa-close delete_basket_item" rel="<?=$arItem["ID"]?>"></i></div>
            </div>
	<?
	}
?>

            <div class="summary">
                <div class="grandtotal">Итого <span><?=$arResult["SUM_FORMATED"]?></span></div>
            </div>
            <div class="cart-buttons">
                <div class="column">
                    <a class="button style-3" href="<?=$arParams["PATH_TO_BASKET"]?>">В корзину</a>
                    <div class="clear"></div>
                </div>
                <div class="column">
                    <a class="button style-4" href="<?=$arParams["PATH_TO_ORDER"]?>">Оформить</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
<?
}		
?>