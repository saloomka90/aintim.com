<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("sale"))
{
	ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
	return;
}
$arParams["PATH_TO_BASKET"] = trim($arParams["PATH_TO_BASKET"]);
$arParams["PATH_TO_ORDER"] = trim($arParams["PATH_TO_ORDER"]);

$allSum = 0.0;
$arBasketItems = array();
$arResult["ITEMS"] = array();

$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);

$fuserId = (int)CSaleBasket::GetBasketUserID(true);
if ($fuserId > 0)
{
	if (isset($_REQUEST["delete_id"]) and $_REQUEST["delete_id"] != "")
	{
		$rsBaskets = CSaleBasket::GetList(
			array("ID" => "ASC"),
			array("FUSER_ID" => $fuserId, "LID" => SITE_ID, "ORDER_ID" => "NULL", "ID" => $_REQUEST["delete_id"]),
			false,
			false,
			array(
				"ID",
			)
		);
		if ($arBasketItem = $rsBaskets->GetNext())
			CSaleBasket::Delete($arBasketItem["ID"]);
	}
	$rsBaskets = CSaleBasket::GetList(
		array("ID" => "ASC"),
		array("FUSER_ID" => $fuserId, "LID" => SITE_ID, "ORDER_ID" => "NULL"),
		false,
		false,
		array(
			"ID", "NAME", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY",
			"PRICE", "WEIGHT", "DETAIL_PAGE_URL", "NOTES", "CURRENCY", "VAT_RATE", "CATALOG_XML_ID",
			"PRODUCT_XML_ID", "SUBSCRIBE", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "TYPE", "SET_PARENT_ID"
		)
	);
	while ($arItem = $rsBaskets->GetNext())
	{
		//echo $arItem["DETAIL_PAGE_URL"]."<br>";
		$arBasketItems[] = $arItem;
	}
}
if (!empty($arBasketItems))
{
	foreach ($arBasketItems as &$arItem)
	{
		if (CSaleBasketHelper::isSetItem($arItem))
			continue;

		if ($arItem["DELAY"]=="N" && $arItem["CAN_BUY"]=="Y")
		{
			$allSum += ($arItem["PRICE"] * $arItem["QUANTITY"]);
			$arItem["PRICE_FORMATED"] = SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
			$arItem["PICTURE"] = "";
			$db_res = CSaleBasket::GetPropsList(
					array(
							"SORT" => "ASC",
							"NAME" => "ASC"
						),
					array("BASKET_ID" => $arItem["ID"], "CODE" => "PICTURE")
				);
			if ($ar_res = $db_res->Fetch())
			{
				if ($ar_res["VALUE"] != "")
				{
					//echo $ar_res["VALUE"];
					$filename =  basename($ar_res["VALUE"]);
					$exist = false;
					$original_pic_path = "/images/".$filename;
					if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
						$exist = true;
					else
					{
						$content = file_get_contents($arItem["PROPERTIES"]["pict1"]["VALUE"]);
						if ($content !== false)
						{
							file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
							if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
								$exist = true;
						}
					}
					if ($exist == true)
					{
						$resized_pic_path = "/resized_images/150x220/".$filename;
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
							$arItem["PICTURE"] = $resized_pic_path;
						else
						{
							$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
							if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>150,"height"=>220), BX_RESIZE_IMAGE_PROPORTIONAL))
								$arItem["PICTURE"] = $resized_pic_path;
						}
					}					
				}
			}
			
			$arResult["ITEMS"][] = $arItem;
		}
	}
}
$arResult["SUM_FORMATED"] = SaleFormatCurrency($allSum, $allCurrency);
if ($_REQUEST["ajax_basket"] == "Y")
	$APPLICATION->RestartBuffer();
$this->IncludeComponentTemplate();
?>
