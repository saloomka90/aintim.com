<?
use Bitrix\Main\Loader;
use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

unset($arParams["IBLOCK_TYPE"]); //was used only for IBLOCK_ID setup with Editor
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["VENDORS_IBLOCK_ID"] = (int)$arParams["VENDORS_IBLOCK_ID"];

if (empty($arParams["ELEMENT_SORT_FIELD"]))
	$arParams["ELEMENT_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER"]))
	$arParams["ELEMENT_SORT_ORDER"] = "asc";
if (empty($arParams["ELEMENT_SORT_FIELD2"]))
	$arParams["ELEMENT_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER2"]))
	$arParams["ELEMENT_SORT_ORDER2"] = "desc";

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);
$arParams["BASKET_URL"]=trim($arParams["BASKET_URL"]);
if($arParams["BASKET_URL"] === '')
	$arParams["BASKET_URL"] = "/personal/basket.php";

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if($arParams["ACTION_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if($arParams["PRODUCT_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if($arParams["PRODUCT_PROPS_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

$arParams["SECTION_ID_VARIABLE"]=trim($arParams["SECTION_ID_VARIABLE"]);
if($arParams["SECTION_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_ID_VARIABLE"]))
	$arParams["SECTION_ID_VARIABLE"] = "SECTION_ID";

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";

$arParams["ELEMENT_COUNT"] = intval($arParams["ELEMENT_COUNT"]);
if($arParams["ELEMENT_COUNT"]<=0)
	$arParams["ELEMENT_COUNT"]=10;

if(!isset($arParams["PROPERTY_CODE"]) || !is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["PROPERTY_CODE"][$k]);
if(!isset($arParams["PRICE_CODE"]) || !is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;
$arParams["USE_PRODUCT_QUANTITY"] = false;

$arParams['ADD_PROPERTIES_TO_BASKET'] = (isset($arParams['ADD_PROPERTIES_TO_BASKET']) && $arParams['ADD_PROPERTIES_TO_BASKET'] === 'N' ? 'N' : 'Y');
if ('N' == $arParams['ADD_PROPERTIES_TO_BASKET'])
{
	$arParams["PRODUCT_PROPERTIES"] = array();
	$arParams["OFFERS_CART_PROPERTIES"] = array();
}
$arParams['PARTIAL_PRODUCT_PROPERTIES'] = (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) && $arParams['PARTIAL_PRODUCT_PROPERTIES'] === 'Y' ? 'Y' : 'N');
if(!isset($arParams["PRODUCT_PROPERTIES"]) || !is_array($arParams["PRODUCT_PROPERTIES"]))
	$arParams["PRODUCT_PROPERTIES"] = array();
foreach($arParams["PRODUCT_PROPERTIES"] as $k=>$v)
	if($v==="")
		unset($arParams["PRODUCT_PROPERTIES"][$k]);

if (!isset($arParams["OFFERS_CART_PROPERTIES"]) || !is_array($arParams["OFFERS_CART_PROPERTIES"]))
	$arParams["OFFERS_CART_PROPERTIES"] = array();
foreach($arParams["OFFERS_CART_PROPERTIES"] as $i => $pid)
	if ($pid === "")
		unset($arParams["OFFERS_CART_PROPERTIES"][$i]);

if (empty($arParams["OFFERS_SORT_FIELD"]))
	$arParams["OFFERS_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER"]))
	$arParams["OFFERS_SORT_ORDER"] = "asc";
if (empty($arParams["OFFERS_SORT_FIELD2"]))
	$arParams["OFFERS_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER2"]))
	$arParams["OFFERS_SORT_ORDER2"] = "desc";

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

$arParams["OFFERS_LIMIT"] = 0;

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

	if(!isset($arParams["OFFERS_FIELD_CODE"]))
		$arParams["OFFERS_FIELD_CODE"] = array();
	elseif (!is_array($arParams["OFFERS_FIELD_CODE"]))
		$arParams["OFFERS_FIELD_CODE"] = array($arParams["OFFERS_FIELD_CODE"]);
	foreach($arParams["OFFERS_FIELD_CODE"] as $key => $value)
		if($value === "")
			unset($arParams["OFFERS_FIELD_CODE"][$key]);

	if(!isset($arParams["OFFERS_PROPERTY_CODE"]))
		$arParams["OFFERS_PROPERTY_CODE"] = array();
	elseif (!is_array($arParams["OFFERS_PROPERTY_CODE"]))
		$arParams["OFFERS_PROPERTY_CODE"] = array($arParams["OFFERS_PROPERTY_CODE"]);
	foreach($arParams["OFFERS_PROPERTY_CODE"] as $key => $value)
		if($value === "")
			unset($arParams["OFFERS_PROPERTY_CODE"][$key]);
		
/*************************************************************************
			Processing of the Buy link
*************************************************************************/
$strError = '';
$successfulAdd = true;

if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && isset($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]))
{
	if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."BUY"]))
		$action = "BUY";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2BASKET"]))
		$action = "ADD2BASKET";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = intval($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]);
	if (($action == "ADD2BASKET" || $action == "BUY") && $productID > 0)
	{
		if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
		{
			$addByAjax = isset($_REQUEST['ajax_basket']) && 'Y' == $_REQUEST['ajax_basket'];
			$QUANTITY = 0;
			$product_properties = array();
			$intProductIBlockID = intval(CIBlockElement::GetIBlockByID($productID));
			if (0 < $intProductIBlockID)
			{
				if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y')
				{
					if ($intProductIBlockID == $arParams["IBLOCK_ID"])
					{
						if (!empty($arParams["PRODUCT_PROPERTIES"]))
						{
							if (
								isset($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
								&& is_array($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
							)
							{
								$product_properties = CIBlockPriceTools::CheckProductProperties(
									$arParams["IBLOCK_ID"],
									$productID,
									$arParams["PRODUCT_PROPERTIES"],
									$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
									$arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
								);
								if (!is_array($product_properties))
								{
									$strError = GetMessage("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR");
									$successfulAdd = false;
								}
							}
							else
							{
								$strError = GetMessage("CATALOG_EMPTY_BASKET_PROPERTIES_ERROR");
								$successfulAdd = false;
							}
						}
					}
					else
					{
						$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
						if (!empty($arParams["OFFERS_CART_PROPERTIES"]) || !empty($skuAddProps))
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								$arParams["IBLOCK_ID"],
								$arParams["OFFERS_CART_PROPERTIES"],
								$skuAddProps
							);
						}
					}
				}
				$QUANTITY = 1;
			}
			else
			{
				$strError = GetMessage('CATALOG_PRODUCT_NOT_FOUND');
				$successfulAdd = false;
			}

			if ($successfulAdd)
			{
				//Выберем торговое предложение (у него нужно XML_ID и свойство CML2_LINK - ID товара)
				$arFilter = array(
					"IBLOCK_ID" => $intProductIBlockID,
					"ID" => $productID,
				);
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"NAME",
					"XML_ID",
					"PROPERTY_CML2_LINK",
				);
				$rsOffers = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				if ($arOffer = $rsOffers->GetNext())
				{
					$product_properties[] = array(
						"NAME" => "aID",
						"CODE" => "aID",
						"VALUE" => $arOffer["XML_ID"],
						"SORT" => 200,
					);					
					$main_item_id = intval($arOffer["PROPERTY_CML2_LINK_VALUE"]);
					if ($main_item_id > 0)
					{
						$arFilter = array(
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ID" => $main_item_id,
						);
						$arSelect = array(
							"ID",
							"IBLOCK_ID",
							"NAME",
							"XML_ID",
							"IBLOCK_SECTION_ID",
							"PROPERTY_pict1"
						);
						$rsItems = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
						if ($arItem = $rsItems->GetNext())
						{
							if ($arItem["XML_ID"] != "")
							{
								$product_properties[] = array(
									"NAME" => "prodID",
									"CODE" => "prodID",
									"VALUE" => $arItem["XML_ID"],
									"SORT" => 200,
								);
								$product_properties[] = array(
									"NAME" => "Ссылка на stripmag",
									"CODE" => "stripmag",
									"VALUE" => "http://stripmag.ru/prod.php?id=".$arItem["XML_ID"],
									"SORT" => 200,
								);										
							}
							if ($arItem["PROPERTY_PICT1_VALUE"] != "")
							{
								$product_properties[] = array(
									"NAME" => "Картинка",
									"CODE" => "PICTURE",
									"VALUE" => $arItem["PROPERTY_PICT1_VALUE"],
									"SORT" => 200,
								);								
							}
						}
					}					
				}
				if(!Add2BasketByProductID($productID, $QUANTITY, $arRewriteFields, $product_properties))
				{
					if ($ex = $APPLICATION->GetException())
						$strError = $ex->GetString();
					else
						$strError = GetMessage("CATALOG_ERROR2BASKET");
					$successfulAdd = false;
				}
			}

			if ($addByAjax)
			{
				if ($successfulAdd)
				{
					$metrika_product = array(
					);					
					if (isset($arItem) && is_array($arItem) && $arPrice = CCatalogProduct::GetOptimalPrice($productID, 1, $USER->GetUserGroupArray(), "N"))
					{
						$metrika_product["id"] = $arItem["XML_ID"];
						$metrika_product["name"] = $arItem["NAME"];
						$metrika_product["price"] = $arPrice["DISCOUNT_PRICE"];

						$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"vendor"));
						if($arVendor = $db_props->Fetch())							
						{
							$vendor_xml_id = trim($arVendor["VALUE"]);
							if ($vendor_xml_id != "")
							{
								$arVendorFilter = array(
									"IBLOCK_ID" => $arParams["VENDORS_IBLOCK_ID"],
									"XML_ID" => $vendor_xml_id,
									"ACTIVE" => "Y"
								);
								$rsVendor = CIBlockElement::GetList(array(), $arVendorFilter, false, array("nTopCount" => 1));
								if ($arVendor = $rsVendor->GetNext())
								{
									$metrika_product["brand"] = $arVendor["NAME"];
								}
							}
						}
						if ($arItem["IBLOCK_SECTION_ID"] > 0)
						{
							$section_path = "";
							$rsPath = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
							$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
							$key = 0;
							while($arPath = $rsPath->GetNext())
							{
								$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
								$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
								
								if ($key > 0)
									$section_path .= "/";
								if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
									$section_path .= $arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"];
								else
									$section_path .= $arPath["NAME"];
								$key++;
							}
							if ($section_path != "")
								$metrika_product["category"] = $section_path;
						}
						if (is_array($product_properties))
						{
							$color = "";
							$size = "";
							foreach ($product_properties as $arProp)
							{
								if ($arProp["CODE"] == "color" && $arProp["VALUE"] != "")
									$color = $arProp["VALUE"];
								else
								if ($arProp["CODE"] == "size" && $arProp["VALUE"] != "")
									$size = $arProp["VALUE"];										
							}
							if ($size != "" || $color != "")
							{
								$metrika_product["variant"] = "";
								if ($size != "")
									$metrika_product["variant"] = "Размер ".$size;
								if ($color != "")
								{
									if ($metrika_product["variant"] != "")
										$metrika_product["variant"] .= ", ";
									$metrika_product["variant"] .= "Цвет ".$color;
								}
							}								
						}
					}
					$addResult = array('STATUS' => 'OK', 'MESSAGE' => GetMessage('CATALOG_SUCCESSFUL_ADD_TO_BASKET'), 'ID' => $productID, 'METRIKA_PRODUCT' => $metrika_product);
				}
				else
				{
					$addResult = array('STATUS' => 'ERROR', 'MESSAGE' => $strError);
				}
				$APPLICATION->RestartBuffer();
				echo CUtil::PhpToJSObject($addResult);
				die();
			}
			else
			{
				if ($successfulAdd)
				{
					$pathRedirect = (
					$action == "BUY"
						? $arParams["BASKET_URL"]
						: $APPLICATION->GetCurPageParam("", array(
							$arParams["PRODUCT_ID_VARIABLE"],
							$arParams["ACTION_VARIABLE"],
							$arParams['PRODUCT_PROPS_VARIABLE']
						))
					);
					LocalRedirect($pathRedirect);
				}
			}
		}
	}
}
if (!$successfulAdd)
{
	ShowError($strError); 
	return;
}

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if (!Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arResultModules = array(
		'iblock' => true,
		'catalog' => false,
		'currency' => false
	);
	$arrFilter=array();
	
	global $CACHE_MANAGER;
	$arConvertParams = array();
	$arResult['CONVERT_CURRENCY'] = $arConvertParams;

	$bOffersIBlockExist = false;
	$bIBlockCatalog = false;
	$arCatalog = false;
	$boolNeedCatalogCache = false;
	$bCatalog = Loader::includeModule('catalog');
	if ($bCatalog)
	{
		$arResultModules['catalog'] = true;
		$arResultModules['currency'] = true;
		$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
		if (!empty($arCatalog) && is_array($arCatalog))
		{
			$bOffersIBlockExist = (
				$arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT
				|| $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL
			);			
			$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
			$boolNeedCatalogCache = true;
		}
	}
	$arResult['CATALOG'] = $arCatalog;
	//This function returns array with prices description and access rights
	//in case catalog module n/a prices get values from element properties
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult['PRICES_ALLOW']))
	{
		$boolNeedCatalogCache = CIBlockPriceTools::SetCatalogDiscountCache($arResult['PRICES_ALLOW'], $USER->GetUserGroupArray());
	}
	$arSectionFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => 0,
		"ACTIVE" => "Y",
		"IBLOCK_ACTIVE" => "Y",
	);
	$arSectionSelect = array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"PICTURE",
		"SECTION_PAGE_URL",
		"UF_BOLD_NAME",
	);
	$arSectionSort = array(
		"SORT" => "ASC",
		"ID" => "DESC",
	);
	$rsSections = CIBlockSection::GetList($arSectionSort, $arSectionFilter, false, $arSectionSelect);
	$arResult["SECTIONS"] = array();
	$arResult["ITEMS"] = array();	
	$arResult["ALL_ELEMENTS"] = array();	
	while ($arSection = $rsSections->GetNext())
	{
		$arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);
		$arResult["SECTIONS"][] = $arSection;
	/************************************
			Elements
	************************************/
	//SELECT
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"XML_ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE",
	);
	//WHERE
	$arrFilter["ACTIVE"] = "Y";
	if($arParams["IBLOCK_ID"] > 0)
		$arrFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
	$arrFilter["IBLOCK_LID"] = SITE_ID;
	$arrFilter["IBLOCK_ACTIVE"] = "Y";
	$arrFilter["ACTIVE_DATE"] = "Y";
	$arrFilter["ACTIVE"] = "Y";
	$arrFilter["CHECK_PERMISSIONS"] = "Y";
	//if ($bIBlockCatalog && 'Y' == $arParams['HIDE_NOT_AVAILABLE'])
		//$arrFilter['CATALOG_AVAILABLE'] = 'Y';
	//$arrFilter["ID"] = 42922;
	$arrFilter["SECTION_ID"] = $arSection["ID"];
	$arrFilter["INCLUDE_SUBSECTIONS"] = "Y";
	
	if($bCatalog && $bOffersIBlockExist)
	{
	// чтобы в выборку попали только те товары, у которых есть торговые предолжения
		$arSubFilter = array();
		$arSubFilter['CATALOG_AVAILABLE'] = 'Y'; // а это условие обеспечит выборку только тех предложений, которые можно купить
		$arSubFilter["IBLOCK_ID"] = $arResult['CATALOG']['IBLOCK_ID'];
		$arSubFilter["ACTIVE_DATE"] = "Y";
		$arSubFilter["ACTIVE"] = "Y";
		$arrFilter["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$arResult['CATALOG']["SKU_PROPERTY_ID"], $arSubFilter);
	}
/*
	?>pre><?
	print_r($arrFilter);
	?></pre><?
*/	
	//ORDER BY
	$arSort = array(
		$arParams["ELEMENT_SORT_FIELD"] => $arParams["ELEMENT_SORT_ORDER"],
		$arParams["ELEMENT_SORT_FIELD2"] => $arParams["ELEMENT_SORT_ORDER2"],
	);
	//PRICES
	$arPriceTypeID = array();
		foreach($arResult["PRICES"] as &$value)
		{
			if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
				continue;
			$arSelect[] = $value["SELECT"];
			$arrFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
		}
		if (isset($value))
			unset($value);

	$currencyList = array();

	$bGetPropertyCodes = !empty($arParams["PROPERTY_CODE"]);
	$bGetProductProperties = !empty($arParams["PRODUCT_PROPERTIES"]);
	$bGetProperties = $bGetPropertyCodes || $bGetProductProperties;

	$intKey = 0;
	$arElementLink = array();
	$rsElements = CIBlockElement::GetList($arSort, $arrFilter, false, array("nTopCount" => $arParams["ELEMENT_COUNT"]), $arSelect);
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
	$arResult["ITEMS"][$arSection["ID"]] = array();
	$arResult["ELEMENTS"] = array();
	while($arItem = $rsElements->GetNext())
	{
		$arItem['ID'] = intval($arItem['ID']);

		$arItem['ACTIVE_FROM'] = $arItem['DATE_ACTIVE_FROM'];
		$arItem['ACTIVE_TO'] = $arItem['DATE_ACTIVE_TO'];

		$arButtons = CIBlock::GetPanelButtons(
			$arItem["IBLOCK_ID"],
			$arItem["ID"],
			$arItem["IBLOCK_SECTION_ID"],
			array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
		);
		$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
		$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

		$arItem["PROPERTIES"] = array();
		$arItem["DISPLAY_PROPERTIES"] = array();
		$arItem["PRODUCT_PROPERTIES"] = array();
		$arItem['PRODUCT_PROPERTIES_FILL'] = array();

		$arResult["ITEMS"][$arSection["ID"]][$intKey] = $arItem;
		$arResult["ELEMENTS"][$intKey] = $arItem["ID"];
		$arElementLink[$arItem['ID']] = &$arResult["ITEMS"][$arSection["ID"]][$intKey];
		$intKey++;
	}
	$arResult['MODULES'] = $arResultModules;

	//if (!empty($arResult["ELEMENTS"]) && ($bGetProperties || ($bCatalog && $boolNeedCatalogCache)))
	if (!empty($arResult["ELEMENTS"]))
	{
		$arPropFilter = array(
			'ID' => $arResult["ELEMENTS"],
			'IBLOCK_ID' => $arParams['IBLOCK_ID']
		);
		CIBlockElement::GetPropertyValuesArray($arElementLink, $arParams["IBLOCK_ID"], $arPropFilter);

		foreach ($arResult["ITEMS"][$arSection["ID"]] as &$arItem)
		{
//echo $arElementLink[$arItem["ID"]]["PROPERTIES"]["pict1"]["VALUE"]."<br>";
			if ($bCatalog && $boolNeedCatalogCache)
			{
				CCatalogDiscount::SetProductPropertiesCache($arItem['ID'], $arItem["PROPERTIES"]);
			}
			
			$arItem["PREVIEW_PICTURE"] = array(
				"SRC" => "",
			);

			if ($arItem["PROPERTIES"]["pict1"]["VALUE"] != "")
			{
//				echo $arItem["PROPERTIES"]["pict1"]["VALUE"]."<br>";									
				$filename =  basename($arItem["PROPERTIES"]["pict1"]["VALUE"]);
				$exist = false;
				$original_pic_path = "/images/".$filename;
				if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
					$exist = true;
				else
				{
					$content = file_get_contents($arItem["PROPERTIES"]["pict1"]["VALUE"]);
					if ($content !== false)
					{
						file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
							$exist = true;
					}
				}
				if ($exist == true)
				{
					$resized_pic_path = "/resized_images/210x160/".$filename;
					if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
						$arItem["PREVIEW_PICTURE"]["SRC"] = $resized_pic_path;
					else
					{
						$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
						if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>210,"height"=>160), BX_RESIZE_IMAGE_PROPORTIONAL))
							$arItem["PREVIEW_PICTURE"]["SRC"] = $resized_pic_path;
					}
				}
				
			}
		
		
			if ($bGetProperties)
			{
				foreach($arParams["PROPERTY_CODE"] as $pid)
				{
					if (!isset($arItem["PROPERTIES"][$pid]))
						continue;
					$prop = &$arItem["PROPERTIES"][$pid];
					$boolArr = is_array($prop["VALUE"]);
					if(
						($boolArr && !empty($prop["VALUE"]))
						|| (!$boolArr && strlen($prop["VALUE"]) > 0)
					)
					{
						$arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "catalog_out");
					}
				}

				if ($bGetProductProperties)
				{
					$arItem["PRODUCT_PROPERTIES"] = CIBlockPriceTools::GetProductProperties(
						$arParams["IBLOCK_ID"],
						$arItem["ID"],
						$arParams["PRODUCT_PROPERTIES"],
						$arItem["PROPERTIES"]
					);
					if (!empty($arItem["PRODUCT_PROPERTIES"]))
					{
						$arItem['PRODUCT_PROPERTIES_FILL'] = CIBlockPriceTools::getFillProductProperties($arItem['PRODUCT_PROPERTIES']);
					}
				}
			}
		}
		if (isset($arItem))
			unset($arItem);
	}

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult["ELEMENTS"]))
	{
		CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
		CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'GET_BY_ID' => 'Y'));
	}
	if (isset($arItem))
		unset($arItem);

	$currentPath = CHTTP::urlDeleteParams(
		$APPLICATION->GetCurPageParam(),
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
	);
	$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');

	$arResult['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~BUY_URL_TEMPLATE']);
	$arResult['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~ADD_URL_TEMPLATE']);
	unset($comparePath, $currentPath);

	foreach ($arResult["ITEMS"][$arSection["ID"]] as &$arItem)
	{
		$arItem["PRICES"] = array();
		$arItem["PRICE_MATRIX"] = false;
		$arItem['MIN_PRICE'] = false;

			$arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);

			if (!empty($arItem['PRICES']))
			{
				$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arItem['PRICES']);
			}

		$arItem["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem);

		$arItem['~BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~BUY_URL_TEMPLATE']);
		$arItem['BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['BUY_URL_TEMPLATE']);
		$arItem['~ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~ADD_URL_TEMPLATE']);
		$arItem['ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['ADD_URL_TEMPLATE']);
	}
	if (isset($arItem))
		unset($arItem);
	
	if(
		$bCatalog
		&& !empty($arResult["ELEMENTS"])
		&& (
			!empty($arParams["OFFERS_FIELD_CODE"])
			|| !empty($arParams["OFFERS_PROPERTY_CODE"])
		)
	)
	{
		$offersFilter = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			//'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE']
			'HIDE_NOT_AVAILABLE' => "Y"
		);
		if (!$arParams["USE_PRICE_COUNT"])
		{
			$offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];
		}
		$arOffers = CIBlockPriceTools::GetOffersArray(
			$offersFilter,
			$arResult["ELEMENTS"],
			array(
				$arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
				$arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
			),
			$arParams["OFFERS_FIELD_CODE"],
			$arParams["OFFERS_PROPERTY_CODE"],
			$arParams["OFFERS_LIMIT"],
			$arResult["PRICES"],
			$arParams['PRICE_VAT_INCLUDE'],
			$arConvertParams
		);
		if(!empty($arOffers))
		{
			foreach ($arResult["ELEMENTS"] as $id)
				$arElementLink[$id]['OFFERS'] = array();
			unset($id);

			foreach($arOffers as $arOffer)
			{
				if (isset($arElementLink[$arOffer["LINK_ELEMENT_ID"]]))
				{
					$arOffer['~BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~BUY_URL_TEMPLATE']);
					$arOffer['BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['BUY_URL_TEMPLATE']);
					$arOffer['~ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~ADD_URL_TEMPLATE']);
					$arOffer['ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['ADD_URL_TEMPLATE']);

					$arElementLink[$arOffer["LINK_ELEMENT_ID"]]['OFFERS'][] = $arOffer;
/*					
					?><pre><?
					print_r($arOffer);
					?></pre><?
*/					
				}
			}
			unset($arOffer);
		}
		unset($arOffers);
	}
	
	unset($currencyList);
	}
	
	$this->SetResultCacheKeys(array(
	));
	$this->IncludeComponentTemplate();

	if ($bCatalog && $boolNeedCatalogCache)
	{
		CCatalogDiscount::ClearDiscountCache(array(
			'PRODUCT' => true,
			'SECTIONS' => true,
			'PROPERTIES' => true
		));
	}
}
?>