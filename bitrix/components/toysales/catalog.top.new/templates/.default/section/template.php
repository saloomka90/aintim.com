<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $arSkuTemplate */
/** @var array $templateData */
global $APPLICATION;
?>
<?
foreach ($arResult["SECTIONS"] as $arSection)
{
?>
                <div class="products-slider-banner">

                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" style="<?if ($arSection["PICTURE"]["SRC"] != "") echo 'background-image: url('.$arSection["PICTURE"]["SRC"].');'?> background-color: #235290;" class="promo-banner-box">
                                <span class="promo-text">
                                    <span class="title"><?=$arSection["NAME"]?></span>
									<?
									if ($arSection["UF_BOLD_NAME"] != "")
									{
										?>
										<span class="description"><?=$arSection["UF_BOLD_NAME"]?></span>
										<?
									}
									?>
                                    <span class="detail-link">перейти в раздел</span>
                                </span>
                            </a>
<?
	if (!empty($arResult["ITEMS"][$arSection["ID"]]))
	{
	?>
                            <div class="products-swiper">
                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="5">
                                    <div class="swiper-wrapper">
	<?
		foreach ($arResult["ITEMS"][$arSection["ID"]] as $arItem)
		{
			$minPrice = false;
			if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
				$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);				
			if (!empty($arItem["OFFERS"]))
			{
		?>
                                        <div class="swiper-slide"> 
                                            <div class="paddings-container">
                                                <div class="product-slide-entry">
                                                    <div class="product-image">
													<?
													if ($arItem["PREVIEW_PICTURE"]["SRC"] != "")
													{
													?>
                                                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""  /></a>
													<?
													}
													?>
                                                        <a class="top-line-a right open-product" rel="<?=$arItem["ID"]?>"><i class="fa fa-expand"></i> <span>Увеличить</span></a>
                                                        <div class="bottom-line">
														<?
														if (count($arItem["OFFERS"]) == 1)
														{
															$arOffer = current($arItem["OFFERS"]);
														?>
                                                                <a class="bottom-line-a buy_item" href="<?=$arOffer["~ADD_URL"]?>" rel="<?=$arOffer["ID"]?>"><i class="fa fa-shopping-cart"></i> Добавить в корзину</a>
																<a style="display:none" class="bottom-line-a" href="<?=$arParams["BASKET_URL"]?>"><i class="fa fa-shopping-cart"></i> Товар в корзине</a>
														<?
														}
														else
														{
														?>
                                                                <a class="bottom-line-a open-product" rel="<?=$arItem["ID"]?>"><i class="fa fa-shopping-cart"></i> Добавить в корзину</a>														
														<?
														}
														?>
                                                        </div>
                                                    </div>
                                                    <a class="tag" href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
                                                    <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                                    <div class="rating-box">
                                                        <div class="star"><i class="fa fa-star"></i></div>
                                                        <div class="star"><i class="fa fa-star"></i></div>
                                                        <div class="star"><i class="fa fa-star"></i></div>
                                                        <div class="star"><i class="fa fa-star"></i></div>
                                                        <div class="star"><i class="fa fa-star"></i></div>
                                                    </div>
                                                    

                                                    <div class="price">
<?													
	if (!empty($minPrice))
	{
		if ($minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
		{
		?>
		                                <div class="prev"><? echo $minPrice['PRINT_VALUE']; ?></div>
		<?
		}
	?>
                                        <div class="current"><?=$minPrice['PRINT_DISCOUNT_VALUE'];?></div>
	<?
	}													
?>	
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<?
			}
		}
		?>
                                    </div>
                                    <div class="pagination"></div>
                                </div>
                            </div>
		<?
	}
?>                  
                </div>
<?
}
?>