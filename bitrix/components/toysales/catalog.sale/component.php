<?
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = (int)$arParams["IBLOCK_ID"];
$arParams["VENDORS_IBLOCK_ID"] = (int)$arParams["VENDORS_IBLOCK_ID"];

$arParams["SECTION_ID"] = (int)$arParams["SECTION_ID"];

if (empty($arParams["PAGER_PARAMS_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"]))
{
	$pagerParameters = array();
}
else
{
	$pagerParameters = $GLOBALS[$arParams["PAGER_PARAMS_NAME"]];
	if (!is_array($pagerParameters))
		$pagerParameters = array();
}

unset($arParams["ELEMENT_SORT_FIELD"], $arParams["ELEMENT_SORT_ORDER"]);

if ($_REQUEST["sort"] != "")
{
	if ($_REQUEST["sort"] == "base")
		unset($_SESSION["toysales"]["sort"]);
	else
		$_SESSION["toysales"]["sort"] = $_REQUEST["sort"];
}
else
if ($_SESSION["toysales"]["sort"] != "")
{
	$_REQUEST["sort"] = $_SESSION["toysales"]["sort"];
}

if ($_REQUEST["sort"] == "bestseller")
{
	$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_bestseller";
	$arParams["ELEMENT_SORT_ORDER"] = "DESC";
}
else
if ($_REQUEST["sort"] == "new")
{
	$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_new";
	$arParams["ELEMENT_SORT_ORDER"] = "DESC";
}
else
if ($_REQUEST["sort"] == "price_asc" )
{
	$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_price";
	$arParams["ELEMENT_SORT_ORDER"] = "ASC";
}
else
if ($_REQUEST["sort"] == "price_desc" )
{
	$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_price";
	$arParams["ELEMENT_SORT_ORDER"] = "DESC";
}

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);
$arParams["BASKET_URL"]=trim($arParams["BASKET_URL"]);
if($arParams["BASKET_URL"] === '')
	$arParams["BASKET_URL"] = "/personal/basket.php";

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if($arParams["ACTION_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if($arParams["PRODUCT_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if($arParams["PRODUCT_PROPS_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

$arParams["SECTION_ID_VARIABLE"]=trim($arParams["SECTION_ID_VARIABLE"]);
if($arParams["SECTION_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_ID_VARIABLE"]))
	$arParams["SECTION_ID_VARIABLE"] = "SECTION_ID";

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

$arParams["OFFERS_LIMIT"] = 0;

if ($_REQUEST["count"] != "")
{
	$_SESSION["toysales"]["count"] = $_REQUEST["count"];
	$arParams["PAGE_ELEMENT_COUNT"] = $_REQUEST["count"];
}
else
if ($_SESSION["toysales"]["count"] != "")
{
	$arParams["PAGE_ELEMENT_COUNT"] = $_SESSION["toysales"]["count"];
}

$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
if($arParams["PAGE_ELEMENT_COUNT"]<=0)
	$arParams["PAGE_ELEMENT_COUNT"]=12;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]=="Y";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]=="Y";

$arNavParams = array(
	"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
);
$arNavigation = CDBResult::GetNavParams($arNavParams);

$strError = '';
$successfulAdd = true;

if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && isset($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]))
{
	if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."BUY"]))
		$action = "BUY";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2BASKET"]))
		$action = "ADD2BASKET";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = intval($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]);
	if (($action == "ADD2BASKET" || $action == "BUY") && $productID > 0)
	{
		if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
		{
			$addByAjax = isset($_REQUEST['ajax_basket']) && 'Y' == $_REQUEST['ajax_basket'];
			$QUANTITY = 0;
			$product_properties = array();
			$intProductIBlockID = intval(CIBlockElement::GetIBlockByID($productID));
			if (0 < $intProductIBlockID)
			{
				if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y')
				{
					if ($intProductIBlockID == $arParams["IBLOCK_ID"])
					{
						if (!empty($arParams["PRODUCT_PROPERTIES"]))
						{
							if (
								isset($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
								&& is_array($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
							)
							{
								$product_properties = CIBlockPriceTools::CheckProductProperties(
									$arParams["IBLOCK_ID"],
									$productID,
									$arParams["PRODUCT_PROPERTIES"],
									$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
									$arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
								);
								if (!is_array($product_properties))
								{
									$strError = GetMessage("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR");
									$successfulAdd = false;
								}
							}
							else
							{
								$strError = GetMessage("CATALOG_EMPTY_BASKET_PROPERTIES_ERROR");
								$successfulAdd = false;
							}
						}
					}
					else
					{
						$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
						if (!empty($arParams["OFFERS_CART_PROPERTIES"]) || !empty($skuAddProps))
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								$arParams["IBLOCK_ID"],
								$arParams["OFFERS_CART_PROPERTIES"],
								$skuAddProps
							);
						}
					}
				}
				$QUANTITY = 1;
			}
			else
			{
				$strError = GetMessage('CATALOG_PRODUCT_NOT_FOUND');
				$successfulAdd = false;
			}

			if ($successfulAdd)
			{
				//Выберем торговое предложение (у него нужно XML_ID и свойство CML2_LINK - ID товара)
				$arFilter = array(
					"IBLOCK_ID" => $intProductIBlockID,
					"ID" => $productID,
				);
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"NAME",
					"XML_ID",
					"PROPERTY_CML2_LINK",
				);
				$rsOffers = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				if ($arOffer = $rsOffers->GetNext())
				{
					$product_properties[] = array(
						"NAME" => "aID",
						"CODE" => "aID",
						"VALUE" => $arOffer["XML_ID"],
						"SORT" => 200,
					);					
					$main_item_id = intval($arOffer["PROPERTY_CML2_LINK_VALUE"]);
					if ($main_item_id > 0)
					{
						$arFilter = array(
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ID" => $main_item_id,
						);
						$arSelect = array(
							"ID",
							"IBLOCK_ID",
							"NAME",
							"XML_ID",
							"IBLOCK_SECTION_ID",
							"PROPERTY_pict1"
						);
						$rsItems = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
						if ($arItem = $rsItems->GetNext())
						{
							if ($arItem["XML_ID"] != "")
							{
								$product_properties[] = array(
									"NAME" => "prodID",
									"CODE" => "prodID",
									"VALUE" => $arItem["XML_ID"],
									"SORT" => 200,
								);
								$product_properties[] = array(
									"NAME" => "Ссылка на stripmag",
									"CODE" => "stripmag",
									"VALUE" => "http://stripmag.ru/prod.php?id=".$arItem["XML_ID"],
									"SORT" => 200,
								);										
							}
							if ($arItem["PROPERTY_PICT1_VALUE"] != "")
							{
								$product_properties[] = array(
									"NAME" => "Картинка",
									"CODE" => "PICTURE",
									"VALUE" => $arItem["PROPERTY_PICT1_VALUE"],
									"SORT" => 200,
								);								
							}
						}
					}					
				}
				if(!Add2BasketByProductID($productID, $QUANTITY, $arRewriteFields, $product_properties))
				{
					if ($ex = $APPLICATION->GetException())
						$strError = $ex->GetString();
					else
						$strError = GetMessage("CATALOG_ERROR2BASKET");
					$successfulAdd = false;
				}
			}

			if ($addByAjax)
			{
				if ($successfulAdd)
				{
					$metrika_product = array(
					);					
					if (isset($arItem) && is_array($arItem) && $arPrice = CCatalogProduct::GetOptimalPrice($productID, 1, $USER->GetUserGroupArray(), "N"))
					{
						$metrika_product["id"] = $arItem["XML_ID"];
						$metrika_product["name"] = $arItem["NAME"];
						$metrika_product["price"] = $arPrice["DISCOUNT_PRICE"];

						$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"vendor"));
						if($arVendor = $db_props->Fetch())							
						{
							$vendor_xml_id = trim($arVendor["VALUE"]);
							if ($vendor_xml_id != "")
							{
								$arVendorFilter = array(
									"IBLOCK_ID" => $arParams["VENDORS_IBLOCK_ID"],
									"XML_ID" => $vendor_xml_id,
									"ACTIVE" => "Y"
								);
								$rsVendor = CIBlockElement::GetList(array(), $arVendorFilter, false, array("nTopCount" => 1));
								if ($arVendor = $rsVendor->GetNext())
								{
									$metrika_product["brand"] = $arVendor["NAME"];
								}
							}
						}
						if ($arItem["IBLOCK_SECTION_ID"] > 0)
						{
							$section_path = "";
							$rsPath = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
							$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
							$key = 0;
							while($arPath = $rsPath->GetNext())
							{
								$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
								$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
								
								if ($key > 0)
									$section_path .= "/";
								if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
									$section_path .= $arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"];
								else
									$section_path .= $arPath["NAME"];
								$key++;
							}
							if ($section_path != "")
								$metrika_product["category"] = $section_path;
						}
						if (is_array($product_properties))
						{
							$color = "";
							$size = "";
							foreach ($product_properties as $arProp)
							{
								if ($arProp["CODE"] == "color" && $arProp["VALUE"] != "")
									$color = $arProp["VALUE"];
								else
								if ($arProp["CODE"] == "size" && $arProp["VALUE"] != "")
									$size = $arProp["VALUE"];										
							}
							if ($size != "" || $color != "")
							{
								$metrika_product["variant"] = "";
								if ($size != "")
									$metrika_product["variant"] = "Размер ".$size;
								if ($color != "")
								{
									if ($metrika_product["variant"] != "")
										$metrika_product["variant"] .= ", ";
									$metrika_product["variant"] .= "Цвет ".$color;
								}
							}								
						}
					}
					$addResult = array('STATUS' => 'OK', 'MESSAGE' => GetMessage('CATALOG_SUCCESSFUL_ADD_TO_BASKET'), 'ID' => $productID, 'METRIKA_PRODUCT' => $metrika_product);					
				}
				else
				{
					$addResult = array('STATUS' => 'ERROR', 'MESSAGE' => $strError);
				}
				$APPLICATION->RestartBuffer();
				echo CUtil::PhpToJSObject($addResult);
				die();
			}
			else
			{
				if ($successfulAdd)
				{
					$pathRedirect = (
					$action == "BUY"
						? $arParams["BASKET_URL"]
						: $APPLICATION->GetCurPageParam("", array(
							$arParams["PRODUCT_ID_VARIABLE"],
							$arParams["ACTION_VARIABLE"],
							$arParams['PRODUCT_PROPS_VARIABLE']
						))
					);
					LocalRedirect($pathRedirect);
				}
			}
		}
	}
}

if (!$successfulAdd)
{
	ShowError($strError);
	return;
}

/*************************************************************************
			Work with cache
*************************************************************************/

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation, $pagerParameters)))
{
	if(!Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arResultModules = array(
		'iblock' => true,
		'catalog' => false,
		'currency' => false
	);

	$arConvertParams = array();


	$bSectionFound = false;
	$arFilter = array(
		"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
		"IBLOCK_ACTIVE"=>"Y",
		"ACTIVE"=>"Y",
		"GLOBAL_ACTIVE"=>"Y",
	);
	if($arParams["SECTION_ID"] > 0)
	{
		$arFilter["ID"]=$arParams["SECTION_ID"];
		$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult = $rsSection->GetNext();
		if($arResult)
			$bSectionFound = true;
	}
	else
	{
		//Root section (no section filter)
		$arResult = array(
			"ID" => 0,
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		);
		$bSectionFound = true;
	}

		
	if(!$bSectionFound)
	{
		$this->AbortResultCache();
		\Bitrix\Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_SECTION_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
		return;
	}
	
	$bIBlockCatalog = false;
	$bOffersIBlockExist = false;
	$arCatalog = false;
	$boolNeedCatalogCache = false;
	$bCatalog = Loader::includeModule('catalog');
	$useCatalogButtons = array();
	if ($bCatalog)
	{
		$arResultModules['catalog'] = true;
		$arResultModules['currency'] = true;
		$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
		if (!empty($arCatalog) && is_array($arCatalog))
		{
			$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
			$bOffersIBlockExist = (
				$arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT
				|| $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL
			);
			$boolNeedCatalogCache = true;
			if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_CATALOG || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
				$useCatalogButtons['add_product'] = true;
			if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
				$useCatalogButtons['add_sku'] = true;
		}
	}
	$arResult['CATALOG'] = $arCatalog;
	$arResult['USE_CATALOG_BUTTONS'] = $useCatalogButtons;
	unset($useCatalogButtons);
	//This function returns array with prices description and access rights
	//in case catalog module n/a prices get values from element properties
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult['PRICES_ALLOW']))
	{
		$boolNeedCatalogCache = CIBlockPriceTools::SetCatalogDiscountCache($arResult['PRICES_ALLOW'], $USER->GetUserGroupArray());
	}

	$arResult['CONVERT_CURRENCY'] = $arConvertParams;

	// list of the element fields that will be used in selection
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"XML_ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
	);
	if ($bIBlockCatalog)
		$arSelect[] = "CATALOG_QUANTITY";
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"MIN_PERMISSION" => "R",
		"!PROPERTY_sale" => false,
	);
	if ($arResult["ID"] > 0)
	{
		$arFilter["SECTION_ID"] = $arResult["ID"];
		$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
	}
	//$arFilter["SECTION_GLOBAL_ACTIVE"] = "Y";


	if($bCatalog && $bOffersIBlockExist)
	{
		// чтобы в выборку попали только те товары, у которых есть торговые предолжения

			$arSubFilter = array();
			$arSubFilter['CATALOG_AVAILABLE'] = 'Y'; // а это условие обеспечит выборку только тех предложений, которые можно купить
			$arSubFilter["IBLOCK_ID"] = $arResult['CATALOG']['IBLOCK_ID'];
			$arSubFilter["ACTIVE_DATE"] = "Y";
			$arSubFilter["ACTIVE"] = "Y";
			$arFilter["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$arResult['CATALOG']["SKU_PROPERTY_ID"], $arSubFilter);

	}
/*	
	?><pre><?
	print_r($arFilter);
	?></pre><?
*/
	//PRICES
	$i = 0;
	$price_id = 0;
	foreach($arResult["PRICES"] as &$value)
	{
		if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
			continue;
		$arSelect[] = $value["SELECT"];
		$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
		if ($i == 0)
			$price_id = $value["ID"];
		$i++;
	}
	if (isset($value))
		unset($value);


	if (!isset($arParams["ELEMENT_SORT_FIELD"]) or empty($arParams["ELEMENT_SORT_FIELD"]))
		$arSort = array(
			"PROPERTY_new" => "DESC",
			"PROPERTY_bestseller" => "DESC",
			"ID" => "DESC",
		);
	else
	{
		$arSort = array(
			$arParams["ELEMENT_SORT_FIELD"] => $arParams["ELEMENT_SORT_ORDER"],
			"ID" => "DESC",
		);
	}


//print_r($arSort);
	$currencyList = array();
	$arSections = array();
/*
	?><pre><?
	print_r(array_merge($arrFilter, $arFilter));
	?></pre><?
*/	
	//EXECUTE
	
	$rsElements = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
	//echo $arParams["DETAIL_URL"];
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);

	$arResult["ITEMS"] = array();
	$arMeasureMap = array();
	$arElementLink = array();
	$intKey = 0;
	while($arItem = $rsElements->GetNext())
	{
		//echo $arItem["ID"]."<br>";
		$arItem['ID'] = (int)$arItem['ID'];

		$arButtons = CIBlock::GetPanelButtons(
			$arItem["IBLOCK_ID"],
			$arItem["ID"],
			$arResult["ID"],
			array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
		);
		$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$arItem["PROPERTIES"] = array();

		$arResult["ITEMS"][$intKey] = $arItem;
		$arResult["ELEMENTS"][$intKey] = $arItem["ID"];
		$arElementLink[$arItem['ID']] = &$arResult["ITEMS"][$intKey];
		$intKey++;
	}
	
	$navComponentParameters = array();
	if ($arParams["PAGER_BASE_LINK_ENABLE"] === "Y")
	{
		$pagerBaseLink = trim($arParams["PAGER_BASE_LINK"]);
		if ($pagerBaseLink === "")
			$pagerBaseLink = $arResult["SECTION_PAGE_URL"];

		if ($pagerParameters && isset($pagerParameters["BASE_LINK"]))
		{
			$pagerBaseLink = $pagerParameters["BASE_LINK"];
			unset($pagerParameters["BASE_LINK"]);
		}

		$navComponentParameters["BASE_LINK"] = CHTTP::urlAddParams($pagerBaseLink, $pagerParameters, array("encode"=>true));
	}
	$rsElements->nPageWindow = 3;
	$arResult["NAV_STRING"] = $rsElements->GetPageNavStringEx(
		$navComponentObject,
		$arParams["PAGER_TITLE"],
		$arParams["PAGER_TEMPLATE"],
		$arParams["PAGER_SHOW_ALWAYS"],
		$this,
		$navComponentParameters
	);
	$arResult["NAV_CACHED_DATA"] = null;
	$arResult["NAV_RESULT"] = $rsElements;
	$arResult["NAV_PARAM"] = $navComponentParameters;

	
	$arResult['MODULES'] = $arResultModules;

	if (isset($arItem))
		unset($arItem);

	if (!empty($arResult["ELEMENTS"]))
	{
		$arPropFilter = array(
			'ID' => $arResult["ELEMENTS"],
			'IBLOCK_ID' => $arParams['IBLOCK_ID']
		);
		CIBlockElement::GetPropertyValuesArray($arElementLink, $arParams["IBLOCK_ID"], $arPropFilter);

		foreach ($arResult["ITEMS"] as &$arItem)
		{
			if ($bCatalog && $boolNeedCatalogCache)
			{
				CCatalogDiscount::SetProductPropertiesCache($arItem['ID'], $arItem["PROPERTIES"]);
			}

			$arItem["PICTURES"] = array();
			$i = 0;
			while ($i < 2)
			{
				$i++;
				if ($arItem["PROPERTIES"]["pict".$i]["VALUE"] != "")
				{
	//				echo $arItem["PROPERTIES"]["pict1"]["VALUE"]."<br>";									
					$filename =  basename($arItem["PROPERTIES"]["pict".$i]["VALUE"]);
					$exist = false;
					$original_pic_path = "/images/".$filename;
					if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
						$exist = true;
					else
					{
						$content = file_get_contents($arItem["PROPERTIES"]["pict".$i]["VALUE"]);
						if ($content !== false)
						{
							file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
							if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
								$exist = true;
						}
					}
					if ($exist == true)
					{
						$resized_pic_path = "/resized_images/210x290/".$filename;
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
							$arItem["PICTURES"][] = $resized_pic_path;
						else
						{
							$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
							//if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>210,"height"=>290), BX_RESIZE_IMAGE_PROPORTIONAL))
							if (ToysalesResizeImage($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, 195, 268))
								$arItem["PICTURES"][] = $resized_pic_path;
						}
					}
				}
			}
		}
		if (isset($arItem))
			unset($arItem);
	}

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult["ELEMENTS"]))
	{
		CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
		CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'GET_BY_ID' => 'Y'));
	}
	if (isset($arItem))
		unset($arItem);

	$currentPath = CHTTP::urlDeleteParams(
		$APPLICATION->GetCurPageParam(),
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
	);
	$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');

	$arResult['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~BUY_URL_TEMPLATE']);
	$arResult['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~ADD_URL_TEMPLATE']);
	unset($comparePath, $currentPath);

	foreach ($arResult["ITEMS"] as &$arItem)
	{
		$arItem["PRICES"] = array();
		$arItem["PRICE_MATRIX"] = false;
		$arItem['MIN_PRICE'] = false;

		$arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		if (!empty($arItem['PRICES']))
			$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arItem['PRICES']);

		$arItem["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem);

		$arItem['~BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~BUY_URL_TEMPLATE']);
		$arItem['BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['BUY_URL_TEMPLATE']);
		$arItem['~ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~ADD_URL_TEMPLATE']);
		$arItem['ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['ADD_URL_TEMPLATE']);
	}
	if (isset($arItem))
		unset($arItem);


	if(
		$bCatalog
		&& !empty($arResult["ELEMENTS"])
	)
	{
		$offersFilter = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'HIDE_NOT_AVAILABLE' => "Y"
		);

		$offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];

		$arOffers = CIBlockPriceTools::GetOffersArray(
			$offersFilter,
			$arResult["ELEMENTS"],
			array(
				$arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
				$arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
			),
			$arParams["OFFERS_FIELD_CODE"],
			$arParams["OFFERS_PROPERTY_CODE"],
			$arParams["OFFERS_LIMIT"],
			$arResult["PRICES"],
			$arParams['PRICE_VAT_INCLUDE'],
			$arConvertParams
		);
		if(!empty($arOffers))
		{
			foreach ($arResult["ELEMENTS"] as $id)
				$arElementLink[$id]['OFFERS'] = array();
			unset($id);

			foreach($arOffers as $arOffer)
			{
				if (isset($arElementLink[$arOffer["LINK_ELEMENT_ID"]]))
				{
					$arOffer['~BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~BUY_URL_TEMPLATE']);
					$arOffer['BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['BUY_URL_TEMPLATE']);
					$arOffer['~ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~ADD_URL_TEMPLATE']);
					$arOffer['ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['ADD_URL_TEMPLATE']);

					$arElementLink[$arOffer["LINK_ELEMENT_ID"]]['OFFERS'][] = $arOffer;

				}
			}
			unset($arOffer);
		}
		unset($arOffers);
	}
/*	
$i = 0;
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if ($i == 0)
		{
			if (is_array($arItem["OFFERS"]))
			{
				?><pre><?
				print_r($arItem["OFFERS"]);
				?></pre><?
			}
		}
		$i++;
	}
*/	
	if (
		'Y' == $arParams['CONVERT_CURRENCY']
		&& !empty($currencyList)
		&& defined("BX_COMP_MANAGED_CACHE")
	)
	{
		$currencyList[$arConvertParams['CURRENCY_ID']] = $arConvertParams['CURRENCY_ID'];
		foreach ($currencyList as &$oneCurrency)
			$CACHE_MANAGER->RegisterTag('currency_id_'.$oneCurrency);
		unset($oneCurrency);
	}
	unset($currencyList);
	//echo "#".count($arResult["ITEMS"])."#";

	$this->SetResultCacheKeys(array(
		"ID",
		"NAV_CACHED_DATA",
		$arParams["BROWSER_TITLE"],
		"NAME",
		"PATH",
		"IBLOCK_SECTION_ID",
	));

	$this->IncludeComponentTemplate();

	if ($bCatalog && $boolNeedCatalogCache)
	{
		CCatalogDiscount::ClearDiscountCache(array(
			'PRODUCT' => true,
			'SECTIONS' => true,
			'PROPERTIES' => true
		));
	}
}
if ($arResult["NAME"] != "")
	$APPLICATION->SetTitle($arResult["NAME"]);
//$APPLICATION->AddChainItem($arResult["VENDOR"]["NAME"]);