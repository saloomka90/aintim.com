<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
                        <div class="col-md-9 col-md-push-3 information-entry">
                            <div class="blog-landing-box type-1">
<?
foreach ($arResult["ITEMS"] as $arItem)
{
?>
                                <div class="blog-entry">
<?
if ($arItem["DETAIL_PICTURE"]["SRC"] != "")
{
?>
                                    <a class="image hover-class-1" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="" /><span class="hover-label">Подробнее</span></a>
<?
}
?>								
<?
if ($arItem["DISPLAY_DAY"] != "" and $arItem["DISPLAY_MONTH"] != "")
{
?>
                                    <div class="date"><?=$arItem["DISPLAY_DAY"]?> <span><?=$arItem["DISPLAY_MONTH"]?></span></div>
<?
}									
?>
                                    <div class="content">
                                        <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                        <div class="subtitle">
<?
if ($arItem["DISPLAY_ACTIVE_FROM"] != "")
{
?>										<?=$arItem["DISPLAY_ACTIVE_FROM"]?> / <?
}
?>										
<?
if (!empty($arItem["CATEGORIES"]))
{
?>
										Категория: 
<?	
	foreach ($arItem["CATEGORIES"] as $key => $arCategory)
	{
		if ($key > 0)
			echo ", ";
	?><a href="<?=$arCategory["SECTION_PAGE_URL"]?>"><?=$arCategory["NAME"]?></a><?
	}
}
?>
										</div>
                                        <div class="description"><?=$arItem["PREVIEW_TEXT"]?></div>
                                        <a class="readmore" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
                                    </div>
                                </div>

<?
}
?>									
                            </div>
                            <div class="page-selector">
<?
if ($arResult["NAV_STRING"] != "")							
{
?>
<div class="pages-box">
<?
	echo $arResult["NAV_STRING"];
?>
</div>
<?
}								
?>
                            </div>
                        </div>