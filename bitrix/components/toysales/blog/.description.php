<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Блог",
	"DESCRIPTION" => "",
	"ICON" => "/images/news_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "Toysales",
		"CHILD" => array(
			"ID" => "toysales_blog",
			"NAME" => "Блог",
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "toysales_blog_cmpx",
			),
		),
	),
);

?>