<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
                            <div class="tabs-entry">
                                <div class="products-swiper">
                                    <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                                        <div class="swiper-wrapper">
<?
if (!empty($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if (!empty($arItem["OFFERS"]))
		{
?>
                                            <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
<?
											if (is_array($arItem["PICTURES"]))
											{
												if ($arItem["PICTURES"][0] != "")
												{
												?>
												<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
														<img src="<?=$arItem["PICTURES"][0]?>" alt=""  />
												<?
													if ($arItem["PICTURES"][1] != "")
													{
													?>
														<img src="<?=$arItem["PICTURES"][1]?>" alt=""  />
													<?
													}
													else
													{
													?>
														<img src="<?=$arItem["PICTURES"][0]?>" alt=""  />
													<?
													}
												?></a><?
												}
											}
											$arOffer = current($arItem["OFFERS"]);
											$minPrice = $arOffer["MIN_PRICE"];
											if (!empty($minPrice) and $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
											{
											?>
											<div class="product-image-label type-4"><span>-<?=$minPrice['DISCOUNT_DIFF_PERCENT']?>%</span></div>
											<?
											}
											else
											if ($arItem["PROPERTIES"]["new"]["VALUE"] == "1")
											{
											?><div class="product-image-label type-1"><span>new</span></div><?
											}
											else
											if ($arItem["PROPERTIES"]["bestseller"]["VALUE"] == "1")
											{
											?><div class="product-image-label type-5"><span>хит</span></div><?
											}												
?>

                                                            <a class="top-line-a right open-product" rel="<?=$arItem["ID"]?>"><i class="fa fa-expand"></i> <span>Увеличить</span></a>
                                                        <div class="bottom-line">
														<?
														if (count($arItem["OFFERS"]) == 1)
														{
															$arOffer = current($arItem["OFFERS"]);
														?>
                                                                <a class="bottom-line-a buy_item" href="<?=$arOffer["~ADD_URL"]?>" rel="<?=$arOffer["ID"]?>"><i class="fa fa-shopping-cart"></i> Добавить в корзину</a>
																<a style="display:none" class="bottom-line-a" href="<?=$arParams["BASKET_URL"]?>"><i class="fa fa-shopping-cart"></i> Товар в корзине</a>
														<?
														}
														else
														{
														?>
                                                                <a class="bottom-line-a open-product" rel="<?=$arItem["ID"]?>"><i class="fa fa-shopping-cart"></i> Добавить в корзину</a>														
														<?
														}
														?>
                                                        </div>
                                                        </div>
                                                        <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                                        <div class="price">
<?													
	if (!empty($minPrice))
	{
		if ($minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
		{
		?>
		                                <div class="prev"><? echo $minPrice['PRINT_VALUE']; ?></div>
		<?
		}
	?>
                                        <div class="current"><?=$minPrice['PRINT_DISCOUNT_VALUE'];?></div>
	<?
	}													
?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<?
		}
	}
}
?>
                                        </div>
                                        <div class="pagination"></div>
                                    </div>
                                </div>
                            </div>