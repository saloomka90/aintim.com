<?
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = (int)$arParams["IBLOCK_ID"];

$arParams["SECTION_ID"] = (int)$arParams["SECTION_ID"];

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arResult["SECTIONS2"]=array();
$arResult["SECTIONS3"]=array();
$arResult["SECTIONS2_1"]=array();

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

$arParams["OFFERS_LIMIT"] = 0;

$arResult["SECTIONS2"]=array();
$arResult["SECTIONS3"]=array();


/*************************************************************************
			Work with cache
*************************************************************************/

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))))
{
	if(!Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	$arResult["SECTION"] = false;
	$intSectionDepth = 0;
	$arCurrentSection = array();
	if($arParams["SECTION_ID"]>0)
	{
		$arFilter["ID"] = $arParams["SECTION_ID"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter);
		$arCurrentSection = $rsSections->GetNext();
	}

	$section_id2 = 0;
	$section_id3 = 0;
	if ($arCurrentSection["ID"] > 0)
	{
		$rsPath = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arCurrentSection["ID"]);
		while($arPath = $rsPath->GetNext())
		{
			if ($arPath["DEPTH_LEVEL"] == 2)
				$section_id2 = $arPath["ID"];
			else
			if ($arPath["DEPTH_LEVEL"] == 3)
				$section_id3 = $arPath["ID"];		
		}
	}
	$bOffersIBlockExist = false;
	$arCatalog = false;
	$bCatalog = Loader::includeModule('catalog');
	if ($bCatalog)
	{
		$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
	}
	$arResult['CATALOG'] = $arCatalog;

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"MIN_PERMISSION" => "R",
		"!PROPERTY_sale" => false,
	);

	//$arFilter["SECTION_GLOBAL_ACTIVE"] = "Y";


	if($bCatalog && $bOffersIBlockExist)
	{
		// чтобы в выборку попали только те товары, у которых есть торговые предолжения

			$arSubFilter = array();
			$arSubFilter['CATALOG_AVAILABLE'] = 'Y'; // а это условие обеспечит выборку только тех предложений, которые можно купить
			$arSubFilter["IBLOCK_ID"] = $arResult['CATALOG']['IBLOCK_ID'];
			$arSubFilter["ACTIVE_DATE"] = "Y";
			$arSubFilter["ACTIVE"] = "Y";
			$arFilter["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$arResult['CATALOG']["SKU_PROPERTY_ID"], $arSubFilter);

	}

	$rsElements = CIBlockElement::GetList($arSort, $arFilter, array("IBLOCK_SECTION_ID"));
	$arSectionFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",		
		"ID" => array(),
	);
	
	$arSectionFilter2 = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",		
	);
	
	while($arItem = $rsElements->GetNext())
	{
		if ($arItem["IBLOCK_SECTION_ID"] > 0)
		{
			$arSectionFilter["ID"][] = $arItem["IBLOCK_SECTION_ID"];
		}
	}
	if (!empty($arSectionFilter["ID"]))
	{
		$rsSections = CIBlockSection::GetList(array("LEFT_MARGIN"=>"ASC"), $arSectionFilter);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);		
		while ($arSection = $rsSections->GetNext())
		{
			if ($arSection["DEPTH_LEVEL"] == 2)
			{
				if ($arSection["ID"] == $section_id2)
				{
					$arSection["SELECTED"] = "Y";
				}
				else
					$arSection["SELECTED"] = "";
				$arResult["SECTIONS2_1"][] = $arSection;
			}
			else
			if ($arSection["DEPTH_LEVEL"] == 3)
			{
				if (!is_array($arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]]))
				{
					$arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]] = array();
					$arSectionFilter2["ID"] = $arSection["IBLOCK_SECTION_ID"];
					$rsSections2 = CIBlockSection::GetList(array(), $arSectionFilter2);
					if ($arSection2 = $rsSections2->GetNext())
					{
						if ($arSection2["ID"] == $section_id2)
							$arSection2["SELECTED"] = "Y";
						else
							$arSection2["SELECTED"] = "";							
						$arResult["SECTIONS2"][] = $arSection2;
					}
				}
				if ($arSection["ID"] == $section_id3)
					$arSection["SELECTED"] = "Y";
				else
					$arSection["SELECTED"] = "";				
				$arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]][] = $arSection;
			}
		}
	}
	$this->IncludeComponentTemplate();
}