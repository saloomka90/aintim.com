<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);

?>
                                <ul>
<?
foreach ($arResult["SECTIONS1"] as $arSection)
{
	if ($arSection["LEVELS"] == 3)
	{
	?>
                                    <li class="full-width-columns">
                                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a><i class="fa fa-chevron-down"></i>
	<?
		if (is_array($arResult["SECTIONS2"][$arSection["ID"]]))
		{
	?>
	<div class="submenu">
	<?
			foreach ($arResult["SECTIONS2"][$arSection["ID"]] as $arSection2)
			{
				?>
                                            <div class="product-column-entry">
                                                <div class="submenu-list-title"><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><?=$arSection2["NAME"]?></a><span class="toggle-list-button"></span></div>
				<?
				if (is_array($arResult["SECTIONS3"][$arSection2["ID"]]))
				{
				?>
												
                                                <div class="description toggle-list-container">
                                                    <ul class="list-type-1">
				<?
                $i = 1;
                $bMore = false;

				foreach ($arResult["SECTIONS3"][$arSection2["ID"]] as $arSection3)
				{
				?>
                                                        <li<?=($i>5)?(" style=\"display:none\""):("")?>><a href="<?=$arSection3["SECTION_PAGE_URL"]?>"><i class="fa fa-angle-right"></i><?=$arSection3["NAME"]?></a></li>
				<?
                            if($i>5 && !$bMore)
                            {
                                $bMore = true;
                            ?>
                                <li><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><i class="fa fa-angle-right"></i>Смотреть ещё</a></li>

                                <?
                            }

                    $i++;
				}
				?>
                                                    </ul>
                                                </div>
				<?
				}
				?>
                                            </div>
				<?
				
			}

	if ($arSection["UF_MENU_TEXT"] != "")
	{
	?>
                                        <div class="submenu-links-line">
                                                <div class="submenu-links-line-container">
                                                    <div class="cell-view">
                                                        <?=$arSection["~UF_MENU_TEXT"]?>
                                                    </div>
                                                </div>
                                            </div>	
	
	<?
	}
	?>
	</div>
	<?
			}
	?>
									</li>
	<?
	}
	else
	{
	?>
                                    <li class="simple-list">
                                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a><i class="fa fa-chevron-down"></i>
	<?
		if (is_array($arResult["SECTIONS2"][$arSection["ID"]]))
		{
		?>										
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">
		<?
			foreach ($arResult["SECTIONS2"][$arSection["ID"]] as $arSection2)
			{
			?>
                                                <li><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><i class="fa fa-angle-right"></i><?=$arSection2["NAME"]?></a></li>
			<?
			}			
		?>
                                            </ul>
                                        </div>
		<?
		}
	?>
                                    </li>
	<?		
	}
}
?>
								</ul>
					
