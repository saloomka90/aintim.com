<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arResult["SECTIONS1"]=array();
$arResult["SECTIONS2"]=array();
$arResult["SECTIONS3"]=array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($USER->GetGroups())))
{
	if(!\Bitrix\Main\Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"<=DEPTH_LEVEL" => 3,
	);

	//ORDER BY
	$arSort = array(
		"left_margin"=>"asc",
	);
	$arSelect = array(
		"UF_MENU_TEXT"
	);
	//EXECUTE
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
	//$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
	$sections1_ids = array();
	$sections2_ids = array();
	$sections3_ids = array();
	while($arSection = $rsSections->GetNext())
	{
		if ($arSection["DEPTH_LEVEL"] == 1)
		{	
			$arSection["LEVELS"] = 2;
			$arResult["SECTIONS1"][$arSection["ID"]] = $arSection;
			$sections1_ids[] = $arSection["ID"];
		}
		else
		{
			$arResult["SECTIONS".$arSection["DEPTH_LEVEL"]][$arSection["IBLOCK_SECTION_ID"]][] = $arSection;
			${"sections".$arSection["DEPTH_LEVEL"]."_ids"}[$arSection["IBLOCK_SECTION_ID"]][] = $arSection["ID"];
		}
	}
	foreach ($sections1_ids as $section_id)
	{
		if (is_array($sections2_ids[$section_id]))
		{
			foreach ($sections2_ids[$section_id] as $section2_id)
			{
				if (is_array($sections3_ids[$section2_id])) //разделы 3 уровня существуют
				{
					$arResult["SECTIONS1"][$section_id]["LEVELS"] = 3;
					break;
				}
			}
		}
	}
	$arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS1"]);
	$this->SetResultCacheKeys(array(
		"SECTIONS_COUNT",
	));
	$this->IncludeComponentTemplate();
}

if($arResult["SECTIONS_COUNT"] > 0)
{
	if(
		$USER->IsAuthorized()
		&& $APPLICATION->GetShowIncludeAreas()
		&& \Bitrix\Main\Loader::includeModule("iblock")
	)
	{
		$UrlDeleteSectionButton = "";

		if(empty($UrlDeleteSectionButton))
		{
			$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
			$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
			$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
			$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
		}

		$arReturnUrl = array(
			"add_section" => (
				'' != $arParams["SECTION_URL"]?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"add_element" => (
				'' != $arParams["SECTION_URL"]?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"delete_section" => $UrlDeleteSectionButton,
		);
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			0,
			0,
			array("RETURN_URL" =>  $arReturnUrl, "CATALOG"=>true)
		);

		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}
}
?>