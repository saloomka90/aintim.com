<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Верхнее меню",
	"DESCRIPTION" => "",
	"ICON" => "/images/sections_top_count.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 20,
	"PATH" => array(
		"ID" => "Toysales",
		"CHILD" => array(
			"ID" => "toysales_catalog",
			"NAME" => "Каталог",
			"SORT" => 30,
			"CHILD" => array(
				"ID" => "toysales_catalog_cmpx",
			),
		),
		"NAME" => "Toysales"
	),
);
?>