<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if (!empty($arResult["ITEMS"]))
{
?>
			<div class="information-blocks">
				<h3 class="block-title inline-product-column-title">Последние записи</h3>
<?
	foreach ($arResult["ITEMS"] as $arItem)
	{
	?>
				<div class="inline-product-entry">
	<?
	if ($arItem["PREVIEW_PICTURE"]["SRC"] != "")
	{
	?>
 <a class="image" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
	<?
	}
	?>
					<div class="content">
						<div class="cell-view">
 <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
							<div class="description"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
						</div>
					</div>
					<div class="clear">
					</div>
				</div>
	<?
	}
	?>	
			</div>
<?
}
?>