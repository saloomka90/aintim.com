<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Последние записи блога",
	"DESCRIPTION" => "",
	"ICON" => "/images/news_line.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "Toysales",
		"CHILD" => array(
			"ID" => "toysales_blog",
			"NAME" => "Блог",
			"SORT" => 10,
		)
	),
);

?>