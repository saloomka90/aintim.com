<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Сумма товаров в корзине",
	"DESCRIPTION" => "",
	"ICON" => "/images/sale_basket.gif",
	"PATH" => array(
		"ID" => "Toysales",
		"CHILD" => array(
			"ID" => "toysales_sale_basket",
			"NAME" => GetMessage("SBBS_NAME")
		)
	),
);
?>