<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("sale"))
{
	ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
	return;
}

$allSum = 0.0;
$arBasketItems = array();
$arResult["ITEMS"] = array();

$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);

$fuserId = (int)CSaleBasket::GetBasketUserID(true);
if ($fuserId > 0)
{
	$rsBaskets = CSaleBasket::GetList(
		array("ID" => "ASC"),
		array("FUSER_ID" => $fuserId, "LID" => SITE_ID, "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N"), 
		false,
		false,
		array(
			"ID", "NAME", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY",
			"PRICE", "WEIGHT", "DETAIL_PAGE_URL", "NOTES", "CURRENCY", "VAT_RATE", "CATALOG_XML_ID",
			"PRODUCT_XML_ID", "SUBSCRIBE", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "TYPE", "SET_PARENT_ID"
		)
	);
	while ($arItem = $rsBaskets->GetNext())
	{
		$allSum += ($arItem["PRICE"] * $arItem["QUANTITY"]);
	}
}
$arResult["SUM_FORMATED"] = " ";
if ($allSum > 0)
	$arResult["SUM_FORMATED"] = SaleFormatCurrency($allSum, $allCurrency);
if ($_REQUEST["ajax_basket"] == "Y")
	$APPLICATION->RestartBuffer();
$this->IncludeComponentTemplate();
?>
