<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
                                <div class="blog-entry">
<?
if ($arResult["DETAIL_PICTURE"]["SRC"] != "")
{
?>
									<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="" />
									<br><br>                                    
<?
}
?>								
                                    <div class="content">
                                        <h1 class="title"><?=$arResult["NAME"]?></h1>
                                        <div class="subtitle">
<?
if ($arResult["DISPLAY_ACTIVE_FROM"] != "")
{
?>										<?=$arResult["DISPLAY_ACTIVE_FROM"]?> / <?
}
?>										
<?
if (!empty($arResult["CATEGORIES"]))
{
?>
										Категория: 
<?	
	foreach ($arResult["CATEGORIES"] as $key => $arCategory)
	{
		if ($key > 0)
			echo ", ";
	?><a href="<?=$arCategory["SECTION_PAGE_URL"]?>"><?=$arCategory["NAME"]?></a><?
	}
}
?>										
										</div>
                                        <div class="article-container style-1"><?=$arResult["DETAIL_TEXT"]?></div>
                                    </div>
                                </div>