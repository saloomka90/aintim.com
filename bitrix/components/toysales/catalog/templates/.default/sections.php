<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
                <div class="information-blocks">
                    <div class="portfolio-container type-3">
                        <div class="row">
                            <div class="col-sm-4 portfolio-entry">
                                <div class="image">
                                    <img alt="" src="/img/portfolio-1.jpg">
                                    <div class="hover-layer"></div>
                                    <div class="title-info">
                                        <a class="title" href="/catalog/zhenshchinam/">Товары для женщин</a>
                                        <div class="subtitle"><a href="/catalog/zhenshchinam/analnye-igrushki/">анальные игрушки</a>, <a href="/catalog/zhenshchinam/vibratory/">вибраторы</a>, <a href="/catalog/zhenshchinam/falloimitatory/">фаллоимитаторы</a>, <a href="/catalog/zhenshchinam/seks-tovary-dlya-zhenshchin/">прочие секс-товары</a></div>
                                        <div class="actions">
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="description"></div>
                                <a class="readmore" href="/catalog/zhenshchinam/">Все товары раздела</a>
                            </div>
                            <div class="col-sm-4 portfolio-entry">
                                <div class="image">
                                    <img alt="" src="/img/portfolio-2.jpg">
                                    <div class="hover-layer"></div>
                                    <div class="title-info">
                                        <a class="title" href="/catalog/param/">Игрушки для пар</a>
                                        <div class="subtitle"><a href="/catalog/param/bdsm-sado-mazo-tovary/">BDSM товары</a>, <a href="/catalog/param/bdsm-sado-mazo-tovary/knuty-plyetki-khlysty/">кнуты и плётки</a>, <a href="/catalog/param/bdsm-sado-mazo-tovary/naruchniki-osheyniki/">наручники и ошейники</a>, <a href="/catalog/param/strapony-falloprotezy/">страпоны</a></div>
                                        <div class="actions">
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="description"></div>
                                <a class="readmore" href="/catalog/param/">Все товары раздела</a>
                            </div>
                            <div class="col-sm-4 portfolio-entry">
                                <div class="image">
                                    <img alt="" src="/img/portfolio-3.jpg">
                                    <div class="hover-layer"></div>
                                    <div class="title-info">
                                        <a class="title" href="/catalog/muzhchinam/">Настоящим мужчинам</a>
                                        <div class="subtitle"><a href="/catalog/muzhchinam/masturbatory/">мастурбаторы</a>, <a href="/catalog/muzhchinam/seks-kukly/">секс куклы</a>, <a href="/catalog/muzhchinam/massazhery-prostaty/">массажеры простаты</a>, <a href="/catalog/muzhchinam/vakuumnye-pompy/">вакуумные помпы</a></div>
                                        <div class="actions">
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="description"></div>
                                <a class="readmore" href="/catalog/muzhchinam/">Все товары раздела</a>
                            </div>
                            <div class="col-sm-4 portfolio-entry">
                                <div class="image">
                                    <img alt="" src="/img/portfolio-4.jpg">
                                    <div class="hover-layer"></div>
                                    <div class="title-info">
                                        <a class="title" href="/catalog/eroticheskaya-odezhda/">Эротическая одежда</a>
                                        <div class="subtitle"><a href="/catalog/eroticheskaya-odezhda/bodi-i-kombinezony/">боди</a>, <a href="/catalog/eroticheskaya-odezhda/igrovye-kostyumy/">игровые костюмы</a>, <a href="/catalog/eroticheskaya-odezhda/sorochki-bebi-doll/">сорочки</a>, <a href="/catalog/eroticheskaya-odezhda/eroticheskoe-plate/">эротическое платье</a></div>
                                        <div class="actions">
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="description"></div>
                                <a class="readmore" href="/catalog/eroticheskaya-odezhda/">Все товары раздела</a>
                            </div>
                            <div class="col-sm-4 portfolio-entry">
                                <div class="image">
                                    <img alt="" src="/img/portfolio-5.jpg">
                                    <div class="hover-layer"></div>
                                    <div class="title-info">
                                        <a class="title" href="/catalog/vse-dlya-seksa/">Все для секса</a>
                                        <div class="subtitle"><a href="/catalog/vse-dlya-seksa/kosmetika-s-feromonami/">косметика с феромонами</a>, <a href="/catalog/vse-dlya-seksa/priyatnye-melochi/prezervativy/">презервативы</a>, <a href="/catalog/vse-dlya-seksa/smazki-lubrikanty/">смазки и лубриканты</a>, <a href="/catalog/vse-dlya-seksa/seks-mebel-i-kacheli/">секс-мебель</a></div>
                                        <div class="actions">
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="description"></div>
                                <a class="readmore" href="/catalog/vse-dlya-seksa/">Все товары раздела</a>
                            </div>
                        </div>
                    </div>
                </div>