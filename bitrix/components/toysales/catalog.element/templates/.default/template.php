<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateLibrary = array();
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
    'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);

unset($currencyList, $templateLibrary);
?>
<?

$strMainID = $this->GetEditAreaId($arResult['ID']);
/*
?><pre><?
print_r($arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["PROPERTIES"]["shipping_date"]);
?></pre><?
*/
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'MAIN_PROPS' => $strMainID . '_main_props',

    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'BUY_LINK' => $strMainID . '_buy_link',
    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
    'IN_BASKET' => $strMainID . '_in_basket',

    'OLD_PRICE' => $strMainID . '_old_price',
    'PRICE' => $strMainID . '_price',

    'PROP_DIV' => $strMainID . '_sku_tree',
    'PROP' => $strMainID . '_prop_',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;
?>
<div class="information-blocks">
    <div class="row" id="<? echo $strMainID; ?>">
    <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
        <div class="col-sm-5 col-md-4 col-lg-8 information-entry">
            <div class="product-preview-box">
                <?
                if (!empty($arResult["BIG_PICTURES"])) {
                    ?>
                    <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1"
                         data-speed="500" data-center="0" data-slides-per-view="1">
                        <div class="swiper-wrapper">
                            <?
                            foreach ($arResult["BIG_PICTURES"] as $key => $pict_src) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="product-zoom-image">
                                        <img src="<?= $arResult["MIDDLE_PICTURES"][$key] ?>" alt="<?=$arResult["NAME"]?>"
                                             data-zoom="<?= $pict_src ?>"/>
                                    </div>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                        <div class="pagination"></div>
                        <div class="product-zoom-container">
                            <div class="move-box">
                                <img class="default-image" src="<?= $arResult['MIDDLE_PICTURES'][0] ?>" alt="<?=$arResult["NAME"]?>"/>
                                <img class="zoomed-image" src="<?= $arResult['BIG_PICTURES'][0] ?>" alt="<?=$arResult["NAME"]?>"/>
                            </div>
                            <div class="zoom-area"></div>
                        </div>
                    </div>
                    <div class="swiper-hidden-edges">
                        <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0"
                             data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3"
                             data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4"
                             data-add-slides="4">
                            <div class="swiper-wrapper">
                                <?
                                foreach ($arResult["MIDDLE_PICTURES"] as $key => $pict_src) {
                                    ?>
                                    <div class="swiper-slide selected">
                                        <div class="paddings-container">
                                            <img src="<?= $pict_src ?>" alt="<?=$arResult["NAME"]?>"/>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="pagination"></div>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>

        <div class="col-sm-7 col-md-4 information-entry">
            <div class="product-detail-box">
                <h1 class="product-title"><?= $arResult["NAME"] ?></h1>
                <h3 class="product-subtitle">Артикул: <?= $arResult["PROPERTIES"]["article"]["VALUE"] ?>,
                    ID: <?= $arResult["XML_ID"] ?></h3>
                <?
                $shipping_date = "";
                if (!empty($arResult["OFFERS"])) {
                    $shipping_date = $arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["PROPERTIES"]["shipping_date"]["VALUE"];
                }
                ?>
                <h3 class="product-subtitle">Наличие <?//= $shipping_date ?>
                    <?
                    if (!empty($arResult["OFFERS"]) and $arResult["ACTIVE"] == "Y") {
                        ?>
                        <span class="inline-label green">на складе</span>
                        <?
                    } else {
                        ?>
                        <span class="inline-label red">нет в наличии</span>
                        <?
                    }
                    ?>
                </h3>
                <?
                if ($USER->IsAdmin()) {
                    foreach ($arResult["SHOPS"] as $arShop) {
                        if (!empty($arShop["ITEMS"])) {
                            ?>
                            <h3 class="product-subtitle"><span title="<?= implode(", ", $arShop["ITEMS"]) ?>"
                                                               class="inline-label green">Есть в магазине:</span> <a
                                        href="/info/contacts/"><?= $arShop["NAME"] ?></a></h3>
                            <?
                        }
                    }
                }
                ?>

                <div class="product-description detail-info-entry"><?= $arResult["SHORT_DETAIL_TEXT"] ?> <a
                            href="#full_text" class="continue-link">Полное описание <i
                                class="fa fa-long-arrow-right"></i></a></div>
                <?
                $minPrice = false;
                if (isset($arResult['MIN_PRICE']) || isset($arResult['RATIO_PRICE'])) {
                    $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
                }
                $boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
                ?>
                <div class="price detail-info-entry">
                    <?
                    
                    if (!empty($minPrice)) {
                        ?>
                        <?if($boolDiscountShow): ?>
                        <div class="prev" id="<? echo $arItemIDs['OLD_PRICE']; ?>"
                             style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></div>
                       	<br> 
                       	<?endif; ?>                       
                       	<div class="current" id="<? echo $arItemIDs['PRICE']; ?>"><?= $minPrice['PRINT_DISCOUNT_VALUE']; ?></div>
                         <?if($boolDiscountShow): ?>   
                            <p class="cur_diff">Ваша выгода - <b><?=$minPrice["PRINT_DISCOUNT_DIFF"]?></b></p>
                         <?endif; ?>  
                        <?
                    }
                    ?>
                </div>

                <?
                if (isset($arResult["OFFERS"]) and !empty($arResult["OFFERS"]) and $arResult["ACTIVE"] == "Y") {
                    $arSkuProps = array();
                    ?>
                    <div id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
                        if (!empty($arResult['OFFERS_PROP'])) {
                            foreach ($arResult['SKU_PROPS'] as &$arProp) {
                                if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']])) {
                                    continue;
                                }
                                $arSkuProps[] = array(
                                    'ID' => $arProp['ID'],
                                    'SHOW_MODE' => $arProp['SHOW_MODE'],
                                    'VALUES_COUNT' => $arProp['VALUES_COUNT']
                                );
                                if ('TEXT' == $arProp['SHOW_MODE']) {
                                    ?>
                                    <div class="size-selector detail-info-entry"
                                         id="<?= $arItemIDs['PROP'] . $arProp['ID']; ?>_list">
                                        <div class="detail-info-entry-title"><?= htmlspecialcharsex($arProp['NAME']) ?></div>
                                        <?
                                        foreach ($arProp['VALUES'] as $arOneValue) {
                                            $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                            ?>
                                            <div class="entry"
                                                 data-treevalue="<?= $arProp['ID'] ?>_<?= $arOneValue['ID'] ?>"
                                                 data-onevalue="<?= $arOneValue['ID'] ?>"><?= $arOneValue['NAME'] ?></div>
                                            <?
                                        }
                                        ?>
                                        <div class="spacer"></div>
                                    </div>
                                    <?
                                } elseif ('PICT' == $arProp['SHOW_MODE']) {
                                    ?>
                                    <div class="color-selector detail-info-entry"
                                         id="<?= $arItemIDs['PROP'] . $arProp['ID']; ?>_list">
                                        <div class="detail-info-entry-title"><?= htmlspecialcharsex($arProp['NAME']) ?></div>
                                        <?
                                        foreach ($arProp['VALUES'] as $arOneValue) {
                                            $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                            ?>
                                            <div class="entry"
                                                 data-treevalue="<?= $arProp['ID'] ?>_<?= $arOneValue['ID'] ?>"
                                                 data-onevalue="<?= $arOneValue['ID'] ?>"
                                                 title="<?= $arOneValue['NAME'] ?>"><img
                                                        src="<?= $arOneValue['PICT']['SRC'] ?>" height="37" width="37"
                                                        alt="<?=$arResult["NAME"]?>"/></div>
                                            <?
                                        }
                                        ?>
                                        <div class="spacer"></div>
                                    </div>
                                    <?
                                }
                            }
                        }
                        ?></div>
                <?
                $canBuy = $arResult['JS_OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
                ?>
                    <br><br>
                    <div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="detail-info-entry"
                         style="display: <? echo($canBuy ? '' : 'none'); ?>;">
                        <a id="<? echo $arItemIDs['BUY_LINK']; ?>" href="javascript:void(0)" class="button style-10">В
                            Корзину</a>
                        <!--<div class="clear"></div>-->
                        <a href="javascript:void(0)" class="link_buy_one_click button style-10">Купить в 1 клик</a>
                        <script>
                            $(function () {
                                $('.link_buy_one_click').click(function(){

                                    $("#buy_one_click").trigger('click');

                                });
                            });
                        </script>
                        <div class="clear"></div>
                    </div>
                    <div class="detail-info-entry" id="<? echo $arItemIDs['IN_BASKET']; ?>" style="display:none">
                        <a class="button style-20" href="<?= $arParams["BASKET_URL"] ?>">Товар в корзине</a>
                        <div class="clear"></div>
                    </div>
                <?

                $arJSParams = array(
                    'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
                    'SHOW_ADD_BASKET_BTN' => false,
                    'SHOW_BUY_BTN' => true,
                    'SHOW_ABSENT' => true,
                    'SHOW_SKU_PROPS' => $arResult['OFFERS_PROPS_DISPLAY'],
                    'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
                    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                    'VISUAL' => array(
                        'ID' => $arItemIDs['ID'],
                        'PICT_ID' => $arItemIDs['PICT'],
                        'PRICE_ID' => $arItemIDs['PRICE'],
                        'OLD_PRICE_ID' => $arItemIDs['OLD_PRICE'],
                        'TREE_ID' => $arItemIDs['PROP_DIV'],
                        'TREE_ITEM_ID' => $arItemIDs['PROP'],
                        'BUY_ID' => $arItemIDs['BUY_LINK'],
                        'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
                        'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
                        'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                        'IN_BASKET_ID' => $arItemIDs['IN_BASKET'],
                        'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                    ),
                    'BASKET' => array(
                        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                        'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
                        'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                        'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                    ),
                    'PRODUCT' => array(
                        'ID' => $arResult['ID'],
                    ),
                    'OFFERS' => $arResult['JS_OFFERS'],
                    'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
                    'TREE_PROPS' => $arSkuProps,
                );
                ?>
                    <script type="text/javascript">
                        var <? echo $strObName; ?> =
                        new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
                    </script>
                    <?
                }
                //print_r($arResult["PROPERTIES"]["vendor"])
                ?>

                <div class="share-box detail-info-entry">
                    <div class="title">Поделитесь с друзьями</div>
                    <div class="socials-box">
                        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"
                                charset="utf-8"></script>
                        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                        <div class="ya-share2"
                             data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,whatsapp"></div>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>
        </div>
        <div class="clear visible-xs visible-sm"></div>
        <?/* ?>
        <div class="col-md-4 col-lg-3 information-entry product-sidebar">
            <div class="row">
                <?
                if (!empty($arResult["VENDOR"])) {
                    ?>
                    <div class="col-md-12">
                        <div class="information-blocks production-logo">
                            <div class="background">
                                <?
                                if ($arResult["VENDOR"]["PREVIEW_PICTURE"]["SRC"] != "") {
                                    ?>
                                    <div class="logo"><a href="<?= $arResult["VENDOR"]["DETAIL_PAGE_URL"] ?>"><img
                                                    src="<?= $arResult["VENDOR"]["PREVIEW_PICTURE"]["SRC"] ?>"
                                                    alt="<?= $arResult["VENDOR"]["NAME"] ?>"/></a></div>
                                    <?
                                }
                                ?>
                                <?= $arResult["VENDOR"]["NAME"] ?><br><a
                                        href="<?= $arResult["VENDOR"]["DETAIL_PAGE_URL"] ?>">Все товары
                                    производителя</a>
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>

                <?
                $frame = $this->CreateFrame()->begin('');
                if (!empty($arResult["SECT_ITEMS"])) {
                    ?>
                    <div class="col-md-12">
                        <div class="information-blocks">
                            <div class="information-entry products-list">
                                <h3 class="block-title inline-product-column-title">Товары раздела</h3>
                                <?
                                foreach ($arResult["SECT_ITEMS"] as $arItem) {
                                    ?>
                                    <div class="inline-product-entry">
                                        <?
                                        if ($arItem["PREVIEW_PICTURE"]["SRC"] != "") {
                                            ?>
                                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="image"><img
                                                        alt="<?= $arItem["NAME"] ?>"
                                                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"></a>
                                            <?
                                        }
                                        ?>
                                        <div class="content">
                                            <div class="cell-view">
                                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                                                   class="title"><?= $arItem["NAME"] ?></a>
                                                <div class="price">
                                                    <?
                                                    if (!empty($arItem["MIN_PRICE"])) {
                                                        if ($arItem["MIN_PRICE"]['DISCOUNT_VALUE'] < $arItem["MIN_PRICE"]['VALUE']) {
                                                            ?>
                                                            <div class="prev"><? echo $arItem["MIN_PRICE"]['PRINT_VALUE']; ?></div>
                                                            <?
                                                        }
                                                        ?>
                                                        <div class="current"><?= $arItem["MIN_PRICE"]['PRINT_DISCOUNT_VALUE']; ?></div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?
                }
                $frame->end();
                ?>
            </div>
        </div>
        
        <?*/ ?>
    </div>    
    
    <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
                            <div class="information-blocks categories-border-wrapper">
	<?$APPLICATION->IncludeComponent(
		"toysales:catalog.left_menu",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
			
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?>							

                            </div>

	 </div>	
    </div>
</div>

<div class="information-blocks">
    <div class="tabs-container style-1">
        <div class="swiper-tabs tabs-switch">
            <a name="full_text"></a>
            <div class="title">Информация</div>
            <div class="list">
                <a class="tab-switcher active">Полное описание</a>
                <?
                if (strpos($APPLICATION->GetCurDir(), "/catalog/eroticheskaya-odezhda/") === 0) {
                    ?>
                    <a class="tab-switcher">Размеры белья</a>
                    <?
                }
                ?>
                <? if(!empty($arResult['DISPLAY_PROPERTIES'])): ?>
                	<a class="tab-switcher">Характеристики</a>
                <? endif; ?>
                <a class="tab-switcher">Доставка и оплата</a>
                <a class="tab-switcher" id="responses">Отзывы</a>
                <div class="clear"></div>
            </div>
        </div>
        <div>
            <div class="tabs-entry">
                <div class="article-container style-1">
                    <div class="row">
                        <div class=" information-entry" id="js_text_detail">                            
                            <p><?= $arResult["DETAIL_TEXT"] ?></p>
                           
                            <script>
								$(document).ready(function(){
									
									var hei = $('#js_text_detail').height();
									if(hei>200){
										$('#show_big_text').show().html('Развернуть текст');
										$('#js_text_detail').height(200).addClass("active");
										
										//console.log($( "#js_text_detail" ).hasClass( "active" ));
										
										$('#show_big_text').click(
												function(){
													if($( "#js_text_detail" ).hasClass( "active" )){
														
														$('#js_text_detail').height('auto').removeClass("active");
    													$('#show_big_text').html('Свернуть текст');
													}else{
														
														$('#js_text_detail').height(200).addClass("active");
														$('#show_big_text').html('Развернуть текст');													
													}
												});
									}
								});
                            </script>                            
                        </div>
 						<div id="show_big_text" >Развернуть текст</div>
                    </div>
                </div>
            </div>
            <?
            if (strpos($APPLICATION->GetCurDir(), "/catalog/eroticheskaya-odezhda/") === 0) {
                ?>
                <div class="tabs-entry">
                    <div class="article-container style-1">
                        <div class="row">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include_areas/sizes.php",
                                    "EDIT_TEMPLATE" => "page_inc.php"
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
            <?
            if (!empty($arResult['DISPLAY_PROPERTIES'])) {
            ?>
            <div class="tabs-entry">
                <div class="article-container style-1">
                    <div class="row">                    
                                <dl>
                                    <?
                                    foreach ($arResult['DISPLAY_PROPERTIES'] as $code => $arOneProp) {
                                        if ($code == "vibration") {
                                            if ($arOneProp['DISPLAY_VALUE'] == "0") {
                                                $arOneProp['DISPLAY_VALUE'] = "нет";
                                            } else {
                                                if ($arOneProp['DISPLAY_VALUE'] == "1") {
                                                    $arOneProp['DISPLAY_VALUE'] = "да";
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="share-box">
                                            <div class="title"><b><?= $arOneProp['NAME'] ?>:</b>
                                                <?
                                                echo(
                                                is_array($arOneProp['DISPLAY_VALUE'])
                                                    ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                                    : $arOneProp['DISPLAY_VALUE']
                                                );
                                                ?>
                                            </div>
                                        </div>
                                        <?
                                    }
                                    unset($arOneProp);
                                    ?>
                                </dl>                               
                    </div>
                </div>
            </div>
            <?
            }
            ?>
            <div class="tabs-entry">
                <div class="article-container style-1">
                    <div class="row">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/include_areas/delivery_payment.php",
                                "EDIT_TEMPLATE" => "page_inc.php"
                            )
                        ); ?>
                    </div>
                </div>
            </div>
            <?
            if ($_REQUEST["result"] == "reply") {
                ?>
                <script>
                    $(function () {
                            $("#responses").click();
                        }
                    );
                </script>
                <?
            }
            ?>
            <div class="tabs-entry" id="response_tab">
                <?
                Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("responses");
                ?>
                <? if ($arParams["USE_REVIEW"] == "Y" && IsModuleInstalled("forum")): ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:forum.topic.reviews",
                        "catalog",
                        Array(
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
                            "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
                            "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
                            "FORUM_ID" => $arParams["FORUM_ID"],
                            "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
                            "SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
                            "DATE_TIME_FORMAT" => "j M Y",
                            "ELEMENT_ID" => $arResult["ID"],
                            "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
                            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                            "URL_TEMPLATES_DETAIL" => $arParams["URL_TEMPLATES_DETAIL"],
                            "ELEMENT_NAME" => $arResult["NAME"],
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "DYNAMIC_WITHOUT_STUB"
                        ),
                        $component
                    ); ?>
                <? endif ?>
                <?
                Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("responses", "")
                ?>
            </div>
        </div>
    </div>
</div>