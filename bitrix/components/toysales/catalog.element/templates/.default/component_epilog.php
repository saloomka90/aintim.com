<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/buy.js');

if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}

?>
<script type="text/javascript">
window.dataLayer.push({
    "ecommerce": {
	"currencyCode": "RUB",
        "detail": {
            "products": [
                {
                    "id": "<?=$arResult["XML_ID"]?>",
                    "name" : "<?=$arResult["NAME"]?>",
                    "price": <?=$arResult["MIN_PRICE"]["DISCOUNT_VALUE"]?>,
                    "brand": "<?=$arResult["VENDOR"]["NAME"]?>",
                    "category": "<?
foreach($arResult["SECTION"]["PATH"] as $key => $arPath)
{
	if ($key > 0)
		echo "/";
	echo $arPath["NAME"];
}
?>"
                }
            ]
        }
    }
});
</script>


