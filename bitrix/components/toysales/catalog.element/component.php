<?
use Bitrix\Main\Context,
	Bitrix\Main\Loader,
	Bitrix\Main\Type\Collection,
	Bitrix\Main\Type\DateTime,
	Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams['SECTION_CODE'] = trim($arParams['SECTION_CODE']);
$arParams["VENDORS_IBLOCK_ID"] = (int)$arParams["VENDORS_IBLOCK_ID"];
$arParams["SHOPS_IBLOCK_ID"] = (int)$arParams["SHOPS_IBLOCK_ID"];

$arParams["ELEMENT_ID"] = intval($arParams["~ELEMENT_ID"]);
if($arParams["ELEMENT_ID"] > 0 && $arParams["ELEMENT_ID"]."" != $arParams["~ELEMENT_ID"])
{
	if (CModule::IncludeModule("iblock"))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_ELEMENT_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
	}
	return;
}

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);
$arParams["BASKET_URL"]=trim($arParams["BASKET_URL"]);
if($arParams["BASKET_URL"] === '')
	$arParams["BASKET_URL"] = "/personal/cart/";

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if($arParams["ACTION_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if($arParams["PRODUCT_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if($arParams["PRODUCT_PROPS_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

$arParams["SECTION_ID_VARIABLE"]=trim($arParams["SECTION_ID_VARIABLE"]);
if($arParams["SECTION_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_ID_VARIABLE"]))
	$arParams["SECTION_ID_VARIABLE"] = "SECTION_ID";
$arParams["CHECK_SECTION_ID_VARIABLE"] = (isset($arParams["CHECK_SECTION_ID_VARIABLE"]) && $arParams["CHECK_SECTION_ID_VARIABLE"] === 'Y' ? 'Y' : 'N');
$arParams['FROM_SECTION'] = '';
if ($arParams["CHECK_SECTION_ID_VARIABLE"] === 'Y')
	$arParams['FROM_SECTION'] = (isset($_REQUEST[$arParams["SECTION_ID_VARIABLE"]]) ? trim($_REQUEST[$arParams["SECTION_ID_VARIABLE"]]) : '');

$arParams["META_KEYWORDS"] = trim($arParams["META_KEYWORDS"]);
$arParams["META_DESCRIPTION"] = trim($arParams["META_DESCRIPTION"]);
$arParams["BROWSER_TITLE"] = trim($arParams["BROWSER_TITLE"]);

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_KEYWORDS"] = (isset($arParams["SET_META_KEYWORDS"]) && $arParams["SET_META_KEYWORDS"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_DESCRIPTION"] = (isset($arParams["SET_META_DESCRIPTION"]) && $arParams["SET_META_DESCRIPTION"] === 'N' ? 'N' : 'Y');
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
$arParams["USE_MAIN_ELEMENT_SECTION"] = $arParams["USE_MAIN_ELEMENT_SECTION"]==="Y";
$arParams["ADD_ELEMENT_CHAIN"] = (isset($arParams["ADD_ELEMENT_CHAIN"]) && $arParams["ADD_ELEMENT_CHAIN"] == "Y");

if(!isset($arParams["PROPERTY_CODE"]) || !is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["PROPERTY_CODE"][$k]);

if(!isset($arParams["PRICE_CODE"]) || !is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if ($arParams["SHOW_PRICE_COUNT"] <= 0)
	$arParams["SHOW_PRICE_COUNT"] = 1;


$arParams['ADD_PROPERTIES_TO_BASKET'] = (isset($arParams['ADD_PROPERTIES_TO_BASKET']) && $arParams['ADD_PROPERTIES_TO_BASKET'] === 'N' ? 'N' : 'Y');
if ('N' == $arParams['ADD_PROPERTIES_TO_BASKET'])
{
	$arParams["PRODUCT_PROPERTIES"] = array();
	$arParams["OFFERS_CART_PROPERTIES"] = array();
}
$arParams['PARTIAL_PRODUCT_PROPERTIES'] = (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) && $arParams['PARTIAL_PRODUCT_PROPERTIES'] === 'Y' ? 'Y' : 'N');
if(!isset($arParams["PRODUCT_PROPERTIES"]) || !is_array($arParams["PRODUCT_PROPERTIES"]))
	$arParams["PRODUCT_PROPERTIES"] = array();
foreach($arParams["PRODUCT_PROPERTIES"] as $k=>$v)
	if($v==="")
		unset($arParams["PRODUCT_PROPERTIES"][$k]);

if (!isset($arParams["OFFERS_CART_PROPERTIES"]) || !is_array($arParams["OFFERS_CART_PROPERTIES"]))
	$arParams["OFFERS_CART_PROPERTIES"] = array();
foreach($arParams["OFFERS_CART_PROPERTIES"] as $i => $pid)
	if ($pid === "")
		unset($arParams["OFFERS_CART_PROPERTIES"][$i]);

if (empty($arParams["OFFERS_SORT_FIELD"]))
	$arParams["OFFERS_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER"]))
	$arParams["OFFERS_SORT_ORDER"] = "asc";
if (empty($arParams["OFFERS_SORT_FIELD2"]))
	$arParams["OFFERS_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER2"]))
	$arParams["OFFERS_SORT_ORDER2"] = "desc";

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";
$arParams["PRICE_VAT_SHOW_VALUE"] = $arParams["PRICE_VAT_SHOW_VALUE"] === "Y";

$arParams['HIDE_NOT_AVAILABLE'] = (!isset($arParams['HIDE_NOT_AVAILABLE']) || 'Y' != $arParams['HIDE_NOT_AVAILABLE'] ? 'N' : 'Y');

$arParams['USE_ELEMENT_COUNTER'] = (isset($arParams['USE_ELEMENT_COUNTER']) && 'N' == $arParams['USE_ELEMENT_COUNTER'] ? 'N' : 'Y');
$arParams["SHOW_DEACTIVATED"] = (isset($arParams['SHOW_DEACTIVATED']) && 'Y' == $arParams['SHOW_DEACTIVATED'] ? 'Y' : 'N');
$arParams["DISABLE_INIT_JS_IN_COMPONENT"] = (isset($arParams["DISABLE_INIT_JS_IN_COMPONENT"]) && $arParams["DISABLE_INIT_JS_IN_COMPONENT"] == 'Y' ? 'Y' : 'N');
$arParams["SET_VIEWED_IN_COMPONENT"] = (isset($arParams["SET_VIEWED_IN_COMPONENT"]) && $arParams["SET_VIEWED_IN_COMPONENT"] == 'Y' ? 'Y' : 'N');

/*************************************************************************
			Processing of the Buy link
*************************************************************************/
$strError = '';
$successfulAdd = true;

if (isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && isset($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]))
{
	if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."BUY"]))
		$action = "BUY";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2BASKET"]))
		$action = "ADD2BASKET";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = (int)$_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]];
	if (($action == "ADD2BASKET" || $action == "BUY") && $productID > 0)
	{
		if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
		{
			$addByAjax = isset($_REQUEST['ajax_basket']) && $_REQUEST['ajax_basket'] === 'Y';
			$QUANTITY = 0;
			$product_properties = array();
			$intProductIBlockID = (int)CIBlockElement::GetIBlockByID($productID);
			if (0 < $intProductIBlockID)
			{
				if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y')
				{
					if ($intProductIBlockID == $arParams["IBLOCK_ID"])
					{
						if (!empty($arParams["PRODUCT_PROPERTIES"]))
						{
							if (
								isset($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
								&& is_array($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
							)
							{
								$product_properties = CIBlockPriceTools::CheckProductProperties(
									$arParams["IBLOCK_ID"],
									$productID,
									$arParams["PRODUCT_PROPERTIES"],
									$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
									$arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
								);
								if (!is_array($product_properties))
								{
									$strError = GetMessage("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR");
									$successfulAdd = false;
								}
							}
							else
							{
								$strError = GetMessage("CATALOG_EMPTY_BASKET_PROPERTIES_ERROR");
								$successfulAdd  = false;
							}
						}
					}
					else
					{
						$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
						if (!empty($arParams["OFFERS_CART_PROPERTIES"]) || !empty($skuAddProps))
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								$arParams["IBLOCK_ID"],
								$arParams["OFFERS_CART_PROPERTIES"],
								$skuAddProps
							);
						}
					}
				}
				$QUANTITY = 1;
			}
			else
			{
				$strError = GetMessage('CATALOG_ELEMENT_NOT_FOUND');
				$successfulAdd = false;
			}
			unset($arItem);
			if ($successfulAdd)
			{
				//Выберем торговое предложение (у него нужно XML_ID и свойство CML2_LINK - ID товара)
				$arFilter = array(
					"IBLOCK_ID" => $intProductIBlockID,
					"ID" => $productID,
				);
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"NAME",
					"XML_ID",
					"PROPERTY_CML2_LINK",
				);
				$rsOffers = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				if ($arOffer = $rsOffers->GetNext())
				{
					$product_properties[] = array(
						"NAME" => "aID",
						"CODE" => "aID",
						"VALUE" => $arOffer["XML_ID"],
						"SORT" => 200,
					);					
					$main_item_id = intval($arOffer["PROPERTY_CML2_LINK_VALUE"]);
					if ($main_item_id > 0)
					{
						$arFilter = array(
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ID" => $main_item_id,
						);
						$arSelect = array(
							"ID",
							"IBLOCK_ID",
							"NAME",
							"XML_ID",
							"IBLOCK_SECTION_ID",
							"PROPERTY_pict1"
						);
						$rsItems = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
						if ($arItem = $rsItems->GetNext())
						{
							if ($arItem["XML_ID"] != "")
							{
								$product_properties[] = array(
									"NAME" => "prodID",
									"CODE" => "prodID",
									"VALUE" => $arItem["XML_ID"],
									"SORT" => 200,
								);
								$product_properties[] = array(
									"NAME" => "Ссылка на stripmag",
									"CODE" => "stripmag",
									"VALUE" => "http://stripmag.ru/prod.php?id=".$arItem["XML_ID"],
									"SORT" => 200,
								);								
							}
							if ($arItem["PROPERTY_PICT1_VALUE"] != "")
							{
								$product_properties[] = array(
									"NAME" => "Картинка",
									"CODE" => "PICTURE",
									"VALUE" => $arItem["PROPERTY_PICT1_VALUE"],
									"SORT" => 200,
								);								
							}
						}
					}					
				}
				if(!Add2BasketByProductID($productID, $QUANTITY, $arRewriteFields, $product_properties))
				{
					if ($ex = $APPLICATION->GetException())
						$strError = $ex->GetString();
					else
						$strError = GetMessage("CATALOG_ERROR2BASKET");
					$successfulAdd = false;
				}
			}

			if ($addByAjax)
			{
				if ($successfulAdd)
				{
					$metrika_product = array(
					);					
					if (isset($arItem) && is_array($arItem) && $arPrice = CCatalogProduct::GetOptimalPrice($productID, 1, $USER->GetUserGroupArray(), "N"))
					{
						$metrika_product["id"] = $arItem["XML_ID"];
						$metrika_product["name"] = $arItem["NAME"];
						$metrika_product["price"] = $arPrice["DISCOUNT_PRICE"];

						$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"vendor"));
						if($arVendor = $db_props->Fetch())							
						{
							$vendor_xml_id = trim($arVendor["VALUE"]);
							if ($vendor_xml_id != "")
							{
								$arVendorFilter = array(
									"IBLOCK_ID" => $arParams["VENDORS_IBLOCK_ID"],
									"XML_ID" => $vendor_xml_id,
									"ACTIVE" => "Y"
								);
								$rsVendor = CIBlockElement::GetList(array(), $arVendorFilter, false, array("nTopCount" => 1));
								if ($arVendor = $rsVendor->GetNext())
								{
									$metrika_product["brand"] = $arVendor["NAME"];
								}
							}
						}
						if ($arItem["IBLOCK_SECTION_ID"] > 0)
						{
							$section_path = "";
							$rsPath = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
							$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
							$key = 0;
							while($arPath = $rsPath->GetNext())
							{
								$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
								$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
								
								if ($key > 0)
									$section_path .= "/";
								if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
									$section_path .= $arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"];
								else
									$section_path .= $arPath["NAME"];
								$key++;
							}
							if ($section_path != "")
								$metrika_product["category"] = $section_path;
						}
						if (is_array($product_properties))
						{
							$color = "";
							$size = "";
							foreach ($product_properties as $arProp)
							{
								if ($arProp["CODE"] == "color" && $arProp["VALUE"] != "")
									$color = $arProp["VALUE"];
								else
								if ($arProp["CODE"] == "size" && $arProp["VALUE"] != "")
									$size = $arProp["VALUE"];										
							}
							if ($size != "" || $color != "")
							{
								$metrika_product["variant"] = "";
								if ($size != "")
									$metrika_product["variant"] = "Размер ".$size;
								if ($color != "")
								{
									if ($metrika_product["variant"] != "")
										$metrika_product["variant"] .= ", ";
									$metrika_product["variant"] .= "Цвет ".$color;
								}
							}								
						}
					}
					$addResult = array('STATUS' => 'OK', 'MESSAGE' => GetMessage('CATALOG_SUCCESSFUL_ADD_TO_BASKET'), 'ID' => $productID, 'METRIKA_PRODUCT' => $metrika_product);
				}
				else
				{
					$addResult = array('STATUS' => 'ERROR', 'MESSAGE' => $strError);
				}
				$APPLICATION->RestartBuffer();
				echo CUtil::PhpToJSObject($addResult);
				die();
			}
			else
			{
				if ($successfulAdd)
				{
					$pathRedirect = (
						$action == "BUY"
						? $arParams["BASKET_URL"]
						: $APPLICATION->GetCurPageParam("", array(
							$arParams["PRODUCT_ID_VARIABLE"],
							$arParams["ACTION_VARIABLE"],
							$arParams['PRODUCT_QUANTITY_VARIABLE'],
							$arParams['PRODUCT_PROPS_VARIABLE']
						))
					);
					LocalRedirect($pathRedirect);
				}
			}
		}
	}
}

if (!$successfulAdd)
{
	ShowError($strError);
	return 0;
}

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if (!Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return 0;
	}

	$arResultModules = array(
		'iblock' => true,
		'catalog' => false,
		'currency' => false,
		'workflow' => false
	);

	//Handle case when ELEMENT_CODE used
	if($arParams["ELEMENT_ID"] <= 0)
	{
		$findFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"IBLOCK_LID" => SITE_ID,
			"IBLOCK_ACTIVE" => "Y",
			"ACTIVE_DATE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
			"MIN_PERMISSION" => 'R',
		);
		//if ($arParams["SHOW_DEACTIVATED"] !== "Y")
			//$findFilter["ACTIVE"] = "Y";

		$arParams["ELEMENT_ID"] = CIBlockFindTools::GetElementID(
			$arParams["ELEMENT_ID"],
			$arParams["ELEMENT_CODE"],
			false,
			false,
			$findFilter
		);
	}

	if($arParams["ELEMENT_ID"] > 0)
	{
		$currentPath = CHTTP::urlDeleteParams(
			$APPLICATION->GetCurPageParam(),
			array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
			array('delete_system_params' => true)
		);
		$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
		if ($arParams['COMPARE_PATH'] == '')
		{
			$comparePath = $currentPath;
		}
		else
		{
			$comparePath = CHTTP::urlDeleteParams(
				$arParams['COMPARE_PATH'],
				array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
				array('delete_system_params' => true)
			);
			$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
		}
		$arParams['COMPARE_PATH'] = $comparePath.$arParams['ACTION_VARIABLE'].'=COMPARE';

		$arUrlTemplates = array();
		$arUrlTemplates['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
		$arUrlTemplates['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
		unset($comparePath, $currentPath);

		$bIBlockCatalog = false;
		$arCatalog = false;
		$boolNeedCatalogCache = false;
		$bCatalog = Loader::includeModule('catalog');
		$useCatalogButtons = array();
		if ($bCatalog)
		{
			$arResultModules['catalog'] = true;
			$arResultModules['currency'] = true;
			$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
			if (!empty($arCatalog) && is_array($arCatalog))
			{
				$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
				$boolNeedCatalogCache = true;
				if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_CATALOG || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
					$useCatalogButtons['add_product'] = true;
				if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
					$useCatalogButtons['add_sku'] = true;
			}
		}
		$arResult['CATALOG'] = $arCatalog;
		//print_r($arResult['CATALOG']);
		//This function returns array with prices description and access rights
		//in case catalog module n/a prices get values from element properties
		$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
		$arResultPricesAllow = CIBlockPriceTools::GetAllowCatalogPrices($arResultPrices);

		if ($bCatalog && $boolNeedCatalogCache && !empty($arResultPricesAllow))
		{
			$boolNeedCatalogCache = CIBlockPriceTools::SetCatalogDiscountCache($arResultPricesAllow, $USER->GetUserGroupArray());
		}

		//SELECT
		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"XML_ID",
			"NAME",
			"ACTIVE",
			"DATE_ACTIVE_FROM",
			"DATE_ACTIVE_TO",
			"SORT",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"DATE_CREATE",
			"CREATED_BY",
			"TIMESTAMP_X",
			"MODIFIED_BY",
			"TAGS",
			"IBLOCK_SECTION_ID",
			"DETAIL_PAGE_URL",
			"LIST_PAGE_URL",
			"DETAIL_PICTURE",
			"PREVIEW_PICTURE",
			"PROPERTY_*",
		);
		if ($bIBlockCatalog)
			$arSelect[] = 'CATALOG_QUANTITY';
		if ($arParams['SET_CANONICAL_URL'] === 'Y')
			$arSelect[] = 'CANONICAL_PAGE_URL';
		//WHERE
		$arFilter = array(
			"ID" => $arParams["ELEMENT_ID"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"IBLOCK_LID" => SITE_ID,
			"IBLOCK_ACTIVE" => "Y",
			"ACTIVE_DATE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
			"MIN_PERMISSION" => 'R',
		);
		//if ($arParams["SHOW_DEACTIVATED"] !== "Y")
			//$arFilter["ACTIVE"] = "Y";

		//ORDER BY
		$arSort = array(
		);

		//PRICES
		$arPriceTypeID = array();
		foreach($arResultPrices as &$value)
		{
			if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
				continue;
			$arSelect[] = $value["SELECT"];
			$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
		}
		if (isset($value))
			unset($value);

		$arSection = false;
		$arSectionFilter = array(
			"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
		);
		if ($arParams["SECTION_ID"] > 0 || $arParams["SECTION_CODE"] !== '')
		{
			if($arParams["SECTION_ID"] > 0)
			{
				$arSectionFilter["ID"] = $arParams["SECTION_ID"];
			}
			else
			{
				$arSectionFilter["HAS_ELEMENT"] = $arParams["ELEMENT_ID"];
				$arSectionFilter["=CODE"] = $arParams["SECTION_CODE"];
			}

			$rsSection = CIBlockSection::GetList(array(), $arSectionFilter);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
		}
		elseif ($arParams['CHECK_SECTION_ID_VARIABLE'] === 'Y' && $arParams['FROM_SECTION'] !== '')
		{
			if (((int)$arParams['FROM_SECTION']).'|' == $arParams['FROM_SECTION'].'|')
			{
				$arSectionFilter["ID"] = $arParams['FROM_SECTION'];
			}
			else
			{
				$arSectionFilter["HAS_ELEMENT"] = $arParams["ELEMENT_ID"];
				$arSectionFilter["=CODE"] = $arParams['FROM_SECTION'];
			}
			$rsSection = CIBlockSection::GetList(array(), $arSectionFilter);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
		}

		$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
		$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
		if(!$arParams["USE_MAIN_ELEMENT_SECTION"])
			$rsElement->SetSectionContext($arSection);

		if($obElement = $rsElement->GetNextElement())
		{
			$arResult = $obElement->GetFields();
			$arResult = array_merge($arResult, $arUrlTemplates);

			if (strlen($arResult["DETAIL_TEXT"]) > 200)
			{
				$arResult["SHORT_DETAIL_TEXT"] = substr($arResult["DETAIL_TEXT"], 0, 200);
				$pos = strrpos($arResult["SHORT_DETAIL_TEXT"], " ");
				if ($pos !== false)
					$arResult["SHORT_DETAIL_TEXT"] = substr($arResult["SHORT_DETAIL_TEXT"], 0, $pos);
			}
			else
				$arResult["SHORT_DETAIL_TEXT"] = $arResult["DETAIL_TEXT"];
			$arResult['ACTIVE_FROM'] = $arResult['DATE_ACTIVE_FROM'];
			$arResult['ACTIVE_TO'] = $arResult['DATE_ACTIVE_TO'];

			$arResult['MODULES'] = $arResultModules;

			$arResult["CAT_PRICES"] = $arResultPrices;
			$arResult['PRICES_ALLOW'] = $arResultPricesAllow;

			$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult["IBLOCK_ID"], $arResult["ID"]);
			$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

			$arResult["PROPERTIES"] = $obElement->GetProperties();
			unset($arResult["VENDOR"]);
			$vendor_xml_id = trim($arResult["PROPERTIES"]["vendor"]["VALUE"]);
			if ($vendor_xml_id != "")
			{
				$arVendorFilter = array(
					"IBLOCK_ID" => $arParams["VENDORS_IBLOCK_ID"],
					"XML_ID" => $vendor_xml_id,
					"ACTIVE" => "Y",
				);
				$rsVendor = CIBlockElement::GetList(array(), $arVendorFilter, false, array("nTopCount" => 1));
				if ($arResult["VENDOR"] = $rsVendor->GetNext())
				{
					$arResult["VENDOR"]["PREVIEW_PICTURE"] = CFile::GetFileArray($arResult["VENDOR"]["PREVIEW_PICTURE"]);
				}
			}
			$arResult["BIG_PICTURES"] = array();
			$arResult["MIDDLE_PICTURES"] = array();

            /**/
            $bClearFirst = false;
            $i=1;

            while($i<=3)
            {


                if(empty($arResult["PROPERTIES"]["pict".$i]["VALUE"]) && !empty($arResult["PROPERTIES"]["pict".$i]["~VALUE"]))
                {

                    $arResult["PROPERTIES"]["pict".$i]["VALUE"] = $arResult["PROPERTIES"]["pict".$i]["~VALUE"];

                }

                /*if($i==1 && $arResult["PROPERTIES"]["pict".$i]["VALUE"] != "")
                {

                    $bClearFirst = true;

                }
                else
                {

                    if($bClearFirst && $arResult["PROPERTIES"]["pict".$i]["VALUE"] != "")
                    {

                        $arResult["PROPERTIES"]["pict".($i-1)]["VALUE"] = "";

                    }

                }*/

                if($arResult["PROPERTIES"]["pict".$i]["VALUE"] != "")
                {

                    $arResult["PROPERTIES"]["pict".($i)]["VALUE"] = "";

                }

                $i++;

            }


               /**/
			$i = 1;

			while ($i <= 10)
			{

			    if(empty($arResult["PROPERTIES"]["pict".$i]["VALUE"]) && !empty($arResult["PROPERTIES"]["pict".$i]["~VALUE"]))
                {

                    $arResult["PROPERTIES"]["pict".$i]["VALUE"] = $arResult["PROPERTIES"]["pict".$i]["~VALUE"];

                }

				if ($arResult["PROPERTIES"]["pict".$i]["VALUE"] != "")
				{
					$filename =  basename($arResult["PROPERTIES"]["pict".$i]["VALUE"]);
					$exist = false;
					$original_pic_path = "/images/".$filename;
					if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path) && filesize($_SERVER["DOCUMENT_ROOT"].$original_pic_path) > 200)
					{
						$exist = true;
					}
					else
					{
						$content = file_get_contents($arResult["PROPERTIES"]["pict".$i]["VALUE"]);
						if ($content !== false && strlen($content) > 200)
						{
							file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
							if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
							{
								$exist = true;
							}
						}
					}
					if ($exist == true)
					{
						$resized_pic_path = "/resized_images/570x765/".$filename;
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
						{
							$arResult["MIDDLE_PICTURES"][] = $resized_pic_path;
							$arResult["BIG_PICTURES"][] = $original_pic_path;						
						}
						else
						{
							$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;

							//if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>570,"height"=>765), BX_RESIZE_IMAGE_PROPORTIONAL))
							if (ToysalesResizeImage($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, 570, 765, true))
							{
								$arResult["MIDDLE_PICTURES"][] = $resized_pic_path;
								$arResult["BIG_PICTURES"][] = $original_pic_path;
							}
						}
					}
				}

				$i++;

			}
		
			if ($bCatalog && $boolNeedCatalogCache)
			{
				CCatalogDiscount::SetProductPropertiesCache($arResult['ID'], $arResult["PROPERTIES"]);
			}

			$arResult["DISPLAY_PROPERTIES"] = array();
			foreach($arParams["PROPERTY_CODE"] as $pid)
			{
				if (!isset($arResult["PROPERTIES"][$pid]))
					continue;
				$prop = &$arResult["PROPERTIES"][$pid];
				$boolArr = is_array($prop["VALUE"]);
				if(
					($boolArr && !empty($prop["VALUE"]))
					|| (!$boolArr && strlen($prop["VALUE"])>0)
				)
				{
					$arResult["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arResult, $prop, "catalog_out");
				}
			}

			$arResult["PRODUCT_PROPERTIES"] = array();
			$arResult['PRODUCT_PROPERTIES_FILL'] = array();
			if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y' && !empty($arParams["PRODUCT_PROPERTIES"]))
			{
				$arResult["PRODUCT_PROPERTIES"] = CIBlockPriceTools::GetProductProperties(
					$arParams["IBLOCK_ID"],
					$arResult["ID"],
					$arParams["PRODUCT_PROPERTIES"],
					$arResult["PROPERTIES"]
				);
				if (!empty($arResult['PRODUCT_PROPERTIES']))
				{
					$arResult['PRODUCT_PROPERTIES_FILL'] = CIBlockPriceTools::getFillProductProperties($arResult['PRODUCT_PROPERTIES']);
				}
			}

			if(!$arSection && $arResult["IBLOCK_SECTION_ID"] > 0)
			{
				$arSectionFilter = array(
					"ID" => $arResult["IBLOCK_SECTION_ID"],
					"IBLOCK_ID" => $arResult["IBLOCK_ID"],
					"ACTIVE" => "Y",
				);
				$rsSection = CIBlockSection::GetList(Array(),$arSectionFilter);
				$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
				$arSection = $rsSection->GetNext();
			}

			if($arSection)
			{
				$arSection["PATH"] = array();
				$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arSection["ID"]);
				$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
				while($arPath = $rsPath->GetNext())
				{
					$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
					$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
					$arSection["PATH"][] = $arPath;
				}
				$arResult["SECTION"] = $arSection;
			}

			$arResult["PRICE_MATRIX"] = false;
			$arResult["PRICES"] = array();
			$arResult['MIN_PRICE'] = false;

			$arResult["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["CAT_PRICES"], $arResult, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
/*			
			?><pre><?
			print_r($arResult["PRICES"]);
			?></pre><?			
*/			
			if (!empty($arResult['PRICES']))
				$arResult['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arResult['PRICES']);


			$arResult["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["CAT_PRICES"], $arResult);

			$arResult['~BUY_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['~BUY_URL_TEMPLATE']);
			$arResult['BUY_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['BUY_URL_TEMPLATE']);
			$arResult['~ADD_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['~ADD_URL_TEMPLATE']);
			$arResult['ADD_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['ADD_URL_TEMPLATE']);

			if(!isset($arParams["OFFERS_FIELD_CODE"]))
				$arParams["OFFERS_FIELD_CODE"] = array();
			elseif (!is_array($arParams["OFFERS_FIELD_CODE"]))
				$arParams["OFFERS_FIELD_CODE"] = array($arParams["OFFERS_FIELD_CODE"]);
			foreach($arParams["OFFERS_FIELD_CODE"] as $key => $value)
				if($value === "")
					unset($arParams["OFFERS_FIELD_CODE"][$key]);

			if(!isset($arParams["OFFERS_PROPERTY_CODE"]))
				$arParams["OFFERS_PROPERTY_CODE"] = array();
			elseif (!is_array($arParams["OFFERS_PROPERTY_CODE"]))
				$arParams["OFFERS_PROPERTY_CODE"] = array($arParams["OFFERS_PROPERTY_CODE"]);
			foreach($arParams["OFFERS_PROPERTY_CODE"] as $key => $value)
				if($value === "")
					unset($arParams["OFFERS_PROPERTY_CODE"][$key]);

			$arResult["OFFERS"] = array();
			$arResult["ALL_OFFERS"] = array();
			if(
				$bCatalog && $arResult["ACTIVE"] == "Y"
				&& (
					!empty($arParams["OFFERS_FIELD_CODE"])
					|| !empty($arParams["OFFERS_PROPERTY_CODE"])
				)
			)
			{
				$offersFilter = array(
					'IBLOCK_ID' => $arParams['IBLOCK_ID'],
					//'HIDE_NOT_AVAILABLE' => "Y"
				);
				if (!$arParams["USE_PRICE_COUNT"])
				{
					$offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];
				}
/*				
				print_r(array(
						$arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
						$arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
					));
*/					
				$arOffers = CIBlockPriceTools::GetOffersArray(
					$offersFilter,
					array($arResult["ID"]),
					array(
						$arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
						$arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
					),
					$arParams["OFFERS_FIELD_CODE"],
					$arParams["OFFERS_PROPERTY_CODE"],
					0,
					$arResult["CAT_PRICES"],
					$arParams['PRICE_VAT_INCLUDE'],
					$arConvertParams
				);
				foreach($arOffers as $arOffer)
				{
					$arOffer['~BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~BUY_URL_TEMPLATE']);
					$arOffer['BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['BUY_URL_TEMPLATE']);
					$arOffer['~ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~ADD_URL_TEMPLATE']);
					$arOffer['ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['ADD_URL_TEMPLATE']);
					if ($arOffer["CAN_BUY"])
						$arResult["OFFERS"][] = $arOffer;
					$arResult["ALL_OFFERS"][] = $arOffer;
				}
				unset($arOffer);
				unset($arOffers);
			}

			$arResult['USE_CATALOG_BUTTONS'] = $useCatalogButtons;
			unset($useCatalogButtons);

			// Товары раздела
			if ($arResult["SECTION"]["ID"] > 0)
			//if ($arResult["IBLOCK_SECTION_ID"] > 0)
			{
				//echo $arResult["SECTION"]["ID"]."<br>";
				$arFilter = array(
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"IBLOCK_LID" => SITE_ID,
					"IBLOCK_ACTIVE" => "Y",
					"ACTIVE_DATE" => "Y",
					"ACTIVE" => "Y",
					"CHECK_PERMISSIONS" => "Y",
					"MIN_PERMISSION" => "R",
					"INCLUDE_SUBSECTIONS" => "Y",
//					"SECTION_GLOBAL_ACTIVE" => "Y",
					"SECTION_ID" => $arResult["SECTION"]["ID"],
					"!ID" => $arResult["ID"],
				);
				//print_r($arCatalog);
				if ($arCatalog['IBLOCK_ID'] > 0)
				{
					// чтобы в выборку попали только те товары, у которых есть торговые предолжения
					$arSubFilter = array();
					$arSubFilter['CATALOG_AVAILABLE'] = 'Y'; // а это условие обеспечит выборку только тех предложений, которые можно купить
					$arSubFilter["IBLOCK_ID"] = $arCatalog['IBLOCK_ID'];
					$arSubFilter["ACTIVE_DATE"] = "Y";
					$arSubFilter["ACTIVE"] = "Y";
					$arFilter["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$arCatalog["SKU_PROPERTY_ID"], $arSubFilter);
				}
				
				// list of the element fields that will be used in selection
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"CODE",
					"XML_ID",
					"NAME",
					"ACTIVE",
					"SORT",
					"IBLOCK_SECTION_ID",
					"DETAIL_PAGE_URL",
//					"PROPERTY_pict1",
				);
				if ($bIBlockCatalog)
					$arSelect[] = "CATALOG_QUANTITY";				
				//PRICES
				$arPriceTypeID = array();
				foreach($arResultPrices as &$value)
				{
					if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
						continue;
					$arSelect[] = $value["SELECT"];
					$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
				}
				if (isset($value))
					unset($value);				
				$arSort = array(
					"RAND" => "ASC",
					"ID" => "DESC",
				);
				$arNavParams = array("nTopCount"=>3);
				$intKey = 0;
				$arResult["SECT_ITEMS"] = array();
				$arSectElementLink = array();
				$arResult["SECT_ELEMENTS"] = array();

				$rsSectionElements = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
				//echo $arParams["DETAIL_URL"];
				$rsSectionElements->SetUrlTemplates($arParams["DETAIL_URL"]);
				$rsSectionElements->SetSectionContext($arResult["SECTION"]);
				while($arItem = $rsSectionElements->GetNext())
				{
					$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"pict1"));
					if($ar_props = $db_props->Fetch())
						$arItem["PROPERTY_PICT1_VALUE"] = $ar_props["VALUE"];
					
					$arItem["PREVIEW_PICTURE"] = array(
						"SRC" => "",
					);					
					if ($arItem["PROPERTY_PICT1_VALUE"] != "")
					{
		//				echo $arItem["PROPERTY_PICT1_VALUE"]."<br>";									
						$filename =  basename($arItem["PROPERTY_PICT1_VALUE"]);
						$exist = false;
						$original_pic_path = "/images/".$filename;
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
							$exist = true;
						else
						{
							$content = file_get_contents($arItem["PROPERTY_PICT1_VALUE"]);
							if ($content !== false)
							{
								file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
								if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
									$exist = true;
							}
						}
						if ($exist == true)
						{
							$resized_pic_path = "/resized_images/55x55/".$filename;
							if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
								$arItem["PREVIEW_PICTURE"]["SRC"] = $resized_pic_path;
							else
							{
								$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
								if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>55,"height"=>55), BX_RESIZE_IMAGE_PROPORTIONAL))
									$arItem["PREVIEW_PICTURE"]["SRC"] = $resized_pic_path;
							}
						}
						
					}					
					$arResult["SECT_ITEMS"][$intKey] = $arItem;
					$arResult["SECT_ELEMENTS"][$intKey] = $arItem["ID"];
					$arSectElementLink[$arItem['ID']] = &$arResult["SECT_ITEMS"][$intKey];
					$intKey++;
				}
				if (!empty($arResult["SECT_ELEMENTS"]))
				{
					$offersFilter = array(
						'IBLOCK_ID' => $arParams['IBLOCK_ID'],
						'HIDE_NOT_AVAILABLE' => "Y"
					);
					$offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];
	//print_r($arSectElementLink);
					$arOffers = CIBlockPriceTools::GetOffersArray(
						$offersFilter,
						$arResult["SECT_ELEMENTS"],
						array(
							"sort" => "asc",
						),
						array(),
						array(),
						0,
						$arResult["CAT_PRICES"],
						$arParams['PRICE_VAT_INCLUDE'],
						$arConvertParams
					);
					if(!empty($arOffers))
					{
						foreach($arOffers as $arOffer)
						{
						/*	
						?><pre><?
						print_r($arOffer);
						?></pre><?
						*/
							if (isset($arSectElementLink[$arOffer["LINK_ELEMENT_ID"]]))
							{
								//echo $arOffer["LINK_ELEMENT_ID"]."<br>";
								//print_r($arElementLink[$arOffer["LINK_ELEMENT_ID"]]);
								if ($arOffer["CAN_BUY"] and empty($arSectElementLink[$arOffer["LINK_ELEMENT_ID"]]["MIN_PRICE"]))
								{
									$arSectElementLink[$arOffer["LINK_ELEMENT_ID"]]["MIN_PRICE"] = $arOffer["MIN_PRICE"];
									//print_r($arSectElementLink[$arOffer["LINK_ELEMENT_ID"]]);
								}
								//$arElementLink[$arOffer["LINK_ELEMENT_ID"]]['OFFERS'][] = $arOffer;
							}
						}
						unset($arOffer);
					}
				}
			}
			
			//Наличие в магазинах
			$arFilter = array(
				"IBLOCK_ID" => $arParams["SHOPS_IBLOCK_ID"],
			);
			$arSort = array(
				"SORT" => "ASC",
				"ID" => "ASC",
			);
			$rsItems = CIBlockElement::GetList($arSort, $arFilter);
			$arResult["SHOPS"] = array();
			while ($obItem = $rsItems->GetNextElement())
			{
				$arItem = $obItem->GetFields();
				$arItem["PROPERTIES"] = $obItem->GetProperties();
				$arResult["SHOPS"][] = array(
					"NAME" => $arItem["NAME"],
					"ITEMS" => array(),
				);
			}
			foreach ($arResult["ALL_OFFERS"] as $arOffer)
			{
				if (!empty($arOffer["PROPERTIES"]["shop_stock"]["VALUE"]))
				{
					foreach ($arOffer["PROPERTIES"]["shop_stock"]["VALUE"] as $key => $value)
					{
						if (isset($arResult["SHOPS"][$key]) && $value > 0)
						{
							//echo $key." ".$arOffer["DISPLAY_PROPERTIES"]["color"]["DISPLAY_VALUE"]." ".$arOffer["DISPLAY_PROPERTIES"]["size"]["DISPLAY_VALUE"]."<br>";						
							$offer_str = "";
							if ($arOffer["DISPLAY_PROPERTIES"]["color"]["DISPLAY_VALUE"] != "")
								$offer_str .= $arOffer["DISPLAY_PROPERTIES"]["color"]["DISPLAY_VALUE"];
							if ($arOffer["DISPLAY_PROPERTIES"]["size"]["DISPLAY_VALUE"] != "")
							{
								if ($offer_str != "")
									$offer_str .= " ";
								$offer_str .= $arOffer["DISPLAY_PROPERTIES"]["size"]["DISPLAY_VALUE"];
							}
							$arResult["SHOPS"][$key]["ITEMS"][] = $offer_str;
						}
					}
				}
			}
			$resultCacheKeys = array(
				"IBLOCK_ID",
				"ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"LIST_PAGE_URL",
				"CANONICAL_PAGE_URL",
				"PROPERTIES",
				"SECTION",
				"IPROPERTY_VALUES",
				"TIMESTAMP_X",
				"BACKGROUND_IMAGE",
				'USE_CATALOG_BUTTONS',
				"XML_ID",
				"MIN_PRICE",
				"VENDOR",
			);

			if ($bCatalog)
			{
				// catalog hit stats
				$productTitle = !empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])
					? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
					: $arResult["NAME"];

				$categoryId = '';
				$categoryPath = array();

				if (isset($arResult['SECTION']['ID']))
				{
					$categoryId = $arResult['SECTION']['ID'];
				}

				if (isset($arResult['SECTION']['PATH']))
				{
					foreach ($arResult['SECTION']['PATH'] as $cat)
					{
						$categoryPath[$cat['ID']] = $cat['NAME'];
					}
				}

				$counterData = array(
					'product_id' => $arResult["ID"],
					'iblock_id' => $arParams['IBLOCK_ID'],
					'product_title' => $productTitle,
					'category_id' => $categoryId,
					'category' => $categoryPath,
				);

				if (empty($arResult["OFFERS"]))
				{
					$counterData['price'] = (isset($arResult['MIN_PRICE']) ? $arResult['MIN_PRICE']['DISCOUNT_VALUE'] : '');
					$counterData['currency'] = (isset($arResult['MIN_PRICE']) ? $arResult['MIN_PRICE']['CURRENCY'] : '');
				}
				else
				{
					$offer = current($arResult["OFFERS"]);
					$counterData['price'] = (isset($offer['MIN_PRICE']) ? $offer['MIN_PRICE']['DISCOUNT_VALUE'] : '');
					$counterData['currency'] = (isset($offer['MIN_PRICE']) ? $offer['MIN_PRICE']['CURRENCY'] : '');
					unset($offer);
				}

				// make sure it is in utf8
				$counterData = \Bitrix\Main\Text\Encoding::convertEncodingArray($counterData, SITE_CHARSET, 'UTF-8');

				// pack value and protocol version
				$rcmLogCookieName = COption::GetOptionString("main", "cookie_name", "BITRIX_SM")."_".\Bitrix\Main\Analytics\Catalog::getCookieLogName();

				$arResult['counterData'] = array(
					'item' => base64_encode(json_encode($counterData)),
					'user_id' => new \Bitrix\Main\Text\JsExpression(
						"function() {
							return BX.message(\"USER_ID\") ? BX.message(\"USER_ID\") : 0;
						}"
					),
					'recommendation' => new \Bitrix\Main\Text\JsExpression(
						"function() {

							var rcmId = \"\";

							var cookieValue = BX.getCookie(\"{$rcmLogCookieName}\");
							var productId = {$arResult["ID"]};

							var cItems = [],
								cItem;

							if (cookieValue)
							{
								cItems = cookieValue.split('.');
							}

							var i = cItems.length;

							while (i--)
							{
								cItem = cItems[i].split('-');

								if (cItem[0] == productId)
								{
									rcmId = cItem[1];
									break;
								}
							}

							return rcmId;
						}"
					),
					'v' => '2'
				);
				$resultCacheKeys[] = 'counterData';

				if ($arParams['SET_VIEWED_IN_COMPONENT'] == 'Y')
				{
					$viewedProduct = array(
						'PRODUCT_ID' => $arResult['ID'],
						'OFFER_ID' => $arResult['ID']
					);
					if (!empty($arResult['OFFERS']))
						$viewedProduct['OFFER_ID'] = $arResult['OFFERS'][0]['ID'];
					$arResult['VIEWED_PRODUCT'] = $viewedProduct;
					$resultCacheKeys[] = 'VIEWED_PRODUCT';
				}
			}

			$this->SetResultCacheKeys($resultCacheKeys);

			// standard output
			$this->IncludeComponentTemplate();

			if ($bCatalog && $boolNeedCatalogCache)
			{
				CCatalogDiscount::ClearDiscountCache(array(
					'PRODUCT' => true,
					'SECTIONS' => true,
					'PROPERTIES' => true
				));
			}
		}
		else
		{
			$this->AbortResultCache();
			\Bitrix\Iblock\Component\Tools::process404(
				trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_ELEMENT_NOT_FOUND")
				,true
				,$arParams["SET_STATUS_404"] === "Y"
				,$arParams["SHOW_404"] === "Y"
				,$arParams["FILE_404"]
			);
		}
	}
	else
	{
		$this->AbortResultCache();
		\Bitrix\Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_ELEMENT_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
	}
}

if(isset($arResult["ID"]))
{
	if (isset($arResult['counterData']))
	{
		// send counter data
		if (\Bitrix\Main\Analytics\Catalog::isOn())
			\Bitrix\Main\Analytics\Counter::sendData('ct', $arResult['counterData']);
	}

	if ('N' != $arParams['USE_ELEMENT_COUNTER'])
	{
		if (Loader::includeModule('iblock'))
			CIBlockElement::CounterInc($arResult["ID"]);
	}

	if ($arParams['SET_CANONICAL_URL'] === 'Y' && $arResult["CANONICAL_PAGE_URL"])
		$APPLICATION->SetPageProperty('canonical', $arResult["CANONICAL_PAGE_URL"]);

	if (!isset($_SESSION["VIEWED_ENABLE"]) && isset($_SESSION["VIEWED_PRODUCT"]) && $_SESSION["VIEWED_PRODUCT"] != $arResult["ID"] && Loader::includeModule("sale"))
	{
		$_SESSION["VIEWED_ENABLE"] = "Y";
		$arFields = array(
			"PRODUCT_ID" => (int)$_SESSION["VIEWED_PRODUCT"],
			"MODULE" => "catalog",
			"LID" => SITE_ID
		);
		CSaleViewedProduct::Add($arFields);
	}

	if (isset($_SESSION["VIEWED_ENABLE"]) && $_SESSION["VIEWED_ENABLE"] == "Y" && $_SESSION["VIEWED_PRODUCT"] != $arResult["ID"] && Loader::includeModule("sale"))
	{
		$arFields = array(
			"PRODUCT_ID" => $arResult["ID"],
			"MODULE" => "catalog",
			"LID" => SITE_ID,
			"IBLOCK_ID" => $arResult["IBLOCK_ID"]
		);
		CSaleViewedProduct::Add($arFields);
	}

	$_SESSION["VIEWED_PRODUCT"] = $arResult["ID"];

	$arTitleOptions = null;
	if($USER->IsAuthorized())
	{
		if(
			$APPLICATION->GetShowIncludeAreas()
			|| $arParams["SET_TITLE"]
			|| isset($arResult[$arParams["BROWSER_TITLE"]])
		)
		{
			if (Loader::includeModule('iblock'))
			{
				$arReturnUrl = array(
					"add_element" => CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "DETAIL_PAGE_URL"),
					"delete_element" => (
						isset($arResult["SECTION"])?
						$arResult["SECTION"]["SECTION_PAGE_URL"]:
						$arResult["LIST_PAGE_URL"]
					),
				);
				$buttonParams = array(
					'RETURN_URL' => $arReturnUrl,
					'CATALOG' => true
				);
				if (isset($arResult['USE_CATALOG_BUTTONS']))
				{
					$buttonParams['USE_CATALOG_BUTTONS'] = $arResult['USE_CATALOG_BUTTONS'];
					if (!empty($buttonParams['USE_CATALOG_BUTTONS']))
						$buttonParams['SHOW_CATALOG_BUTTONS'] = true;
				}
				$arButtons = CIBlock::GetPanelButtons(
					$arResult["IBLOCK_ID"],
					$arResult["ID"],
					$arResult["IBLOCK_SECTION_ID"],
					$buttonParams
				);
				unset($buttonParams);

				if($APPLICATION->GetShowIncludeAreas())
					$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

				if($arParams["SET_TITLE"] || isset($arResult[$arParams["BROWSER_TITLE"]]))
				{
					$arTitleOptions = array(
						'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_element"]["ACTION"],
						'PUBLIC_EDIT_LINK' => $arButtons["edit"]["edit_element"]["ACTION"],
						'COMPONENT_NAME' => $this->GetName(),
					);
				}
			}
		}
	}

	if($arParams["SET_TITLE"])
	{
		if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
			$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"], $arTitleOptions);
		else
			$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
	}

	if ($arParams["SET_BROWSER_TITLE"] === 'Y')
	{
		$browserTitle = Collection::firstNotEmpty(
			$arResult["PROPERTIES"], array($arParams["BROWSER_TITLE"], "VALUE")
			,$arResult, $arParams["BROWSER_TITLE"]
			,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_TITLE"
		);
		if (is_array($browserTitle))
			$APPLICATION->SetPageProperty("title", implode(" ", $browserTitle), $arTitleOptions);
		elseif ($browserTitle != "")
			$APPLICATION->SetPageProperty("title", $browserTitle, $arTitleOptions);
	}

	if ($arParams["SET_META_KEYWORDS"] === 'Y')
	{
		$metaKeywords = Collection::firstNotEmpty(
			$arResult["PROPERTIES"], array($arParams["META_KEYWORDS"], "VALUE")
			,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_KEYWORDS"
		);
		if (is_array($metaKeywords))
			$APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
		elseif ($metaKeywords != "")
			$APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);
	}

	if ($arParams["SET_META_DESCRIPTION"] === 'Y')
	{
		$metaDescription = Collection::firstNotEmpty(
			$arResult["PROPERTIES"], array($arParams["META_DESCRIPTION"], "VALUE")
			,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_DESCRIPTION"
		);
		if (is_array($metaDescription))
			$APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
		elseif ($metaDescription != "")
			$APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);
	}

	if (!empty($arResult['BACKGROUND_IMAGE']) && is_array($arResult['BACKGROUND_IMAGE']))
		$APPLICATION->SetPageProperty("backgroundImage", 'style="background-image: url(\''.$arResult['BACKGROUND_IMAGE']['SRC'].'\')"');

	if ($arParams["ADD_SECTIONS_CHAIN"] && !empty($arResult["SECTION"]["PATH"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
				$APPLICATION->AddChainItem($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arPath["~SECTION_PAGE_URL"]);
			else
				$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
	if ($arParams["ADD_ELEMENT_CHAIN"])
	{
		if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
			$APPLICATION->AddChainItem($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]);
		else
			$APPLICATION->AddChainItem($arResult["NAME"]);
	}

	if ($arParams["SET_LAST_MODIFIED"] && $arResult["TIMESTAMP_X"])
		Context::getCurrent()->getResponse()->setLastModified(DateTime::createFromUserTime($arResult["TIMESTAMP_X"]));

	if ($arParams['SET_VIEWED_IN_COMPONENT'] == 'Y' && !empty($arResult['VIEWED_PRODUCT']))
	{
		if (Loader::includeModule('catalog') && Loader::includeModule('sale'))
		{
			\Bitrix\Catalog\CatalogViewedProductTable::refresh(
				$arResult['VIEWED_PRODUCT']['OFFER_ID'],
				CSaleBasket::GetBasketUserID(),
				SITE_ID,
				$arResult['VIEWED_PRODUCT']['PRODUCT_ID']
			);
		}
	}

	return $arResult["ID"];
}
else
{
	return 0;
}