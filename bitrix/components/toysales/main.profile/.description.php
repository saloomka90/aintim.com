<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Изменение профиля",
	"DESCRIPTION" => "Изменение профиля",
	"ICON" => "/images/user_register.gif",
	"PATH" => array(
			"ID" => "Toysales",
			"CHILD" => array(
				"ID" => "toysales_user",
				"NAME" => GetMessage("MAIN_USER_GROUP_NAME")
			),
		),	
);
?>