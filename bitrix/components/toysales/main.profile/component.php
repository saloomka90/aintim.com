<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

// start values
$arResult["VALUES"] = array();
$arResult["ERRORS"] = array();
$register_done = false;

$form_fields = array(
	"NAME",
	"LAST_NAME",
	"PERSONAL_GENDER",
	"EMAIL",
);

$required_fields = array(
	"NAME",
	"EMAIL",
);

// register user
$arResult["USER_ID"] = $USER->GetID();
if ($arResult["USER_ID"] > 0)
{
	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_REQUEST["update_profile"]))
	{
		// check emptiness of required fields
		foreach ($required_fields as $key)
		{
			if ($_REQUEST["REGISTER"][$key] == "")
			{
					$arResult["ERRORS"][] = "Не все необходимые поля заполнены";
					break;
			}
		}

		if(strlen($_REQUEST["REGISTER"]["EMAIL"]) > 0)
		{
			$res = CUser::GetList($b, $o, array("LOGIN_EQUAL" => $_REQUEST["REGISTER"]["EMAIL"], "!ID" => $arResult["USER_ID"]));
			if($tmp_user = $res->Fetch())
			{
				$arResult["ERRORS"][] = "Указанный Email уже зарегистрирован в системе";
			}
		}

		if (empty($arResult["ERRORS"])) // if there;s no any errors - update user's profile
		{
			$arResult["VALUES"] = array(
				"LOGIN" => $_REQUEST["REGISTER"]["EMAIL"],
				"EMAIL" => $_REQUEST["REGISTER"]["EMAIL"],
				"NAME" => $_REQUEST["REGISTER"]["NAME"],
				"LAST_NAME" => $_REQUEST["REGISTER"]["LAST_NAME"],
				"PERSONAL_GENDER" => $_REQUEST["REGISTER"]["PERSONAL_GENDER"],
	//			"CONFIRM_PASSWORD" => $_REQUEST["REGISTER"]["CONFIRM_PASSWORD"],
			);
			if ($_REQUEST["REGISTER"]["PASSWORD"] != "")
				$arResult["VALUES"]["PASSWORD"] = $_REQUEST["REGISTER"]["PASSWORD"];

			$user = new CUser();
			if ($user->Update($arResult["USER_ID"], $arResult["VALUES"]))
			{
				LocalRedirect($APPLICATION->GetCurPage()."?result=ok");
			}
			else
				$arResult["ERRORS"][] = $user->LAST_ERROR;
		}
		foreach ($form_fields as $field_code)
		{
			$arResult["FORM_FIELDS"][$field_code] = htmlspecialcharsbx($_REQUEST["REGISTER"][$field_code]);
		}		
	}
	else
	{
		$rsUser = CUser::GetByID($arResult["USER_ID"]);
		if ($arUser = $rsUser->GetNext())
		{
			foreach ($form_fields as $field_code)
			{
				$arResult["FORM_FIELDS"][$field_code] = $arUser[$field_code];
			}		
		}
	}
}

// all done
$this->IncludeComponentTemplate();
?>