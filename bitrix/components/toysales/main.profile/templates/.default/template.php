<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
                                   <div class="information-blocks">
<?
if($arResult["USER_ID"]<= 0)
{
?>
                                <div class="message-box message-danger">
                                    <div class="message-icon"><i class="fa fa-times"></i></div>
                                    <div class="message-text">Вы не авторизованы</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>	
<?								
}
else
{
if ($_REQUEST["result"] == "ok")
{
?>
                                <div class="message-box message-success">
                                    <div class="message-icon"><i class="fa fa-check"></i></div>
                                    <div class="message-text">Изменения сохранены</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>	
<?
}

if (count($arResult["ERRORS"]) > 0)
{
	foreach ($arResult["ERRORS"] as $key => $error)
	{
	?>
                                <div class="message-box message-danger">
                                    <div class="message-icon"><i class="fa fa-times"></i></div>
                                    <div class="message-text"><?=$error?></div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>		
	<?								
	}
}
?>								   
                                        <form method="post" action="<?=$APPLICATION->GetCurPage()?>" name="regform">
										<input type="hidden" name="update_profile" value="Y">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Имя <span>*</span></label>
                                                    <input class="simple-field" value="<?=$arResult["FORM_FIELDS"]["NAME"]?>" type="text" name="REGISTER[NAME]" placeholder="" required  />
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Фамилия </label>
                                                    <input class="simple-field" value="<?=$arResult["FORM_FIELDS"]["LAST_NAME"]?>" type="text" name="REGISTER[LAST_NAME]" placeholder="" />
                                                    <div class="clear"></div>
                                                </div>                                                

                                                <div class="col-sm-12">
                                                    <label>Email <span>*</span></label>
                                                    <input class="simple-field" value="<?=$arResult["FORM_FIELDS"]["EMAIL"]?>" type="email" name="REGISTER[EMAIL]" placeholder="" />
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Пол</label>
                                                    <div class="simple-drop-down simple-field">
                                                        <select name="REGISTER[PERSONAL_GENDER]">
                                                            <option value="">неизвестно</option>
                                                            <option value="M"<?if ($arResult["FORM_FIELDS"]["PERSONAL_GENDER"] == "M") echo " selected"?>>Мужской</option>
                                                            <option value="F"<?if ($arResult["FORM_FIELDS"]["PERSONAL_GENDER"] == "F") echo " selected"?>>Женский</option>
                                                        </select>
                                                    </div>
                                                <div class="clear"></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>Изменить пароль</label>
                                                    <input class="simple-field" type="password" name="REGISTER[PASSWORD]" placeholder="Введите пароль" />
                                                    <div class="clear"></div>
                                                </div>     
                                                <div class="col-sm-6">
                                                    <a href="#" class="button style-12" id="update_info">Обновить информацию</a>
                                                </div>                                                                                            
                                            </div>
                                            
                                        </form>
<?
}
?>										
                                    </div>