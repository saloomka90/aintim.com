<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Товары производителя",
	"DESCRIPTION" => "",
	"ICON" => "/images/cat_list.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 30,
	"PATH" => array(
		"ID" => "Toysales",
		"CHILD" => array(
			"ID" => "toysales_catalog",
			"NAME" => GetMessage("T_IBLOCK_DESC_CATALOG"),
			"SORT" => 30,
/*
			"CHILD" => array(
				"ID" => "toysales_catalog_cmpx",
			),
*/
		),
	),
);

?>