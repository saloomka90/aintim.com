<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="information-blocks">
                                <h1 class="block-title"><?=$arResult["VENDOR"]["NAME"]?></h1>
                                <div class="from-the-blog-entry">
<?
if ($arResult["VENDOR"]["DETAIL_PICTURE"]["SRC"] != "")
{
?>								
                                   <img alt="<?=$arResult["VENDOR"]["NAME"]?>" src="<?=$arResult["VENDOR"]["DETAIL_PICTURE"]["SRC"]?>">
<?
}
?>
                                    <div class="description"><?=$arResult["VENDOR"]["DETAIL_TEXT"]?></div>
                                    <div class="clear"></div>
                                </div>
                            </div> 						
                            <div class="page-selector">
<?
if ($arResult["NAV_STRING"] != "")							
{
?>
<div class="pages-box hidden-xs">
<?
	echo $arResult["NAV_STRING"];
?>
</div>
<?
}								
?>								
                                <div class="shop-grid-controls">
                                    <div class="entry">
                                        <div class="inline-text">Сортировать по</div>
                                        <div class="simple-drop-down">
											<select id="select_sort">
                                                <option value="base">По умолчанию</option>
                                                <option value="price_desc"<?if ($_REQUEST["sort"] == "price_desc") echo " selected";?>>Сначала дорогие</option>
                                                <option value="price_asc"<?if ($_REQUEST["sort"] == "price_asc") echo " selected";?>>Сначала дешевые</option>
                                                <option value="new"<?if ($_REQUEST["sort"] == "new") echo " selected";?>>Новинки</option>
                                                <option value="bestseller"<?if ($_REQUEST["sort"] == "bestseller") echo " selected";?>>Хиты продаж</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="entry">
                                        <div class="view-button active grid"><i class="fa fa-th"></i></div>
                                        <div class="view-button list"><i class="fa fa-list"></i></div>
                                    </div>
                                    <div class="entry">
                                        <div class="inline-text"></div>
                                        <div class="simple-drop-down" style="width: 75px;">
											<select id="select_count">
												<option value="12" <?if ($arParams["PAGE_ELEMENT_COUNT"] == 12) echo " selected";?>>12</option>
												<option value="20" <?if ($arParams["PAGE_ELEMENT_COUNT"] == 20) echo " selected";?>>20</option>
												<option value="30" <?if ($arParams["PAGE_ELEMENT_COUNT"] == 30) echo " selected";?>>30</option>
												<option value="40" <?if ($arParams["PAGE_ELEMENT_COUNT"] == 40) echo " selected";?>>40</option>
                                            </select>
                                        </div>
                                        <div class="inline-text">на странице</div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
<?
if (!empty($arResult['ITEMS']))
{
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));

	$strFullPath = $_SERVER['DOCUMENT_ROOT'].$this->GetFolder();
	$templateLibrary = array();
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);
?>
<div class="row shop-grid grid-view">
<?	
		foreach ($arResult["ITEMS"] as $arItem)
		{
			if (!empty($arItem["OFFERS"]))
			{
		?>
                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
											<?
											if (is_array($arItem["PICTURES"]))
											{
												if ($arItem["PICTURES"][0] != "")
												{
												?>
												<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
														<img src="<?=$arItem["PICTURES"][0]?>" alt=""  />
												<?
													if ($arItem["PICTURES"][1] != "")
													{
													?>
														<img src="<?=$arItem["PICTURES"][1]?>" alt=""  />
													<?
													}
													else
													{
													?>
														<img src="<?=$arItem["PICTURES"][0]?>" alt=""  />
													<?
													}
												?></a><?
												}

											}
											$arOffer = current($arItem["OFFERS"]);
											$minPrice = $arOffer["MIN_PRICE"];
											if (!empty($minPrice) and $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
											{
											?>
											<div class="product-image-label type-4"><span>-<?=$minPrice['DISCOUNT_DIFF_PERCENT']?>%</span></div>
											<?
											}
											else
											if ($arItem["PROPERTIES"]["new"]["VALUE"] == "1")
											{
											?><div class="product-image-label type-1"><span>new</span></div><?
											}
											else
											if ($arItem["PROPERTIES"]["bestseller"]["VALUE"] == "1")
											{
											?><div class="product-image-label type-5"><span>хит</span></div><?
											}
											?>                                            
                                                        <a class="top-line-a right open-product" rel="<?=$arItem["ID"]?>"><i class="fa fa-expand"></i> <span>Увеличить</span></a>
                                        </div>

                                        <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                        <div class="article-container style-1">
                                            <p><?=$arItem["DETAIL_TEXT"]?> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#full_text" class="continue-link">На страницу товара <i class="fa fa-long-arrow-right"></i></a></p>
                                        </div>
                                        <div class="price">
<?													
	if (!empty($minPrice))
	{
		if ($minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
		{
		?>
		                                <div class="prev"><? echo $minPrice['PRINT_VALUE']; ?></div>
		<?
		}
	?>
                                        <div class="current"><?=$minPrice['PRINT_DISCOUNT_VALUE'];?></div>
	<?
	}													
?>
                                        </div>
										<?
														if (count($arItem["OFFERS"]) == 1)
														{
															$arOffer = current($arItem["OFFERS"]);
														?>
                                        <a class="button style-15 buy_item2" href="<?=$arOffer["~ADD_URL"]?>" id="buy_item_<?=$arOffer["ID"]?>"><i class="fa fa-shopping-cart"></i> В корзину</a>
                                        <a class="button style-15" style="display:none" id="in_basket_<?=$arOffer["ID"]?>" href="<?=$arParams["BASKET_URL"]?>"><i class="fa fa-shopping-cart"></i> Товар в корзине</a>
														<?
														}
														else
														if (count($arItem["OFFERS"]) > 1)
														{
														?>
																<a class="button style-15 open-product" href="<?=$arOffer["~ADD_URL"]?>" rel="<?=$arItem["ID"]?>"><i class="fa fa-shopping-cart"></i> В корзину</a>
														<?
														}
										?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
<?
			}
		}
?>
</div>
<?
}
?>
                            <div class="page-selector">
<?
if ($arResult["NAV_STRING"] != "")							
{
?>
<div class="pages-box">
<?
	echo $arResult["NAV_STRING"];
?>
</div>
<?
}								
?>
                                <div class="clear"></div>
                            </div>
                        </div>
<script>
$(document).ready(function()
{
	$("#select_count").change(function()
	{
		if ($(this).val() != "")
		{
				var base_location = "<?=$APPLICATION->GetCurPageParam("count=#count#",array("count"))?>";
				base_location = base_location.replace("#count#", $(this).val());
				document.location = base_location;	
		}
	}
	);
	
	$("#select_sort").change(function()
	{
		if ($(this).val() == "")
		{
				var base_location = "<?=$APPLICATION->GetCurPageParam("",array("sort"))?>";
		}
		else
		{
				var base_location = "<?=$APPLICATION->GetCurPageParam("sort=#sort#",array("sort"))?>";
				base_location = base_location.replace("#sort#", $(this).val());
		}
		document.location = base_location;	
	}
	);
	
}
);
</script>						
<?