<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
                <div class="contact-spacer"></div>
                
                <div class="information-blocks">
                    <div class="clear"></div>
                    <div class="row">
                        <div class="col-md-8 information-entry">
                            <h3 class="block-title main-heading">Оставьте свой отзыв</h3>
<?
	if (!empty($arResult["ERRORS"]))
	{
		foreach ($arResult["ERRORS"] as $key => $error)
		{
		?>
									<div class="message-box message-danger">
										<div class="message-icon"><i class="fa fa-times"></i></div>
										<div class="message-text"><?=$error?></div>
										<div class="message-close"><i class="fa fa-times"></i></div>
									</div>		
		<?								
		}
	}	
?>	
                            <form action="<?=POST_FORM_ACTION_URI?>" method="POST">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Ваше Имя <span>*</span></label>
                                        <input class="simple-field" type="text" name="response_name" placeholder="Введите ваше имя (обязательно)" required value="<?=$arResult["POST"]["response_name"]?>" />
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Ваш E-mail <span>*</span></label>
                                        <input class="simple-field" type="email" name="response_email" placeholder="Введите ваш e-mail (обязательно)" required value="<?=$arResult["POST"]["response_email"]?>" />
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Номер Заказа <span>*</span></label>
                                        <input class="simple-field" type="text" name="response_order_id" placeholder="Введите номер заказа (обязательно)" required value="<?=$arResult["POST"]["response_order_id"]?>" />
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Ваша оценка магазину </label>
                                            <div class="simple-drop-down simple-field">
                                                <select name="response_mark">
                                                    <option value="5"<?if ($_REQUEST["response_mark"] == 5) echo ' selected';?>>Отличный магазин</option>
                                                    <option value="4"<?if ($_REQUEST["response_mark"] == 4) echo ' selected';?>>Хороший магазин</option>
                                                    <option value="3"<?if ($_REQUEST["response_mark"] == 3) echo ' selected';?>>Обычный магазин</option>
                                                    <option value="2"<?if ($_REQUEST["response_mark"] == 2) echo ' selected';?>>Плохой магазин</option>
                                                    <option value="1"<?if ($_REQUEST["response_mark"] == 1) echo ' selected';?>>Ужасный магазин</option>
                                                </select>
                                            </div>
                                        <div class="clear"></div>
                                    </div>                                    
                                    <div class="col-sm-12">
                                        <label>Ваш Отзыв <span>*</span></label>
                                        <textarea class="simple-field" name="response_text" placeholder="Напишите ваш отзыв (обязательно)" required><?=$arResult["POST"]["response_text"]?></textarea>
                                        <div class="clear"></div>
                                    </div>
<?
if (!$arResult["USER"]["IS_AUTHORIZED"] and !empty($arResult["CAPTCHA_CODE"]))
{
?>
                                    <div class="col-sm-6">
										<input type="hidden" name="captcha_code" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                        <label>Код с картинки <span>*</span></label>
										<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br><br>
                                        <input class="simple-field" type="text" name="captcha_word" placeholder="Введите символы с картинки (обязательно)" required value="" />
                                        <div class="clear"></div>
                                    </div>
<?
}
?>                                    

                                    <div class="col-sm-12">
                                        <div class="button style-10">Отправить Отзыв<input type="submit" name="add_response" value="Y" /></div>
                                        <div class="clear"></div>
                                    </div>                                    
                                    
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4 information-entry">
                            <h3 class="block-title main-heading">Как писать отзыв</h3>
                            <div class="article-container style-1">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include_areas/responses.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>
                                    <div class="col-sm-12">
                                        <a href="<?=$APPLICATION->GetCurDir()?>"><div class="button style-2">Прочитать отзывы покупателей</div></a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>