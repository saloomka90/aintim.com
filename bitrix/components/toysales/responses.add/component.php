<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock") or !CModule::IncludeModule('catalog'))
{
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return 0;
}
//$this->setFrameMode(false);
/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";

$rsIBlock = CIBlock::GetList(array(), array(
	"ACTIVE" => "Y",
	"ID" => $arParams["IBLOCK_ID"],
	"SITE_ID" => SITE_ID,
));

if($arResult = $rsIBlock->GetNext())
{
	$arResult["USER"]["IS_AUTHORIZED"] = $USER->IsAuthorized();
	$arResult["USER"]["FIRST_NAME"] = $USER->GetFirstName();
	if ($_REQUEST["add_response"] == "Y") // запрос на добавление отзыва
	{
		$arResult["POST"]["response_name"] = htmlspecialcharsbx($_REQUEST["response_name"]);
		$arResult["POST"]["response_email"] = htmlspecialcharsbx($_REQUEST["response_email"]);
		$arResult["POST"]["response_text"] = htmlspecialcharsbx($_REQUEST["response_text"]);
		$arResult["POST"]["response_order_id"] = htmlspecialcharsbx($_REQUEST["response_order_id"]);

		$arResult["REQUIRED_FIELDS"] = array(
			"response_name",
			"response_email",
			"response_text",
			"response_order_id",
		);		
		if (!$arResult["USER"]["IS_AUTHORIZED"])
			$arResult["REQUIRED_FIELDS"][] = "captcha_word";
		foreach ($arResult["REQUIRED_FIELDS"] as $key)
		{
			if ($_REQUEST[$key] == "")
			{
					$arResult["ERRORS"][] = "Не все необходимые поля заполнены";
					break;
			}
		}
/*	
		if ($_REQUEST["response_name"] == "")
			$arResult["ERRORS"][] = "Введите ваше имя";

		if ($_REQUEST["response_email"] == "")
			$arResult["ERRORS"][] = "Введите ваш e-mail";
		
		if ($_REQUEST["response_text"] == "")
			$arResult["ERRORS"][] = "Введите текст отзыва";
				
		if ($_REQUEST["response_order_id"] == "")
			$arResult["ERRORS"][] = "Введите номер заказа";
*/
		if (!$arResult["USER"]["IS_AUTHORIZED"])
		{
			if ($_REQUEST["captcha_word"] != "")
			{
				include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
				$cpt = new CCaptcha();
				if (strlen($_REQUEST["captcha_code"]) > 0)
				{
					$captchaPass = COption::GetOptionString("main", "captcha_password", "");
					if (!$cpt->CheckCodeCrypt($_REQUEST["captcha_word"], $_REQUEST["captcha_code"], $captchaPass))
					{
						$arResult["ERRORS"][] = "Указан неправильный код проверки";
					}
				}
				else
				{
					if (!$cpt->CheckCode($_REQUEST["captcha_word"], 0))
					{
							$arResult["ERRORS"][] = "Указан неправильный код проверки";
					}
				}
			}
		}
		if (empty($arResult["ERRORS"]))
		{
			$el = new CIBlockElement;

			$mark = intval($_REQUEST["response_mark"]);
			if ($mark < 1 or $mark > 5)
				$mark = 5;
			$PROP = array(
				"order_id" => intval($_REQUEST["response_order_id"]),
				"email" => $_REQUEST["response_email"],
				"mark" => $mark,
			);

			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"PROPERTY_VALUES"=> $PROP,
				"NAME" => $_REQUEST["response_name"],
				"PREVIEW_TEXT" => $_REQUEST["response_text"],
				"ACTIVE" => "Y"
			);
			if ($arResult["USER"]["IS_AUTHORIZED"])
			{
				$response_result = "auth_ok";
			}
			else
			{
				$response_result = "ok";
			}
			if ($PRODUCT_ID = $el->Add($arLoadProductArray))
			{
				$arEventFields = array(
					"LINK" => "http://".SITE_SERVER_NAME."/bitrix/admin/iblock_element_edit.php?ID=".$PRODUCT_ID."&type=".$arParams["IBLOCK_TYPE"]."&lang=ru&IBLOCK_ID=".$arParams["IBLOCK_ID"],
				);
				CEvent::Send("NEW_SHOP_RESPONSE", SITE_ID, $arEventFields);
				//LocalRedirect($APPLICATION->GetCurPageParam("response_result=".$response_result, array("response_result"))."#reviews");
				LocalRedirect($APPLICATION->GetCurDir()."?result=ok");
			}
			else
				$arResult["ERRORS"][] = $el->LAST_ERROR;
		}
	}
	if (!$arResult["USER"]["IS_AUTHORIZED"])
	{
		include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
		$cpt = new CCaptcha();
		$captchaPass = COption::GetOptionString("main", "captcha_password", "");
		if (strLen($captchaPass) <= 0)
		{
			$captchaPass = randString(10);
			COption::SetOptionString("main", "captcha_password", $captchaPass);
		}
		$cpt->SetCodeCrypt($captchaPass);
		$arResult["CAPTCHA_CODE"] = htmlspecialcharsbx($cpt->GetCodeCrypt());
	}
	$this->IncludeComponentTemplate();
}
else
{
	$this->AbortResultCache();
	ShowError("Инфоблок не найден");
	@define("ERROR_404", "Y");
}

if(isset($arResult["ID"]))
{
	if($USER->IsAuthorized())
	{
		if($GLOBALS["APPLICATION"]->GetShowIncludeAreas() && CModule::IncludeModule("iblock"))
			$this->AddIncludeAreaIcons(CIBlock::ShowPanel($arResult["ID"], 0, 0, $arResult["IBLOCK_TYPE_ID"], true));
		if($arParams["DISPLAY_PANEL"] && CModule::IncludeModule("iblock"))
			CIBlock::ShowPanel($arResult["ID"], 0, 0, $arResult["IBLOCK_TYPE_ID"], false, $this->GetName());
	}
}
?>