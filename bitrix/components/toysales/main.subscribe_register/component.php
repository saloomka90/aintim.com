<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

// apply default param values
$arDefaultValues = array(
	"AUTH" => "Y",
);

foreach ($arDefaultValues as $key => $value)
{
	if (!is_set($arParams, $key))
		$arParams[$key] = $value;
}
$result = array();
// if user registration blocked - return auth form
if (COption::GetOptionString("main", "new_user_registration", "N") == "N")
	$result["ERROR"] = "Регистрация не доступна";

$arResult["USE_EMAIL_CONFIRMATION"] = COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" ? "Y" : "N";
$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
if($def_group <> "")
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(explode(",", $def_group));
else
	$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy(array());

$register_done = false;

// register user
if (!$USER->IsAuthorized())
{
	if(strlen($_REQUEST["email"]) > 0)
	{
		$res = CUser::GetList($b, $o, array("LOGIN" => $_REQUEST["email"]));
		if($res->Fetch())
			$result["ERROR"] = "<b>Ошибка!</b> Указанный E-mail уже зарегистрирован в системе!";
	}
	else
		$result["ERROR"] = "Укажите E-mail";

	if ($result["ERROR"] == "")
	{
		$bConfirmReq = COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y";

		$email_arr = explode("@", $_REQUEST["email"], 2);
		
		$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if($def_group!="")
		{
			$GROUP_ID = explode(",", $def_group);
			$arPolicy = $USER->GetGroupPolicy($GROUP_ID);
		}
		else
		{
			$arPolicy = $USER->GetGroupPolicy(array());
		}

		$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
		if($password_min_length <= 0)
			$password_min_length = 6;
		$password_chars = array(
			"abcdefghijklnmopqrstuvwxyz",
			"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
			"0123456789",
		);
		if($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
			$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
					
		$arResult["VALUES"] = array(
			"LOGIN" => $_REQUEST["email"],
			"EMAIL" => $_REQUEST["email"],
			"NAME" => trim($email_arr[0]),
			"PASSWORD" => $_REQUEST["REGISTER"]["PASSWORD"],
			"CONFIRM_PASSWORD" => $_REQUEST["REGISTER"]["PASSWORD"],
		);
		$arResult["VALUES"]["PASSWORD"] = $arResult["VALUES"]["CONFIRM_PASSWORD"] = randString($password_min_length+2, $password_chars);		
		
		$arResult['VALUES']["CHECKWORD"] = randString(8);
		$arResult['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
		$arResult['VALUES']["ACTIVE"] = $bConfirmReq? "N": "Y";
		$arResult['VALUES']["CONFIRM_CODE"] = $bConfirmReq? randString(8): "";
		$arResult['VALUES']["LID"] = SITE_ID;

		$arResult['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
		$arResult['VALUES']["USER_HOST"] = @gethostbyaddr($REMOTE_ADDR);

		$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if($def_group != "")
			$arResult['VALUES']["GROUP_ID"] = explode(",", $def_group);

		$bOk = true;

		$events = GetModuleEvents("main", "OnBeforeUserRegister");
		while($arEvent = $events->Fetch())
		{
			if(ExecuteModuleEventEx($arEvent, array($arResult['VALUES'])) === false)
			{
				if($err = $APPLICATION->GetException())
				{
					$result["ERROR"] = $err->GetString();
					break;
				}

				$bOk = false;
				break;
			}
		}
//print_r($arResult['VALUES']);
		if ($bOk)
		{
			$user = new CUser();
			$ID = $user->Add($arResult["VALUES"]);
		}

		if (intval($ID) > 0)
		{
			$register_done = true;
			$result["REGISTER"] = "Y";

			// authorize user
			if ($arParams["AUTH"] == "Y" && $arResult["VALUES"]["ACTIVE"] == "Y")
			{
				if (!$arAuthResult = $USER->Login($arResult["VALUES"]["LOGIN"], $arResult["VALUES"]["PASSWORD"]))
					$result["ERROR"] = $arAuthResult;
			}

			$arResult['VALUES']["USER_ID"] = $ID;

			$arEventFields = $arResult['VALUES'];
			//unset($arEventFields["PASSWORD"]);
			//unset($arEventFields["CONFIRM_PASSWORD"]);

			$event = new CEvent;
			$event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);
			if($bConfirmReq)
			{
				$event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);
				$result["EMAIL_CONFIRMATION_SENT"] = "Y";
			}
		}
		else
		{
			$result["ERROR"] = $user->LAST_ERROR;
		}
/*
		if(count($arResult["ERRORS"]) <= 0)
		{
			if(COption::GetOptionString("main", "event_log_register", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER", "main", $ID);
		}
		else
		{
			if(COption::GetOptionString("main", "event_log_register_fail", "N") === "Y")
				CEventLog::Log("SECURITY", "USER_REGISTER_FAIL", "main", $ID, implode("<br>", $arResult["ERRORS"]));
		}
*/
		$events = GetModuleEvents("main", "OnAfterUserRegister");
		while ($arEvent = $events->Fetch())
			ExecuteModuleEventEx($arEvent, array($arResult['VALUES']));
	}
}
$APPLICATION->RestartBuffer();
//echo CUtil::PhpToJSObject($result);
echo json_encode($result);
die();


?>