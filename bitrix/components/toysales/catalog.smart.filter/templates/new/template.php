<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/*
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
*/
?>
<?$this->SetViewTarget("smart_filter");?>
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">

			<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>
				<?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
				{
					$key = $arItem["ENCODED_ID"];
					if(isset($arItem["PRICE"])):
						if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
							continue;

						$precision = 2;
						if (Bitrix\Main\Loader::includeModule("currency"))
						{
							$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
							$precision = $res['DECIMALS'];
						}
						?>
						<!--<pre><?print_r($arItem)?></pre>-->
                            <div class="information-blocks">
                                <div class="block-title size-2">Цена</div>

											<input
												class="min-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												style="display:none"
											/>
											<input
												class="max-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												style="display:none"
											/>
<?
if ($arItem["VALUES"]["MIN"]["HTML_VALUE"] > $arItem["VALUES"]["MIN"]["VALUE"])
	$cur_min = $arItem["VALUES"]["MIN"]["HTML_VALUE"];
else
	$cur_min = $arItem["VALUES"]["MIN"]["VALUE"];
if ($arItem["VALUES"]["MAX"]["HTML_VALUE"] > $cur_min)
	$cur_max = $arItem["VALUES"]["MAX"]["HTML_VALUE"];
else
	$cur_max = $arItem["VALUES"]["MAX"]["VALUE"];
$cur_min = intval($cur_min);
$cur_max = intval($cur_max);
?>								
                                <div class="range-wrapper">
                                    <div id="range_<?=$key?>"></div>
                                    <div class="range-price">
                                        Цена: 
                                        <div class="slider-val" id="min-slider-val-<?=$key?>"><b><span><?=$cur_min?></span> руб.</b></div>
                                        <b>-</b>
                                        <div class="slider-val" id="max-slider-val-<?=$key?>"><b><span><?=$cur_max?></span> руб.</b></div>
                                    </div>
                                </div>
                            </div>
<?
									$arJsParams = array(
										"sliderId" => "range_".$key,
										"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
										"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
										"minValId" => "min-slider-val-".$key,
										"maxValId" => "max-slider-val-".$key,										
										"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
										"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
										"curMinPrice" => $cur_min,
										"curMaxPrice" => $cur_max,
										"step" => 100,
									);
									?>

									<script type="text/javascript">
										$(document).ready(function(){
											FilterSlider(<?=CUtil::PhpToJSObject($arJsParams)?>);
										});
									</script>
									
	
					<?endif;
				}

				//not prices
				foreach($arResult["ITEMS"] as $key=>$arItem)
				{
					if(
						empty($arItem["VALUES"])
						|| isset($arItem["PRICE"])
					)
						continue;

					if (
						$arItem["DISPLAY_TYPE"] == "A"
						&& (
							$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
						)
					)
						continue;
					?>
                        <div class="information-blocks">
                            <div class="block-title size-2"><?=$arItem["NAME"]?></div>

							<?
							$arCur = current($arItem["VALUES"]);

							switch ($arItem["DISPLAY_TYPE"])
							{
								case "A"://NUMBERS_WITH_SLIDER
								?>
											<input
												class="min-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
												size="5"
												style="display:none"
											/>
											<input
												class="max-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
												size="5"
												style="display:none"
											/>
<?
if ($arItem["VALUES"]["MIN"]["HTML_VALUE"] > $arItem["VALUES"]["MIN"]["VALUE"])
	$cur_min = $arItem["VALUES"]["MIN"]["HTML_VALUE"];
else
	$cur_min = $arItem["VALUES"]["MIN"]["VALUE"];
if ($arItem["VALUES"]["MAX"]["HTML_VALUE"] > $cur_min)
	$cur_max = $arItem["VALUES"]["MAX"]["HTML_VALUE"];
else
	$cur_max = $arItem["VALUES"]["MAX"]["VALUE"];
$cur_min = intval($cur_min);
$cur_max = intval($cur_max);
?>											
                                <div class="range-wrapper">
                                    <div id="range_<?=$key?>"></div>
                                    <div class="range-price">
                                        <?=$arItem["NAME"]?>: 
                                        <div class="slider-val" id="min-slider-val-<?=$key?>"><b><span><?=$cur_min?></span> см.</b></div>
                                        <b>-</b>
                                        <div class="slider-val" id="max-slider-val-<?=$key?>"><b><span><?=$cur_max?></span> см.</b></div>
                                    </div>
                                </div>
								<?
									$arJsParams = array(
										"sliderId" => "range_".$key,
										"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
										"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
										"minValId" => "min-slider-val-".$key,
										"maxValId" => "max-slider-val-".$key,										
										"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
										"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
										"curMinPrice" => $cur_min,
										"curMaxPrice" => $cur_max,
										"step" => 1,
									);
									?>

									<script type="text/javascript">
										$(document).ready(function(){
											FilterSlider(<?=CUtil::PhpToJSObject($arJsParams)?>);
										});
									</script>
								<?									
									break;
								case "G"://CHECKBOXES_WITH_PICTURES

								$new_arr = array();
								foreach ($arItem["VALUES"] as $val => $ar)
								{
									$new_arr[$val] = $ar["ELEMENT_COUNT"];
								}
								arsort($new_arr);
?>

                                <div class="color-selector detail-info-entry">
									<input
										style="display: none"
										type="radio"
										value=""
										name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
										id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
									/>	
									<label for="<? echo "all_".$arCur["CONTROL_ID"] ?>" data-role="label_<? echo "all_".$arCur["CONTROL_ID"] ?>" class="bx-filter-param-label" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')); unset_active(this);">
									<div style="background: url('/img/all_colors.gif');" class="entry"></div>
									</label>								
									<?
									$i = 0;
									$checked_found = false;
									foreach ($new_arr as $val => $el_count)
									{
										$ar = $arItem["VALUES"][$val];
										if (($ar["DISABLED"] and !$ar["CHECKED"]) or $ar["VALUE"] == "") //пропускаем цвета, для которых нет элементов по условиям данного фильтра
											continue;
										$i++;
										if ($i > 20)
										{
											if ($checked_found == true)
												break;
											else
											if (!$arItem["VALUES"][$val]["CHECKED"])
												continue;
										}
										?>
													<input
														style="display: none"
														type="radio"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													/>										
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " active";
										?>
										<label title="<?=$ar["VALUE"]?> (<?=$el_count?>)" for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); unset_active(this);">
												<?
												if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"]))
												{
													?>
												<div class="entry<?=$class?>" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></div>
													<?
												}
												else
												{
													?>
												<div class="entry<?=$class?>" style="background-image:url('/img/no_pict.gif');"></div>
													<?
												}													
												?>
										</label>										
										<?
										if ($arItem["VALUES"][$val]["CHECKED"])
											$checked_found = true;
									}
									?>									
                                    <div class="spacer"></div>
                                </div>
<?								
									break;
								case "P"://DROPDOWN
								$new_arr = array();
								//print_r($arItem["VALUES"]);
								foreach ($arItem["VALUES"] as $val => $ar)
								{
									$new_arr[$val] = $ar["ELEMENT_COUNT"];
								}
								arsort($new_arr);								
								?>
                                    <div class="simple-drop-down simple-field">
                                        <select name="<? echo $arCur["CONTROL_NAME_ALT"] ?>" onChange="smartFilter.click(this)">
											<option value="">Все</option>
<?
									$i = 0;

									$checked_found = false;
									foreach ($new_arr as $val => $el_count)
									{
										$ar = $arItem["VALUES"][$val];
										if ($ar["DISABLED"] and !$ar["CHECKED"]) //пропускаем размеры, для которых нет элементов по условиям данного фильтра
											continue;
										
										$i++;
										if ($i > 20)
										{
											if ($checked_found == true)
												break;
											else
											if (!$arItem["VALUES"][$val]["CHECKED"])
												continue;
										}
										?>
										<option value="<? echo $ar["HTML_VALUE_ALT"] ?>" <? echo $ar["CHECKED"]? 'selected': '' ?>><?=$ar["VALUE"];?></option>
										<?
										if ($arItem["VALUES"][$val]["CHECKED"])
											$checked_found = true;												
									}
?>                                            
                                        </select>
                                    </div>								
								<?
									break;
								case "K"://RADIO_BUTTONS
?>
                                <div class="row">
                                    <div class="col-xs-6">

										<label class="checkbox-entry radio" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
												<input
													type="radio"
													value=""
													name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
													id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
													onclick="smartFilter.click(this)"
												/>
												<span class="check"></span> Все
										</label>
									<?
									$new_arr = array();
									$items_cnt = 0;
									foreach ($arItem["VALUES"] as $val => $ar)
									{
										$new_arr[$val] = $ar["ELEMENT_COUNT"];
										if ($ar["CHECKED"] or !$ar["DISABLED"])
											$items_cnt++;
									}
									arsort($new_arr);
									if ($items_cnt > 20)
										$items_cnt = 20;
									$column_items_cnt = ceil(($items_cnt + 1) / 2);
									//echo $column_items_cnt;
									?>
									<?
									$i = 0;
									$k = 1;
									$checked_found = false;
									foreach ($new_arr as $val => $el_count)
									{
										$ar = $arItem["VALUES"][$val];
										if ($ar["DISABLED"] and !$ar["CHECKED"]) //пропускаем размеры, для которых нет элементов по условиям данного фильтра
											continue;
										
										$i++;
										$k++;
										if ($i > 20)
										{
											if ($checked_found == true)
												break;
											else
											if (!$arItem["VALUES"][$val]["CHECKED"])
												continue;
										}
										if ($k > $column_items_cnt)
										{
											$k = 0;
										?>
										</div><div class="col-xs-6">
										<?
										}
										?>
											<label class="checkbox-entry radio" for="<? echo $ar["CONTROL_ID"] ?>">
													<input
														type="radio"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
														onclick="smartFilter.click(this)"
													/>
													<span class="check"></span> <?=$ar["VALUE"];?>
											</label>
										<?
										if ($arItem["VALUES"][$val]["CHECKED"])
											$checked_found = true;										
									}
									?>
									</div>
								</div>
<?								
									break;
								default://CHECKBOXES
									break;								
							}
							?>

						</div>
				<?
				}
				?>
  
                            <div class="information-blocks">
                                <div class="cart-buttons">
                                    <div class="column">
                                        <a class="button style-3" href="<?=$arResult["JS_FILTER_PARAMS"]["SEF_DEL_FILTER_URL"]?>">сбросить</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>                                
                            </div>    
		</form>
		<br><br>
<?$this->EndViewTarget("smart_filter");?>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>