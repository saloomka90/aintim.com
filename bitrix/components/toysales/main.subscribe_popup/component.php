<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
	
$arResult = array();
$settings = array(
	'TOYSALES_POPUP_PICTURE',
	'TOYSALES_POPUP_SHOW',
	'TOYSALES_POPUP_HTML',
	'TOYSALES_POPUP_DELAY',
);
//unset($_SESSION['hide_popup']);
foreach($settings as $code)
	$arResult[$code] = COption::GetOptionString('toysales.main', $code, '', SITE_ID);	
if (!$USER->IsAuthorized() && !isset($_SESSION['hide_popup']) && ($arResult["TOYSALES_POPUP_SHOW"] == "yes" || $arResult["TOYSALES_POPUP_SHOW"] == "index_only" && $APPLICATION->GetCurPage(false) == "/"))
{
	$_SESSION['hide_popup'] = "Y";
	$value = COption::GetOptionString('toysales.main',$code,'',SITE_ID);	
	$this->IncludeComponentTemplate();	
}
?>