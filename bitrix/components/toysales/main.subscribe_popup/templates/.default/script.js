$(function() {
	$("[name='subscr_button']").click(function(){
		$.get(
			'/ajax/reg.php',
			{'email': $("[name='subscr_email']").val()},
			//{'email': 'test@mail.ru'},
			ShowSubscrResult,
			'JSON'
		);			
	});
});
	
function ShowSubscrResult(result)
{
	if (result.ERROR != undefined && result.ERROR != '')
		$(".subscr_error").show().html(result.ERROR+"<br><br>");
	else
	if (result.EMAIL_CONFIRMATION_SENT == 'Y')
		$(".subscr_block").html("Вам отправлено письмо");
	else
	if (result.REGISTER == 'Y')
		$(".subscr_block").html("Вы успешно зарегистрированы");
}