<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<script>
$(window).load(function(){	
<?
if (intval($arResult["TOYSALES_POPUP_DELAY"]) > 0)
{
?>setTimeout(function(){showPopup($('#subscribe-popup'));}, <?=intval($arResult["TOYSALES_POPUP_DELAY"])*1000?>);<?	
}
else
{
?>showPopup($('#subscribe-popup'));<?
}
?>
});
</script>
    <div id="subscribe-popup" class="overlay-popup">
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
					<?
					if ($arResult["TOYSALES_POPUP_PICTURE"] != "")
					{
					?>
                        <img class="popup-background" src="<?=$arResult["TOYSALES_POPUP_PICTURE"]?>" alt="" />					
					<?
					}
					?>
						<div class="subscr_block">
						<?=$arResult["TOYSALES_POPUP_HTML"]?>
                        <div class="styled-form">
                            <div class="form-title">Ваш Email</div>
								<div class="subscr_error" style="display:hidden; color:red"></div>
                                <div class="submit-wrapper">отправить<input type="button" name="subscr_button" value=""></div>
                                <div class="field-wrapper">
                                    <input type="text" name="subscr_email" placeholder="Введите e-mail адрес" value="">
                                </div>
                        </div>
						</div>
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>