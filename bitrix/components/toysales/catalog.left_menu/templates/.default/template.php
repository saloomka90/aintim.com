<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
                                <div class="block-title size-3"><?=$arResult["TOP_SECTION"]["NAME"]?></div>
                                <div class="accordeon">
<?
foreach ($arResult["SECTIONS2"] as $arSection2)
{
	if (empty($arResult["SECTIONS3"][$arSection2["ID"]]))
	{
	?>	
	                                <div class="accordeon-title2<?if ($arSection2["SELECTED"] == "Y") echo ' active'?>"><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><?=$arSection2["NAME"]?></a></div>
	<?	
	}
	else
	{
?>
                                    <div class="accordeon-title<?if ($arSection2["SELECTED"] == "Y") echo ' active'?>"><?=$arSection2["NAME"]?></div>
<?
	if (!empty($arResult["SECTIONS3"][$arSection2["ID"]]))
	{
	?>
                                    <div class="accordeon-entry"<?if ($arSection2["SELECTED"] == "Y") echo ' style="display:block"'?>>
                                        <div class="article-container style-1">
                                            <ul>
	<?
		foreach ($arResult["SECTIONS3"][$arSection2["ID"]] as $arSection3)
		{
		?>
                                                <li<?if ($arSection3["SELECTED"] == "Y") echo ' class="active"';?>><a href="<?=$arSection3["SECTION_PAGE_URL"]?>">
												<?
												/*
												if ($arSection3["SELECTED"] == "Y")
												{
													?><b><?=$arSection3["NAME"]?></b><?
												}
												else
													echo $arSection3["NAME"];
												*/
												?>
												<?=$arSection3["NAME"];?>
												</a></li>
		<?
		}
		?>
                                            </ul>
                                        </div>
                                    </div>
	<?
	}
	}
}
?>								
                                </div>  