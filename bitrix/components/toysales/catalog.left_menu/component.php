<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arResult["SECTIONS2"]=array();
$arResult["SECTIONS3"]=array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if(!\Bitrix\Main\Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	$arResult["SECTION"] = false;
	$intSectionDepth = 0;
	if($arParams["SECTION_ID"]>0)
	{
		$arFilter["ID"] = $arParams["SECTION_ID"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter);
		$arCurrentSection = $rsSections->GetNext();
	}
	elseif('' != $arParams["SECTION_CODE"])
	{
		$arFilter["=CODE"] = $arParams["SECTION_CODE"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter);
		$arCurrentSection = $rsSections->GetNext();
	}

	if(!is_array($arCurrentSection))
	{
		$this->AbortResultCache();
		return;		
	}

	$section_id2 = 0;
	$section_id3 = 0;


	$rsPath = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arCurrentSection["ID"]);
	while($arPath = $rsPath->GetNext())
	{
		if ($arPath["DEPTH_LEVEL"] == 1)
			$arResult["TOP_SECTION"] = $arPath;
		else
		if ($arPath["DEPTH_LEVEL"] == 2)
			$section_id2 = $arPath["ID"];
		else
		if ($arPath["DEPTH_LEVEL"] == 3)
			$section_id3 = $arPath["ID"];		
	}

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",		
		"LEFT_MARGIN" => $arResult["TOP_SECTION"]["LEFT_MARGIN"]+1,
		"RIGHT_MARGIN" => $arResult["TOP_SECTION"]["RIGHT_MARGIN"],
		"<=DEPTH_LEVEL" => 3,
	);
	//echo "<b>".$arResult["TOP_SECTION"]["NAME"]."</b><br>";

	$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
	while ($arSection = $rsSections->GetNext())
	{
		if ($arSection["DEPTH_LEVEL"] == 2)
		{
			if ($arSection["ID"] == $section_id2)
				$arSection["SELECTED"] = "Y";
			else
				$arSection["SELECTED"] = "";
			$arResult["SECTIONS2"][] = $arSection;
		}
		else
		if ($arSection["DEPTH_LEVEL"] == 3)
		{
			if ($arSection["ID"] == $section_id3)
				$arSection["SELECTED"] = "Y";
			else
				$arSection["SELECTED"] = "";
			if (!is_array($arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]]))
				$arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]] = array();
			$arResult["SECTIONS3"][$arSection["IBLOCK_SECTION_ID"]][] = $arSection;
		}			
		//echo $arSection["NAME"]."<br>";
	}

	
	$this->IncludeComponentTemplate();
}
?>