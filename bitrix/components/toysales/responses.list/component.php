<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock") or !CModule::IncludeModule('catalog'))
{
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return 0;
}
/*************************************************************************
	Processing of received parameters
*************************************************************************/
$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";

$arParams["DATE_FORMAT"] = trim($arParams["DATE_FORMAT"]);
if(strlen($arParams["DATE_FORMAT"])<=0)
	$arParams["DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));

$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
if($arParams["PAGE_ELEMENT_COUNT"]<=0)
	$arParams["PAGE_ELEMENT_COUNT"]=20;

$arNavParams = array("nPageSize"=>$arParams["PAGE_ELEMENT_COUNT"]);

$arNavigation = CDBResult::GetNavParams($arNavParams);

$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";

if (isset($_REQUEST["mark"]) && $_REQUEST["mark"] != "")
{
	$mark = intval($_REQUEST["mark"]);
	if ($mark >= 1 && $mark <= 5)
		$arParams["MARK"] = $mark;
}


$rsIBlock = CIBlock::GetList(array(), array(
	"ACTIVE" => "Y",
	"ID" => $arParams["IBLOCK_ID"],
	"SITE_ID" => SITE_ID,
));

if($arResult = $rsIBlock->GetNext())
{
	// список отзывов
	$arResult["ITEMS"] = array();
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
	);
	
	if (isset($arParams["MARK"]))
		$arFilter["PROPERTY_mark"] = $arParams["MARK"];
	$arSelect = array(
		"IBLOCK_ID",
		"ID",
		"NAME",
		"PREVIEW_TEXT",
		"TIMESTAMP_X",
		"DETAIL_TEXT",
		"DATE_CREATE",
	);
	$arSort = array(
		"ID" => "DESC",
	);
	$rsItems = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
	while ($obItem = $rsItems->GetNextElement())
	{
		$arItem = $obItem->GetFields();
		$arItem["PROPERTIES"] = $obItem->GetProperties();
		$arItem["DISPLAY_DATE_CREATE"] = CIBlockFormatProperties::DateFormat($arParams["DATE_FORMAT"], MakeTimeStamp($arItem["DATE_CREATE"], CSite::GetDateFormat()));
		if ($arItem["DETAIL_TEXT"] != "")
			$arItem["DISPLAY_DATE_MODIFY"] = CIBlockFormatProperties::DateFormat("d.m.Y H:i", MakeTimeStamp($arItem["TIMESTAMP_X"], CSite::GetDateFormat()));
		$arItem["DISPLAY_DAY"] = CIBlockFormatProperties::DateFormat("j", MakeTimeStamp($arItem["DATE_CREATE"], CSite::GetDateFormat()));
		$arItem["DISPLAY_MONTH"] = CIBlockFormatProperties::DateFormat("M", MakeTimeStamp($arItem["DATE_CREATE"], CSite::GetDateFormat()));		
		$arResult["ITEMS"][] = $arItem;
	}
	
	$arResult["NAV_STRING"] = $rsItems->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
	);
	$rsCategories = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_mark"));
	$arResult["MARKS"] = array(
		5 => "Отличный магазин",
		4 => "Хороший магазин",		
		3 => "Обычный магазин",		
		2 => "Плохой магазин",
		1 => "Ужасный магазин",
	);	
	$arResult["MARKS_CNT"] = array();
	while ($arCategory = $rsCategories->GetNext())
	{
		$arResult["MARKS_CNT"][$arCategory["PROPERTY_MARK_VALUE"]] = $arCategory["CNT"];
	}
	
	$this->IncludeComponentTemplate();
}
else
{
	ShowError("Раздел не найден");
	@define("ERROR_404", "Y");
}

if(isset($arResult["ID"]))
{
	if($USER->IsAuthorized())
	{
		if($GLOBALS["APPLICATION"]->GetShowIncludeAreas() && CModule::IncludeModule("iblock"))
			$this->AddIncludeAreaIcons(CIBlock::ShowPanel($arResult["ID"], 0, 0, $arResult["IBLOCK_TYPE_ID"], true));
		if($arParams["DISPLAY_PANEL"] && CModule::IncludeModule("iblock"))
			CIBlock::ShowPanel($arResult["ID"], 0, 0, $arResult["IBLOCK_TYPE_ID"], false, $this->GetName());
	}
}
?>