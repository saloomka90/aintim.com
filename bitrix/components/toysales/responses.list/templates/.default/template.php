<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
                <div class="information-blocks">
                    <div class="row">
                        
                        <div class="col-md-9 col-md-push-3 information-entry">
<?
if ($_REQUEST["result"] == "ok")
{
?>
                            <div class="information-blocks">
                                <div class="message-box message-success">
                                    <div class="message-icon"><i class="fa fa-check"></i></div>
                                    <div class="message-text"><b>Спасибо!</b> Ваш отзыв принят и опубликован.</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
                            </div>
<?
}
?>							
                            <div class="blog-landing-box type-1">
<?
foreach ($arResult["ITEMS"] as $arItem)
{
?>
                                <div class="blog-entry">
                                    <div class="date"><?=$arItem["DISPLAY_DAY"]?> <span><?=$arItem["DISPLAY_MONTH"]?></span></div>
                                    <div class="content">
                                        <h2 class="title"><?=$arResult["MARKS"][$arItem["PROPERTIES"]["mark"]["VALUE"]]?> от <?=$arItem["NAME"]?></h3>
                                        <div class="subtitle">Отзыв добавлен <?=$arItem["DISPLAY_DATE_CREATE"]?></div>
                                        <div class="description"><?=$arItem["PREVIEW_TEXT"]?></div>
<?
	if ($arItem["DETAIL_TEXT"] != "")
	{
	?>
                                    <blockquote class="latest-review">
                                        <div class="text"><?=$arItem["DETAIL_TEXT"]?>
                                        </br>
                                        <footer><cite>Администратор сайта <?=SITE_SERVER_NAME?></cite>, <?=$arItem["DISPLAY_DATE_MODIFY"]?></footer>
                                        </div>
                                        
                                    </blockquote>	
	<?
	}
?>									
                                    </div>
                                </div>
<?
}
?>							
                            </div>
<?
if ($arResult["NAV_STRING"] != "")							
{
?>
                            <div class="page-selector">
							<div class="description"></div>							
<div class="pages-box">
<?
	echo $arResult["NAV_STRING"];
?>
</div>
							<div class="clear"></div>
                            </div>
<?
}								
?>
                        </div>

                        <div class="col-md-3 col-md-pull-9 information-entry blog-sidebar">
                            <div class="information-blocks">
                                <div class="categories-list">
                                    <div class="block-title size-3">Отзывы о магазине</div>
                                    <ul>
<?
	foreach ($arResult["MARKS"] as $mark => $mark_name)
	{
	?>
                                        <li><a href="?mark=<?=$mark?>"<?if (isset($arParams["MARK"]) && $arParams["MARK"] == $mark) echo ' class="active"';?>><?=$mark_name?><span>(<?=intval($arResult["MARKS_CNT"][$mark])?>)</span></a></li>	
	<?
	}
?>									
                                    </ul>
                                </div>
                            </div>
                            

                            <div class="information-blocks">
                                    <div class="col-sm-12">
                                        <a href="add.php"><div class="button style-4">Написать отзыв о магазине</div></a>
                                    </div>
                            </div>                            
						</div>
					</div>
				</div>