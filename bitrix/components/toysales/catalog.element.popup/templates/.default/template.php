<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateLibrary = array();
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);

unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
/*
?><pre><?
print_r($arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["PROPERTIES"]["shipping_date"]);
?></pre><?
*/
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'MAIN_PROPS' => $strMainID.'_main_props',

	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'BUY_LINK' => $strMainID.'_buy_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'IN_BASKET' => $strMainID.'_in_basket',	
	
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',

	'PROP_DIV' => $strMainID.'_sku_tree',
	'PROP' => $strMainID.'_prop_',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop'
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);	
	
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));

	$strFullPath = $_SERVER['DOCUMENT_ROOT'].$this->GetFolder();

?>
<?
//echo $strObName." ";
	$minPrice = false;
	if (isset($arResult['MIN_PRICE']) || isset($arResult['RATIO_PRICE']))
		$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);	
	$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);	
?>	
        <div class="overflow">

            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container" id="<? echo $strMainID; ?>">

                        <div class="row">
                            <div class="col-sm-6 information-entry">
                                <div class="product-preview-box">
                                    <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
									<?
									if (!empty($arResult["BIG_PICTURES"]))
									{
									?>
                                        <div class="swiper-wrapper">
									<?
										foreach ($arResult["BIG_PICTURES"] as $key => $pict_src)
										{
										?>
                                            <div class="swiper-slide">
                                                <div class="product-zoom-image">
                                                    <img src="<?=$arResult["MIDDLE_PICTURES"][$key]?>" alt="" data-zoom="<?=$pict_src?>" />
                                                </div>
                                            </div>
										<?	
										}
									?>
                                        </div>
                                        <div class="pagination"></div>
                                        <div class="product-zoom-container">
                                            <div class="move-box">
                                                <img class="default-image" src="<?=$arResult['MIDDLE_PICTURES'][0]?>" alt="" />
                                                <img class="zoomed-image" src="<?=$arResult['BIG_PICTURES'][0]?>" alt="" />
                                            </div>
                                            <div class="zoom-area"></div>
                                        </div>
									<?
									}
									?>
                                    </div>
                                    <div class="swiper-hidden-edges">
                                        <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                            <div class="swiper-wrapper">
									<?
										foreach ($arResult["MIDDLE_PICTURES"] as $key => $pict_src)
										{
										?>											
                                                <div class="swiper-slide selected">
                                                    <div class="paddings-container">
                                                        <img src="<?=$pict_src?>" alt="" />
                                                    </div>
                                                </div>
										<?
										}
									?>
                                            </div>
                                            <div class="pagination"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 information-entry">
                                <div class="product-detail-box">
                                    <h1 class="product-title"><?=$arResult["NAME"]?></h1>
                                    <h3 class="product-subtitle">Артикул: <?=$arResult["PROPERTIES"]["article"]["VALUE"]?>, ID: <?=$arResult["XML_ID"]?></h3>
<?
$shipping_date = "";
if (!empty($arResult["OFFERS"]))
{
	$shipping_date = $arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["PROPERTIES"]["shipping_date"]["VALUE"];
}
?>
                                    <h3 class="product-subtitle">Дата отгрузки <?=$shipping_date?> 
<?
if (!empty($arResult["OFFERS"]))
{
?>
									<span class="inline-label green">на складе</span>
<?	
}
else
{
?>
									<span class="inline-label red">нет в наличии</span>
<?	
}	
?>									
									</h3>
                                    <div class="product-description detail-info-entry"><?=$arResult["DETAIL_TEXT"]?> <a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="continue-link">На страницу товара <i class="fa fa-long-arrow-right"></i></a></div>
                                    <div class="price detail-info-entry">
<?
	if (!empty($minPrice))
	{
	?>
										<div class="prev" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></div>		
                                        <div class="current" id="<? echo $arItemIDs['PRICE']; ?>"><?=$minPrice['PRINT_DISCOUNT_VALUE'];?></div>
	<?
	}
?>
                                    </div>
<?					
		if (isset($arResult["OFFERS"]) and !empty($arResult["OFFERS"]))
		{
			if (!empty($arResult['OFFERS_PROP']))
			{
				$arSkuProps = array();
				?><div id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
	foreach ($arResult['SKU_PROPS'] as &$arProp)
	{
		if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
			continue;
		$arSkuProps[] = array(
			'ID' => $arProp['ID'],
			'SHOW_MODE' => $arProp['SHOW_MODE'],
			'VALUES_COUNT' => $arProp['VALUES_COUNT']
		);
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
			?>
				<div class="size-selector detail-info-entry" id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_list">
				<div class="detail-info-entry-title"><?=htmlspecialcharsex($arProp['NAME'])?></div>
				<?
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
				?>
					<div class="entry" data-treevalue="<?=$arProp['ID']?>_<?=$arOneValue['ID']?>" data-onevalue="<?=$arOneValue['ID']?>"><?=$arOneValue['NAME']?></div>
				<?
				}
				?>
				<div class="spacer"></div></div>
				<?
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
			?>
				<div class="color-selector detail-info-entry" id="<?=$arItemIDs['PROP'].$arProp['ID'];?>_list">
				<div class="detail-info-entry-title"><?=htmlspecialcharsex($arProp['NAME'])?></div>;
			<?
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
				?>
					<div class="entry" data-treevalue="<?=$arProp['ID']?>_<?=$arOneValue['ID']?>" data-onevalue="<?=$arOneValue['ID']?>"  style="background-image:url('<?=$arOneValue['PICT']['SRC']?>');" title="<?=$arOneValue['NAME']?>">&nbsp;</div>
				<?
				}
				?>
				<div class="spacer"></div></div>
				<?
			}
	}
				?>
				</div>
				<?
			}
			else
			{
				?><div id="<? echo $arItemIDs['PROP_DIV']; ?>"></div><?
			}
			
	$canBuy = $arResult['JS_OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
	?>
	<br><br>
                                    <div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="detail-info-entry" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
                                        <a id="<? echo $arItemIDs['BUY_LINK']; ?>" href="javascript:void(0)" class="button style-10">В Корзину</a>
                                        <div class="clear"></div>
                                    </div>
                                <div class="detail-info-entry" id="<? echo $arItemIDs['IN_BASKET']; ?>" style="display:none">
                                    <a class="button style-20" href="<?=$arParams["BASKET_URL"]?>">Товар в корзине</a>
                                    <div class="clear"></div>
                                </div>									
	<?
				$arJSParams = array(
					'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
					'SHOW_ADD_BASKET_BTN' => false,
					'SHOW_BUY_BTN' => true,
					'SHOW_ABSENT' => true,
					'SHOW_SKU_PROPS' => $arResult['OFFERS_PROPS_DISPLAY'],
					'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
					'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
					'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
					'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
					'DEFAULT_PICTURE' => array(
						'PICTURE' => $arResult['PREVIEW_PICTURE'],
					),
					'VISUAL' => array(
						'ID' => $arItemIDs['ID'],
						'PICT_ID' => $arItemIDs['PICT'],
						'OLD_PRICE_ID' => $arItemIDs['OLD_PRICE'],
						'PRICE_ID' => $arItemIDs['PRICE'],
						'TREE_ID' => $arItemIDs['PROP_DIV'],
						'TREE_ITEM_ID' => $arItemIDs['PROP'],
						'BUY_ID' => $arItemIDs['BUY_LINK'],
						'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
						'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
						'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
						'IN_BASKET_ID' => $arItemIDs['IN_BASKET'],
						'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
					),
					'BASKET' => array(
						'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
						'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
						'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
						'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
						'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
					),
					'PRODUCT' => array(
						'ID' => $arResult['ID'],
						'NAME' => $productTitle
					),
					'OFFERS' => $arResult['JS_OFFERS'],
					'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
					'TREE_PROPS' => $arSkuProps,
				);
				?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>
				<?			
	}
	?>
                                </div>
                            </div>
                        </div>

                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>