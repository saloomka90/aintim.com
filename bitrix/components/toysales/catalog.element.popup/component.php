<?
use Bitrix\Main\Loader;
use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;
$arParams["CACHE_TYPE"] = "N";

unset($arParams["IBLOCK_TYPE"]); //was used only for IBLOCK_ID setup with Editor
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

if (empty($arParams["ELEMENT_SORT_FIELD"]))
	$arParams["ELEMENT_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER"]))
	$arParams["ELEMENT_SORT_ORDER"] = "asc";
if (empty($arParams["ELEMENT_SORT_FIELD2"]))
	$arParams["ELEMENT_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER2"]))
	$arParams["ELEMENT_SORT_ORDER2"] = "desc";

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);
$arParams["BASKET_URL"]=trim($arParams["BASKET_URL"]);
if($arParams["BASKET_URL"] === '')
	$arParams["BASKET_URL"] = "/personal/basket.php";

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if($arParams["ACTION_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if($arParams["PRODUCT_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if($arParams["PRODUCT_PROPS_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

if(!isset($arParams["PRICE_CODE"]) || !is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;
$arParams["USE_PRODUCT_QUANTITY"] = false;

$arParams['ADD_PROPERTIES_TO_BASKET'] = (isset($arParams['ADD_PROPERTIES_TO_BASKET']) && $arParams['ADD_PROPERTIES_TO_BASKET'] === 'N' ? 'N' : 'Y');
if ('N' == $arParams['ADD_PROPERTIES_TO_BASKET'])
{
	$arParams["PRODUCT_PROPERTIES"] = array();
	$arParams["OFFERS_CART_PROPERTIES"] = array();
}
$arParams['PARTIAL_PRODUCT_PROPERTIES'] = (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) && $arParams['PARTIAL_PRODUCT_PROPERTIES'] === 'Y' ? 'Y' : 'N');
if(!isset($arParams["PRODUCT_PROPERTIES"]) || !is_array($arParams["PRODUCT_PROPERTIES"]))
	$arParams["PRODUCT_PROPERTIES"] = array();
foreach($arParams["PRODUCT_PROPERTIES"] as $k=>$v)
	if($v==="")
		unset($arParams["PRODUCT_PROPERTIES"][$k]);

if (!isset($arParams["OFFERS_CART_PROPERTIES"]) || !is_array($arParams["OFFERS_CART_PROPERTIES"]))
	$arParams["OFFERS_CART_PROPERTIES"] = array();
foreach($arParams["OFFERS_CART_PROPERTIES"] as $i => $pid)
	if ($pid === "")
		unset($arParams["OFFERS_CART_PROPERTIES"][$i]);

if (empty($arParams["OFFERS_SORT_FIELD"]))
	$arParams["OFFERS_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER"]))
	$arParams["OFFERS_SORT_ORDER"] = "asc";
if (empty($arParams["OFFERS_SORT_FIELD2"]))
	$arParams["OFFERS_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER2"]))
	$arParams["OFFERS_SORT_ORDER2"] = "desc";

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';
$arParams["CACHE_GROUPS"] = "N";

$arParams["ELEMENT_ID"] = intval($_REQUEST["id"]);
/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if (!Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arResultModules = array(
		'iblock' => true,
		'catalog' => false,
		'currency' => false
	);
	$arrFilter=array();
	
	global $CACHE_MANAGER;

	$bIBlockCatalog = false;
	$arCatalog = false;
	$boolNeedCatalogCache = false;
	$bCatalog = Loader::includeModule('catalog');
	if ($bCatalog)
	{
		$arResultModules['catalog'] = true;
		$arResultModules['currency'] = true;
		$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
		if (!empty($arCatalog) && is_array($arCatalog))
		{
			$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
			$boolNeedCatalogCache = true;
		}
	}
	//This function returns array with prices description and access rights
	//in case catalog module n/a prices get values from element properties
	$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$arResultPricesAllow = CIBlockPriceTools::GetAllowCatalogPrices($arResultPrices);

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResultPricesAllow))
	{
		$boolNeedCatalogCache = CIBlockPriceTools::SetCatalogDiscountCache($arResultPricesAllow, $USER->GetUserGroupArray());
	}

	/************************************
			Elements
	************************************/
	//SELECT
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"XML_ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE",
	);
	//WHERE
	$arrFilter["ACTIVE"] = "Y";
	if($arParams["IBLOCK_ID"] > 0)
		$arrFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
	$arrFilter["IBLOCK_LID"] = SITE_ID;
	$arrFilter["IBLOCK_ACTIVE"] = "Y";
	$arrFilter["ACTIVE_DATE"] = "Y";
	$arrFilter["ACTIVE"] = "Y";
	$arrFilter["CHECK_PERMISSIONS"] = "Y";

	$arrFilter["ID"] = $arParams["ELEMENT_ID"];
	//PRICES
	$arPriceTypeID = array();
	foreach($arResultPrices as &$value)
	{
		if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
			continue;
		$arSelect[] = $value["SELECT"];
		$arrFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
	}
	if (isset($value))
		unset($value);

	$bGetPropertyCodes = !empty($arParams["PROPERTY_CODE"]);
	$bGetProductProperties = !empty($arParams["PRODUCT_PROPERTIES"]);
	$bGetProperties = $bGetPropertyCodes || $bGetProductProperties;

	$rsElements = CIBlockElement::GetList(array(), $arrFilter, false, array("nTopCount"=>1), $arSelect);
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);

	if($obItem = $rsElements->GetNextElement())
	{
		//echo "1";
		$arResult = $obItem->GetFields();
		if (strlen($arResult["DETAIL_TEXT"]) > 200)
		{
			$arResult["DETAIL_TEXT"] = substr($arResult["DETAIL_TEXT"], 0, 200);
			$pos = strrpos($arResult["DETAIL_TEXT"], " ");
			if ($pos !== false)
				$arResult["DETAIL_TEXT"] = substr($arResult["DETAIL_TEXT"], 0, $pos);
		}
		
		$arResult["PROPERTIES"] = $obItem->GetProperties();
		$arResult['ID'] = intval($arResult['ID']);

		$arResult['ACTIVE_FROM'] = $arResult['DATE_ACTIVE_FROM'];
		$arResult['ACTIVE_TO'] = $arResult['DATE_ACTIVE_TO'];

		$arButtons = CIBlock::GetPanelButtons(
			$arResult["IBLOCK_ID"],
			$arResult["ID"],
			$arResult["IBLOCK_SECTION_ID"],
			array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
		);
		$arResult["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arResult["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$arResult["DISPLAY_PROPERTIES"] = array();
		$arResult["PRODUCT_PROPERTIES"] = array();
		$arResult['PRODUCT_PROPERTIES_FILL'] = array();

		$arResult['MODULES'] = $arResultModules;

		if ($bCatalog && $boolNeedCatalogCache)
		{
			CCatalogDiscount::SetProductPropertiesCache($arResult['ID'], $arResult["PROPERTIES"]);
		}
		
		$arResult["BIG_PICTURES"] = array();
		$arResult["MIDDLE_PICTURES"] = array();
		$i = 1;
		while ($i <= 10)
		{
			if ($arResult["PROPERTIES"]["pict".$i]["VALUE"] != "")
			{
				$filename =  basename($arResult["PROPERTIES"]["pict".$i]["VALUE"]);
				//echo $filename."<br>";
				$exist = false;
				$original_pic_path = "/images/".$filename;
				if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
					$exist = true;
				else
				{
					$content = file_get_contents($arResult["PROPERTIES"]["pict".$i]["VALUE"]);
					if ($content !== false)
					{
						file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
							$exist = true;
					}
				}
				if ($exist == true)
				{
					$resized_pic_path = "/resized_images/570x765/".$filename;
					if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
					{
						$arResult["MIDDLE_PICTURES"][] = $resized_pic_path;
						$arResult["BIG_PICTURES"][] = $original_pic_path;						
					}
					else
					{
						$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
						if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>570,"height"=>765), BX_RESIZE_IMAGE_PROPORTIONAL))
						{
							$arResult["MIDDLE_PICTURES"][] = $resized_pic_path;
							$arResult["BIG_PICTURES"][] = $original_pic_path;
						}
					}
					//echo $arResult["PREVIEW_PICTURE"]["SRC"];
				}
			}
			$i++;
		}


		$currentPath = "/index.php?";

		$arResult['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~BUY_URL_TEMPLATE']);
		$arResult['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~ADD_URL_TEMPLATE']);


		$arResult["PRICES"] = array();
		$arResult["PRICE_MATRIX"] = false;
		$arResult['MIN_PRICE'] = false;

		$arResult["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arResult, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		if (!empty($arResult['PRICES']))
		{
			$arResult['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arResult['PRICES']);
		}

		$arResult["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResultPrices, $arResult);

		$arResult['~BUY_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['~BUY_URL_TEMPLATE']);
		$arResult['BUY_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['BUY_URL_TEMPLATE']);
		$arResult['~ADD_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['~ADD_URL_TEMPLATE']);
		$arResult['ADD_URL'] = str_replace('#ID#', $arResult["ID"], $arResult['ADD_URL_TEMPLATE']);

		if(!isset($arParams["OFFERS_FIELD_CODE"]))
			$arParams["OFFERS_FIELD_CODE"] = array();
		elseif (!is_array($arParams["OFFERS_FIELD_CODE"]))
			$arParams["OFFERS_FIELD_CODE"] = array($arParams["OFFERS_FIELD_CODE"]);
		foreach($arParams["OFFERS_FIELD_CODE"] as $key => $value)
			if($value === "")
				unset($arParams["OFFERS_FIELD_CODE"][$key]);

		if(!isset($arParams["OFFERS_PROPERTY_CODE"]))
			$arParams["OFFERS_PROPERTY_CODE"] = array();
		elseif (!is_array($arParams["OFFERS_PROPERTY_CODE"]))
			$arParams["OFFERS_PROPERTY_CODE"] = array($arParams["OFFERS_PROPERTY_CODE"]);
		foreach($arParams["OFFERS_PROPERTY_CODE"] as $key => $value)
			if($value === "")
				unset($arParams["OFFERS_PROPERTY_CODE"][$key]);

		if(
			$bCatalog
			&& (
				!empty($arParams["OFFERS_FIELD_CODE"])
				|| !empty($arParams["OFFERS_PROPERTY_CODE"])
			)
		)
		{
			$offersFilter = array(
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'HIDE_NOT_AVAILABLE' => "Y"
			);
			if (!$arParams["USE_PRICE_COUNT"])
			{
				$offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];
			}
			
			$arOffers = CIBlockPriceTools::GetOffersArray(
				$offersFilter,
				array($arResult["ID"]),
				array(
					$arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
					$arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
				),
				$arParams["OFFERS_FIELD_CODE"],
				$arParams["OFFERS_PROPERTY_CODE"],
				0,
				$arResultPrices,
				$arParams['PRICE_VAT_INCLUDE'],
				$arConvertParams
			);
			foreach($arOffers as $arOffer)
			{
				$arOffer['~BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~BUY_URL_TEMPLATE']);
				$arOffer['BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['BUY_URL_TEMPLATE']);
				$arOffer['~ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~ADD_URL_TEMPLATE']);
				$arOffer['ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['ADD_URL_TEMPLATE']);
				$arResult["OFFERS"][] = $arOffer;
			}
			unset($arOffer);
			unset($arOffers);
		}
		$this->SetResultCacheKeys(array(
		));
		$this->IncludeComponentTemplate();

		if ($bCatalog && $boolNeedCatalogCache)
		{
			CCatalogDiscount::ClearDiscountCache(array(
				'PRODUCT' => true,
				'SECTIONS' => true,
				'PROPERTIES' => true
			));
		}
	}
}
?>