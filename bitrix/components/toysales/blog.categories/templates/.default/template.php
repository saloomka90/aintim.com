<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if (!empty($arResult["SECTIONS"]))
{
?>
                            <div class="information-blocks">
                                <div class="categories-list">
                                    <div class="block-title size-3">Категории</div>
                                    <ul>
<?
	foreach ($arResult["SECTIONS"] as $arSection)
	{
	?>
                                        <li><a<?if ($arSection["SELECTED"]) echo ' class="active"';?> href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?><span>(<?=$arSection["ELEMENT_CNT"]?>)</span></a></li>
	<?
	}
?>
                                    </ul>
                                </div>
                            </div>
<?
}
?>