<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if(!\Bitrix\Main\Loader::includeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);
	$rsIBlock = CIBlock::GetList(array(), array(
		"ACTIVE" => "Y",
		"ID" => $arParams["IBLOCK_ID"],
	));
	if ($arResult = $rsIBlock->GetNext())
	{
		$arFilter = array(
			"IBLOCK_ID" => $arResult["ID"],
			"ACTIVE" => "Y",
			"SECTION_ID" => 0,
			"CNT_ACTIVE" => "Y",
		);
		$arResult["SECTIONS"] = array();
		$rsSections = CIBlockSection::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), $arFilter, true);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$cur_page = $APPLICATION->GetCurPage(false);
		while ($arSection = $rsSections->GetNext())
		{
			if ($arSection["SECTION_PAGE_URL"] == $cur_page)
				$arSection["SELECTED"] = true;
			else
				$arSection["SELECTED"] = false;
			$arResult["SECTIONS"][] = $arSection;
		}

		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
		ShowError("Раздел не найден");
		@define("ERROR_404", "Y");
		return;
	}	
}
?>