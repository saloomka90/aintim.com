<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["PAY_SYSTEM"]) && is_array($arResult["PAY_SYSTEM"]))
{
?>
                                <div class="accordeon-title active"><span class="number">3</span>Способ оплаты</div>
                                <div class="accordeon-entry" style="display: block;">
                                    <div class="row">
                                        <div class="col-md-12 information-entry">
                                            <div class="article-container style-1">
                                                <p>
<?
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
	{
	?>
                                                    <label class="checkbox-entry radio">
								<input type="radio"
								id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
								name="PAY_SYSTEM_ID"
								value="<?=$arPaySystem["ID"]?>"
								<?if ($arPaySystem["CHECKED"]=="Y") echo " checked=\"checked\"";?>
								onclick="submitForm();" />
                                                         <span class="check"></span> <b><?=$arPaySystem["PSA_NAME"];?></b>
	<?
	if ($arPaySystem["DESCRIPTION"] != "")
	{
	?>
														<br/><?=$arPaySystem["DESCRIPTION"]?>
	<?
	}
	?>
                                                    </label>
	<?
	}
?>
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>     
<?
}
?>								