<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<script type="text/javascript">
	if(typeof submitForm === 'function')
		BX.addCustomEvent('onDeliveryExtraServiceValueChange', function(){ submitForm(); });
</script>
<?
	if(!empty($arResult["DELIVERY"]))
	{
	?>
                                <div class="accordeon-title active"><span class="number">2</span>Способ доставки</div>
                                <div class="accordeon-entry" style="display: block;">
                                    <div class="row">
                                        <div class="col-md-12 information-entry">
<?
		foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
		{
		?>
                                            <div class="article-container style-1">
                                                <p>
                                                    <label class="checkbox-entry radio">
					<input type="radio"
						id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
						name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
						value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
						onclick="submitForm();"
						/>
													
                                                        <span class="check"></span> <b><?= htmlspecialcharsbx($arDelivery["NAME"])?><?
														if(isset($arDelivery["PRICE"]))
														{
															if ($arDelivery["PRICE"] == 0)
															{/*
																?> (бесплатно)<?
															*/}
															else
															{
															?>, <?=$arDelivery["PRICE_FORMATED"]?><?
															}
														}
														?>
														</b><br/>
														<?
														if (strlen($arDelivery["DESCRIPTION"])>0)
															echo $arDelivery["DESCRIPTION"]."<br />";
														if ($delivery_id == "2")
														{
														?>
														<a href="/info/contacts/" target="_blank" class="continue-link">Посмотреть схему проезда <i class="fa fa-long-arrow-right"></i></a>
														<?
														}
														else
if (in_array($arDelivery["ID"], array(9,10)) and ($arDelivery["CHECKED"] == "Y"))
{
?>
<script type="text/javascript" src="https://pickpoint.ru/select/postamat.js" /></script>
<a href="#" class="continue-link" onclick="PickPoint.open(picpoint_result); return false">Выберите пункт выдачи PickPoint</a>
<br><div id="picpoint_text">
<?
if ($_REQUEST["picpoint_name"] != "") echo htmlspecialcharsbx($_REQUEST["picpoint_name"]);
if ($_REQUEST["picpoint_address"] != "") echo "<br>".htmlspecialcharsbx($_REQUEST["picpoint_address"]);
?></div>
<input type="hidden" name="picpoint_id" value="<?=htmlspecialcharsbx($_REQUEST["picpoint_id"])?>">
<input type="hidden" name="picpoint_name" value="<?=htmlspecialcharsbx($_REQUEST["picpoint_name"])?>">
<input type="hidden" name="picpoint_address" value="<?=htmlspecialcharsbx($_REQUEST["picpoint_address"])?>">
<?
}															
														?>
                                                    </label>
                                                </p>
                                            </div>
		<?
			if ($arDelivery["CHECKED"] == 'Y')
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");
			}
		}
?>
                                        </div>

                                    </div>
                                </div>
	<?
	}
	?>