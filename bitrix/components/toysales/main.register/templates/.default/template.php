<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
                            <div class="information-blocks">
                                <h3 class="block-title main-heading">Регистрация на сайте</h3>
<?
if($USER->IsAuthorized())
{
?>								
                                <div class="message-box message-success">
                                    <div class="message-icon"><i class="fa fa-check"></i></div>
                                    <div class="message-text"><b>Спасибо!</b> Вы успешно зарегистрировались на сайте.</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
<?
}
else
if($arResult["EMAIL_CONFIRMATION_SENT"] === "Y")
{
?>
                                <div class="message-box message-success">
                                    <div class="message-icon"><i class="fa fa-check"></i></div>
                                    <div class="message-text"><b>Спасибо!</b> На указанный e-mail выслан запрос на подтверждение - проверьте пожалуйста почту и перейдите по ссылке в письме для завершения регистрации.</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
<?	
}
else
{
?>
								<!--
                                <div class="message-box message-danger">
                                    <div class="message-icon"><i class="fa fa-times"></i></div>
                                    <div class="message-text"><b>Ошибка!</b> Указанный Email уже зарегистрирован в системе!</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
                                -->
<?
if (count($arResult["ERRORS"]) > 0)
{
	foreach ($arResult["ERRORS"] as $key => $error)
	{
	?>
                                <div class="message-box message-danger">
                                    <div class="message-icon"><i class="fa fa-times"></i></div>
                                    <div class="message-text"><?=$error?></div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>		
	<?								
	}
}
?>								
                                <form method="post" action="<?=POST_FORM_ACTION_URI?>">
								<input type="hidden" name="register_submit_button" value="Y">
<?
if($arResult["BACKURL"] <> '')
{
?>
								<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Имя <span>*</span></label>
                                            <input class="simple-field" type="text" placeholder="Ваше имя (обязательно)" name="REGISTER[NAME]" value="<?=htmlspecialcharsbx($_REQUEST["REGISTER"]["NAME"])?>" required />
                                            <div class="clear"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Фамилия</label>
                                            <input class="simple-field" type="text" placeholder="Ваша Фамилия" name="REGISTER[LAST_NAME]" value="<?=htmlspecialcharsbx($_REQUEST["REGISTER"]["LAST_NAME"])?>" />
                                            <div class="clear"></div>
                                        </div>
                                        <div class="col-sm-6">
                                        <label>Email<span>*</span></label>
                                        <input class="simple-field" type="email" placeholder="Введите Email (обязательно)" name="REGISTER[EMAIL]" value="<?=htmlspecialcharsbx($_REQUEST["REGISTER"]["EMAIL"])?>" required />
                                        <label>Пароль<span>*</span></label>
                                        <input class="simple-field" type="password" placeholder="Введите пароль" name="REGISTER[PASSWORD]" required />
                                            <div class="button style-10">Зарегистрироваться<input type="submit" value="" /></div>
                                        </div>
                                    </div>
                                    
                                </form>
<?
}
?>
                            </div>							