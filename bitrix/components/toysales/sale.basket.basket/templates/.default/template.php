<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
?>
<?
use Bitrix\Sale\DiscountCouponsManager;
$APPLICATION->AddHeadScript($templateFolder."/script.js");

$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delete_coupon" => $APPLICATION->GetCurPage()."?delete_coupon=#COUPON#",
);
unset($curPage);

if (empty($arResult["ITEMS"]["AnDelCanBuy"]))
{
?>
                <div class="information-blocks">
                    <div class="message-box message-warning">
                        <div class="message-icon"><i class="fa fa-exclamation"></i></div>
                        <div class="message-text"><b>Пусто!</b> В вашей корзине много места, но пока совсем нет товара. Самое время <a href="/catalog/" class="continue-link">перейти к покупкам <i class="fa fa-long-arrow-right"></i></a></div>
                        <div class="message-close"><i class="fa fa-times"></i></div>
                    </div>
                    
                </div>
<?	
}
else
{
?>
                <div class="information-blocks">
                    <div class="row">
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">					
				<input type="hidden" name="BasketRefresh">
				<input type="hidden" name="BasketOrder">
		
                        <div class="col-sm-9 information-entry">
                            <h3 class="cart-column-title size-1">Ваш заказ</h3>
		<?
		if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
		{
		?><div id="warning_message"><?
			foreach ($arResult["WARNING_MESSAGE"] as $v)
				ShowError($v);
		?><br></div><?
		}
		?>
							
<?
foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arItem)							
{
?>
                            <div class="traditional-cart-entry style-1">
<?
if ($arItem["PREVIEW_PICTURE_SRC"] != "")							
{
?>
                                <a class="image" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img alt="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>"></a>
<?
}
?>								
                                <div class="content">
                                    <div class="cell-view">
                                        <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                        <div class="inline-description"><?=$arItem["SKU_PROPS"]["size"]?>
										<?
										if ($arItem["SKU_PROPS"]["color"] != "")
										{
											if ($arItem["SKU_PROPS"]["size"] != "")
												echo " / ";
											echo $arItem["SKU_PROPS"]["color"];
										}
										?>
										</div>
                                        <div class="price">
											<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
												<div class="prev"><?=$arItem["FULL_PRICE_FORMATED"]?></div>
											<?endif;?>										
                                            <div class="current"><?=$arItem["PRICE_FORMATED"]?></div>
                                        </div>
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title">Количество</div>
                                            <div class="entry number-minus"> </div>
                                            <div class="entry number" id="quantity_cont_<?=$arItem["ID"]?>"><?=$arItem["QUANTITY"]?></div>
											<input type="hidden" name="QUANTITY_<?=$arItem["ID"]?>" value="<?=$arItem["QUANTITY"]?>">
                                            <div class="entry number-plus"> </div>
                                            <a class="button style-15 BasketRefresh">обновить корзину</a>
                                            <a class="remove-button" href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?
}
?>							
                        </div>
		</form>						

                        <div class="col-sm-3 information-entry">
                            <h3 class="cart-column-title size-1" style="text-align: center;">Сумма</h3>
                            <div class="sidebar-subtotal">
                                <div class="price-data">
                                    <div class="main"><?=str_replace(" ", " ", $arResult["allSum_FORMATED"])?></div>
                                    <div class="title">заказы на сумму от 3000 рублей</div>
                                    <div class="subtitle">ДОСТАВЛЯЮТСЯ БЕСПЛАТНО</div>
                                    <a class="button style-10 BasketOrder">Оформить заказ</a>                                    
                                </div>
                                <div class="additional-data">
                                    <h3 class="cart-column-title">Скидка <span class="inline-label green">Промокод</span></h3>
									<form method="get" action="<?=POST_FORM_ACTION_URI?>" name="coupon_form" id="coupon_form">	
                                        <label>Введите промокод, если он у вас есть.</label>
                                        <input type="text" name="coupon" class="simple-field size-1" placeholder="" value="">
                                        <div style="margin-top: 10px;" class="button style-16 AddCoupon">Применить<input type="submit"></div>
									</form>
                                    <div style="height: 20px;"></div> 
<?
if (!empty($arResult['COUPON_LIST']))
{
?>
<div class="information-blocks">
<?
					foreach ($arResult['COUPON_LIST'] as $oneCoupon)
					{
						switch ($oneCoupon['STATUS'])
						{
							case DiscountCouponsManager::STATUS_APPLYED:
								$couponClass = 'message-success';
								break;
							default:
								$couponClass = 'message-danger';
						}
					?>
                                        <div class="message-box <?=$couponClass?>">
                                            <div class="message-icon"><i class="fa fa-check"></i></div>
                                            <div class="message-text"><?=htmlspecialcharsbx($oneCoupon['COUPON']);?></div>
                                            <div class="message-close2"><a href="<?=str_replace("#COUPON#", urlencode($oneCoupon['COUPON']), $arUrls["delete_coupon"])?>"><i title="отменить код" class="fa fa-times"></i></a></div>
                                        </div>					
					<?
					}
?>
</div>
<?
}
?>									

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
	<?
}
?>