<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

if (!empty($arResult["ITEMS"]["AnDelCanBuy"]))
{
	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as &$arItem)
	{
		$arItem["PREVIEW_PICTURE_SRC"] = "";
		$arItem["SKU_PROPS"] = array();
		if (is_array($arItem["PROPS"]))
		{
			foreach ($arItem["PROPS"] as $arProp)
			{
				if ($arProp["VALUE"] != "")
				{
					if ($arProp["CODE"] == "PICTURE")
					{
						$filename =  basename($arProp["VALUE"]);
						$exist = false;
						$original_pic_path = "/images/".$filename;
						if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
							$exist = true;
						else
						{
							$content = file_get_contents($arProp["VALUE"]);
							if ($content !== false)
							{
								file_put_contents($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $content);
								if (file_exists($_SERVER["DOCUMENT_ROOT"].$original_pic_path))
									$exist = true;
							}
						}
						if ($exist == true)
						{
							$resized_pic_path = "/resized_images/210x290/".$filename;
							if (file_exists($_SERVER["DOCUMENT_ROOT"].$resized_pic_path))
								$arItem["PREVIEW_PICTURE_SRC"] = $resized_pic_path;
							else
							{
								$destinationFile = $_SERVER["DOCUMENT_ROOT"].$resized_pic_path;
								//if (CFile::ResizeImageFile($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, array("width"=>210,"height"=>290), BX_RESIZE_IMAGE_PROPORTIONAL))
								if (ToysalesResizeImage($_SERVER["DOCUMENT_ROOT"].$original_pic_path, $destinationFile, 210, 290))
									$arItem["PREVIEW_PICTURE_SRC"] = $resized_pic_path;
							}
						}
					}
					else
					if ($arProp["CODE"] == "size")
					{
						$arItem["SKU_PROPS"]["size"] = $arProp["VALUE"];
					}
					else
					if ($arProp["CODE"] == "color")
					{
						$arItem["SKU_PROPS"]["color"] = $arProp["VALUE"];
					}					
				}
			}
		}
	}
}