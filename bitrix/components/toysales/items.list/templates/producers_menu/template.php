<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ITEMS"]))
{
?>
                            <div class="information-blocks categories-border-wrapper">
                                <div class="block-title size-3">Производители</div>
                                <div class="accordeon">
                                        <div class="article-container style-1">
                                            <ul>
<?
	foreach ($arResult["ITEMS"] as $arItem)
	{
	?>
                                                <li<?if ($arItem["SELECTED"]) echo ' class="active"';?>><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></li>	
	<?
	}
?>											
                                            </ul>
                                        </div>
                                </div>
                            </div>
<?
}
?>