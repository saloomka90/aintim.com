<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ITEMS"]))
{
?>

                <div class="parallax-slide fullwidth-block small-slide" style="margin-bottom: 30px; margin-top: -25px;">
                    <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                        <div class="swiper-wrapper">
<?
	foreach ($arResult["ITEMS"] as $arItem)
	{
		if ($arItem["PREVIEW_PICTURE"]["SRC"] != "")
		{
		?>
                            <div class="swiper-slide no-shadow active" data-val="0" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"> 
							<?=$arItem["PREVIEW_TEXT"]?>
                            </div>
		<?
		}
	}
	?>
                        </div>
                        <div class="pagination"></div>
                    </div>
                </div>

<?
}
?>