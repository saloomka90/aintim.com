<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ITEMS"]))
{
?>
                <div class="information-blocks">
                    <h3 class="block-title">Наши производители</h3>
                    <div class="sidebar-logos-row">

<?
	foreach ($arResult["ITEMS"] as $key => $arItem)
	{
		if ($arItem["PREVIEW_PICTURE"]["SRC"] != "")
		{
?>
<div class="entry"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><img alt="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a></div>
<?
		}
	}
?>
					</div>
				</div>
<?
}
?>