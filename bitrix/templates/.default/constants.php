<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
define("IMPORT_PRICE_TYPE", 1); //Тип цены: 1 - розничная(колонка price), 2 - оптовая (basewholeprice)
define("IMPORT_NACENKA", 0); // Наценка в процентах

// в файлах выгрузки есть 2 колонки price (рекомендованная розничная цена) , basewholeprice (базовая оптовая цена)
// можно выбрать какой тип цены будет отображаться на сайте и применить к значению свою наценку
// пример1: показывать на сайте базовую оптовую цену с наценкой 40% 
// define("IMPORT_PRICE_TYPE", 2);
// define("IMPORT_NACENKA", 40); 

// пример2: показывать на сайте рекомендованную розничную цену с наценкой 0% 
// define("IMPORT_PRICE_TYPE", 1);
// define("IMPORT_NACENKA", 0); 
?>