<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

//if (strpos($arParams["~AUTH_RESULT"], "Логин или EMail не найдены.") === 0)
//var_dump($arParams["~AUTH_RESULT"]);

?>
                            <div class="information-blocks">
                                <h3 class="block-title main-heading">Восстановление пароля</h3>
<?
if ($arParams["~AUTH_RESULT"]["TYPE"] == "ERROR")
{
?>
								<div class="message-box message-danger">
                                    <div class="message-icon"><i class="fa fa-times"></i></div>
                                    <div class="message-text"><b>Ошибка!</b> Указанный Email не зарегистрирован в системе!</div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
<?
}
else
if ($arParams["~AUTH_RESULT"]["MESSAGE"] != "")
{
?>
                                <div class="message-box message-success">
                                    <div class="message-icon"><i class="fa fa-check"></i></div>
                                    <div class="message-text"><?=$arParams["~AUTH_RESULT"]["MESSAGE"]?></div>
                                    <div class="message-close"><i class="fa fa-times"></i></div>
                                </div>
<?
}
?>
							
<form name="bform" method="post" action="<?=$arResult["AUTH_URL"]?>">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">
                                    <div class="row">
                                        <div class="col-sm-12">
                                        <label>Email<span>*</span></label>
                                        <input name="USER_LOGIN" class="simple-field" type="email" placeholder="Введите Email, указанный при регистрации  (обязательно)" required value="" />
                                            <div class="button style-10">Восстановить пароль<input type="submit" name="send_account_info" value="" /></div>
                                        </div>
                                    </div>
	
</form>
							</div>