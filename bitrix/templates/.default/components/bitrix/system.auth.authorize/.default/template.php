<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
if ($arParams["~AUTH_RESULT"]["MESSAGE"] != "")
	$arParams["~AUTH_RESULT"]["MESSAGE"]= str_replace("логин", "e-mail", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>Зарегистрированные клиенты</h3>
                                    <p>Если у вас есть учётная запись на нашем сайте, пожалуйста, авторизируйтесь.</p>
                                    <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="continue-link" rel="nofollow">Забыли пароль?</a>
                                </div>
<?			
ShowMessage($arParams["~AUTH_RESULT"]);
if ($arParams["~AUTH_RESULT"]["MESSAGE"] != "")
{
	?><br><br><?
}
ShowMessage($arResult['ERROR_MESSAGE']);
?>
								<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

									<input type="hidden" name="AUTH_FORM" value="Y" />
									<input type="hidden" name="TYPE" value="AUTH" />
									<?if (strlen($arResult["BACKURL"]) > 0):?>
									<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
									<?endif?>
									<?foreach ($arResult["POST"] as $key => $value):?>
									<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
									<?endforeach?>                                
                                    <label>Email<span>*</span></label>
                                    <input name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" class="simple-field" type="email" placeholder="Введите Email (обязательно)" required />
                                    <label>Пароль<span>*</span></label>
                                    <input name="USER_PASSWORD" class="simple-field" type="password" placeholder="Введите пароль" required />
                                    <div class="button style-10">Войти<input type="submit" name="Login" value="" /></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>Новые клиенты</h3>
                                    <p>Создав учётную запись на нашем сайте, вы будете тратить меньше времени на оформление заказа, сможете хранить несколько адресов доставки, отслеживать состояние заказов, а также многое другое.</p>
                                </div>
                                <a href="/register/" class="button style-12">Зарегистрироваться</a>
                            </div>
                        </div>
					</div>
				</div>
