<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul>

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?
	if ($arItem["DEPTH_LEVEL"] == 1)
	{
		if ($previousLevel == 2)
		{
		?>
                                            </ul>
                                        </div>
                                    </li>		
		<?
		}
		if ($arItem["IS_PARENT"])
		{
		?>
                                    <li class="simple-list">
                                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">		
		<?
		}
		else
		{
		?>
		                                    <li class="simple-list">
                                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><i class="fa fa-chevron-down"></i>
											</li>
		<?
		}
	}
	else
	{
	?>
	<li><a href="<?=$arItem["LINK"]?>"><i class="fa fa-angle-right"></i><?=$arItem["TEXT"]?></a></li>
	<?
	}
	?>
	
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel == 2)
{
?>
                                            </ul>
                                        </div>
                                    </li>		
<?
}
?>
                                    <li class="fixed-header-visible">
                                        <a href="/personal/cart/" class="fixed-header-square-button open-cart-popup"><i class="fa fa-shopping-cart"></i></a>
                                        <a class="fixed-header-square-button open-search-popup"><i class="fa fa-search"></i></a>
                                    </li>
</ul>
<?endif?>