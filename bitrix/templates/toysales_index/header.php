<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("fx"));?>
<?
if (!$USER->IsAuthorized()) { 
CJSCore::Init(array('ajax', 'json', 'ls', 'session', 'popup', 'pull')); 
} 
?>
<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-30321438-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-30321438-1');
</script>

	<?$APPLICATION->ShowHead();?>
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
	<?
	$APPLICATION->AddHeadScript('/bitrix/templates/toysales/js/jquery-2.1.3.min.js');
	$APPLICATION->AddHeadScript('/bitrix/templates/toysales/js/global.js');

	$APPLICATION->SetAdditionalCSS("/bitrix/templates/toysales/css/bootstrap.min.css");	
	$APPLICATION->SetAdditionalCSS("/bitrix/templates/toysales/css/idangerous.swiper.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/templates/toysales/css/font-awesome.min.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/templates/toysales/css/style.css");	
	?>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700,500,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!--[if IE 9]>
		<?$APPLICATION->SetAdditionalCSS("/bitrix/templates/toysales/css/ie9.css");?>
    <![endif]-->
    <link rel="shortcut icon" href="/favicon-6.ico" />
  	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body class="style-<?if ($APPLICATION->GetCurPage(false) == "/") echo "21"; else echo "10";?>">
<?$APPLICATION->ShowPanel()?>
<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
</script>
    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div>
                <img src="/bitrix/templates/toysales/img/18big.png" alt="">
                <div class="title">Сайт предназначен только для лиц старше 18 лет.</div>
            </div>        
            <div class="title">загружается</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="<?if ($APPLICATION->GetCurPage(false) == "/") echo "wide"; else echo "content";?>-center fixed-header-margin">
            <!-- HEADER -->
            <div class="header-wrapper style-10">
                <header class="type-1">

                    <div class="header-product">
                        <div class="logo-wrapper">
                            <a href="/" id="logo"><img alt="" src="/bitrix/templates/toysales/img/logo.png"></a>
                        </div>
                        <div class="product-header-message">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/bitrix/templates/toysales/include_areas/header_text.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>						
                        </div>
                        <div class="product-header-content">
                            <div class="line-entry">
                                <div class="menu-button responsive-menu-toggle-class"><i class="fa fa-reorder"></i></div>
                                <div class="header-top-entry increase-icon-responsive open-search-popup">
                                    <div class="title"><i class="fa fa-search"></i> <span>Поиск</span></div>
                                </div>
<?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form",
	"top",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"FORGOT_PASSWORD_URL" => "/auth/",
		"PROFILE_URL" => "/personal/",
		"REGISTER_URL" => "/register/",
		"SHOW_ERRORS" => "N"
	)
);?>
                                <div class="header-top-entry">
                                    <div class="title"><i class="fa fa-phone"></i><span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/bitrix/templates/toysales/include_areas/header_phone.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>															
									</span></div>
                                </div>
                            </div>
                            <div class="middle-line"></div>
                            <div class="line-entry">
                                <a href="/info/contacts/" class="header-functionality-entry"><i class="fa fa-map-marker"></i><span>Контакты</span></a>
                                <a href="/personal/cart/" class="header-functionality-entry open-cart-popup"><i class="fa fa-shopping-cart"></i><span>Корзина</span> <b class="cart-sum">
<?$APPLICATION->IncludeComponent(
	"toysales:sale.basket.basket.line",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
	)
);?>								
								</b></a>
                            </div>
                        </div>
                    </div>

                    <div class="close-header-layer"></div>
                    <div class="navigation">
                        <div class="navigation-header responsive-menu-toggle-class">
                            <div class="title">Меню</div>
                            <div class="close-menu"></div>
                        </div>
                        <div class="nav-overflow">
                            <nav>
<?$APPLICATION->IncludeComponent(
	"toysales:catalog.section.list",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog"
	)
);?>   

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "top",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N"
	),
	false
);?>
                                <div class="clear"></div>

                                <a class="fixed-header-visible additional-header-logo"><img src="/bitrix/templates/toysales/img/logo.png" alt=""/></a>
                            </nav>
                        </div>
                    </div>
                </header>
                <div class="clear"></div>
            </div>

            <div class="content-push">
<?
if ($APPLICATION->GetCurPage(false) != "/")
{
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	),
false
);?>
<?
}
?>			