<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

        <!-- FOOTER -->
                <div class="footer-wrapper style-3">
                    <footer class="type-1">
                        <div class="footer-columns-entry">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/footer.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>
                        </div>
                    </footer>
                </div>
            </div>

        </div>
        <div class="clear"></div>

    </div>


    <div class="search-box popup">
        <form action="/search/">
            <div class="search-button">
                <i class="fa fa-search"></i>
                <input type="submit" />
            </div>
            <div class="search-drop-down">
                <div class="title"><span>на сайте</span></div>
            </div>
            <div class="search-field">
                <input type="text" name="search_text" value="" placeholder="что искать?" />
            </div>
        </form>
    </div>


    <div class="cart-box popup">
<?$APPLICATION->IncludeComponent(
	"toysales:sale.basket.basket.small",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_ORDER" => "/personal/order/make/"
	)
);?>
    </div>
<div id="product-popup" class="overlay-popup"></div>
<?$APPLICATION->IncludeComponent(
	"toysales:main.subscribe_popup",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default"
	)
);?>

    <!--<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-2.1.3.min.js"></script>-->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/idangerous.swiper.min.js"></script>
    <!-- custom scrollbar -->
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jscrollpane.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui.min.js"></script>	

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/bitrix/templates/toysales/include_areas/counter.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>


<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=13376740&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/13376740/3_1_FFFFFFFF_FFFFFFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="13376740" data-lang="ru" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter13376740 = new Ya.Metrika({
                    id:13376740,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/13376740" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
