<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams, $arResult
 * @var CBitrixComponentTemplate $this
 * @var CMain $APPLICATION
 * @var CUser $USER
 */
?>
<a name="review_anchor"></a>
                                    <br><h3 class="additional-blog-title">Добавить отзыв</h3>
<?
if (!empty($arResult["ERROR_MESSAGE"])):
	$arResult["ERROR_MESSAGE"] = preg_replace(array("/<br(.*?)><br(.*?)>/is", "/<br(.*?)>$/is"), array("<br />", ""), $arResult["ERROR_MESSAGE"]);
	?>
	<div class="reviews-note-box reviews-note-error">
		<div class="reviews-note-box-text"><?=ShowError($arResult["ERROR_MESSAGE"], "reviews-note-error");?></div>
	</div>
<?
endif;
?>
<script type="text/javascript">
window.reviewsCtrlEnterHandler<?=CUtil::JSEscape($arParams["form_index"]);?> = function()
{
	if (window.<?=$arParams["jsObjName"]?>)
		window.<?=$arParams["jsObjName"]?>.SaveContent();
	BX.submit(BX('<?=$arParams["FORM_ID"] ?>'), 'preview_comment', 'N');
}
</script>
<div class="reviews-reply-form2" <?=(($arParams['SHOW_MINIMIZED'] == "Y") ? 'style="display:none;"' : '' )?>>
<form name="<?=$arParams["FORM_ID"] ?>" id="<?=$arParams["FORM_ID"]?>" action="<?=POST_FORM_ACTION_URI?>#postform"<?
?> method="POST" enctype="multipart/form-data" <?
?> onsubmit="return window.UC.f<?=$arParams["FORM_ID"]?>.validate('<?=$arParams["AJAX_POST"]?>');"<?
?> class="reviews-form2">
	<input type="hidden" name="index" value="<?=htmlspecialcharsbx($arParams["form_index"])?>" />
	<input type="hidden" name="back_page" value="<?=$arResult["CURRENT_PAGE"]?>" />
	<input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
	<input type="hidden" name="SECTION_ID" value="<?=$arResult["ELEMENT_REAL"]["IBLOCK_SECTION_ID"]?>" />
	<input type="hidden" name="save_product_review" value="Y" />
	<input type="hidden" name="preview_comment" value="N" />
	<?=bitrix_sessid_post()?>
	<?
	if ($arParams['AUTOSAVE'])
		$arParams['AUTOSAVE']->Init();
	?>
                                        <div class="row">
<!--										
                                            <div class="col-sm-6">
                                                <label>Ваше имя <span>*</span></label>
                                                <input class="simple-field" type="text" placeholder="Ваше имя (обязательно)" name="REVIEW_AUTHOR" id="REVIEW_AUTHOR<?=$arParams["form_index"]?>" value="<?=$arResult["REVIEW_AUTHOR"]?>" />
                                                <div class="clear"></div>
                                            </div>
-->											
                                            <div class="col-sm-12">
                                                <label>Ваше сообщение <span>*</span></label>
                                                <textarea class="simple-field" name="REVIEW_TEXT"  placeholder="Ваше сообщение (обязательно)"><?=$arResult["REVIEW_TEXT"]?></textarea>
                                                <div class="button style-10">Отправить<input type="submit" name="send_button" value="Отправить" /></div>
                                            </div>
                                        </div>
	</form>
<script type="text/javascript">
BX.ready(function(){
	window["UC"] = (!!window["UC"] ? window["UC"] : {});
	window["UC"]["l<?=$arParams["FORM_ID"]?>"] = new FTRList({
		id : <?=CUtil::PhpToJSObject(array_keys($arResult["MESSAGES"]))?>,
		form : BX('<?=$arParams["FORM_ID"]?>'),
		preorder : '<?=$arParams["PREORDER"]?>',
		pageNumber : <?=intval($arResult['PAGE_NUMBER']);?>,
		pageCount : <?=intval($arResult['PAGE_COUNT']);?>
	});
	window["UC"]["f<?=$arParams["FORM_ID"]?>"] = new FTRForm({
		form : BX('<?=$arParams["FORM_ID"]?>'),
		editorName : '<?=$arParams["jsObjName"]?>',
		editorId : '<?=$arParams["LheId"]?>'
	});
	<? if ($arParams['SHOW_MINIMIZED'] == "Y")
	{
	?>
	BX.addCustomEvent(this.form, 'onBeforeShow', function(obj) {
		var link = BX('sw<?=$arParams["FORM_ID"]?>');
		if (link) {
			link.innerHTML = BX.message('MINIMIZED_EXPAND_TEXT');
			BX.removeClass(BX.addClass(link.parentNode, "reviews-expanded"), "reviews-minimized");
		}
	});
	BX.addCustomEvent(this.form, 'onBeforeHide', function(obj) {
		var link = BX('sw<?=$arParams["FORM_ID"]?>');
		if (link) {
			link.innerHTML = BX.message('MINIMIZED_MINIMIZE_TEXT');
			BX.removeClass(BX.addClass(link.parentNode, "reviews-minimized"), "reviews-expanded");
		}
	});
	<?
	}
	?>
});
</script>
</div>
<?
if ($arParams['AUTOSAVE'])
	$arParams['AUTOSAVE']->LoadScript(array(
		"formID" => CUtil::JSEscape($arParams["FORM_ID"]),
		"controlID" => "REVIEW_TEXT"
	));
?>