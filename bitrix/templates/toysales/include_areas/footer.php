<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="row">
	<div class="col-md-3">
 <img src="/bitrix/templates/toysales/img/18.png" class="footer-logo" alt="">
		<div class="footer-description">
			 Широкий ассортимент товаров от эротического белья до элитных секс-игрушек способен удовлетворить, во всех смыслах этого слова, каждого покупателя.
		</div>
		<div class="clear">
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<h3 class="column-title">Информация</h3>
		<ul class="column">
			<li><a href="/info/">О компании</a></li>
			<li><a href="/info/contacts/">Контакты</a></li>
			<li><a href="/info/delivery/">Доставка и оплата</a></li>
			<li><a href="/info/privacy/">Конфиденциальность</a></li>
			<li><a href="/info/refund/">Гарантии и возврат</a></li>
		</ul>
		<div class="clear">
		</div>
	</div>
	<div class="col-md-3">
		<h3 class="column-title">Адрес</h3>
		<div class="footer-address">
			 Москва, ул. Профсоюзная, д. 6А, стр. 2<br>
			 Телефон: <a href="tel:+74993756560">+7 (499) 375-65-60</a><br>
			 Email: <a href="mailto:info@aintim.com">info@aintim.com</a><br>
 <a href="http://www.aintim.com"><b>www.aintim.com</b></a>
		</div>
		<div class="clear">
		</div>
	</div>
	<div class="clearfix visible-sm-block">
	</div>
	<div class="col-md-3">
		<h3 class="column-title">Время работы</h3>
		<div class="footer-description">
 <b>Понедельник-Пятница:</b> 10.00 - 18.00<br>
 <b>Суббота:</b> 10.00 - 17.00<br>
 <b>Воскресенье:</b> выходной.
		</div>
		<div class="clear">
		</div>
	</div>
</div>
 <br>