<?
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
//Вписывает изображение в указанный прямоугольник, дополняя свободное пространство белыми полосами
//Т.е. у картинки будут ровно те размеры, которые заданы
//Если параметр width_only равен true, то белые полосы добавляются только по бокам
function ToysalesResizeImage($original_path, $destination_path, $max_width, $max_height, $width_only = false)
{
	$max_rel = $max_width / $max_height;
	$img_arr = GetImageSize($original_path);  // Получаем инфу о картинке(размеры и тип)
	$original_width = $img_arr[0];
	$original_height = $img_arr[1];
	$type = $img_arr[2];
	if ($original_width <= $max_width and $original_height <= $max_height) // если картинка сразу вписывается в нащи размеры 
	{
		$to_resize_w = $original_width;
		$to_resize_h = $original_height;
		$w = $original_width;
		$h = $original_height;		
	}
	else
	{                                          
		$rel = $original_width / $original_height;
		if ($rel > $max_rel)
		{
			$width = $max_width;
			$height = $original_height * $max_width / $original_width;
		}
		else
		if ($rel < $max_rel)
		{
			$height = $max_height;
			$width = $original_width * $max_height / $original_height;
		}
		else
		if ($rel == $max_rel)
		{
			$width = $max_width;
			$height = $max_height;
		}
		$width = round($width);
		$height = round($height);
		if ($width > $max_width)
			$width = $max_width;
		if ($height > $max_height)
			$height = $max_height;
		$to_resize_w = $width;
		$to_resize_h = $height;
		$w = $original_width;
		$h = $original_height;
	}
	
	if ($width_only == true)
		$result_height = $to_resize_h;
	else
		$result_height = $max_height;
	
		if (function_exists('imagecreatetruecolor'))
		{
			$img_640 = imagecreatetruecolor($max_width, $result_height);
		}
		else
		{
			$img_640 = ImageCreate($max_width, $result_height);
		}
		$white = imagecolorallocate($img_640, 255, 255, 255);
		imagefill($img_640, 0, 0, $white);
		
		if ($type == 1)
			$img_640_src = Imagecreatefromgif($original_path);
		else if ($type == 2)
			$img_640_src = Imagecreatefromjpeg($original_path);
		else if ($type == 3)
			$img_640_src = Imagecreatefrompng($original_path);

		$x_pos = intval(($max_width - $to_resize_w) / 2);
		$y_pos = intval(($result_height - $to_resize_h) / 2);
		if ($img_640 and $img_640_src)
		{
			if (function_exists('imagecopyresampled'))
			{
				ImageCopyResampled($img_640, $img_640_src, $x_pos, $y_pos, 0, 0, $to_resize_w, $to_resize_h, $w, $h);
			}
			else
			{
				ImageCopyResized($img_640, $img_640_src, $x_pos, $y_pos, 0, 0, $to_resize_w, $to_resize_h, $w, $h);
			}
		}	

	if ($type == 1)
	{
		if (imagegif($img_640, $destination_path))
			return $destination_path;
	}
	else
	if ($type == 2 or $type == 3)
	{
		if (imagejpeg($img_640, $destination_path, 100))
			return $destination_path;
	}
	return false;
}

//-- Добавление обработчика события

AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");

//-- Собственно обработчик события

function bxModifySaleMails($orderID, &$eventName, &$arFields)
{
	if ($arOrder = CSaleOrder::GetByID($orderID))
	{
		$strOrderList = "";
		$arBasketList = array();
		$fullBasketList = array();
		$dbBasketItems = CSaleBasket::GetList(
				array("ID" => "ASC"),
				array("ORDER_ID" => $orderID),
				false,
				false,
				array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY", "TYPE", "SET_PARENT_ID", "DETAIL_PAGE_URL")
			);
		while ($arItem = $dbBasketItems->Fetch())
		{
			if (CSaleBasketHelper::isSetItem($arItem))
				continue;
			$arBasketList[] = $arItem;
		}

		$arBasketList = getMeasures($arBasketList);

		if (!empty($arBasketList) && is_array($arBasketList))
		{
			foreach ($arBasketList as $arItem)
			{
				$props = "";
				$rsBasketProps = CSaleBasket::GetPropsList(
					array('SORT' => 'ASC', 'ID' => 'ASC'),
					array('BASKET_ID' => $arItem["ID"], 'CODE' => array('size', 'color', 'prodID'))
				);
				while ($arBasketProp = $rsBasketProps->GetNext())
				{
					if ($arBasketProp["VALUE"] != "")
					{
						if ($props != "")
							$props .= ", ";
						$props .= $arBasketProp["NAME"].": ".$arBasketProp["VALUE"];
					}
				}
				if ($props != "")
					$props = " [".$props."]";				
				$measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : "шт.";
					$strOrderList .= '<a href="http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"].'" style="color:#2e6eb6;">'.$arItem["NAME"].'</a>'.$props." - ".$arItem["QUANTITY"]." ".$measureText.": ".SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
				$strOrderList .= "\n<br>";
			}
		}
		$arFields["ORDER_LIST"] = $strOrderList;
	}
}

AddEventHandler("sale", "OnOrderSave", "FireOnOrderSave");

function FireOnOrderSave($orderID, $arFields, $arOrder, $isNew)
{

	//$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/log_order_save.txt","ab");

	//@fwrite($fp, "#isNew - #".$content."\n");
	
	//@fwrite($fp, print_r($arFields, true)."\n");
	//@fwrite($fp, print_r($arOrder, true)."\n");
	
	if ($isNew == true)
	{
		if (is_array($arOrder["BASKET_ITEMS"]))
		{
			foreach ($arOrder["BASKET_ITEMS"] as $arBasketItem)
			{
				if (!isset($arBasketItem["PROPS"]["aID"]) || !isset($arBasketItem["PROPS"]["prodID"])  || !isset($arBasketItem["PROPS"]["stripmag"])) // Если не заполнено хотя бы одно из нужных нам свойств
				{
					$new_props = array();
					// Сохраняем текущие свойства товара
					foreach ($arBasketItem["PROPS"] as $arProp)
					{
						unset($arProp["ID"]);
						$new_props[] = $arProp;
					}
					
					$xml_id_arr = explode("#", $arBasketItem["PRODUCT_XML_ID"]);
					if ($xml_id_arr[0] != "") 
					{
						if (!isset($arBasketItem["PROPS"]["prodID"]))
							$new_props[] = array(
								"CODE" => "prodID",
								"VALUE" => $xml_id_arr[0],
								"NAME" => "prodID",							
								"SORT" => "210",
							);
						if (!isset($arBasketItem["PROPS"]["stripmag"])) 
						{
							$new_props[] = array(
								"CODE" => "stripmag",
								"VALUE" => "http://stripmag.ru/prod.php?id=".$xml_id_arr[0],
								"NAME" => "Ссылка на stripmag",							
								"SORT" => "220",
							);
						}					
							
					}
					if (!isset($arBasketItem["PROPS"]["aID"]) && $xml_id_arr[1] != "") 
					{
						$new_props[] = array(
							"CODE" => "aID",
							"VALUE" => $xml_id_arr[1],
							"NAME" => "aID",							
							"SORT" => "200",
						);
					}					
					$arBasketFields = array(
						"PROPS" => $new_props
					);
					CSaleBasket::Update($arBasketItem["ID"], $arBasketFields);
				}				
			}
		}
	}

	//if ($fp)
		//@fclose($fp);	
}

//Тресков В.
//Формирование дескрипшенов к товарам

\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock','OnAfterIBlockElementAdd', array('CIBlockExtend','OnAddUpdateHandler'));
\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock','OnAfterIBlockElementUpdate', array('CIBlockExtend','OnAddUpdateHandler'));

class CIBlockExtend
{

    function OnAddUpdateHandler(&$arFields)
    {

        if($arFields['IBLOCK_ID']==5)
        {

            $arElement = \CIBlockElement::GetList(
                array(),
                array(
                    "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                    "ID" => $arFields['ID']
                ),
                false,
                false,
                array(
                    "ID",
                    "IBLOCK_ID",
                    "PREVIEW_TEXT"
                )
            )->Fetch();

            if($arElement)
            {

                $sStr = substr($arElement['PREVIEW_TEXT'],0,200);

                $iLastDotted = strrpos($sStr, ".");

                if($iLastDotted!==false)
                {

                    $sStr = substr($sStr,0,$iLastDotted+1);

                }


                \CIBlockElement::SetPropertyValuesEx(
                    $arElement['ID'],
                    $arElement['IBLOCK_ID'],
                    array(
                        'SEO_DESCRIPTION' => $sStr
                    )
                );

            }

        }

    }

}


/*Version 0.3 2011-04-25*/
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "DoIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "DoIBlockAfterSave");
function DoIBlockAfterSave($arg1, $arg2 = false)
{
 $ELEMENT_ID = false;
 $IBLOCK_ID = false;
 $OFFERS_IBLOCK_ID = false;
 $OFFERS_PROPERTY_ID = false;
 if (CModule::IncludeModule('currency'))
 $strDefaultCurrency = CCurrency::GetBaseCurrency();

 //Check for catalog event
 if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
 {
 //Get iblock element
 $rsPriceElement = CIBlockElement::GetList(
 array(),
 array(
 "ID" => $arg2["PRODUCT_ID"],
 ),
 false,
 false,
 array("ID", "IBLOCK_ID")
 );
 if($arPriceElement = $rsPriceElement->Fetch())
 {
 $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
 if(is_array($arCatalog))
 {
 //Check if it is offers iblock
 if($arCatalog["OFFERS"] == "Y")
 {
 //Find product element
 $rsElement = CIBlockElement::GetProperty(
 $arPriceElement["IBLOCK_ID"],
 $arPriceElement["ID"],
 "sort",
 "asc",
 array("ID" => $arCatalog["SKU_PROPERTY_ID"])
 );
 $arElement = $rsElement->Fetch();
 if($arElement && $arElement["VALUE"] > 0)
 {
 $ELEMENT_ID = $arElement["VALUE"];
 $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
 }
 }
 //or iblock which has offers
 elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
 {
 $ELEMENT_ID = $arPriceElement["ID"];
 $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
 }
 //or it's regular catalog
 else
 {
 $ELEMENT_ID = $arPriceElement["ID"];
 $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = false;
 $OFFERS_PROPERTY_ID = false;
 }
 }
 }
 }
 //Check for iblock event
 elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
 {
 //Check if iblock has offers
 $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
 if(is_array($arOffers))
 {
 $ELEMENT_ID = $arg1["ID"];
 $IBLOCK_ID = $arg1["IBLOCK_ID"];
 $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
 $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
 }
 }

 if($ELEMENT_ID)
 {
 static $arPropCache = array();
 if(!array_key_exists($IBLOCK_ID, $arPropCache))
 {
 //Check for MINIMAL_PRICE property
 $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
 $arProperty = $rsProperty->Fetch();
 if($arProperty)
 $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
 else
 $arPropCache[$IBLOCK_ID] = false;
 }

 if($arPropCache[$IBLOCK_ID])
 {
 //Compose elements filter
 if($OFFERS_IBLOCK_ID)
 {
 $rsOffers = CIBlockElement::GetList(
 array(),
 array(
 "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
 "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
 ),
 false,
 false,
 array("ID")
 );
 while($arOffer = $rsOffers->Fetch())
 $arProductID[] = $arOffer["ID"];

 if (!is_array($arProductID))
 $arProductID = array($ELEMENT_ID);
 }
 else
 $arProductID = array($ELEMENT_ID);

 $minPrice = false;
 $maxPrice = false;
 //Get prices
 $rsPrices = CPrice::GetList(
 array(),
 array(
 "PRODUCT_ID" => $arProductID,
 )
 );
 while($arPrice = $rsPrices->Fetch())
 {
 if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY'])
 $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);

 $PRICE = $arPrice["PRICE"];

 if($minPrice === false || $minPrice > $PRICE)
 $minPrice = $PRICE;

 if($maxPrice === false || $maxPrice < $PRICE)
 $maxPrice = $PRICE;
 }

 //Save found minimal price into property
 if($minPrice !== false)
 {
 CIBlockElement::SetPropertyValuesEx(
 $ELEMENT_ID,
 $IBLOCK_ID,
 array(
 "MINIMUM_PRICE" => $minPrice,
 "MAXIMUM_PRICE" => $maxPrice,
 )
 );
 }
 }
 }
}
?>