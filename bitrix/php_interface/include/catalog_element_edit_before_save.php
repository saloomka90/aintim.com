<?
function BXIBlockAfterSave($arFields)
{
	$arFilter = array(
		"ID" => $arFields['ID'],
	);
	$rsItem = CIBlockElement::GetList(array(), $arFilter);
	$str = "";
	if ($obItem = $rsItem->GetNextElement())
	{
		$dir_path_arr = array(
			$_SERVER["DOCUMENT_ROOT"]."/images/"
		);
		$dir_path = $_SERVER["DOCUMENT_ROOT"]."/resized_images/";
		if ($handle = opendir($dir_path))
		{
			while (false !== ($entry = readdir($handle)))
			{
				if ($entry != "." && $entry != ".." && is_dir($dir_path.$entry))
				{
					$dir_path_arr[] = $dir_path.$entry."/";
				}
			}
			closedir($handle);
		}		
		$arItem = $obItem->GetFields();
		$arItem["PROPERTIES"] = $obItem->GetProperties();
		$i = 1;
		while ($i <= 10)
		{
			if ($arItem["PROPERTIES"]["pict".$i]["VALUE"] != "")
			{
				$filename = basename($arItem["PROPERTIES"]["pict".$i]["VALUE"]);
				foreach ($dir_path_arr as $dir_path)
				{
					if (file_exists($dir_path.$filename))
					{
						unlink($dir_path.$filename);
						$str .= $dir_path.$filename."\n";
					}
				}
			}
			$i++;
		}
	}
	//if ($str != "")
	//	AddMessage2Log($str);
}
?>