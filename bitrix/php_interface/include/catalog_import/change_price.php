<?
@set_time_limit(0);

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default/constants.php");

if (!defined("IMPORT_PRICE_TYPE"))
	define("IMPORT_PRICE_TYPE", 1);
if (!defined("IMPORT_PRICE_TYPE"))
	define("IMPORT_PRICE_TYPE", 0);

$import_price_type = IMPORT_PRICE_TYPE;
if ($import_price_type != 1 and $import_price_type != 2)
	$import_price_type = 1;
$import_nacenka = intval(IMPORT_NACENKA)/100;

$URL_DATA_FILE = $_SERVER["DOCUMENT_ROOT"]."/import/bitrix_stock.csv";
$NEW_FILE = $_SERVER["DOCUMENT_ROOT"]."/import/bitrix_stock_new.csv";

$handle = @fopen($URL_DATA_FILE, "rb");
$handle2 = @fopen($NEW_FILE, "wb");

if ($handle and $handle2)
{
	$i = 0;
	$new_content = "";
	while (($data = fgetcsv($handle, 1000, ";")) !== false)
	{
		$i++;
		if ($i == 1)
		{
			unset($data[11]);
		}
		else
		{
			if ($import_price_type == 1)
			{
				$data[10] = ceil($data[10] + $data[10] * $import_nacenka);
				unset($data[11]);
			}
			else
			{
				$data[11] = ceil($data[11] + $data[11] * $import_nacenka);
				unset($data[10]);
			}			
		}
		$new_content .= implode(";",$data);
		$new_content .= "\n";
	}
	@fwrite($handle2, $new_content);
	@fclose($handle);
	@fclose($handle2);
}		
?>