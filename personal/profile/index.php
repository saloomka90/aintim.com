<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 col-sm-push-3">
                            <div class="article-container style-1">
                                <h1>Профиль пользователя</h1>
                                <p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.</p>
                                
                                <h2>Личные данные</h2>
							
<?$APPLICATION->IncludeComponent("toysales:main.profile", "", Array(
	),
	false
);?>

                            </div>

                        </div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/personal/menu_inc.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>		
                    </div>
                </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>