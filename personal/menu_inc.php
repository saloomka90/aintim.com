<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
                        <div class="col-sm-3 col-sm-pull-9 blog-sidebar">
                            <div class="information-blocks">
                                <div class="categories-list account-links">
                                    <div class="block-title size-3">Личный кабинет</div>
                                    <ul>
                                        <li><a href="/personal/profile/">Персональные данные </a></li>
                                        <li><a href="/personal/order/">Заказы</a></li>
                                        <li><a href="/personal/cart/">Моя корзина</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>