<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 col-sm-push-3">
                            <div class="article-container style-1">
                                <h1>Персональный раздел</h1>
                                <p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.</p>
                                
                                <h2>Личная информация</h2>
                                <a href="/personal/profile/" class="continue-link">Изменить регистрационные данные <i class="fa fa-long-arrow-right"></i></a><br/>
                                <h2>Заказы</h2>
                                <a href="/personal/order/" class="continue-link">Ознакомиться с состоянием заказов <i class="fa fa-long-arrow-right"></i></a><br/>
                                <a href="/personal/cart/" class="continue-link">Посмотреть содержимое корзины <i class="fa fa-long-arrow-right"></i></a><br/>
                                <a href="/personal/order/" class="continue-link">Посмотреть историю заказов <i class="fa fa-long-arrow-right"></i></a><br/>
                            </div>


                        </div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/personal/menu_inc.php",
		"EDIT_TEMPLATE" => "page_inc.php"
	)
);?>	
                    </div>
                </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
