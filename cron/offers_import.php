<?
//$_SERVER["DOCUMENT_ROOT"] = "/var/www/intim1/data/www/intim-1.ru";
include("config.php");

$start_time = time();

$siteID = 's1';  // your site ID - need for language ID

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);
if (preg_match('/^[a-z0-9_]{2}$/i', $siteID) === 1)
{
	define('SITE_ID', $siteID);
}
else
{
	die('No defined site - $siteID');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!defined('LANGUAGE_ID') || preg_match('/^[a-z]{2}$/i', LANGUAGE_ID) !== 1)
	die('Language id is absent - defined site is bad');

set_time_limit (0);

if (!defined("CATALOG_LOAD_NO_STEP"))
	define("CATALOG_LOAD_NO_STEP", true);

if (CModule::IncludeModule("catalog"))
{
	$profile_id = 4;

	$ar_profile = CCatalogImport::GetByID($profile_id);
	if (!$ar_profile) die("No profile");

	$success = false;
	$import_data = file_get_contents('http://stripmag.ru/datafeed/bitrix_stock.csv');
	$data_file = $_SERVER["DOCUMENT_ROOT"]."/import/offers.csv";
	if ($import_data !== false)
	{
		$fp = @fopen($data_file,"wb");
		if ($fp)
		{
			@fwrite($fp, $import_data);
			@fclose($fp);
/*
			include($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default/constants.php");

			if (!defined("IMPORT_PRICE_TYPE"))
				define("IMPORT_PRICE_TYPE", 1);
			if (!defined("IMPORT_PRICE_TYPE"))
				define("IMPORT_PRICE_TYPE", 0);

			$import_price_type = IMPORT_PRICE_TYPE;
*/
			$import_price_type = COption::GetOptionString('toysales.main', 'TOYSALES_IMPORT_PRICE_TYPE', 1);

			if ($import_price_type != 1 and $import_price_type != 2)
				$import_price_type = 1;

//			$import_nacenka = intval(IMPORT_NACENKA)/100;
			$import_nacenka = intval(COption::GetOptionString('toysales.main', 'TOYSALES_IMPORT_NACENKA', 0))/100;

			$fp = @fopen($data_file, "rb");
			if ($fp)
			{
				$i = 0;
				$new_content = "";
				while (($data = fgetcsv($fp, 1000, ";")) !== false)
				{
					$i++;
					if ($i == 1)
					{
						unset($data[11]);
					}
					else
					{
						if ($import_price_type == 1)
						{
							$data[10] = ceil($data[10] + $data[10] * $import_nacenka);
							unset($data[11]);
						}
						else
						{
							$data[11] = ceil($data[11] + $data[11] * $import_nacenka);
							unset($data[10]);
						}			
					}
					$new_content .= implode(";",$data);
					$new_content .= "\n";
					//if ($i > 100)
					//	break;
				}
				@fclose($fp);
				$fp = @fopen($data_file, "wb");
				if ($fp)
				{
					@fwrite($fp, $new_content);
					@fclose($fp);
					$success = true;
				}
			}					
		}
	}
	if ($success === false)
		exit;

	$strFile = CATALOG_PATH2IMPORTS.$ar_profile["FILE_NAME"]."_run.php";
	if (!file_exists($_SERVER["DOCUMENT_ROOT"].$strFile))
	{
		$strFile = CATALOG_PATH2IMPORTS_DEF.$ar_profile["FILE_NAME"]."_run.php";
		if (!file_exists($_SERVER["DOCUMENT_ROOT"].$strFile))
		{
			die("No import script");
		}
	}

	$bFirstLoadStep = true;

	$arSetupVars = array();
	$intSetupVarsCount = 0;
	if ($ar_profile["DEFAULT_PROFILE"] != 'Y')
	{
		parse_str($ar_profile["SETUP_VARS"], $arSetupVars);
		if (!empty($arSetupVars) && is_array($arSetupVars))
		{
			$intSetupVarsCount = extract($arSetupVars, EXTR_SKIP);
		}
	}

	global $arCatalogAvailProdFields;
	$arCatalogAvailProdFields = CCatalogCSVSettings::getSettingsFields(CCatalogCSVSettings::FIELDS_ELEMENT);
	global $arCatalogAvailPriceFields;
	$arCatalogAvailPriceFields = CCatalogCSVSettings::getSettingsFields(CCatalogCSVSettings::FIELDS_CATALOG);
	global $arCatalogAvailValueFields;
	$arCatalogAvailValueFields = CCatalogCSVSettings::getSettingsFields(CCatalogCSVSettings::FIELDS_PRICE);
	global $arCatalogAvailQuantityFields;
	$arCatalogAvailQuantityFields = CCatalogCSVSettings::getSettingsFields(CCatalogCSVSettings::FIELDS_PRICE_EXT);
	global $arCatalogAvailGroupFields;
	$arCatalogAvailGroupFields = CCatalogCSVSettings::getSettingsFields(CCatalogCSVSettings::FIELDS_SECTION);

	global $defCatalogAvailProdFields;
	$defCatalogAvailProdFields = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_ELEMENT);
	global $defCatalogAvailPriceFields;
	$defCatalogAvailPriceFields = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_CATALOG);
	global $defCatalogAvailValueFields;
	$defCatalogAvailValueFields = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_PRICE);
	global $defCatalogAvailQuantityFields;
	$defCatalogAvailQuantityFields = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_PRICE_EXT);
	global $defCatalogAvailGroupFields;
	$defCatalogAvailGroupFields = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_SECTION);
	global $defCatalogAvailCurrencies;
	$defCatalogAvailCurrencies = CCatalogCSVSettings::getDefaultSettings(CCatalogCSVSettings::FIELDS_CURRENCY);

	CCatalogDiscountSave::Disable();
	include($_SERVER["DOCUMENT_ROOT"].$strFile);
	CCatalogDiscountSave::Enable();

	CCatalogImport::Update($profile_id, array(
		"=LAST_USE" => $DB->GetNowFunction()
		)
	);
	@copy($data_file, $_SERVER["DOCUMENT_ROOT"]."/import/offers/offers_".date("d.m.Y H i").".csv");

	$timestamp = time() - 7 * 86400;
	$dir_path = $_SERVER["DOCUMENT_ROOT"]."/import/offers/";
	if ($handle = opendir($dir_path))
	{
		while (false !== ($entry = readdir($handle)))
		{
			if (is_file($dir_path.$entry))
			{
				if (filemtime($dir_path.$entry) < $timestamp)
					@unlink($dir_path.$entry);
			}
		}
		closedir($handle);
	}
	$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/cron/logs/offers_import_date.txt","wb");
	if ($fp)
	{
		@fwrite($fp, date("d.m.Y H:i:s"));
		fclose($fp);
	}	
}

$end_time = time();
echo "work_time ".ceil(($end_time - $start_time)/60)." minutes\n";
?>