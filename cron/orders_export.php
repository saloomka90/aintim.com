<?
include("config.php");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/cron/logs/orders_export_date.txt","wb");
if ($fp)
{
	@fwrite($fp, date("d.m.Y H:i:s"));
	fclose($fp);
}
$log_error = "";

$api_key = trim(COption::GetOptionString('toysales.main', 'TOYSALES_P5S_API_KEY'));

CModule::IncludeModule('sale');
//Соответствие доставок Битрикса доставкам P5S
$delivery_arr = array(
	2 => 4,
	9 => 5,
	10 => 5,
	11 => 1,
	12 => 1,
	13 => 7,
	14 => 7,
	15 => 2,
	16 => 2,
);
$arFilter = array(
	"STATUS_ID" => "ZS",
	"CANCELED" => "N",
);
$rsOrders = CSaleOrder::GetList(array(), $arFilter);
while ($arOrder = $rsOrders->GetNext())
{
	//echo "<br>#".$arOrder["DELIVERY_ID"]."#<br>";
	$dbProps = CSaleOrderPropsValue::GetList(
		array("SORT" => "ASC"),
		array(
			"ORDER_ID" => $arOrder["ID"],
		)
	);
	$arOrder["LOCATION_NAME"] = "";
	while ($arProp = $dbProps->Fetch())
	{
		if ($arProp["VALUE"] != "")
		{
			if ($arProp["CODE"] == "LOCATION")
			{
				$res = \Bitrix\Sale\Location\LocationTable::getList(array(
					'filter' => array(
						'=ID' => $arProp["VALUE"], 
						'=PARENTS.NAME.LANGUAGE_ID' => LANGUAGE_ID,
						'=PARENTS.TYPE.NAME.LANGUAGE_ID' => LANGUAGE_ID,
					),
					'select' => array(
						'I_ID' => 'PARENTS.ID',
						'I_NAME_RU' => 'PARENTS.NAME.NAME',
						'I_TYPE_CODE' => 'PARENTS.TYPE.CODE',
				//        'I_TYPE_NAME_RU' => 'PARENTS.TYPE.NAME.NAME'
					),
					'order' => array(
						'PARENTS.DEPTH_LEVEL' => 'asc'
					)
				));
				while($item = $res->fetch())
				{
					if (in_array($item["I_TYPE_CODE"], array("CITY", "VILLAGE", "REGION")))
					{
						if ($arOrder["LOCATION_NAME"] != "")
							$arOrder["LOCATION_NAME"] .= " ";
						$arOrder["LOCATION_NAME"] .= $item["I_NAME_RU"];
					}
				}

/*
				if ($arLocs = CSaleLocation::GetByID($arProp["VALUE"], LANGUAGE_ID))
				{
					//print_r($arLocs);
					$arOrder["PROPS"]["LOCATION_CITY"] = $arLocs["CITY_NAME"];
				}
*/
			}
			else
				$arOrder["PROPS"][$arProp["CODE"]] = $arProp["VALUE"];
		}
	}
	if ($arOrder["PROPS"]["CITY"] != "")
		$arOrder["LOCATION_NAME"] .= " ".$arOrder["PROPS"]["CITY"];
	//echo $arOrder["DATE_INSERT"]."<br>";
	
	$curl_opt = array(
		//"ApiKey" => "56d951ea285e55.42009720",
		"ApiKey" => $api_key,
		"TestMode" => 0,
//		"order" => "4819311-1-1537, 6064411-1-1537",
		"ExtOrderID" => $arOrder["ACCOUNT_NUMBER"],
		"ExtOrderPaid" => ($arOrder["PAYED"] == "Y") ? 1 : 0,
		"ExtDeliveryCost" => intval($arOrder["PRICE_DELIVERY"]),
		"dsDelivery" => $delivery_arr[$arOrder["DELIVERY_ID"]],
		"dsFio" => $arOrder["PROPS"]["NAME"]." ".$arOrder["PROPS"]["LAST_NAME"],
		"dsMobPhone" => $arOrder["PROPS"]["PHONE"],
		"dsEmail" => $arOrder["PROPS"]["EMAIL"],
//		"dsCity" => ($arOrder["PROPS"]["LOCATION_CITY"] != "") ? $arOrder["PROPS"]["LOCATION_CITY"] : $arOrder["PROPS"]["CITY"],
		"dsCity" => $arOrder["LOCATION_NAME"],
		"dsComments" => $arOrder["USER_DESCRIPTION"]." ".$arOrder["PROPS"]["ADDITIONAL_INFO"],
		"ExtDateOfAdded" => ConvertDateTime($arOrder["DATE_INSERT"], "YYYY-MM-DD HH:MI:SS"),
	);	
	if ($delivery_arr[$arOrder["DELIVERY_ID"]] == 5) // PickPoint
		$curl_opt["dsPickPointID"] = $arOrder["PROPS"]["PICKPOINT_ID"];
	
	if ($delivery_arr[$arOrder["DELIVERY_ID"]] == 2 or $delivery_arr[$arOrder["DELIVERY_ID"]] == 1 or $delivery_arr[$arOrder["DELIVERY_ID"]] == 7) // Почта, Курьер по Москве, Курьер по Питеру
	{
		$curl_opt["dsStreet"] = $arOrder["PROPS"]["STREET"];
		$curl_opt["dsHouse"] = $arOrder["PROPS"]["HOUSE"];
		$curl_opt["dsFlat"] = $arOrder["PROPS"]["FLAT"];

		if ($delivery_arr[$arOrder["DELIVERY_ID"]] == 2) // Почта
		{
			$curl_opt["dsPostcode"] = $arOrder["PROPS"]["INDEX"];
		}
		
	}

	$dbBasketItems = CSaleBasket::GetList(
			array(
					"NAME" => "ASC",
					"ID" => "ASC"
				),
			array(
					"ORDER_ID" => $arOrder["ID"]
				),
			false,
			false,
			array()
		);
	$i = 0;
	while ($arBasketItem = $dbBasketItems->GetNext())
	{
		//echo $arBasketItem["NAME"]." ".$arBasketItem["QUANTITY"]." ".round($arBasketItem["PRICE"],2)."<br>";
		
		//echo $arBasketItem["PRODUCT_ID"]."<br>";
		$db_res = CSaleBasket::GetPropsList(
				array(
						"SORT" => "ASC",
						"NAME" => "ASC"
					),
				array(
					"BASKET_ID" => $arBasketItem["ID"],
					"CODE" => "PRODUCT.XML_ID"
				)
			);
		if ($ar_res = $db_res->Fetch())
		{
			if (($pos = strpos($ar_res["VALUE"], "#")) !== false)
				$product_xml_id = substr($ar_res["VALUE"], $pos+1);
			else
				$product_xml_id = $ar_res["VALUE"];
			if ($i > 0)
				$curl_opt["order"] .= ",";
			$curl_opt["order"] .= $product_xml_id."-".$arBasketItem["QUANTITY"]."-".round($arBasketItem["PRICE"],2);
		}
		//echo $product_xml_id."<br>";
		$i++;
	}
	/*
	?><pre><?
	print_r($curl_opt);
	?></pre><?
	*/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "http://stripmag.ru/api/ds_order.php");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_POST, TRUE);

	//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_opt);
	$prop_values = array();
	try
	{
		$response = curl_exec($ch);	
		?><pre><?
		echo htmlspecialcharsbx($response);
		?></pre><?
		
		$error_arr = array(
		"2" => "Bad key, Проверьте корректность Вашего ApiKey",
		"3" => "Bad order request, Не корректные данные в поле order",
		"4" => "Order do not placed. Some items not at stock OR some problem in aID., Заказ не размещен, Либо каких-то товаров недостаточное количество на нашем складе, либо какие-то aID не найдены в нашей системе.",
		"5" => "TestMode. Data was checked. Order have NOT placed. Включен тестовый режим. Данные проверены, но заказ не размещается.",
		"6" => "Попытка размещения Drop Shipping заказа из под оптового аккаунта не имеющего статус Drop Shipping. Уточните у нашего менеджера - подписан ли Ваш «Договор Прямой Поставки» и зачислен ли депозит на Ваш аккаунт.",
		"7" => "Внутренний номер DS-заказа (ExtOrderID) не уникален.",
		"8" => "Не задан внутренний номер заказа (ExtOrderID).",
		"9" => "Не корректный формат даты размещения заказа ExtDateOfAdded. Корректный формат - YYYY-MM-DD HH:MM:SS.",
		"10" => "Не указан статус оплаты заказа (ExtOrderPaid).",
		"11" => "Не корректно указана стоимость доставки ExtDeliveryCost. Значение может быть только числом.",
		"12" => "Стоимость доставки ExtDeliveryCost не указанa.",
		"13" => "Не выбран способ доставки заказа dsDelivery.",
		"14" => "ФИО покупателя (dsFio) - обязательны для заполнения!",
		"15" => "Телефон покупателя (dsMobPhone) - обязателен для заполнения!",
		"16" => "Email покупателя (dsEmail) - обязателен для заполнения!",
		"17" => "Не известный метод доставки. Вероятно вы указали в поле dsDelivery, значение не соответствующее ни одному из обрабатываемых нами.",
		"18" => "В случае доставки Почтой России, название населенного пункта (dsCity) обязательно для заполнения!",
		"19" => "В случае доставки через PickPoint, индентификатор постомата или ПВЗ (dsPickPointID) обязателен!",
		);
		if (!empty($response))
		{
			$prop_values["P5S_RESPONSE"] = $response;
			try
			{	
				$xml = new SimpleXMLElement($response);
				//var_dump($xml);
				$status = intval($xml->ResultStatus);
				//foreach ($xml as $key => $value)
					//echo $key." ".$value."<br>";
				//echo $status;
				
				if ($status == 1) // Выгрузка прошла успешно
				{
					if (CSaleOrder::StatusOrder($arOrder["ID"], "DS")) //Передан в службу доставки
					{
						$prop_values["P5S_ID"] = intval($xml->orderID);
					}
					echo "Номер заказа - ".$xml->orderID;
				}
				else
				{
					?>Ошибка<br><?
					if (isset($error_arr[$status]))
					{
						$prop_values["P5S_ERROR"] = $error_arr[$status];
						echo $error_arr[$status]."<br>";
					}
					CSaleOrder::StatusOrder($arOrder["ID"], "ZE"); //Подтвержден, ошибка при выгрузке
				}
			}
			catch (Exception $e)
			{
				$log_error .= $e."\n";
				$prop_values["P5S_ERROR"] = $e;
				echo htmlspecialcharsbx($e);
			}
		}
		else
			$log_error .= "empty response\n";		
	}
	catch (Exception $e)
	{ 
		$log_error .= $e."\n";
		echo $e."<br>";
		$prop_values["P5S_ERROR"] = $e;
	}
	if (!empty($prop_values))
	{
		foreach ($prop_values as $prop_code => $prop_value)
		{
			$db_props = CSaleOrderProps::GetList(
				array(),
				array(
					"PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
					"CODE" => $prop_code,
				),
				false,
				false,
				array()
			);
			if ($props = $db_props->Fetch())
			{
				$arFields = array(
					"ORDER_ID" => $arOrder["ID"],
					"ORDER_PROPS_ID" => $props["ID"],
					"VALUE" => $prop_value,
					"NAME" => $props["NAME"],
					"CODE" => $props["CODE"],
				);
				$db_vals = CSaleOrderPropsValue::GetList(
					array("SORT" => "ASC"),
					array(
						"ORDER_ID" => $arOrder["ID"],
						"ORDER_PROPS_ID" => $props["ID"],
					)
				);							
				if ($arVals = $db_vals->Fetch()) // если такое свойство заказа уже заполнено
				{
					if (($prop_code == "P5S_ERROR") and $arVals["VALUE"] != "") 
					{
						$arFields["VALUE"] = $arVals["VALUE"]."\n".$arFields["VALUE"]; //добвляем текст к уже имеющемуся
					}
					CSaleOrderPropsValue::Update($arVals["ID"],$arFields);
/*					
					?><pre><?
					print_r($arFields);
					?></pre><?
*/					
				}
				else
				{
					
					CSaleOrderPropsValue::Add($arFields);
/*					
					?><pre><?
					print_r($arFields);
					?></pre><?					
*/					
				}
			}
		}
	}
	curl_close($ch);								
}								
if ($log_error != "")
{
	$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/cron/orders_export_logs/".date("d.m.Y").".txt","ab");
	if ($fp)
	{
		@fwrite($fp, $log_error);
	}
}
?>