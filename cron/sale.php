<?
include("config.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
set_time_limit (0);

$start_time = time();
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
$IBLOCK_ID = 5;

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/cron/log_sale.txt");

$arFilter = array(
	"IBLOCK_ID"=>$IBLOCK_ID,
	"ACTIVE"=>"Y",
	"GLOBAL_ACTIVE"=>"Y",
//	"SECTION_ID" => 86,
//	"INCLUDE_SUBSECTIONS" => "Y",
);
$arSelect = array("ID");
$rsItems = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while ($arItem = $rsItems->GetNext())
{
	CIBlockElement::SetPropertyValuesEx($arItem["ID"], $IBLOCK_ID, array("sale"=>""));
}

$dbProductDiscounts = CCatalogDiscount::GetList(
    array("SORT" => "ASC"),
    array(
            "ACTIVE" => "Y",
            "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), 
                                               "YYYY-MM-DD HH:MI:SS",
                                               CSite::GetDateFormat("FULL")),
            "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), 
                                             "YYYY-MM-DD HH:MI:SS", 
                                             CSite::GetDateFormat("FULL")),											   
        ),
    false,
    false,
    array()
);
if ($dbProductDiscounts->SelectedRowsCount())
{
	$step = intval($_REQUEST["step"]);
	$arSectionFilter = array(
		"IBLOCK_ID" => $IBLOCK_ID,
		"ACTIVE" => "Y",
		"SECTION_ID" => 0,
//		"ID" => array(237, 240),
	);
/*	
	if ($step <= 1)
		$arSectionFilter["ID"] = array(266,256,249);
	else
	if ($step == 2)
		$arSectionFilter["ID"] = array(240);
	else
	if ($step == 3)
		$arSectionFilter["ID"] = array(237);
	else
		exit;
*/
	$rsSections = CIBlockSection::GetList(array("ID"=>"DESC"), $arSectionFilter);
	while ($arSection = $rsSections->GetNext())
	{
		$arSectionFilter2 = array(
			"IBLOCK_ID" => $IBLOCK_ID,
			"ACTIVE" => "Y",
			"SECTION_ID" => $arSection["ID"],
		);
		$rsSections2 = CIBlockSection::GetList(array("ID"=>"DESC"), $arSectionFilter2);
		while ($arSection2 = $rsSections2->GetNext())		
		{
	//echo $arSection["NAME"]."<br>";
			$arFilter = array(
				"IBLOCK_ID"=>$IBLOCK_ID,
				"ACTIVE"=>"Y",
				"GLOBAL_ACTIVE"=>"Y",
				"SECTION_ID" => $arSection2["ID"],
				"INCLUDE_SUBSECTIONS" => "Y",
			);

			$arSelect = array(
				"ID",
				"IBLOCK_ID",
				"NAME"
			);
			$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], array("BASE"));
			$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

			foreach($arResult["PRICES"] as &$value)
			{
				if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
					continue;
				$arSelect[] = $value["SELECT"];
				$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = 1;
				if ($i == 0)
					$price_id = $value["ID"];
				$i++;
			}
			if (isset($value))
				unset($value);

			$arSort	= array(
				"ID" => "DESC",
			);

			$arNavParams = array("nTopCount" => 10000);
			$arResult["ELEMENTS"] = array();
			$rsItems = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
			while ($arItem = $rsItems->GetNext())
			{
				//echo $arItem["NAME"]."<br>";
				$arResult["ELEMENTS"][] = $arItem["ID"];	
			}
			$sale_items_ids = array();
			if (!empty($arResult["ELEMENTS"]))
			{
				//print_r($arResult["ELEMENTS"]);
					$offersFilter = array(
						'IBLOCK_ID' => $IBLOCK_ID,
						'HIDE_NOT_AVAILABLE' => "Y"
					);
					$offersFilter['SHOW_PRICE_COUNT'] = 1;

					$arOffers = CIBlockPriceTools::GetOffersArray(
						$offersFilter,
						$arResult["ELEMENTS"],
						array(),
						array("NAME"),
						array(),
						0,
						$arResult["PRICES"],
						true
					);
					//var_dump($arOffers);
					foreach($arOffers as $arOffer)
					{
						if (is_array($arOffer["MIN_PRICE"]) && $arOffer["MIN_PRICE"]["DISCOUNT_VALUE"] < $arOffer["MIN_PRICE"]["VALUE"] && !in_array($arOffer["LINK_ELEMENT_ID"], $sale_items_ids)) // на торговое предложение действует скидка
						{
							$sale_items_ids[] = $arOffer["LINK_ELEMENT_ID"];
	//AddMessage2Log($arOffer["NAME"]."\n");

	//						echo $arOffer["NAME"]."<br>";
						}
			/*			
					?><pre><?
					print_r($arOffer);
					?></pre><?
			*/		
					}
	/*
					?><pre><?
					print_r($sale_items_ids);
					?></pre><?
	*/
					$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>"sale", "VALUE"=> "да"));
					if($enum_fields = $property_enums->GetNext())
						$sale_value_id = $enum_fields["ID"];
					foreach ($sale_items_ids as $item_id)
						CIBlockElement::SetPropertyValuesEx($item_id, $IBLOCK_ID, array("sale"=>$sale_value_id));	
			}
			//AddMessage2Log($arSection2["NAME"]."- done \n");
		}
		AddMessage2Log($arSection["NAME"]."- done \n");
	}
}
$end_time = time();
//echo "work_time ".ceil(($end_time - $start_time)/60)." minutes\n";
AddMessage2Log("work_time ".ceil(($end_time - $start_time)/60)." minutes\n");
?>