<?
include("config.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
//Импорт цветов

@set_time_limit(0);
$start_time = time();
// подключаем модули
CModule::IncludeModule('highloadblock');

// необходимые классы
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


$items_hlblock   = HL\HighloadBlockTable::getById(3)->fetch();
//var_dump($hlblock);
$items_entity   = HL\HighloadBlockTable::compileEntity( $items_hlblock );
$items_entity_data_class = $items_entity->getDataClass();


$import_data = file_get_contents('http://stripmag.ru/datafeed/bitrix_colors.csv');
if ($import_data !== false)
{
	$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/import/colors.csv","wb");
	if ($fp)
	{
		@fwrite($fp, $import_data);
		@fclose($fp);		
	}
	else
		exit();
}
else
	exit();

$handle = fopen($_SERVER["DOCUMENT_ROOT"]."/import/colors.csv", "r");
$items_added = 0;

$itemsGetListParams = Array(
    "select" => Array("ID", "UF_XML_ID"),
    "filter" => Array(),
	"order" => Array(
		"ID" => "ASC",
	),
    "limit" => 1,
);

if ($handle)
{
	$i = 0;
	while (($data = fgetcsv($handle, 1000, ";")) !== false)
	{
		$i++;
		if ($i == 1)
			continue;

		$name = trim($data[0]);
		$xml_id = trim($data[1]);

		if ($name == "" or $xml_id == "")
			continue;

		$itemsGetListParams["filter"] = array(
			"UF_XML_ID" => $xml_id,
		);
		$rsData = $items_entity_data_class::getList($itemsGetListParams);
		$rsData = new CDBResult($rsData);
	
		if ($arRes = $rsData->Fetch())
		{
			continue;			
		}

		$item_result = $items_entity_data_class::add(array(
			'UF_NAME'	=> $name,
			'UF_XML_ID' => $xml_id,
		));			
		$items_added++;

//		if ($i > 10)
//			break;		
//		if ($items_added > 50)
//			break;
	}
	fclose($handle);	
}
@copy($_SERVER["DOCUMENT_ROOT"]."/import/colors.csv", $_SERVER["DOCUMENT_ROOT"]."/import/colors/colors_".date("d.m.Y H i").".csv");		

$timestamp = time() - 7 * 86400;
$dir_path = $_SERVER["DOCUMENT_ROOT"]."/import/colors/";
if ($handle = opendir($dir_path))
{
	while (false !== ($entry = readdir($handle)))
	{
		if (is_file($dir_path.$entry))
		{
			if (filemtime($dir_path.$entry) < $timestamp)
				@unlink($dir_path.$entry);
		}
	}
	closedir($handle);
}
$end_time = time();
echo "скрипт работал ".($end_time - $start_time)." секунд";

?>