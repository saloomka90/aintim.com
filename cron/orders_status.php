<?
include("config.php");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/cron/logs/orders_status_date.txt","wb");
if ($fp)
{
	@fwrite($fp, date("d.m.Y H:i:s"));
	fclose($fp);
}
$log_error = "";

CModule::IncludeModule('sale');
$arFilter = array(
	"STATUS_ID" => array("DS", "PR", "WC", "IR", "RS", "SP", "IC", "SC", "WP"),
	"CANCELED" => "N"
);

//$api_key = trim(COption::GetOptionString('toysales.main', 'TOYSALES_P5S_API_KEY'));

$curl_opt = array(
	"ApiKey" => trim(COption::GetOptionString('toysales.main', 'TOYSALES_P5S_API_KEY'))
);
$rsOrders = CSaleOrder::GetList(array(), $arFilter);
$i = 0;
$status_arr = array(
	"1" => "DS", 
	"2" => "PR", 
	"3" => "WC",
	"4" => "IR",
	"5" => "RS",
	"6" => "SP",
	"7" => "F",
	"8" => "C",
	"9" => "IC",
	"10" => "AC",
	"11" => "SC",
	"12" => "WP",
	"13" => "DE",
);
while ($arOrder = $rsOrders->GetNext())
{
	$dbProps = CSaleOrderPropsValue::GetList(
		array("SORT" => "ASC"),
		array(
			"ORDER_ID" => $arOrder["ID"],
			"CODE" => "P5S_ID"
		)
	);
	if ($arProp = $dbProps->Fetch())
	{
		if ($arProp["VALUE"] != "")
		{
//			if ($i > 0)
//				$curl_opt["orderID"] .= ',';
			$curl_opt["orderID"] = $arProp["VALUE"];
			$i++;
			//$curl_opt["ExtOrderID"] = $arOrder["ID"];
		}
	$error = "";
	$ch = curl_init();
//echo "<br>";
//print_r($curl_opt);
	curl_setopt($ch, CURLOPT_URL, "http://stripmag.ru/api/ds_get_order_data.php");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_POST, TRUE);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_opt);
	try
	{
		$response = curl_exec($ch);	
		
		if (!empty($response))
		{
			try
			{
				$xml = new SimpleXMLElement($response);
				/*
				?><pre><?
				print_r($xml);
				?></pre><?
				*/
				$status = intval($xml->ResultStatus);
				if ($status == 1)
				{
					$order_id = intval($xml->Orders->Orders_Item->ExtOrderID);
					if ($order_id == $arOrder["ACCOUNT_NUMBER"])
					{
						$order_status = intval($xml->Orders->Orders_Item->status);
						//echo $order_status."<br>";
						if ($status_arr[$order_status] != "" and $status_arr[$order_status] != $arOrder["STATUS_ID"])
						{
							//echo $status_arr[$order_status]."<br>";
							CSaleOrder::StatusOrder($arOrder["ID"], $status_arr[$order_status]);
							if (in_array($order_status, array(8,10,13)))
							{
								CSaleOrder::CancelOrder($arOrder["ID"], "Y");
							}
						}
						//echo $order_id." ".intval($xml->Orders->Orders_Item->status)."<br>";
					}
				}
				else
				if ($status == 21)
					$error = "Заказ не найден";
			}
			catch (Exception $e)
			{
				$log_error .= $e."\n";
				$log_error .= $response."\n";				
				$log_error .= "Bitrix ID - ".$arOrder["ID"].", P5S ID - ".$curl_opt["orderID"]."\n";
				$error = $e;
				echo htmlspecialcharsbx($e);
			}
		}
		else
			$log_error .= "empty response "."Bitrix ID - ".$arOrder["ID"].", P5S ID - ".$curl_opt["orderID"]."\n";
	}
	catch (Exception $e)
	{
		$log_error .= $e."\n";
		$log_error .= "Bitrix ID - ".$arOrder["ID"].", P5S ID - ".$curl_opt["orderID"]."\n";		
		$error = $e;
		echo $e."<br>";
	}
	if ($error != "")
	{
			$db_props = CSaleOrderProps::GetList(
				array(),
				array(
					"PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
					"CODE" => "P5S_ERROR",
				),
				false,
				false,
				array()
			);
			if ($props = $db_props->Fetch())
			{
				$arFields = array(
					"ORDER_ID" => $arOrder["ID"],
					"ORDER_PROPS_ID" => $props["ID"],
					"VALUE" => $error,
					"NAME" => $props["NAME"],
					"CODE" => $props["CODE"],
				);
				$db_vals = CSaleOrderPropsValue::GetList(
					array("SORT" => "ASC"),
					array(
						"ORDER_ID" => $arOrder["ID"],
						"ORDER_PROPS_ID" => $props["ID"],
					)
				);							
				if ($arVals = $db_vals->Fetch()) // если такое свойство заказа уже заполнено
				{
					if ($arVals["VALUE"] != "") 
					{
						$arFields["VALUE"] = $arVals["VALUE"]."\n".$arFields["VALUE"]; //добвляем текст к уже имеющемуся
					}
					CSaleOrderPropsValue::Update($arVals["ID"],$arFields);
/*					
					?><pre><?
					print_r($arFields);
					?></pre><?
*/					
				}
				else
				{
					
					CSaleOrderPropsValue::Add($arFields);
/*					
					?><pre><?
					print_r($arFields);
					?></pre><?					
*/					
				}
			}	
	}
	curl_close($ch);								
	}
}								
if ($log_error != "")
{
	$log_error = date("d.m.Y H:i:s")."\n".$log_error;
	$fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/cron/orders_status_logs/".date("d.m.Y").".txt","ab");
	if ($fp)
	{
		@fwrite($fp, $log_error);
	}
}
?>