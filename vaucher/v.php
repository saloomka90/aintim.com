﻿<?php 
  $dir_img = '/var/www/rcp/data/www/rcptest.ru/extnew/images/voucher/';
  $dir_qrcode = '/var/www/rcp/data/app/vouchers/qrcode/';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style>		
		body {
			margin: 0;
			padding: 0;
			font-family: Tahoma;
			font-size: 13px;			
		}		
		.red {
			color: orangered;
		}
		.table {
			width: 100%;
			margin: 5px 0;
			font-size: 12px;
		}
		.table thead tr td {
			font-size: 10px;
		}
		.table-border {
			border-collapse: collapse;
		}
		.table-border td {
			padding: 2px 5px;
			border: 1px solid #333;
		}
		p {
			margin: 0 0 5px;
		}
		
		.voucher {
			float: right;
			font-size: 28px;
			font-weight: bold;
			/* position: absolute; */
			/* padding-left: 460px; */
			/* padding-top: 120px; */
			margin-top: -100px;
			z-index: 3;
			position: relative;
		}
		
		@media print {
			.more {
				page-break-after: always;
			} 
		}		
	</style>
</head>
<body>
	
	<div class="header">
		<img src="https://rcptest.ru/extnew/images/voucher/header.jpg" style="width: 80%;">
		<img src="rqcode.png">
	</div>
	<table>
		<tr>
			<td align="left"><b>КАК ПОЛУЧИТЬ RUSSIA CITYPASS / HOW TO RECEIVE RUSSIA CITYPASS</b></td>
		</tr>
		<tr height="15"><td></td></tr>
		<tr>
			<td align="left">Распечатайте и обменяйте данный ваучер на ваш заказ в выбранной вами точке выдачи.</td>
		</tr>
		<tr>
			<td align="left">Print and exchange this voucher for your order at selected pick up point.</td>
		</tr>
	</table>

	<table class="table table-border">
		<tr>
			<td style="text-align: right;">
				Номер ваучера / Voucher number
			</td>
			<td colspan="3"  style="text-align: center; font-weight: bold;">
				5238-9FAE-8A63-EE27
			</td>
		</tr>
		<tr>
			<td style="width: 200px; text-align: right;">
				Фамилия Имя / Name
			</td>
			<td colspan="3">
				MANIT CHANTARAKANA
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">
				Дата выдачи / DATE OF RECIEVING
			</td>
			<td colspan="3">
				21/11/17
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">
				ЗАКАЗ / ORDER
			</td>
			<td colspan="3">
                <?$total_all = 0; 
                foreach($OrderObject as $e): 
                        $total = 0; 
                        $total += $e['price'] * $e['quantity'] - $e['price']/100 * $e['discount']; 
                        $total_all += $total;
                        $currency = $e['price_currency'];
                        if ($currency == 'rub') { $curr_symbol = 'руб.'; }
                        else if ($currency == 'eur') {$curr_symbol = 'euro'; }
                        else {$curr_symbol = '$';}
                
                
                ?>
            	    <table class="table table-border" style="border-style: none;">
                		<thead>
                		<tr>
                			<td>Карта / Card</td>
                			<td>цена / price></td>
                			<td>кол-во / quant.</td>
                			<td>скидка / discount</td>
                			<td>сумма / sum></td>
                		</tr>
                		</thead>
            		    <tbody id="shop-list">
                			<tr>
                				<td style="vertical-align: middle; font-weight: bold;">
                					<?=app::$lang->select($e['name'])?>
                				</td>
                				<td style="vertical-align: middle;">
                					<span cell-price="{id}"><?=format::num($e['price'])?> <?=$curr_symbol;?></span>
                				</td>
                				<td style="vertical-align: middle;">
                					<?=$e['quantity']?> шт.
                				</td>
                				<td style="vertical-align: middle;">
                					<?=$e['discount']?>%
                				</td>
                				<td style="vertical-align: middle;">
                					<span cell-price="{id}"><?=round($e['price_total'] - $e['price_total']/100 * $e['discount'],2)?> <?=$curr_symbol;?></span>
                				</td>
                			</tr>
                			<? foreach(Order::getOrderExtra($e['order_id'], $e['object_id']) as $ex): 
                			          $total += $ex['price'] * $ex['quantity'] - $ex['price']/100 * $e['discount']
                			?>
                    			<tr>                    				<td style="vertical-align: middle;"><?=app::$lang->select($ex['name'])?></td>
                    				<td style="vertical-align: middle;"><span cell-price="{id}"><?=format::num($ex['price'])?> <?=$curr_symbol;?></span></td>
                    				<td style="vertical-align: middle;"><?=$ex['quantity']?> шт.</td>
                    				<td style="vertical-align: middle;"><?=$e['discount']?>%</td>
                    				<td style="vertical-align: middle;"><span cell-price="{id}"><?=round($ex['price']*$ex['quantity'] - $ex['price']*$ex['quantity']/100 * $e['discount'],2)?> <?=$curr_symbol;?></span></td>
                    			</tr>
                			<?endforeach?>
                        </tbody>
                		<tfoot>
                		<tr>
                			<td style="text-align: right; vertical-align: middle; font-weight: bold;">итого / Total</td>
                			<td colspan="4" style="text-align: right; vertical-align: middle; font-weight: bold;">
                				<span class="bg-info" style="padding: 5px; margin: 3px; float: left;"><span id="shop-price"><?=round($total,2)?> <?=$curr_symbol;?></span></span>
                			</td>
                		</tr>
                		</tfoot>
            	    </table>
                <?endforeach?>
			</td>
		</tr>
		<?if(!empty($order['promocode'])):?>
			<tr>
    			<td style="text-align: right;">
    				Промокод / Promocode
    			</td>
    			<td colspan="3">
    				<?=$order['promocode']?>
    			</td>
    		</tr>
		<?endif?>		
    	<?if($order['delivery_price']>0):?>
    		<tr>			
    			<td rowspan="4">Доставка курьером / Courier delivery</td>
    			<td>Стоимость / Price</td>
    			<td colspan="2">
    			    <?=$order['delivery_price']?> руб./RUB
    			</td>
    		</tr>
    		<tr>
    			<td>Дата / Date</td>
    			<td colspan="2">
    			    <?=$order['delivery_date']?>
    			</td>
    		</tr>
    		<tr>
    			<td>Телефон / Telephone</td>
    			<td colspan="2">
    			    <?=$order['delivery_phone']?>
    			</td>
    		</tr>
    		<tr>
    			<td>Комментарий / Comments</td>
    			<td colspan="2">
    				<?=$order['delivery_description']?>
    			</td>
    		</tr>
		<?endif?>
	</table>
	<div style="text-align: left; border-bottom: black dashed 1px;"></div>
	<table class="table">
		<tr>
			<td colspan="2" style="text-align: left; border-bottom: black solid 1px;">
				<p>заказ получил, претензий не имею / order received, I have no complaints</p>
				<br>
				<br>
			</td>
		</tr>
		<tr>
			<td width="50%">
				подпись / signature
			</td>
			<td>
				расшифровка / print name
			</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: black dashed 1px;">
			    <br>
			</td>
		</tr>
		<tr>
			<td>
				<p>заказ выдал</p>
				<br>
				<br>
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td style="border-top: black solid 1px;">
				подпись / signature
			</td>
			<td style="border-top: black solid 1px;">
				расшифровка / print name
			</td>
		</tr>
	</table>	
	<table class="table"><tr><td><img src="1.jpg"></td></tr></table>
	<?if($order['delivery_price']<=0):
	   
	   if($order['delivery_id']==115) {
	       $dlv_img_map = 'spb_map.jpg';
	       $dlv_img_entry = 'spb_enter.jpg';
	       $dlv_img_entry2 = 'spb_enter2.jpg';
	       
	   } else {
	       $dlv_img_map = 'map.jpg';
	       $dlv_img_entry = 'gum.jpg';
	       $dlv_img_entry2 = 'tic.jpg';
	   }
	?>
		<div class="more"></div>
		
		<?if($order['delivery_id']>0):?>
		<table class="table">
    		<tr>			
    			<td rowspan="6">Пункт выдачи / PICKUP POINT</td>
    			<td>Город / City</td>
    			<td colspan="2"></td>
    		</tr>
    		<tr>
    			<td>Метро / Metro</td>
    			<td colspan="2">
    				<?if(!empty($object['subway'])):?>
    					<?=app::$lang->select($object['subway'])?>
    				<?endif?>
    			</td>
    		</tr>
    		<tr>
    			<td>Адрес / ADDRESS</td>
    			<td colspan="2">
    				<?if(!empty($object['address'])):?>
    					<?=app::$lang->select($object['address'])?>
    				<?endif?>
    			</td>
    		</tr>
    		<tr>
    			<td>Название / NAME</td>
    			<td colspan="2">
    				<?=$deliveryTitle;?>
    			</td>
    		</tr>
    		<tr>
    			<td>Телефон / TELEPHONE</td>
    			<td colspan="2">
    				<?if(!empty($object['phone'])):?>
    					<?=app::$lang->select($object['phone'])?>
    				<?endif?>
    			</td>
    		</tr>
    		<tr>			
    			<td>Время работы / WORKING HOURS</td>
    			<td colspan="2">
    			    <?if(!empty($object['working'])):?>
						<?foreach(app::$lang->select($object['working']) as $week=>$work):?>
							<?if(empty($work)) continue;?>
								<p style="font-size:10px;"><span align="right"><?=Other::showWeek($week);?></span> : <span align="left"><?=$work?></span></p>
						<?endforeach?>
					<?endif?>
    			</td>
    		</tr>
    	<?endif?>
		</table>
		
		
		
    	<div class="content">
    		<table>
    			<tr>
    				<td colspan="2" align="center">
    					<img src="<?=$dir_img;?><?=$dlv_img_map;?>" style="padding-bottom:20px; width:690px; height:300px;">
    				</td>
    			</tr>
    			<tr>
    				<td align="left" style="width: 50%; text-align: left;">
    					<img src="<?=$dir_img;?><?=$dlv_img_entry;?>" style="width:340px; height:200px;">
    				</td>
    				<td align="rigth">
    					<img src="<?=$dir_img;?><?=$dlv_img_entry2;?>" style="width:340px; height:200px;">
    				</td>
    			</tr>
    		</table>
    		<div style="text-align: left; margin:10px 0;border-bottom: black dashed 1px; font-size:12px"></div>
    		<table>
    			<tr>
    				<td align="left"><b>ПОЛИТИКА ОТМЕНЫ / CANCELLATION POLICY</b></td>
    			</tr>
    			<tr height="10"><td></td></tr>
    			<tr>
    				<td align="left">Продукт возврату не подлежит. / Product is nonrefundable.</td>
    			</tr>
    			<tr height="60"><td><br></td></tr>
    			<tr>
    				<td align="left"><b>ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ / ADDITIONAL INFORMATION</b></td>
    			</tr>
    			<tr height="10"><td></td></tr>
    			<tr>
    				<td align="left">Если у вас возникли какие-либо вопросы, пожалуйста, свяжитесь с нашим контактным центром или напишите нам на почту. Контактная информация представлена выше.</td>
    			</tr>
    			<tr>
    				<td align="left">If you have any questions, please, contact our call center or write to our email. Contact information is above.</td>
    			</tr>
    		</table>
    	</div>
	<?endif?>
	
	<?
		//if(access::check('content.static')):
	    $aero_tickets = db::query('SELECT t.* FROM aeroexpress_orders o, aeroexpress_tikets t WHERE o.order_id = t.order_id and o.pid = ?', $order['id'])->all();
		foreach ($aero_tickets as $aet) {
				//$aeh = AeroExpreess::getOrderHistory($aet['order_id']);
				//echo $aet['order_id'];
			
	?>
	    <div class="more"></div>
    	<link href="/var/www/russiacitypass/data/app/aeroexpress/index_css.css" type="text/css" rel="stylesheet" media="all">
    	<div class="wrap">
            <div class="ticket">
        	    <!-- <img src="/var/www/russiacitypass/data/app/aeroexpress/images/tckt-bg.jpg" alt="" class="ticketbg"> -->
        	    <table style="height:240px;">
        			<tr>
        				<td align="left" style="width:440px; text-align: left;">
        					<img src="/var/www/russiacitypass/data/app/aeroexpress/images/logo.jpg">
        				</td>
        				<td align="left">
        					<img style="width:144px;" class="qr" src="/var/www/russiacitypass/data/app/aeroexpress/tickets/<?=$aet['ticket_id'];?>.png" alt="">
        				</td>
        			</tr>
        		</table>
        	    <!-- <img src="/var/www/russiacitypass/data/app/aeroexpress/images/disc_rd.png" alt="" class="b1bg"> -->
                <div class="logo">
                    <div class="s1">
                        <div class="tckt-top nuclear">
                            <div class="title"><span>Маршрутная квитанция</span> / E-ticket</div>
                        </div>
                        <div class="tckt-mdl clear nuclear">
                            <div class="leftcol">
                                <ul class="red">
                                    <li><span class="cell-1"><b>Дата отправления</b> / Departure date</span><span class="cell-2">10.10.2016</span><span class="cell-1 comment">Действует до 03:00 следующих суток</span></li>
                                    <li><span class="cell-1"><b>Билет действителен на следующих<br>маршрутах ООО "Аэроэкспресс"</b><br>Ticket valid in all directions<br> of Aeroexpress operation</span><span class="cell-2 smallsize"><?=$aet['stDepart'];?></span>
                                    </li>
                                    <li><span class="cell-1"><b>№ билета</b> / Ticket No.</span><span class="cell-2"><?=$aet['ticket_id'];?></span></li>
                                    <li><span class="cell-1"><b>№ заказа</b> / Order No.</span><span class="cell-2"><?=$aet['order_id'];?></span></li>
                                    <li><span class="cell-1"><b>Тариф</b> / Fare</span><span class="cell-2"> <?=$aet['tariff'];?></span></li>
                                    <li><span class="cell-1"><b>Стоимость, руб.*</b> / Price, RUB*</span><span class="cell-2"><?=$aet['ticketPrice'];?></span><span class="cell-1 comment">* Включая 0% НДС /incl. VAT 0%</span>
                                    </li>
                                    <li><span class="cell-1"><b>Время оплаты</b> / Time of payment</span><span class="cell-2">07.10.2016 16:59</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tckt-btm nuclear">
        		            <div class="hotline">Горячая линия (звонок по России бесплатный)</div>
                            <div class="phone">8 800 700-33-77</div>
                            <div class="hotline">Hot line (your call within Russia is Free)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?
		}
    //endif;
    ?>
</body>
</html>