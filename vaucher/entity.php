<?php 
    $cityid = $object['city_id'];
    setcookie("cityid", $cityid, 60*60*24*7, '/');
    if ($cityid==2) {
        $card_img = 'cart-card-spb.png';
        $cityname = $lang->_('Санкт-Петербург');
        $city_link = 'piter';
        $city_class = '.piter';
        $city_color = 'color-blue';
    }	
    else {
        $card_img = 'main-cart.png';
        $cityname = $lang->_('Москва');
        $city_link = 'moscow';
        $city_class = '.moscow';
        $city_color = 'color-red';
    }
    if ($object['id'] == 192) {
        $carusel_landing = 'carusel-landing';
    } else {
        $carusel_landing = '';
    }
	
	if ($cityid==3) {
        $card_img = 'cart-card-spb.png';
        $cityname = $lang->_('Отели');
        $city_link = 'hotels';
        $city_class = '.hotels';
        $city_color = 'color-blue';
    }
?>
<!--
<div itemscope itemtype="http://schema.org/Product" >
	<meta itemprop="name" content="<?=$Title?>" >
	<meta itemprop="description" content="<?=$lang->select($object['short_descr'])?>" >
	<meta itemprop="url" content="entity/<?=$object['alt'];?>/" >
	<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" >
	<meta itemprop="bestRating" content="5" >
	<meta itemprop="ratingValue" content="4,85" >
	<meta itemprop="ratingCount" content="40" >
	</div>
</div>
-->
<div itemscope itemtype="http://schema.org/Organization">
</div>

<div class="clearfix"></div>

<div class="main-bg-header"></div>
<div class="main-bg-body">
	<div class="container main-inner">
		<div class="row">
		    <?php if ($object['id'] != 192) { ?>
    	        <div class="col-lg-3 col-lg-push-9">
    				<div class="row sidebar-offseted" id="sidebar-main">
    				    <div class="col-lg-12 col-sm-6 col-md-6 sidebar-main-1" id="sidebar-main-1" style="margin-top: 0px !important;">
                        	<div class="row">
                        		<div class="col-md-12 not-xs">
                        			<center><img src="<?=dir_ext;?>images/<?=$card_img;?>" width="100%" alt="Карта - Russia CityPass" title="Карта - Russia CityPass"></center>
                        		</div>
                        	</div>
                        </div>
    					<?php
    						include(__DIR__.'/side-basket.php');
    					?>
    				</div>
    			</div>
    		<?php } ?>
				
				<?php
				if ($object['id'] == '192') { 
				    echo '<div class="col-lg-12">';
				} else {
				    echo '<div class="col-lg-9 col-lg-pull-3">';
				}
				?>
					<div id="section-guide" class="row">
						
					        <div class="row col-md-12 hidden-xs <?=$city_color;?>" style="margin: 20px 20px -10px 5px;">
			                    <a class="<?=$city_color;?> " href="<?=$lang->path;?>/"><?=$lang->_('Главная');?></a>
			                    <span style="margin:0px 5px;">/</span>
			                    <a class="<?=$city_color;?>" href="<?=$city_link;?>/"><?=$cityname;?></a>
			                    <span style="margin:0px 5px;">/</span>
			                    <a class="<?=$city_color;?> " href="included/show/<?=$object['cat_alt'];?>/"><?=$lang->select($object['cat_name']);?></a>
			                    <span style="margin:0px 5px;">/</span>
			                    <span class="<?=$city_color;?> "><?=$lang->select($object['title'])?></span>
			                </div>
			            
						<div class="col-md-12 blocknote">
							<div class="blocknote-inner padding-50">
							    <?if(access::check('object.edit')):?>
							        <br><a href="/acp/object/edit/<?=$object['id']?>" id="add-card" role="button" class="btn btn-danger btn-block color-white">Редактировать</a>
								<?endif?>
								<h1><?=$lang->select($object['title'])?></h1>
								<? if (!empty($lang->select($object['fast_track']))) { ?>
								    <div>
									    <div class="fast-track" style="position: inherit !important;" data-content="<?=$lang->select($object['fast_track']);?>"></div>
							        </div>
							    <? } ?>
								
								
								<?/**/?>
								<?
								//echo "массив - ".$object_bonuses;
								if (empty($lang->select($e['fast_track']))) {$iii=1;}else{$iii=2;}
								
								if(isset($object_bonuses) && is_array($object_bonuses)):?>
									<?
									?>
									<div class="track">									
									<?
									foreach($object_bonuses as $sKey=>$bonus):
										?>
										<div class="fast-track_<?=$iii;?>_prew" data-original-title="<?=$bonuses[$bonus['bonus_id']]['description'];?>"><img src="<?=$bonus['img_link']?>" width="43"/></div>
										<?
										$iii++;
									endforeach;
									?>	
</div>									
								<?endif;?>
								<?/**/?>
			
			
								
								<p class="font-rlh font-18"><?=$lang->select($object['description'])?></p>
								<div class="font-rlh font-16 color-piter"><span id="to_contact" style="cursor:pointer;"><?=$lang->_('Контакты и расположение');?></span></div>
								
								<div style="margin-top: 10px; float:right;">
                                    <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="facebook,twitter,google,vkontakte,odnoklassniki,email,print"></div>
							    </div>
							</div>
						</div>
					</div>
					<? 
					//print_r($coverFade);
					if(!empty($coverFade)):?>
    					<div id="section-guide" class="row">
    						<div class="col-md-12 blocknote">
    							<div class="blocknote-inner padding-30">
    								<div id="carousel-example-generic" class="carousel slide <?=$carusel_landing;?>" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <? $i = 0; foreach ($coverFade as $path => $offset): ?>
                                                <li data-target="#carousel-example-generic" data-slide-to="<?=$i;?>" <? if (!$i++): ?> class="active"<? endif ?> ></li>
    					                    <? endforeach ?>
                                        </ol>
                                        <div class="carousel-inner ">
                                            <? $i = 0; foreach ($coverFade as $path => $offset): ?>
        						                <div class="item <? if (!$i++): ?> active<? endif ?>">
                                                    <img class="not-xs" src="<?=$path?>" style="width:788px;height:503px;" alt="<?=$lang->select($object['name'])?>: <?=$headTitle?>, Фото <?=$i?>" title="<?=$lang->select($object['name'])?>: <?=$headTitle?>, Фото <?=$i?>">
                                                    <img class="not-lg not-md not-sm" src="<?=$path?>" style="width:788px;" alt="<?=$lang->select($object['name'])?>: <?=$headTitle?>, Фото <?=$i?>" title="<?=$lang->select($object['name'])?>: <?=$headTitle?>, Фото <?=$i?>">
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>
    					                    <? endforeach ?>
                                        </div>
                                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>
    								
    							</div>
    						</div>
    					</div>
					<?endif?>
    				<? if ($cityid == 1) { ?>
        				<div id="section-guide" class="row">
            				<div class="col-md-12 blocknote">
            					<div class="blocknote-inner padding-30">
                        			<h2><?=$lang->_('Что еще включено в карту <span class="color-red">Moscow CityPass</span>');?></h2>
                        			<p class="font-rlh font-18"><?=$lang->_('Moscow CityPass – это единая электронная карта для туристов, которая включает посещение лучших музеев и экскурсий, выгодные предложения и скидки.');?></p>
                                    <div id="section-economy" class="row"> 
                                        <div class="padding-15">
                                            <h3><?=$lang->_('В комплект Moscow CityPass входят:');?></h3>
                                            <div class="kit row">
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__1"></div>
                                                    <div class="kit_name smpt15">
                                                        <?=$lang->_('ПЛАСТИКОВАЯ КАРТА-КЛЮЧ');?>
                                                        <br>
                                                        <span><?=$lang->_('бесплатный вход<br class="hidden-xs"> во все<br class="hidden-xs"> достопримечательности');?></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__2"></div>
                                                    <div class="kit_name smpt5">
                                                        <?=$lang->_('Путеводитель 100+ страниц
                                                        <br>
                                                        <span>на русском, китайском и английском языках</span>');?>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__3"></div>
                                                    <div class="kit_name smpt15">
                                                        <?=$lang->_('Карта-города
                                                        <br>
                                                        <span>с отметками всех участников программы</span>');?>
                                                    </div>
                                                </div>
                                            </div>
                        
                                            <div class="kit row">
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__4"></div>
                                                    <div class="kit_name">
                                                        <?=$lang->_('SIM карта:
                                                        <br>
                                                        <span>интернет и связь <br class="hidden-sm"> (24 часа бесплатно)</span>');?>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__5"></div>
                                                    <div class="kit_name smpt15">
                                                        <?=$lang->_('Скидки и бонусы
                                                        <br>
                                                        <span>в магазинах, ресторанах и на такси</span>');?>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-lg-4">
                                                    <div class="kit_icon kit_icon__6"></div>
                                                    <div class="kit_name smpt5 tpt5">
                                                        <?=$lang->_('Вход без очереди
                                                        <br>
                                                        <span>в самые популярные объекты</span>');?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    <? }
                    else if ($cityid == 2) { 
                    ?>
                    
                    <? } ?>

            
	
					<div id="section-landmark-list" class="row">						
						<div class="col-md-12 blocknote">
							<div class="blocknote-inner padding-30">
								<div class="hidden-xs"><h2><?=$lang->_('Контакты и расположение')?></h2></div>
								<div class="title-container hidden-sm hidden-md hidden-lg"><h2 class="pull-left">На карте</h2><button type="button" class="btn btn-default pull-right">СМОТРЕТЬ</button></div>
								<div class="clearfix"></div>
								<div class="row">
								    <div class="col-md-12">
										<?php 
										   include(__DIR__.'/map_entity.php') 
										 ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="not-xs not-md col-lg-6  nopadding">
											<div class="promo-address font-rlh">
												<br>
												<h4 style="font-weight: bold;"><?=$lang->_('Контакты')?></h4>
												<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="hidden">
    												<span itemprop="addressLocality" class="hidden">Москва</span>
												</div>
												<p class="promo-address-line"><?=$lang->_('Адрес')?>: <br><span itemprop="address" style="font-weight: bold;"><?=$lang->select($object['address'])?></span></p>
											    <p class="promo-address-line"><?=$lang->_('Метро')?>: <br>
											    <?foreach($subway as $subways):?>
											        <? if ($subways['line_icon']) { ?>
											            <img src="<?=$subways['line_icon']?>" style="width: 30px; padding-bottom: 3px;">
											     <? } ?>
											        <span style="font-weight: bold; color: <?=$subways['line_color']?>;"><?=$lang->select($subways['station_name'])?></span><br>
											    <?endforeach?>
											    </p>
												<p class="promo-address-line"><?=$lang->_('Телефон')?>: <br><span itemprop="telephone" style="font-weight: bold;"><a href="tel:<?=str_replace(' ','',$lang->select($object['phone']))?>">+<?=Other::format_phone($lang->select($object['phone']))?></a></span></p>
												<p class="promo-address-line"><?=$lang->_('Сайт')?>: <br><span style="font-weight: bold;"><span><?=$lang->select($object['site'])?></span></p>
												<!-- <p class="promo-address-line"><?=$lang->_('Сайт')?>: <br><span style="font-weight: bold;"><a target="_blank" rel="nofollow" href="<?=mb_substr($lang->select($object['site']), 0, 4) == 'http' ? $lang->select($object['site']) : 'http://'.$lang->select($object['site'])?>"><?=$lang->select($object['site'])?></a></span></p> -->
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-lg-6  nopadding">
											<div class="promo-address font-rlh">
												<br>
												<h4 style="font-weight: bold;"><?=$lang->_('Расписание')?></h4>
												<div>
													<table cellpadding="0" cellspacing="0" border="0" width="100%">
														<tbody>
														    <?if(!empty($object['working'])):?>
                                								<?foreach($lang->select($object['working']) as $week=>$work):?>
                                									<?if(empty($work)) continue;?>
                                									<tr>
            															<td>
            																<span class="guide-text">
                                                                                <span><?=Other::showWeek($week)?></span>
            																	<p class="color-blue" style="background-color: #F7F8FA;white-space: pre-line; font-size: 0.8em;margin-left: 10px;"><?=$work?></p>
            																</span>
            															</td>
            														</tr>
                                								<?endforeach?>
                                							<?endif?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?if($object_nearby):?>
					<div id="section-landmark-list" class="row">						
						<div class="col-md-12 blocknote">
							<div class="blocknote-inner padding-30">
								<div class="hidden-xs"><h2><?=$lang->_('Поблизости')?></h2></div>
								<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-12">
									    <? foreach ($object_nearby as $e): ?>
                        					<div class="col-sm-4 col-lg-4 col-md-4">
                        						<div class="panel-group" id="acc-include-cp" style="margin-top: 0px;">
                        							<div class="panel" style="background-color: rgba(255, 255, 255, 0);">
                        								<div class="panel-body" style="padding: 0px 0px;">
                        									<div class="row promo-item-list">
                        										<div class="col-md-12">
                        											<div class="row promo-item" style="padding: 0px 0px 20px 0px;">
                        												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        												    <div class="promo-item-label <?=$city_link;?>">&lt;<?=$e['distance']?>m</div>
                        													<a href="<?=$lang->path?>/entity/<?=$e['alt']?>" class="color-black">
                        														<div>
                        														    <!-- <span class="h5" style="color: #d24a43; font-weight: bold;">меньше <?=$e['distance']?> метров</span> -->
                        															<img src="<?=Other::showImage($e['picture'], 'middle')?>"  style="width:100%; height:162px;" alt="<?=$lang->select($e['title'])?>">
                        															<? if (!empty($lang->select($e['fast_track']))) { ?> <div class="fast-track" data-original-title="<?=$lang->select($e['fast_track'])?>"></div> <? } ?>
                        														</div>
                        														<div class="<?=$card_class;?>" style="padding-top: 10px;">
                        															<span class="h4"><?=$lang->select($e['title'])?></span>
                        															<noindex><p><?=$lang->select($e['short_descr'])?></p></noindex> 
                        														</div>
                        													</a>
                        												</div>
                        											</div>
                        										</div>
                        									</div>
                        								</div>
                        							</div>
                        						</div>
                        					</div>
                        				<? endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?endif?>
					<?php 
                        include(__DIR__.'/review.php');
                        include(__DIR__.'/partners.php');
                    ?>

				</div>
			</div>
	</div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="//leclick.ru/reservation/css/style.css" />

<script src="//leclick.ru/reservation/js/all.in.one.js"></script>
<script type="text/javascript">(function() {
    if (window.pluso)if (typeof window.pluso.start == "function") return;
    if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
    }})();
</script>