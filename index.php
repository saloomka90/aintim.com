<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет магазин интимных товаров Аинтим. Большой выбор интим-товаров");
$APPLICATION->SetPageProperty("keywords", "секс шоп, сексшоп, секс-игрушки");
$APPLICATION->SetPageProperty("title", "Интернет магазин интимных товаров Аинтим. Большой выбор интим-товаров, секс-шоп.");
$APPLICATION->SetTitle("Интернет магазин интимных товаров Аинтим. Большой выбор интим-товаров");
?>
<?$APPLICATION->IncludeComponent(
	"toysales:items.list", 
	"banner", 
	array(
		"CACHE_TIME" => "300",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => "banner",
		"DETAIL_URL" => "",
		"DISPLAY_PANEL" => "Y",
		"ELEMENT_ID" => "",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "banner",
		"SORT_BY" => "SORT",
		"SORT_ORDER" => "ASC"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"toysales:blocks.main",
	"",
	Array(
		"CACHE_TIME" => "300",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_PANEL" => "Y",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "catalog",
		"SORT_BY" => "SORT",
		"SORT_ORDER" => "ASC"
	)
);?> <?$APPLICATION->IncludeComponent(
	"toysales:catalog.top.new", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"BASKET_URL" => "/personal/cart/",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "7",
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_FIELD2" => "timestamp_x",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "catalog",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "size",
			1 => "color",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "size",
			1 => "color",
			2 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "size",
			1 => "color",
		),
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
			0 => "pict1",
			1 => "batteries",
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SECTION",
		"VENDORS_IBLOCK_TYPE" => "catalog",
		"VENDORS_IBLOCK_ID" => "4"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"toysales:items.list", 
	"producers", 
	array(
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "producers",
		"DISPLAY_PANEL" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"SORT_BY" => "SORT",
		"SORT_ORDER" => "ASC",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"DETAIL_URL" => ""
	),
	false
);?>
<div class="clear">
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>